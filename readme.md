## About Laravel

Go to [Laravel Docs](https://laravel.com/docs/)

## Quickstart

- Install homestead and configure the **Homestead.yaml** file:

    ip: "192.168.10.xx"
    memory: 8194
    cpus: 2
    provider: virtualbox
    
    folders:
        - map: ~/Desktop/project_code
          to: /home/vagrant/code
    
    sites:
        - map: your_site_url.dev
          to: /home/vagrant/code/public
          
    ...
          
- update the hosts file to point `your_site_url.dev` to `192.168.10.xx`
- run `vagrant up` in the machine directory
- run `git clone project_ssh_url project_code` outside the project_code directory
- `vagrant ssh` into the machine
- `cd /home/vagrant/code`
- `composer install`

## Set traffic as internal on Google Analytics

```
javascript:function setCookie(cname, cvalue, exdays) {var d = new Date();d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));var expires = 'expires=' + d.toUTCString();document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';}setCookie('internal_traffic', 'true', 9999);alert('Cookie: internal_traffic\nValue: true\nDomain: ' + window.location.hostname)
```