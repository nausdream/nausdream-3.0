let mix = require('laravel-mix');
let env = process.env.NODE_ENV || 'production';
let section = process.env.section || 'users';

// Load env file
require('dotenv').config();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
if (section === 'admin') {
    console.log("Preparing BACKOFFICE assets...");

    mix.setPublicPath(path.normalize('public/backoffice'))
        .sass('resources/assets/backoffice/sass/app.scss', 'public/backoffice/css')
        .js('resources/assets/backoffice/js/app.js', 'public/backoffice/js/')
        .extract(['jquery', 'lodash', 'bootstrap-sass', 'codemirror', 'summernote'])
        .autoload({
            jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
        });
}

if (section === 'users') {
    console.log("Preparing USERS assets...");

    mix.sass('resources/assets/users/sass/app.scss', 'public/css', {
        data: [
            '$cloudinary_secure_url: "' + process.env.CLOUDINARY_SECURE_URL + '"; '+
            '$cloudinary_base_folder: "' + process.env.CLOUDINARY_BASE_FOLDER + '"; ']
    })
        .js('resources/assets/users/js/app.js', 'public/js')
        .js('resources/assets/users/js/pages/base.js', 'public/js')
        .js('resources/assets/users/js/pages/header.js', 'public/js')
        .js('resources/assets/users/js/pages/home.js', 'public/js')
        .js('resources/assets/users/js/pages/harbour-center.js', 'public/js')
        .js('resources/assets/users/js/pages/listing.js', 'public/js')
        .js('resources/assets/users/js/pages/booking.js', 'public/js')
        .js('resources/assets/users/js/pages/cookie-policy-banner.js', 'public/js')
        .js('resources/assets/users/js/pages/static-pages.js', 'public/js')
        .js('resources/assets/users/js/campaigns/refer-a-friend.js', 'public/js')
        .extract(['jquery', 'bootstrap'])
        .autoload({
            jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
        });
}

if (env === 'development') {
    console.log("#######################################");
    console.log("# Webpack running in development MODE #");
    console.log("#######################################");
    mix.sourceMaps();
}

if (env === 'production') {
    console.log("######################################");
    console.log("# Webpack running in production MODE #");
    console.log("######################################");
}

mix.version();
