<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\ExperienceCopy::class, function (Faker $faker) {
    $title = $faker->text(\App\Constants::EXPERIENCE_TITLE_LENGTH);
    $description = $faker->realText(\App\Constants::MYSQL_TEXT_MAX_LENGTH/50);
    $short_description = $faker->realText(\App\Constants::MYSQL_TEXT_MAX_LENGTH/100);

    return [
        "language" => \App\TranslationModels\Language::inRandomOrder()->first()->language,
        "title" => $title,
        "description" => $description,
        "short_description" => $short_description,
        "meeting_point" => $faker->sentence(5),
        "features" => implode(",", [$faker->text(10), $faker->text(10), $faker->text(10)]),
        "meta_title" => \App\Helpers\SeoHelper::getMetaTitle($title),
        "meta_description" => \App\Helpers\SeoHelper::getMetaDescription($description),
        "slug_url" => \App\Helpers\SeoHelper::getSlugUrl($title)
    ];
});
