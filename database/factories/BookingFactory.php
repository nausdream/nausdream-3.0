<?php

use App\Constants;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Booking::class, function (Faker $faker) {
    return [
        "status" => $faker->numberBetween(Constants::BOOKING_PAYMENT_LINK_SENT, Constants::BOOKING_REFUNDED),
        "language" => \App\TranslationModels\Language::inRandomOrder()->first()->language,
        "user_first_name" => $faker->firstName,
        "user_last_name" => $faker->lastName,
        "user_email" => $faker->email,
        "user_phone" => $faker->phoneNumber,
        "user_place_of_birth" => $faker->city,
        "user_birth_date" => $faker->date(),
        "user_city" => $faker->city,
        "user_address" => $faker->address,
        "user_zip_code" => $faker->postcode,
        "meeting_point" => $faker->streetAddress,
        "captain_name" => $faker->firstName . ' ' . $faker->lastName,
        "captain_email" => $faker->email,
        "captain_phone" => $faker->phoneNumber,
        "currency" => \App\Models\Currency::inRandomOrder()->first()->name,
        "price" => $faker->randomFloat(2, 10, 250),
        "lat" => $faker->latitude,
        "lng" => $faker->longitude,
        "payment_datetime" => $faker->dateTime(),
        "experience_date" => $faker->date(),
        "departure_time" => $faker->time("H:i"),
        "return_time" => $faker->time("H:i"),
        "deleted_for" => $faker->text(),
        "experience_description" => $faker->text(),
        "experience_title" => $faker->realText(Constants::EXPERIENCE_TITLE_LENGTH),
        "experience_link" => $faker->url
    ];
});