<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Experience::class, function (Faker $faker) {
    return [
        "business_id" => $faker->unique()->randomElement([
            "CAG_1", "CAG_2", "CAG_3", "CAG_4", "CAG_5",
            "BCN_1", "BCN_2", "BCN_3", "BCN_4", "BCN_5",
            "NAP_1", "NAP_2", "NAP_3", "NAP_4", "NAP_5",
            "PAL_1", "PAL_2", "PAL_3", "PAL_4", "PAL_5",
            "CSM_1", "CSM_2", "CSM_3", "CSM_4", "CSM_5",
            "VCR_1", "VCR_2", "VCR_3", "VCR_4", "VCR_5",
            "MAR_1", "MAR_2", "MAR_3", "MAR_4", "MAR_5",
            "SPZ_1", "SPZ_2", "SPZ_3", "SPZ_4", "SPZ_5",
            "STB_1", "STB_2", "STB_3", "STB_4", "STB_5"
        ]),
        "trekksoft_id" => $faker->randomNumber(5),
        "boat_length_min" => $faker->numberBetween(0, 10),
        "boat_length_max" => $faker->numberBetween(10, 20),
        "departure_port" => $faker->city,
        "lat" => $faker->latitude,
        "lng" => $faker->longitude,
        "meeting_safety_margin" => $faker->numberBetween(0, 15),
        "children_age" => $faker->numberBetween(0, 7),
        "price" => $faker->randomFloat(2, 30, 150),
        "currency" => \App\Models\Currency::inRandomOrder()->first()->name,
        "is_searchable" => $faker->boolean(),
        "is_finished" => $faker->boolean(),
        "weekdays" => implode(",", $faker->randomElements(["sun", "mon", "tue", "wed", "thu", "fri", "sat"], 3))
    ];
});
