<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\ExperienceFeedback::class, function (Faker $faker) {
    return [
        "score" => $faker->numberBetween(\App\Constants::FEEDBACK_SCORE_MIN, \App\Constants::FEEDBACK_SCORE_MAX),
        "text" => $faker->realText(\App\Constants::FEEDBACK_TEXT_LENGTH),
        "customer_name" => $faker->name()
    ];
});
