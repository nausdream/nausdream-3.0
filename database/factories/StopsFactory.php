<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Stop::class, function (Faker $faker) {
    return [
        "stop_number" => $faker->numberBetween(1, 3),
        "language" => \App\TranslationModels\Language::inRandomOrder()->first()->language,
        "text" => $faker->realText(\App\Constants::MYSQL_TEXT_MAX_LENGTH / 100),
        "duration" => $faker->numberBetween(10, 50)
    ];
});
