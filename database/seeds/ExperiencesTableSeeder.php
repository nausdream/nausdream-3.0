<?php

use App\Models\Experience;
use Illuminate\Database\Seeder;

class ExperiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Experience::class, 45)->create()->each(function(Experience $experience){
            $experience->harbour_center()->associate(\App\Models\HarbourCenter::inRandomOrder()->first());
            $experience->administrator()->associate(\App\Models\Administrator::inRandomOrder()->first());
            $experience->boat_type()->associate(\App\Models\BoatType::inRandomOrder()->first());
            $experience->difficulty()->associate(\App\Models\Difficulty::inRandomOrder()->first());
            $experience->save();
            $experience->sentiment()->save(\App\Models\Sentiment::inRandomOrder()->first());
            $experience->experience_copies()->saveMany(factory(\App\Models\ExperienceCopy::class, 1)->make(["language" => "en"]));
            $experience->experience_copies()->saveMany(factory(\App\Models\ExperienceCopy::class, 1)->make(["language" => "it"]));
            $experience->experience_feedbacks()->saveMany(factory(\App\Models\ExperienceFeedback::class, 3)->make());
            $experience->experience_photos()->saveMany(factory(\App\Models\ExperiencePhoto::class, 5)->make());
            $experience->included_services()->saveMany(\App\Models\IncludedService::inRandomOrder()->get()->take(5));
            $experience->schedules()->saveMany(factory(\App\Models\Schedule::class, 5)->make());
            $experience->spoken_languages()->saveMany(\App\Models\SpokenLanguage::inRandomOrder()->get()->take(5));
            $experience->stops()->saveMany(factory(\App\Models\Stop::class, 5)->make());
        });
    }
}
