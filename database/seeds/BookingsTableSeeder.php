<?php

use App\Models\Booking;
use Illuminate\Database\Seeder;

class BookingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Booking::class, 45)->create()->each(function(Booking $booking){
            $booking->experience()->associate(\App\Models\Experience::inRandomOrder()->first());
            $booking->boat_type()->associate(\App\Models\BoatType::inRandomOrder()->first());
            $booking->guests()->saveMany(factory(\App\Models\Guest::class, 3)->make());
            $booking->payment_methods()->saveMany(\App\Models\PaymentMethod::all()->random(random_int(1,\App\Models\PaymentMethod::count())));
            $booking->payment_method()->associate(\App\Models\PaymentMethod::inRandomOrder()->first());
            $booking->included_services()->saveMany(\App\Models\IncludedService::all()->random(random_int(0,5)));
            $booking->save();
        });
    }
}
