<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExperienceTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('experience_types', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('name', Constants::EXPERIENCE_TYPE_LENGTH);
			$table->string('business_id', Constants::BUSINESS_ID_EXPERIENCE_TYPE_LENGTH);

            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('experience_types');
	}

}
