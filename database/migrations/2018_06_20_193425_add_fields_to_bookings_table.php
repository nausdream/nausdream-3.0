<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function(Blueprint $table){
            $table->string("operator", \App\Constants::FIRST_NAME_LENGTH + \App\Constants::LAST_NAME_LENGTH)->nullable();
            $table->string("acquisition_channel", \App\Constants::CHANNEL_NAME_LENGTH)->nullable();
            $table->boolean("soft_complain")->nullable();
            $table->boolean("hard_complain")->nullable();
            $table->text("notes")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function(Blueprint $table){
            $table->dropColumn("operator");
            $table->dropColumn("acquisition_channel");
            $table->dropColumn("soft_complain");
            $table->dropColumn("hard_complain");
            $table->dropColumn("notes");
        });
    }
}
