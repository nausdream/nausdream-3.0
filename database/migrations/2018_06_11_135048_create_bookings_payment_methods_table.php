<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings_payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('payment_method_id')->unsigned()->nullable();
            $table->bigInteger('booking_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('bookings_payment_methods', function(Blueprint $table)
        {
            $table->foreign('payment_method_id', 'bookings_payment_methods_fk0')->references('id')->on('payment_methods')->onDelete('set null');
            $table->foreign('booking_id', 'bookings_payment_methods_fk1')->references('id')->on('bookings')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings_payment_methods', function(Blueprint $table)
        {
            $table->dropForeign('bookings_payment_methods_fk0');
            $table->dropForeign('bookings_payment_methods_fk1');
        });

        Schema::dropIfExists('bookings_payment_methods');
    }
}
