<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExperiencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('experiences', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->bigInteger('harbour_center_id')->unsigned()->nullable();
            $table->bigInteger('experience_type_id')->unsigned()->nullable();
            $table->string('trekksoft_id', Constants::TREKKSOFT_ID_LENGTH)->nullable();
			$table->string('departure_port', Constants::PORT_NAME_LENGTH)->nullable();
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->integer('meeting_safety_margin')->nullable();
			$table->boolean('is_searchable')->default(1);
			$table->boolean('is_finished')->default(0);
			$table->bigInteger('admin_id')->unsigned()->nullable();
            $table->integer('children_age')->nullable();
            $table->string('weekdays', Constants::WEEKDAYS_LENGTH)->nullable();
            $table->string('business_id', Constants::BUSINESS_ID_EXPERIENCE_LENGTH)->nullable();
			$table->bigInteger('boat_type_id')->unsigned()->nullable();
            $table->bigInteger('difficulty_id')->unsigned()->nullable();
            $table->bigInteger('sentiment_id')->unsigned()->nullable();
            $table->double('price')->nullable();
            $table->string('currency', Constants::CURRENCY_LENGTH)->nullable();
            $table->integer('boat_length_min')->nullable();
            $table->integer('boat_length_max')->nullable();

            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('experiences');
	}

}
