<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', Constants::FIRST_NAME_LENGTH + Constants::LAST_NAME_LENGTH)->nullable();
            $table->string('email', Constants::EMAIL_LENGTH)->nullable();
            $table->string('language', Constants::LANGUAGE_LENGTH)->nullable();
            $table->boolean('marketing_accepted')->nullable();
            $table->string('experience_business_id', Constants::BUSINESS_ID_EXPERIENCE_LENGTH)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
