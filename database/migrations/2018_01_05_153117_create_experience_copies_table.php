<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExperienceCopiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('experience_copies', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('language', Constants::LANGUAGE_LENGTH)->nullable();
			$table->string('slug_url')->unique()->nullable();
			$table->string('meta_title', Constants::SEO_META_TITLE_LENGTH)->nullable();
			$table->string('meta_description', Constants::SEO_META_DESCRIPTION_LENGTH)->nullable();
			$table->string('redirect_slug_url')->nullable();
			$table->string('title', Constants::EXPERIENCE_TITLE_LENGTH)->nullable();
			$table->text('description')->nullable();
            $table->text('features')->nullable();
            $table->string('meeting_point', Constants::EXPERIENCE_MEETING_POINT_LENGTH)->nullable();
			$table->bigInteger('experience_id')->unsigned()->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('experience_copies');
	}

}
