<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\TranslationModels\User::create(
            ['first_name' => 'Luca', 'last_name' => 'Puddu', 'email' => 'luca@nausdream.com', 'is_admin' => true, 'phone' => '+393495867908']
        );
        \App\TranslationModels\User::create(
            ['first_name' => 'Giuseppe', 'last_name' => 'Basciu', 'email' => 'giuseppe@nausdream.com', 'is_admin' => true, 'phone' => '+573106047394']
        );
        \App\TranslationModels\User::create(
            ['first_name' => 'Ousmane', 'last_name' => 'Dieng', 'email' => 'ousmane@nausdream.com', 'phone' => '+393403224160']
        );
        \App\TranslationModels\User::create(
            ['first_name' => 'Marco', 'last_name' => 'Deiosso', 'email' => 'marco@nausdream.com', 'phone' => '+393485447113']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $user = \App\TranslationModels\User::where('phone', '+393495867908')->first();
        if (isset($user)) {
            $user->forceDelete();
        }
        $user = \App\TranslationModels\User::where('phone', '+573106047394')->first();
        if (isset($user)) {
            $user->forceDelete();
        }
        $user = \App\TranslationModels\User::where('phone', '+393403224160')->first();
        if (isset($user)) {
            $user->forceDelete();
        }
        $user = \App\TranslationModels\User::where('phone', '+393485447113')->first();
        if (isset($user)) {
            $user->forceDelete();
        }
    }
}
