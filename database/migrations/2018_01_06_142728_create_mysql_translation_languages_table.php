<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMysqlTranslationLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_translation')->create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constants::LANGUAGE_LENGTH);
            $table->boolean('rtl')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->dropIfExists('languages');
    }
}
