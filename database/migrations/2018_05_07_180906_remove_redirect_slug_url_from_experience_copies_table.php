<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRedirectSlugUrlFromExperienceCopiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experience_copies', function(Blueprint $table){
            $table->dropColumn("redirect_slug_url");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_copies', function(Blueprint $table){
            $table->string('redirect_slug_url')->nullable();
        });
    }
}
