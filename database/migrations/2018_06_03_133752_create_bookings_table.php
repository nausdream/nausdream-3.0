<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('status')->nullable();
            $table->string('language', Constants::LANGUAGE_LENGTH)->nullable();
            $table->string('user_first_name', Constants::FIRST_NAME_LENGTH)->nullable();
            $table->string('user_last_name', Constants::LAST_NAME_LENGTH)->nullable();
            $table->string('user_place_of_birth', Constants::PHONE_LENGTH)->nullable();
            $table->date('user_birth_date')->nullable();
            $table->string('user_city', Constants::CITY_NAME_LENGTH)->nullable();
            $table->string('user_address', Constants::ADDRESS_LENGTH)->nullable();
            $table->string('user_zip_code', Constants::ZIP_CODE_LENGTH)->nullable();
            $table->string('user_email', Constants::EMAIL_LENGTH)->nullable();
            $table->string('user_phone', Constants::PHONE_LENGTH)->nullable();
            $table->string('meeting_point', Constants::EXPERIENCE_MEETING_POINT_LENGTH)->nullable();
            $table->string('captain_name', Constants::FIRST_NAME_LENGTH + Constants::LAST_NAME_LENGTH)->nullable();
            $table->string('captain_email', Constants::EMAIL_LENGTH)->nullable();
            $table->string('captain_phone', Constants::PHONE_LENGTH)->nullable();
            $table->string('currency', Constants::CURRENCY_LENGTH)->nullable();
            $table->float('price')->nullable();
            $table->datetime('payment_datetime')->nullable();
            $table->datetime('quote_expiration_datetime')->nullable();
            $table->date('experience_date')->nullable();
            $table->time('departure_time')->nullable();
            $table->time('return_time')->nullable();
            $table->text('deleted_for')->nullable();
            $table->text('experience_description')->nullable();
            $table->string('experience_title', Constants::EXPERIENCE_TITLE_LENGTH)->nullable();
            $table->string('experience_link', Constants::BOOKING_EXPERIENCE_LINK_LENGTH)->nullable();

            $table->bigInteger('boat_type_id')->unsigned()->nullable();
            $table->bigInteger('experience_id')->unsigned()->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('bookings', function(Blueprint $table)
        {
            $table->foreign('boat_type_id', 'bookings_fk0')->references('id')->on('boat_types')->onDelete('set null');
            $table->foreign('experience_id', 'bookings_fk1')->references('id')->on('experiences')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function(Blueprint $table)
        {
            $table->dropForeign('bookings_fk0');
            $table->dropForeign('bookings_fk1');
        });

        Schema::dropIfExists('bookings');
    }
}
