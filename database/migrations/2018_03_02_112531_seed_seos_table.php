<?php

use App\Models\Seo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedSeosTable extends Migration
{
    private $boatTypes;
    private $harbourCenters;
    private $durations;

    /**
     * SeedQueriesTable constructor.
     */
    public function __construct()
    {
        $this->boatTypes = [
            'it' => [
                'barca' => null,
                'barca-vela' => 2,
                'barca-motore' => 1,
                'catamarano' => 3,
                'gommone' => 4,
            ],
            'en' => [
                'boat' => null,
                'sailboat' => 2,
                'motorboat' => 1,
                'catamaran' => 3,
                'rubberboat' => 4,
            ]
        ];
        $this->harbourCenters = [
            'barcelona' => ['en' => 'barcelona', 'it' => 'barcellona'],
            'cagliari' =>  ['en' => 'cagliari', 'it' => 'cagliari'],
            'catania' =>  ['en' => 'catania', 'it' => 'catania'],
            'ibiza' =>  ['en' => 'ibiza', 'it' => 'barcellona'],
            'mallorca' =>  ['en' => 'mallorca', 'it' => 'maiorca'],
            'marseilles' =>  ['en' => 'marseilles', 'it' => 'marsiglia'],
            'naples' =>  ['en' => 'naples', 'it' => 'napoli'],
            'olbia' =>  ['en' => 'olbia', 'it' => 'olbia'],
            'palermo' =>  ['en' => 'palermo', 'it' => 'palermo'],
            'savona' =>  ['en' => 'savona', 'it' => 'savona'],
            'la-spezia' =>  ['en' => 'la-spezia', 'it' => 'la-spezia'],
            'san-teodoro-budoni' =>  ['en' => 'san-teodoro-budoni', 'it' => 'san-teodoro-budoni']
        ];
        $this->durations = [
            'it' => [
                'mezza-giornata' => 'hd',
                'giornata-intera' => 'fd'
            ],
            'en' => [
                'half-day' => 'hd',
                'full-day' => 'fd'
            ]
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Add italian queries
        foreach ($this->harbourCenters as $harbourCenter => $translations) {
            foreach ($this->boatTypes['it'] as $boatType => $bt_id) {
                \App\Models\Seo::create([
                    'language' => 'it',
                    'query' => "escursioni-$boatType-".$translations['it'],
                    'boat_type_id' => $bt_id,
                    'harbour_center_id' => \App\Models\HarbourCenter::where('name', $harbourCenter)->first()->id,
                    'duration' => null,
                    'og_images' => null
                ]);

                foreach ($this->durations['it'] as $duration => $duration_code) {
                    \App\Models\Seo::create([
                        'language' => 'it',
                        'query' => "escursioni-$boatType-$duration-".$translations['it'],
                        'boat_type_id' => $bt_id,
                        'harbour_center_id' => \App\Models\HarbourCenter::where('name', $harbourCenter)->first()->id,
                        'duration' => $duration_code,
                        'og_images' => null
                    ]);
                }
            }
        }

        // Add english queries
        foreach ($this->harbourCenters as $harbourCenter => $translations) {
            foreach ($this->boatTypes['en'] as $boatType => $bt_id) {
                \App\Models\Seo::create([
                    'language' => 'en',
                    'query' => "$boatType-trips-".$translations['en'],
                    'boat_type_id' => $bt_id,
                    'harbour_center_id' => \App\Models\HarbourCenter::where('name', $harbourCenter)->first()->id,
                    'duration' => null,
                    'og_images' => null
                ]);

                foreach ($this->durations['en'] as $duration => $duration_code) {
                    \App\Models\Seo::create([
                        'language' => 'en',
                        'query' => "$duration-$boatType-trips-".$translations['en'],
                        'boat_type_id' => $bt_id,
                        'harbour_center_id' => \App\Models\HarbourCenter::where('name', $harbourCenter)->first()->id,
                        'duration' => $duration_code,
                        'og_images' => null
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        // Remove italian queries
        foreach ($this->harbourCenters as $harbourCenter => $translations) {
            foreach ($this->boatTypes['it'] as $boatType => $bt_id) {
                $query = Seo::where('query', "escursioni-$boatType-".$translations['it'])->first();

                if (isset($query)) {
                    $query->delete();
                }

                foreach ($this->durations['it'] as $duration => $duration_code) {
                    $query = Seo::where('query', "escursioni-$boatType-$duration-".$translations['it'])->first();

                    if (isset($query)) {
                        $query->delete();
                    }
                }
            }
        }

        // Remove english queries
        foreach ($this->harbourCenters as $harbourCenter => $translations) {
            foreach ($this->boatTypes['en'] as $boatType => $bt_id) {
                $query = Seo::where('query', "$boatType-trips-".$translations['en'])->first();

                if (isset($query)) {
                    $query->delete();
                }

                foreach ($this->durations['en'] as $duration => $duration_code) {
                    $query = Seo::where('query', "$duration-$boatType-trips-".$translations['en'])->first();

                    if (isset($query)) {
                        $query->delete();
                    }
                }
            }
        }
    }
}
