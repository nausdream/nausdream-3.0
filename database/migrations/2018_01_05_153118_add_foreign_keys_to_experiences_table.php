<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExperiencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experiences', function(Blueprint $table)
		{
			$table->foreign('harbour_center_id', 'experiences_fk0')->references('id')->on('harbour_centers')->onDelete('set null');
			$table->foreign('experience_type_id', 'experiences_fk1')->references('id')->on('experience_types')->onDelete('set null');
			$table->foreign('admin_id', 'experiences_fk2')->references('id')->on('administrators')->onDelete('set null');
			$table->foreign('boat_type_id', 'experiences_fk3')->references('id')->on('boat_types')->onDelete('set null');
            $table->foreign('difficulty_id', 'experiences_fk4')->references('id')->on('difficulties')->onDelete('set null');
            $table->foreign('sentiment_id', 'experiences_fk5')->references('id')->on('sentiments')->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experiences', function(Blueprint $table)
		{
			$table->dropForeign('experiences_fk0');
			$table->dropForeign('experiences_fk1');
			$table->dropForeign('experiences_fk2');
			$table->dropForeign('experiences_fk3');
            $table->dropForeign('experiences_fk4');
            $table->dropForeign('experiences_fk5');
		});
	}

}
