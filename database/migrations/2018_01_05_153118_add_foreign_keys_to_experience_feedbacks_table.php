<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExperienceFeedbacksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experience_feedbacks', function(Blueprint $table)
		{
			$table->foreign('experience_id', 'experience_feedbacks_fk0')->references('id')->on('experiences')->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experience_feedbacks', function(Blueprint $table)
		{
			$table->dropForeign('experience_feedbacks_fk0');
		});
	}

}
