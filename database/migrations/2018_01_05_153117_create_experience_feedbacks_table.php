<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExperienceFeedbacksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('experience_feedbacks', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->integer('score');
			$table->text('text')->nullable();
			$table->bigInteger('experience_id')->unsigned()->nullable();
			$table->string('customer_name', Constants::FEEDBACK_CUSTOMER_NAME_LENGTH);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('experience_feedbacks');
	}

}
