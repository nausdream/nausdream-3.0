<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddB2cHarbourCenters extends Migration
{
    private $b2cHarbourCenters = [
        'villasimius-costa-rei' => ['en' => 'villasimius-costa-rei', 'it' => 'villasimius-costa-rei', 'name' => 'villasimius-costa-rei', 'business_id' => 'vcr', 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0],
        'costa-smeralda' => ['en' => 'costa-smeralda', 'it' => 'costa-smeralda', 'name' => 'costa-smeralda', 'business_id' => 'csm', 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0]
    ];
    private $boatTypes = [
        'it' => [
            'barca' => null,
            'barca-vela' => 2,
            'barca-motore' => 1,
            'catamarano' => 3,
            'gommone' => 4,
        ],
        'en' => [
            'boat' => null,
            'sailboat' => 2,
            'motorboat' => 1,
            'catamaran' => 3,
            'rubberboat' => 4,
        ]
    ];
    private $durations = [
        'it' => [
            'mezza-giornata' => 'hd',
            'giornata-intera' => 'fd'
        ],
        'en' => [
            'half-day' => 'hd',
            'full-day' => 'fd'
        ]
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->b2cHarbourCenters as $harbourCenter) {
            $new = \App\Models\HarbourCenter::where('name', $harbourCenter['name'])->first();
            if (!isset($new)){
                $new = \App\Models\HarbourCenter::create(['name' => $harbourCenter['name'], 'business_id' => $harbourCenter['business_id'], 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0]);
            }
            foreach ($this->boatTypes['it'] as $boatType => $bt_id) {
                \App\Models\Seo::create([
                    'language' => 'it',
                    'query' => "escursioni-$boatType-".$harbourCenter['name'],
                    'boat_type_id' => $bt_id,
                    'harbour_center_id' => $new->id,
                    'duration' => null,
                    'og_images' => null
                ]);

                foreach ($this->durations['it'] as $duration => $duration_code) {
                    \App\Models\Seo::create([
                        'language' => 'it',
                        'query' => "escursioni-$boatType-$duration-".$harbourCenter['name'],
                        'boat_type_id' => $bt_id,
                        'harbour_center_id' => $new->id,
                        'duration' => $duration_code,
                        'og_images' => null
                    ]);
                }
            }

            foreach ($this->boatTypes['en'] as $boatType => $bt_id) {
                \App\Models\Seo::create([
                    'language' => 'en',
                    'query' => "$boatType-trips-".$harbourCenter['name'],
                    'boat_type_id' => $bt_id,
                    'harbour_center_id' => $new->id,
                    'duration' => null,
                    'og_images' => null
                ]);

                foreach ($this->durations['en'] as $duration => $duration_code) {
                    \App\Models\Seo::create([
                        'language' => 'en',
                        'query' => "$duration-$boatType-trips-".$harbourCenter['name'],
                        'boat_type_id' => $bt_id,
                        'harbour_center_id' => $new->id,
                        'duration' => $duration_code,
                        'og_images' => null
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->b2cHarbourCenters as $harbourCenter) {
            $delete = \App\Models\HarbourCenter::where('name', $harbourCenter)->first();
            $delete->seos()->forceDelete();
            $delete->forceDelete();
        }
    }
}
