<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultipleSentimentsToExperiences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experiences', function(Blueprint $table){
            $table->dropForeign("experiences_fk5");
            $table->dropColumn("sentiment_id");
        });

        Schema::create('experiences_sentiments', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('experience_id')->unsigned()->nullable();
            $table->bigInteger('sentiment_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // Seed translations
        $englishLanguage = \App\TranslationModels\Language::where("language", "en")->first();
        $sentiments = [
            "panoramic" => "Panoramic",
            "naturalistic" => "Naturalistic",
            "bathing" => "Bathing",
            "cultural" => "Cultural",
            "fun" => "Fun",
        ];
        $page = \App\TranslationModels\Page::create(["name" => "sentiments"]);
        foreach ($sentiments as $name => $translation) {
            $sentence = new \App\TranslationModels\Sentence(["name" => $name]);
            $page->sentences()->save($sentence);

            $sentenceTranslation = new \App\TranslationModels\SentenceTranslation(["text" => $translation]);
            $sentenceTranslation->language()->associate($englishLanguage->id);
            $sentenceTranslation->sentence()->associate($sentence->id);
            $sentenceTranslation->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experiences', function(Blueprint $table){
            $table->bigInteger('sentiment_id')->unsigned()->nullable();
            $table->foreign('sentiment_id', 'experiences_fk5')->references('id')->on('sentiments')->onDelete('set null');
        });

        Schema::drop('experiences_sentiments');
    }
}
