<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('query', Constants::QUERY_LENGTH);
            $table->string('language', Constants::LANGUAGE_LENGTH)->nullable();
            $table->string('duration', Constants::DURATION_LENGTH)->nullable();
            $table->text('og_images')->nullable();

            $table->bigInteger('harbour_center_id')->unsigned()->nullable();
            $table->bigInteger('boat_type_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            // Add foreign keys
            $table->foreign('harbour_center_id')->references('id')->on('harbour_centers')->onDelete('set null');
            $table->foreign('boat_type_id')->references('id')->on('boat_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seos', function (Blueprint $table){
            $table->dropForeign('seos_harbour_center_id_foreign');
            $table->dropForeign('seos_boat_type_id_foreign');
        });

        Schema::dropIfExists('seos');
    }
}
