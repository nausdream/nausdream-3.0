<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpokenLanguagesExperiencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('spoken_languages_experiences', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->bigInteger('experience_id')->unsigned()->nullable();
			$table->bigInteger('spoken_language_id')->unsigned()->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('spoken_languages_experiences');
	}

}
