<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuperAdminField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('administrators', function(Blueprint $table){
            $table->boolean("super_admin")->default(false)->nullable();
        });

        $luca = \App\Models\Administrator::where('email', 'luca@nausdream.com')->first();
        $luca->super_admin = true;
        $luca->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('administrators', function(Blueprint $table){
            $table->dropColumn("super_admin");
        });
    }
}
