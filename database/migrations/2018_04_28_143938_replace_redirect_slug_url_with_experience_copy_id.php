<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReplaceRedirectSlugUrlWithExperienceCopyId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('old_experience_links', function(Blueprint $table){
            $table->dropColumn("redirect_slug_url");
            $table->bigInteger('experience_copy_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('old_experience_links', function(Blueprint $table){
            $table->dropColumn("experience_copy_id");
            $table->string('redirect_slug_url')->nullable();
        });
    }
}
