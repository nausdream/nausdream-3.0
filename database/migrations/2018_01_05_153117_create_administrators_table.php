<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdministratorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', Constants::FIRST_NAME_LENGTH);
            $table->string('last_name', Constants::LAST_NAME_LENGTH);
            $table->string('email', Constants::EMAIL_LENGTH);
            $table->string('phone', Constants::PHONE_LENGTH);
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('administrators');
    }

}
