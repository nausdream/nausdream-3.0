<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferAFriendCampaignToExistingBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $referAFriend = \App\Models\Campaign::where('name', 'refer-a-friend')->get()->first();

        if (isset($referAFriend)) {
            \App\Models\Booking::where('status', \App\Constants::BOOKING_PAYMENT_LINK_SENT)->each(function (\App\Models\Booking $booking) use ($referAFriend) {
                $booking->campaigns()->attach($referAFriend->id);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $referAFriend = \App\Models\Campaign::where('name', 'refer-a-friend')->get()->first();

        if (isset($referAFriend)) {
            \App\Models\Booking::where('status', \App\Constants::BOOKING_PAYMENT_LINK_SENT)->each(function (\App\Models\Booking $booking) use ($referAFriend) {
                $booking->campaigns()->detach($referAFriend->id);
            });
        }
    }
}
