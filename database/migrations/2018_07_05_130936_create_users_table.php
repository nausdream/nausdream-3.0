<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('language', Constants::LANGUAGE_LENGTH)->nullable();
            $table->string('first_name', Constants::FIRST_NAME_LENGTH)->nullable();
            $table->string('last_name', Constants::LAST_NAME_LENGTH)->nullable();
            $table->string('place_of_birth', Constants::PHONE_LENGTH)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('city', Constants::CITY_NAME_LENGTH)->nullable();
            $table->string('address', Constants::ADDRESS_LENGTH)->nullable();
            $table->string('zip_code', Constants::ZIP_CODE_LENGTH)->nullable();
            $table->string('email', Constants::EMAIL_LENGTH)->unique()->nullable();
            $table->string('phone', Constants::PHONE_LENGTH)->nullable();
            $table->boolean('marketing_accepted')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
