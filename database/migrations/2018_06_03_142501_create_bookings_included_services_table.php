<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsIncludedServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings_included_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('included_service_id')->unsigned()->nullable();
            $table->bigInteger('booking_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('bookings_included_services', function(Blueprint $table)
        {
            $table->foreign('included_service_id', 'bookings_included_services_fk0')->references('id')->on('included_services')->onDelete('set null');
            $table->foreign('booking_id', 'bookings_included_services_fk1')->references('id')->on('bookings')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings_included_services', function(Blueprint $table)
        {
            $table->dropForeign('bookings_included_services_fk0');
            $table->dropForeign('bookings_included_services_fk1');
        });

        Schema::dropIfExists('bookings_included_services');
    }
}
