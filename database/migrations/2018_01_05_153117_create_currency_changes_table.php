<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrencyChangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currency_changes', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->string('currency_in', Constants::CURRENCY_LENGTH);
			$table->string('currency_out', Constants::CURRENCY_LENGTH);
			$table->double('change');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('currency_changes');
	}

}
