<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMysqlTranslationSentenceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_translation')->create('sentence_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('text');

            $table->bigInteger('sentence_id')->unsigned()->nullable();
            $table->bigInteger('language_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->dropIfExists('sentence_translations');
    }
}
