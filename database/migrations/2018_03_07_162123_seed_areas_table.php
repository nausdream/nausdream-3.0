<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedAreasTable extends Migration
{
    private $areas;
    private $newHarbourCenters;
    private $harbourCenters;

    /**
     * SeedAreasTable constructor.
     */
    public function __construct()
    {
        $this->areas = [
            'catalonia', 'provence', 'southern-italy-and-islands', 'northern-and-central-italy'
        ];
        $this->newHarbourCenters = [
            'vcr' => 'villasimius-costa-rei',
            'csm' => 'costa-smeralda'
        ];
        $this->harbourCenters = [
            'catalonia' => ['bcn'],
            'provence' => ['mar'],
            'southern-italy-and-islands' => ['cag', 'vcr', 'stb', 'csm', 'nap'],
            'northern-and-central-italy' => ['spz']
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Add new harbour centers first
        foreach ($this->newHarbourCenters as $businessId => $name) {
            \App\Models\HarbourCenter::create(['name' => $name, 'business_id' => $businessId, 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0]);
        }

        foreach ($this->areas as $areaName) {
            // Add area to db
            $area = \App\Models\Area::create(['name' => $areaName]);

            // Associate area to each harbour center
            foreach ($this->harbourCenters[$areaName] as $harbourCenterName) {
                $harbourCenter = \App\Models\HarbourCenter::where('business_id', $harbourCenterName)->first();
                $harbourCenter->area()->associate($area);
                $harbourCenter->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->areas as $areaName) {
            // Dissociate area for each harbour center
            foreach ($this->harbourCenters[$areaName] as $harbourCenterName) {
                $harbourCenter = \App\Models\HarbourCenter::where('business_id', $harbourCenterName)->first();
                if (isset($harbourCenter)) {
                    $harbourCenter->area()->dissociate();
                    $harbourCenter->save();
                }
            }

            // Delete areas
            $area = \App\Models\Area::where('name', $areaName);
            if (isset($area)) {
                $area->forceDelete();
            }
        }

        // Delete new harbour centers
        foreach ($this->newHarbourCenters as $businessId => $name) {
            $new = \App\Models\HarbourCenter::where('business_id', $businessId)->first();
            if (isset($new)) {
                $new->forceDelete();
            }
        }
    }
}
