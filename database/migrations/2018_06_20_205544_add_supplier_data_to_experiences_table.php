<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupplierDataToExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experiences', function (Blueprint $table) {
            $table->string("captain_name", \App\Constants::FIRST_NAME_LENGTH)->nullable();
            $table->string("captain_email", \App\Constants::EMAIL_LENGTH)->nullable();
            $table->string("captain_phone", \App\Constants::PHONE_LENGTH)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experiences', function (Blueprint $table) {
            $table->dropColumn("captain_name");
            $table->dropColumn("captain_email");
            $table->dropColumn("captain_phone");
        });
    }
}
