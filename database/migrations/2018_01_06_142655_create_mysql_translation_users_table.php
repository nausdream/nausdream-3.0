<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMysqlTranslationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_translation')->create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', Constants::FIRST_NAME_LENGTH);
            $table->string('last_name', Constants::LAST_NAME_LENGTH);
            $table->string('email', Constants::EMAIL_LENGTH)->unique();
            $table->string('phone', Constants::PHONE_LENGTH)->unique();
            $table->boolean('is_admin');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->dropIfExists('users');
    }
}
