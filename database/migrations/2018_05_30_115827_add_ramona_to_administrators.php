<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRamonaToAdministrators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        App\Models\Administrator::create(
            ['first_name' => 'Ramona', 'last_name' => 'Casto', 'email' => 'ramona.casto33@gmail.com', 'phone' => '+393401795082']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Models\Administrator::where('email', 'ramona.casto33@gmail.com')->forceDelete();
    }
}
