<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Administrator::create(
            ['first_name' => 'Giuseppe', 'last_name' => 'Basciu', 'email' => 'giuseppe@nausdream.com', 'phone' => '+573106047394']
        );
        \App\Models\Administrator::create(
            ['first_name' => 'Ousmane', 'last_name' => 'Dieng', 'email' => 'ousmane@nausdream.com', 'phone' => '+393403224160']
        );
        \App\Models\Administrator::create(
            ['first_name' => 'Maria Antonietta', 'last_name' => 'Melis', 'email' => 'maria.antonietta@nausdream.com', 'phone' => '+393270457440']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $user = \App\Models\Administrator::where('phone', '+573106047394')->first();
        if (isset($user)) {
            $user->forceDelete();
        }
        $user = \App\Models\Administrator::where('phone', '+393403224160')->first();
        if (isset($user)) {
            $user->forceDelete();
        }
        $user = \App\Models\Administrator::where('phone', '+393270457440')->first();
        if (isset($user)) {
            $user->forceDelete();
        }
    }
}
