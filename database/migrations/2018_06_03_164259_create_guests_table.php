<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('first_name', Constants::FIRST_NAME_LENGTH)->nullable();
            $table->string('last_name', Constants::LAST_NAME_LENGTH)->nullable();
            $table->date('birth_date')->nullable();
            $table->integer('guest_type')->nullable();
            $table->string('place_of_birth', Constants::PLACE_OF_BIRTH_LENGTH)->nullable();

            $table->bigInteger('booking_id')->unsigned()->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('guests', function(Blueprint $table)
        {
            $table->foreign('booking_id', 'guests_fk0')->references('id')->on('bookings')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guests', function(Blueprint $table)
        {
            $table->dropForeign('guests_fk0');
        });

        Schema::dropIfExists('guests');
    }
}
