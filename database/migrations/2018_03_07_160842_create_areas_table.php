<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', \App\Constants::AREA_NAME_LENGTH);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('harbour_centers', function (Blueprint $table) {
            // Add column
            $table->bigInteger('area_id')->unsigned()->nullable();

            // Set foreign key on area_id
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('harbour_centers', function (Blueprint $table) {
            // Delete foreign constraint
            $table->dropForeign('harbour_centers_area_id_foreign');

            // Drop column
            $table->dropColumn('area_id')->unsigned()->nullable();
        });

        Schema::dropIfExists('areas');
    }
}
