<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExperiencesIncludedServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experiences_included_services', function(Blueprint $table)
		{
			$table->foreign('included_service_id', 'experiences_included_services_fk0')->references('id')->on('included_services')->onDelete('set null');
			$table->foreign('experience_id', 'experiences_included_services_fk1')->references('id')->on('experiences')->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experiences_included_services', function(Blueprint $table)
		{
			$table->dropForeign('experiences_included_services_fk0');
			$table->dropForeign('experiences_included_services_fk1');
		});
	}

}
