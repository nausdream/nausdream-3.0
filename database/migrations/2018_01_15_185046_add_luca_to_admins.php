<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLucaToAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        App\Models\Administrator::create(
            ['first_name' => 'Luca', 'last_name' => 'Puddu', 'email' => 'luca@nausdream.com', 'phone' => '+393495867908']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Models\Administrator::where('email', 'luca@nausdream.com')->forceDelete();
    }
}
