<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValentinaToAdministrators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        App\Models\Administrator::create(
            ['first_name' => 'Valentina', 'last_name' => 'Pili', 'email' => 'valentina@nausdream.com', 'phone' => '+393481211025']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Models\Administrator::where('email', 'valentina@nausdream.com')->forceDelete();
    }
}
