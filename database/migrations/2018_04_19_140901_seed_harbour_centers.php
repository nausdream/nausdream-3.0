<?php

use App\Models\Seo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedHarbourCenters extends Migration
{
    private $newHC;
    private $thaiHC;
    private $boatTypes;
    private $durations;
    private $harbourCenters;


    /**
     * SeedHarbourCenters constructor.
     */
    public function __construct()
    {
        $southern_italy = \App\Models\Area::where('name', 'southern-italy-and-islands')->first()->id;
        $northern_italy = \App\Models\Area::where('name', 'northern-and-central-italy')->first()->id;
        $this->newHC = [
            [
                'name' => 'alghero-stintino', 'business_id' => 'ast',
                'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0,
                'area_id' => $southern_italy],
            [
                'name' => 'trieste', 'business_id' => 'tri',
                'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0,
                'area_id' => $northern_italy]
        ];
        $this->thaiHC = [
            [
                'name' => 'phuket', 'business_id' => 'phu',
                'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0],
            [
                'name' => 'koh-samui', 'business_id' => 'kos',
                'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0]
        ];

        // New harbour centers SEO
        $this->boatTypes = [
            'it' => [
                'barca' => null,
                'barca-vela' => 2,
                'barca-motore' => 1,
                'catamarano' => 3,
                'gommone' => 4,
            ],
            'en' => [
                'boat' => null,
                'sailboat' => 2,
                'motorboat' => 1,
                'catamaran' => 3,
                'rubberboat' => 4,
            ]
        ];
        $this->harbourCenters = [
            'alghero-stintino' => ['en' => 'alghero-stintino', 'it' => 'alghero-stintino'],
            'trieste' =>  ['en' => 'trieste', 'it' => 'trieste'],
            'phuket' =>  ['en' => 'phuket', 'it' => 'phuket'],
            'koh-samui' =>  ['en' => 'koh-samui', 'it' => 'koh-samui']
        ];
        $this->durations = [
            'it' => [
                'mezza-giornata' => 'hd',
                'giornata-intera' => 'fd'
            ],
            'en' => [
                'half-day' => 'hd',
                'full-day' => 'fd'
            ]
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('harbour_centers', function (Blueprint $table) {
            $table->boolean('is_visible')->default(1);
        });

        $stb = \App\Models\HarbourCenter::where('business_id', 'stb')->first();
        $stb->name = "san-teodoro";
        $stb->business_id = "std";
        $stb->save();

        $thailand = \App\Models\Area::create(['name' => 'thailand']);

        foreach ($this->newHC as $harbourCenter) {
            $id = $harbourCenter['area_id'];
            $harbourCenter = \App\Models\HarbourCenter::create($harbourCenter);
            $harbourCenter->area()->associate(\App\Models\Area::find($id));
            $harbourCenter->save();
        }

        foreach ($this->thaiHC as $harbourCenter) {
            $harbourCenter = \App\Models\HarbourCenter::make($harbourCenter);
            $thailand->harbour_centers()->save($harbourCenter);
        }

        // ADD SEOS
        // Add italian queries
        foreach ($this->harbourCenters as $harbourCenter => $translations) {
            foreach ($this->boatTypes['it'] as $boatType => $bt_id) {
                Seo::create([
                    'language' => 'it',
                    'query' => "escursioni-$boatType-".$translations['it'],
                    'boat_type_id' => $bt_id,
                    'harbour_center_id' => \App\Models\HarbourCenter::where('name', $harbourCenter)->first()->id,
                    'duration' => null,
                    'og_images' => null
                ]);

                foreach ($this->durations['it'] as $duration => $duration_code) {
                    Seo::create([
                        'language' => 'it',
                        'query' => "escursioni-$boatType-$duration-".$translations['it'],
                        'boat_type_id' => $bt_id,
                        'harbour_center_id' => \App\Models\HarbourCenter::where('name', $harbourCenter)->first()->id,
                        'duration' => $duration_code,
                        'og_images' => null
                    ]);
                }
            }
        }

        // Add english queries
        foreach ($this->harbourCenters as $harbourCenter => $translations) {
            foreach ($this->boatTypes['en'] as $boatType => $bt_id) {
                Seo::create([
                    'language' => 'en',
                    'query' => "$boatType-trips-".$translations['en'],
                    'boat_type_id' => $bt_id,
                    'harbour_center_id' => \App\Models\HarbourCenter::where('name', $harbourCenter)->first()->id,
                    'duration' => null,
                    'og_images' => null
                ]);

                foreach ($this->durations['en'] as $duration => $duration_code) {
                    Seo::create([
                        'language' => 'en',
                        'query' => "$duration-$boatType-trips-".$translations['en'],
                        'boat_type_id' => $bt_id,
                        'harbour_center_id' => \App\Models\HarbourCenter::where('name', $harbourCenter)->first()->id,
                        'duration' => $duration_code,
                        'og_images' => null
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Schema::table('harbour_centers', function (Blueprint $table) {
            $table->dropColumn('is_visible');
        });

        $std = \App\Models\HarbourCenter::where('business_id', 'std')->first();
        $std->name = "san-teodoro-budoni";
        $std->business_id = "stb";
        $std->save();

        // REMOVE SEOS
        // Remove italian queries
        foreach ($this->harbourCenters as $harbourCenter => $translations) {
            foreach ($this->boatTypes['it'] as $boatType => $bt_id) {
                $query = Seo::where('query', "escursioni-$boatType-".$translations['it'])->first();

                if (isset($query)) {
                    $query->delete();
                }

                foreach ($this->durations['it'] as $duration => $duration_code) {
                    $query = Seo::where('query', "escursioni-$boatType-$duration-".$translations['it'])->first();

                    if (isset($query)) {
                        $query->delete();
                    }
                }
            }
        }

        // Remove english queries
        foreach ($this->harbourCenters as $harbourCenter => $translations) {
            foreach ($this->boatTypes['en'] as $boatType => $bt_id) {
                $query = Seo::where('query', "$boatType-trips-".$translations['en'])->first();

                if (isset($query)) {
                    $query->delete();
                }

                foreach ($this->durations['en'] as $duration => $duration_code) {
                    $query = Seo::where('query', "$duration-$boatType-trips-".$translations['en'])->first();

                    if (isset($query)) {
                        $query->delete();
                    }
                }
            }
        }

        foreach ($this->newHC as $harbourCenter) {
            $harbourCenter = \App\Models\HarbourCenter::where('business_id', $harbourCenter['business_id'])->first();
            if (isset($harbourCenter)) {
                $harbourCenter->forceDelete();
            }
        }

        $thailand = \App\Models\Area::where(['name' => 'thailand'])->first();

        $thailand->harbour_centers()->forceDelete();

        if (isset($thailand)){
            $thailand->forceDelete();
        }
    }
}
