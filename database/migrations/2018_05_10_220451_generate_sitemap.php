<?php

use Illuminate\Database\Migrations\Migration;
use Watson\Sitemap\Facades\Sitemap;

class GenerateSitemap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Helpers\SitemapHelper::generate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Storage::delete('public/sitemap.xml');
    }
}
