<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldExperienceLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_experience_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constants::LANGUAGE_LENGTH)->nullable();
            $table->string('slug_url')->unique()->nullable();
            $table->string('redirect_slug_url')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_experience_links');
    }
}
