<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentMethodIdToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function(Blueprint $table)
        {
            $table->bigInteger('payment_method_id')->unsigned()->nullable();

            $table->foreign('payment_method_id', 'bookings_fk2')->references('id')->on('payment_methods')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function(Blueprint $table)
        {
            $table->dropForeign('bookings_fk2');
            $table->dropColumn('payment_method_id');
        });
    }
}
