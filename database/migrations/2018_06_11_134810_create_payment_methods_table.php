<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', \App\Constants::PAYMENT_METHOD_NAME_LENGTH);

            $table->softDeletes();
            $table->timestamps();
        });

        // Seed initial values
        \App\Models\PaymentMethod::create(['name' => 'paypal']);
        \App\Models\PaymentMethod::create(['name' => 'stripe']);
        \App\Models\PaymentMethod::create(['name' => 'wire_transfer']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
