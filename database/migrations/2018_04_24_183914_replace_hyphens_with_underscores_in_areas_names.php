<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReplaceHyphensWithUnderscoresInAreasNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Area::all()->each(function($area, $key){
            $area->name = str_replace('-', '_', $area->name);
            $area->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Area::all()->each(function($area, $key){
            $area->name = str_replace('_', '-', $area->name);
            $area->save();
        });
    }
}
