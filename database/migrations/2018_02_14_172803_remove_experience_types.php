<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveExperienceTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experiences', function(Blueprint $table)
        {
            $table->dropForeign('experiences_fk1');
            $table->dropColumn('experience_type_id');
        });

        Schema::drop('experience_types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('experience_types', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('name', Constants::EXPERIENCE_TYPE_LENGTH);
            $table->string('business_id', Constants::BUSINESS_ID_EXPERIENCE_TYPE_LENGTH);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('experiences', function(Blueprint $table)
        {
            $table->bigInteger('experience_type_id')->unsigned()->nullable();
            $table->foreign('experience_type_id', 'experiences_fk1')->references('id')->on('experience_types')->onDelete('set null');
        });
    }
}
