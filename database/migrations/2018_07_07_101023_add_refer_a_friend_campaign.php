<?php

use App\Models\Campaign;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferAFriendCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Campaign::create([
            'name' => 'refer-a-friend',
            'is_active' => true,
            'attributes' => [
                /*
                 * The campaign's start datetime. Starting from this date, it generates coupons for buying users.
                 */
                'start' => Carbon::create(2018, 7, 12, 0, 0, 0, \App\Constants::TIMEZONE),
                /*
                 * When new referral coupon are not generated anymore
                 */
                'stop' => Carbon::create(2018, 7, 31, 0, 0, 0, \App\Constants::TIMEZONE),
                /**
                 * Default friend coupons. Generated upon booking from any user
                 * in the campaign timeframe
                 */
                'friend_coupon' => [
                    'value' => 15,
                    'type' => \App\Constants::COUPON_PERCENTAGE,
                    'stock' => 99999,
                    'expiration' => Carbon::create(2018, 8, 12, 0, 0, 0, \App\Constants::TIMEZONE)
                ],
                /**
                 * Default referrer coupons. Created when
                 * the friend (referral) books with the referrer coupon, and sent to the referrer via email.
                 */
                'referrer_coupon' => [
                    'value' => 15,
                    'type' => \App\Constants::COUPON_PERCENTAGE,
                    'stock' => 1,
                    'expiration' => Carbon::create(2018, 9, 30, 0, 0, 0, \App\Constants::TIMEZONE)
                ],

            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $referAFriend = Campaign::where('name', 'refer-a-friend');
        if (isset($referAFriend)) {
            $referAFriend->delete();
        }
    }
}
