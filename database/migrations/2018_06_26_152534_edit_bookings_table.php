<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->renameColumn('acquisition_channel', 'contact_channel');
        });
        Schema::table('bookings', function (Blueprint $table) {
            $table->string('acquisition_channel', Constants::CHANNEL_NAME_LENGTH)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('acquisition_channel');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->renameColumn('contact_channel', 'acquisition_channel');
        });
    }
}
