<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExperienceCopiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experience_copies', function(Blueprint $table)
		{
			$table->foreign('experience_id', 'experience_copies_fk0')->references('id')->on('experiences')->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experience_copies', function(Blueprint $table)
		{
			$table->dropForeign('experience_copies_fk0');
		});
	}

}
