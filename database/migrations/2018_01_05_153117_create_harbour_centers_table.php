<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHarbourCentersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('harbour_centers', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->double('point_a_lat');
			$table->double('point_a_lng');
			$table->double('point_b_lat');
			$table->double('point_b_lng');
			$table->string('name', Constants::HARBOUR_CENTER_NAME_LENGTH);
			$table->string('business_id', Constants::BUSINESS_ID_HARBOUR_CENTER_LENGTH);

            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('harbour_centers');
	}

}
