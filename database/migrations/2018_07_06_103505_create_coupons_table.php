<?php

use App\Constants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('stock')->unsigned()->nullable();
            $table->string('code', Constants::COUPON_CODE_LENGTH)->nullable();
            $table->dateTime('expiration')->nullable();
            $table->smallInteger('type')->nullable();
            $table->float('value')->nullable();
            $table->string('currency', Constants::CURRENCY_LENGTH)->nullable();

            $table->bigInteger('referrer_booking_id')->unsigned()->nullable();
            $table->foreign('referrer_booking_id', 'coupons_fk0')->references('id')->on('bookings')->onDelete('set null');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
