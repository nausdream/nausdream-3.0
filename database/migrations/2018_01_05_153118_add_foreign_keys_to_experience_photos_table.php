<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExperiencePhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('experience_photos', function(Blueprint $table)
		{
			$table->foreign('experience_id', 'experience_photos_fk0')->references('id')->on('experiences')->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('experience_photos', function(Blueprint $table)
		{
			$table->dropForeign('experience_photos_fk0');
		});
	}

}
