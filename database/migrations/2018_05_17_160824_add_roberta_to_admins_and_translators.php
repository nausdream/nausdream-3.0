<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRobertaToAdminsAndTranslators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        App\Models\Administrator::create(
            ['first_name' => 'Roberta', 'last_name' => 'Cinus', 'email' => 'bebacinus@gmail.com', 'phone' => '+393407306113']
        );
        \App\TranslationModels\User::create(
            ['first_name' => 'Roberta', 'last_name' => 'Cinus', 'email' => 'bebacinus@gmail.com', 'phone' => '+393407306113']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Models\Administrator::where('phone', '+393407306113')->forceDelete();
        App\TranslationModels\User::where('phone', '+393407306113')->forceDelete();
    }
}
