<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * Setup foreign key for translation database
         */
        Schema::connection('mysql_translation')->table('sentences', function (Blueprint $table) {
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('set null');
        });

        Schema::connection('mysql_translation')->table('sentence_translations', function (Blueprint $table) {
            $table->foreign('sentence_id')->references('id')->on('sentences')->onDelete('set null');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('set null');
        });

        Schema::connection('mysql_translation')->table('languages_users', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::connection('mysql_translation')->table('sentences', function (Blueprint $table) {
            $table->dropForeign('sentences_page_id_foreign');
        });

        Schema::connection('mysql_translation')->table('sentence_translations', function (Blueprint $table) {
            $table->dropForeign('sentence_translations_sentence_id_foreign');
            $table->dropForeign('sentence_translations_language_id_foreign');
        });

        Schema::connection('mysql_translation')->table('languages_users', function (Blueprint $table) {
            $table->dropForeign('languages_users_user_id_foreign');
            $table->dropForeign('languages_users_language_id_foreign');
        });

        Schema::enableForeignKeyConstraints();
    }
}
