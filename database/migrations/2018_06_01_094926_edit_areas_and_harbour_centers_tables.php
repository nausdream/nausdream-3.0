<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditAreasAndHarbourCentersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas', function(Blueprint $table){
            $table->boolean("hidden")->default(false)->nullable();
            $table->smallInteger("order")->nullable();
        });

        Schema::table('harbour_centers', function(Blueprint $table){
            $table->boolean("hidden")->default(false)->nullable();
            $table->smallInteger("order")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas', function(Blueprint $table){
            $table->dropColumn("hidden");
            $table->dropColumn("order");
        });

        Schema::table('harbour_centers', function(Blueprint $table){
            $table->dropColumn("hidden");
            $table->dropColumn("order");
        });
    }
}
