<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSpokenLanguagesExperiencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('spoken_languages_experiences', function(Blueprint $table)
		{
			$table->foreign('experience_id', 'spoken_languages_experiences_fk0')->references('id')->on('experiences')->onDelete('set null');
			$table->foreign('spoken_language_id', 'spoken_languages_experiences_fk1')->references('id')->on('spoken_languages')->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('spoken_languages_experiences', function(Blueprint $table)
		{
			$table->dropForeign('spoken_languages_experiences_fk0');
			$table->dropForeign('spoken_languages_experiences_fk1');
		});
	}

}
