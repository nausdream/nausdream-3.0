<?php

use App\Models\Booking;
use App\Models\Lead;
use Illuminate\Database\Migrations\Migration;

class SeedUsersFromLeadsAndBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $leads = \App\Models\Lead::all();
        $leads->each(function (Lead $lead) {
            \App\Models\User::updateOrCreate(['email' => $lead->email],
                [
                    'first_name' => $lead->name,
                    'language' => $lead->language,
                    'email' => $lead->email,
                    'phone' => $lead->phone,
                    'marketing_accepted' => $lead->marketing_accepted,
                ]);
        });

        $bookings = \App\Models\Booking::all();
        $bookings->each(function (Booking $booking) {
            $user = \App\Models\User::updateOrCreate(['email' => $booking->user_email],
                [
                    'first_name' => $booking->user_first_name,
                    'last_name' => $booking->user_last_name,
                    'place_of_birth' => $booking->user_place_of_birth,
                    'birth_date' => $booking->user_birth_date,
                    'city' => $booking->user_city,
                    'address' => $booking->user_address,
                    'zip_code' => $booking->user_zip_code,
                    'language' => $booking->language,
                    'email' => $booking->user_email,
                    'phone' => $booking->user_phone
                ]);

            $booking->user()->associate($user);
            $booking->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
