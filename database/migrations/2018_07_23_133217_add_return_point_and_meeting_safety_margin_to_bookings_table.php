<?php

use App\Constants;
use App\Models\Booking;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReturnPointAndMeetingSafetyMarginToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('meeting_safety_margin')->nullable();
            $table->string('return_point', Constants::EXPERIENCE_MEETING_POINT_LENGTH)->nullable();
        });

        \App\Models\Booking::each(function(Booking $booking){
            $booking->meeting_safety_margin = $booking->experience->meeting_safety_margin;
            $booking->return_point = $booking->meeting_point;
            $booking->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('meeting_safety_margin');
            $table->dropColumn('return_point');
        });
    }
}
