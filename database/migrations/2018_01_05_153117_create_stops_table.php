<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stops', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->integer('stop_number');
			$table->bigInteger('experience_id')->unsigned()->nullable();
			$table->string('language', \App\Constants::LANGUAGE_LENGTH);
			$table->text('text');
            $table->integer('duration')->nullable();
            $table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stops');
	}

}
