<?php

use App\Constants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMysqlTranslationSentencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_translation')->create('sentences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', Constants::SENTENCE_NAME_LENGTH);

            $table->bigInteger('page_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->dropIfExists('sentences');
    }
}
