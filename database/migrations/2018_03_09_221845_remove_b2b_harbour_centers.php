<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveB2bHarbourCenters extends Migration
{
    private $b2bHarbourCenters = [
        'catania' => ['name' => 'catania', 'business_id' => 'cat', 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0],
        'ibiza' => ['name' => 'ibiza', 'business_id' => 'ibz', 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0],
        'mallorca' => ['name' => 'mallorca', 'business_id' => 'mai', 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0],
        'olbia' => ['name' => 'olbia', 'business_id' => 'olb', 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0],
        'palermo' => ['name' => 'palermo', 'business_id' => 'pal', 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0],
        'savona' => ['name' => 'savona', 'business_id' => 'sav', 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0],
    ];
    private $boatTypes = [
        'it' => [
            'barca' => null,
            'barca-vela' => 2,
            'barca-motore' => 1,
            'catamarano' => 3,
            'gommone' => 4,
        ],
        'en' => [
            'boat' => null,
            'sailboat' => 2,
            'motorboat' => 1,
            'catamaran' => 3,
            'rubberboat' => 4,
        ]
    ];
    private $durations = [
        'it' => [
            'mezza-giornata' => 'hd',
            'giornata-intera' => 'fd'
        ],
        'en' => [
            'half-day' => 'hd',
            'full-day' => 'fd'
        ]
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->b2bHarbourCenters as $harbourCenter) {
            $delete = \App\Models\HarbourCenter::where('name', $harbourCenter)->first();
            $delete->seos()->forceDelete();
            $delete->forceDelete();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $languages = \App\Helpers\LanguageHelper::getLanguages();
        foreach ($this->b2bHarbourCenters as $harbourCenter) {
            $new = \App\Models\HarbourCenter::create(['name' => $harbourCenter['name'], 'business_id' => $harbourCenter['business_id'], 'point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 0, 'point_b_lng' => 0]);
            foreach ($languages as $language) {
                foreach ($this->boatTypes[$language] as $boatType => $bt_id) {
                    \App\Models\Seo::create([
                        'language' => $language,
                        'query' => "$boatType-trips-".$harbourCenter['name'],
                        'boat_type_id' => $bt_id,
                        'harbour_center_id' => $new->id,
                        'duration' => null,
                        'og_images' => null
                    ]);

                    foreach ($this->durations[$language] as $duration => $duration_code) {
                        \App\Models\Seo::create([
                            'language' => $language,
                            'query' => "$duration-$boatType-trips-".$harbourCenter['name'],
                            'boat_type_id' => $bt_id,
                            'harbour_center_id' => $new->id,
                            'duration' => $duration_code,
                            'og_images' => null
                        ]);
                    }
                }
            }
        }
    }
}
