<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;

class FixReferAFriendCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $refer = \App\Models\Campaign::where('name', 'refer-a-friend')->get()->first();

        if (isset($refer)) {
            $refer->attributes = [
                /*
                 * The campaign's start datetime. Starting from this date, it generates coupons for buying users.
                 */
                'start' => Carbon::create(2018, 7, 12, 0, 0, 0, \App\Constants::TIMEZONE),
                /*
                 * When new referral coupon are not generated anymore
                 */
                'stop' => Carbon::create(2018, 8, 1, 0, 0, 0, \App\Constants::TIMEZONE),
                /**
                 * Default friend coupons. Generated upon booking from any user
                 * in the campaign timeframe
                 */
                'friend_coupon' => [
                    'value' => 15,
                    'type' => \App\Constants::COUPON_PERCENTAGE,
                    'stock' => 99999,
                    'expiration' => Carbon::create(2018, 8, 13, 0, 0, 0, \App\Constants::TIMEZONE)
                ],
                /**
                 * Default referrer coupons. Created when
                 * the friend (referral) books with the referrer coupon, and sent to the referrer via email.
                 */
                'referrer_coupon' => [
                    'value' => 15,
                    'type' => \App\Constants::COUPON_PERCENTAGE,
                    'stock' => 1,
                    'expiration' => Carbon::create(2018, 10, 1, 0, 0, 0, \App\Constants::TIMEZONE)
                ]
            ];
            $refer->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $refer = \App\Models\Campaign::where('name', 'refer-a-friend')->get()->first();

        if (isset($refer)) {
            $refer->attributes = [
                /*
                 * The campaign's start datetime. Starting from this date, it generates coupons for buying users.
                 */
                'start' => Carbon::create(2018, 7, 12, 0, 0, 0, \App\Constants::TIMEZONE),
                /*
                 * When new referral coupon are not generated anymore
                 */
                'stop' => Carbon::create(2018, 7, 31, 0, 0, 0, \App\Constants::TIMEZONE),
                /**
                 * Default friend coupons. Generated upon booking from any user
                 * in the campaign timeframe
                 */
                'friend_coupon' => [
                    'value' => 15,
                    'type' => \App\Constants::COUPON_PERCENTAGE,
                    'stock' => 99999,
                    'expiration' => Carbon::create(2018, 8, 12, 0, 0, 0, \App\Constants::TIMEZONE)
                ],
                /**
                 * Default referrer coupons. Created when
                 * the friend (referral) books with the referrer coupon, and sent to the referrer via email.
                 */
                'referrer_coupon' => [
                    'value' => 15,
                    'type' => \App\Constants::COUPON_PERCENTAGE,
                    'stock' => 1,
                    'expiration' => Carbon::create(2018, 9, 30, 0, 0, 0, \App\Constants::TIMEZONE)
                ]
            ];
            $refer->save();
        }
    }
}
