<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings_campaigns', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('campaign_id')->unsigned()->nullable();
            $table->bigInteger('booking_id')->unsigned()->nullable();

            $table->foreign('campaign_id', 'bookings_campaigns_fk0')->references('id')->on('campaigns')->onDelete('set null');
            $table->foreign('booking_id', 'bookings_campaigns_fk1')->references('id')->on('bookings')->onDelete('set null');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings_campaigns');
    }
}
