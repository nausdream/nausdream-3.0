<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationLikeToDoToLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('leads', function ($table)
        {
            $table->string('location',150)->nullable();
            $table->text('like_to_do')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('leads', function (Blueprint $table) {
            $table->dropColumn('location');
            $table->dropColumn('like_to_do');
        });
    }
}
