<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ADMINISTRATORS
use App\Constants;

Route::namespace('Admin')->middleware('auth.admin')->group(function () {
    App::setLocale(\App\Constants::ADMIN_DEFAULT_LANGUAGE);

    Route::get('/administrator/logout', [
        'as' => Constants::R_ADMIN_LOGOUT,
        'uses' => function () {
            Auth::guard(Constants::GUARD_ADMIN)->logout();
            return redirect('/administrator/login');
        }
    ]);
    Route::get('/administrator', [
        'as' => Constants::R_ADMIN_HOME,
        'uses' => function () {
            return redirect()->route(Constants::R_ADMIN_BOOKINGS_LIST);
        }
    ]);

    // PRODUCTS
    Route::get('/administrator/products/new', [
        'as' => Constants::R_ADMIN_PRODUCTS_NEW,
        'uses' => 'ProductsController@getAddView'
    ]);
    Route::post('/administrator/products/new', [
        'as' => Constants::R_ADMIN_PRODUCTS_NEW_POST,
        'uses' => 'ProductsController@add'
    ]);
    Route::get('/administrator/products', [
        'as' => Constants::R_ADMIN_PRODUCTS_LIST,
        'uses' => 'ProductsController@list'
    ]);
    Route::post('/administrator/products/{id}/edit', [
        'as' => Constants::R_ADMIN_PRODUCTS_EDIT_POST,
        'uses' => 'ProductsController@edit'
    ]);
    Route::get('/administrator/products/{id}/edit', [
        'as' => Constants::R_ADMIN_PRODUCTS_EDIT_FORM,
        'uses' => 'ProductsController@getEditView'
    ]);
    Route::post('/administrator/products/delete', [
        'as' => Constants::R_ADMIN_PRODUCTS_DELETE,
        'uses' => 'ProductsController@delete'
    ]);
    Route::get('/administrator/products/copy/get', [
        'as' => Constants::R_ADMIN_PRODUCTS_GET_COPY,
        'uses' => 'ProductsController@getCopy'
    ]);

    // BOOKINGS
    Route::get('/administrator/bookings/new', [
        'as' => Constants::R_ADMIN_BOOKINGS_NEW,
        'uses' => 'BookingsController@getAddView'
    ]);
    Route::post('/administrator/bookings/new', [
        'as' => Constants::R_ADMIN_BOOKINGS_NEW_POST,
        'uses' => 'BookingsController@add'
    ]);
    Route::get('/administrator/bookings', [
        'as' => Constants::R_ADMIN_BOOKINGS_LIST,
        'uses' => 'BookingsController@list'
    ]);
    Route::post('/administrator/bookings/{id}/edit', [
        'as' => Constants::R_ADMIN_BOOKINGS_EDIT_POST,
        'uses' => 'BookingsController@edit'
    ]);
    Route::get('/administrator/bookings/{id}/edit', [
        'as' => Constants::R_ADMIN_BOOKINGS_EDIT_FORM,
        'uses' => 'BookingsController@getEditView'
    ]);
    Route::get('/administrator/bookings/{id}/email', [
        'as' => Constants::R_ADMIN_BOOKINGS_EMAIL_BODY,
        'uses' => 'BookingsController@getBookingEmail'
    ]);
    Route::post('/administrator/bookings/email', [
        'as' => Constants::R_ADMIN_BOOKINGS_EMAIL_BODY_NEW,
        'uses' => 'BookingsController@getBookingEmailDraft'
    ]);
    Route::post('/administrator/bookings/delete', [
        'as' => Constants::R_ADMIN_BOOKINGS_DELETE_POST,
        'uses' => 'BookingsController@delete'
    ]);

    // SITEMAP
    Route::get('/administrator/sitemap', [
        'as' => Constants::R_ADMIN_SITEMAP_VIEW,
        'uses' => 'SitemapController@getView'
    ]);
    Route::post('/administrator/sitemap/generate', [
        'as' => Constants::R_ADMIN_SITEMAP_GENERATE,
        'uses' => 'SitemapController@generate'
    ]);
    Route::get('/administrator/sitemap/download', [
        'as' => Constants::R_ADMIN_SITEMAP_DOWNLOAD,
        'uses' => 'SitemapController@download'
    ]);

    // AREAS
    Route::get('/administrator/areas', [
        'as' => Constants::R_ADMIN_AREAS,
        'uses' => 'AreasController@getView'
    ]);
    Route::post('/administrator/areas/edit', [
        'as' => Constants::R_ADMIN_AREAS_EDIT,
        'uses' => 'AreasController@edit'
    ]);
    Route::post('/administrator/areas/new', [
        'as' => Constants::R_ADMIN_AREAS_NEW,
        'uses' => 'AreasController@addArea'
    ]);
    Route::post('/administrator/harbour-centers/new', [
        'as' => Constants::R_ADMIN_HC_NEW,
        'uses' => 'AreasController@addHarbourCenter'
    ]);


    Route::get('/administrator/included-services', [
        'as' => Constants::R_ADMIN_INCLUDED_SERVICES,
        'uses' => 'IncludedServicesController@getView'
    ]);
    Route::post('/administrator/included-services/add', [
        'as' => Constants::R_ADMIN_INCLUDED_SERVICES_ADD,
        'uses' => 'IncludedServicesController@add'
    ]);
    Route::post('/administrator/included-services/delete', [
        'as' => Constants::R_ADMIN_INCLUDED_SERVICES_DELETE,
        'uses' => 'IncludedServicesController@delete'
    ]);
});

Route::get('/administrator/sitemap/direct', [
    'as' => Constants::R_ADMIN_SITEMAP_DIRECT,
    'uses' => 'Admin\SitemapController@getXML'
]);

Route::middleware(['auth.admin'])->group(function () {
    // SEO
    Route::post('/seo/meta-title', [
        'as' => Constants::R_SEO_META_TITLE,
        'uses' => 'SeosController@getMetaTitle'
    ]);
    Route::post('/seo/slug-url', [
        'as' => Constants::R_SEO_SLUG_URL,
        'uses' => 'SeosController@getSlugUrl'
    ]);
    Route::post('/seo/meta-description', [
        'as' => 'seo.meta-description',
        'uses' => 'SeosController@getMetaDescription'
    ]);

    Route::get('/maps-link', [
        'as' => 'maps.link',
        'uses' => function (Illuminate\Http\Request $request) {
            return \App\Helpers\GoogleMapsHelper::getLinkFromCoordinates($request->lat, $request->lng);
        }
    ]);
});
Route::get('/administrator/error', [
    'as' => Constants::R_ADMIN_ERROR,
    'uses' => function () {
        return view(Constants::R_ADMIN_ERROR);
    }
]);
Route::post('/administrator/auth', [
    'as' => Constants::R_ADMIN_ACCOUNTKIT,
    'uses' => 'Admin\AdminAuthController@check'
]);
Route::get('/administrator/login', [
    'as' => Constants::R_ADMIN_LOGIN,
    'uses' => function () {
        return view(Constants::R_ADMIN_LOGIN, ['to' => '']);
    }
]);

//TRANSLATORS
Route::namespace('Translations')->middleware('auth.translator')->group(function () {
    Route::get('/translator/logout', [
        'as' => Constants::R_TRANSLATOR_LOGOUT,
        'uses' => function () {
            Auth::guard(Constants::GUARD_TRANSLATOR)->logout();
            return redirect('/translator/login');
        }
    ]);
    Route::get('/translator', [
        'as' => Constants::R_TRANSLATOR_HOME,
        'uses' => function () {
            return redirect()->route(Constants::R_TRANSLATOR_PAGES_LIST);
        }
    ]);
    Route::post('/translator/pages/new', [
        'as' => Constants::R_TRANSLATOR_PAGES_NEW_POST,
        'uses' => 'PagesController@add'
    ]);
    Route::get('/translator/pages', [
        'as' => Constants::R_TRANSLATOR_PAGES_LIST,
        'uses' => 'PagesController@list'
    ]);
    Route::post('/translator/pages/{id}/edit', [
        'as' => Constants::R_TRANSLATOR_PAGES_EDIT_POST,
        'uses' => 'PagesController@edit'
    ]);
    Route::get('/translator/pages/{id}/edit', [
        'as' => Constants::R_TRANSLATOR_PAGES_EDIT_FORM,
        'uses' => 'PagesController@getEditView'
    ]);

    Route::post('/translator/pages/delete', [
        'as' => Constants::R_TRANSLATOR_PAGES_DELETE,
        'uses' => 'PagesController@delete'
    ]);

    Route::post('/translator/sentences/new', [
        'as' => Constants::R_TRANSLATOR_SENTENCES_NEW,
        'uses' => 'PagesController@addSentence'
    ]);

    Route::post('/translator/sentences/delete', [
        'as' => Constants::R_TRANSLATOR_SENTENCES_DELETE,
        'uses' => 'PagesController@deleteSentence'
    ]);
});
Route::get('/translator/error', [
    'as' => Constants::R_TRANSLATOR_ERROR,
    'uses' => function () {
        return view(Constants::R_TRANSLATOR_ERROR);
    }
]);
Route::post('/translator/auth', [
    'as' => Constants::R_TRANSLATOR_ACCOUNTKIT,
    'uses' => 'Translations\TranslatorAuthController@check'
]);
Route::get('/translator/login', [
    'as' => Constants::R_TRANSLATOR_LOGIN,
    'uses' => function () {
        return view(Constants::R_TRANSLATOR_LOGIN, ['to' => '']);
    }
]);

// Save cookies consent on db
Route::post('/cookies', [
    'as' => Constants::R_USERS_COOKIES,
    'uses' => 'CookiesController@saveCookieConsent'
]);

// Users
Route::namespace('Users')->middleware(['language', 'set.admin'])->group(function () {
    Route::get('/{lang?}', [
        'as' => Constants::R_USERS_HOME,
        'uses' => 'StaticPagesController@getHome'
    ]);

    Route::get('/{lang}/' . Constants::HARBOUR_CENTER_DIRECT . '/{business_id}', [
        'as' => Constants::R_USERS_HARBOUR_CENTER_DIRECT,
        'uses' => 'HarbourCenterController@getFromName'
    ]);

    Route::get('/{lang}/' . Constants::HARBOUR_CENTER_PREFIX . '/{slug}', [
        'as' => Constants::R_USERS_HARBOUR_CENTER,
        'uses' => 'HarbourCenterController@get'
    ]);

    Route::get('/{lang}/' . Constants::LISTING_PREFIX . '/{slug_url}', [
        'as' => Constants::R_USERS_LISTING,
        'uses' => 'ListingController@get'
    ]);

    Route::post('/{lang}/' . Constants::LISTING_PREFIX . '/{slug_url}/book', [
        'as' => Constants::R_USERS_CONTACT_FORM,
        'uses' => 'ListingController@contact'
    ]);

    Route::post('/{lang}/contact-custom-form', [
        'as' => Constants::R_USERS_CONTACT_CUSTOM_FORM,
        'uses' => 'ListingController@contactCustom'
    ]);

    Route::get('/{lang}/bookings/{id}/checkout', [
        'as' => Constants::R_USERS_BOOKING_CHECKOUT,
        'uses' => 'BookingController@getCheckoutView'
    ]);

    Route::post('/{lang}/bookings/{id}/checkout', [
        'as' => Constants::R_USERS_BOOKING_CHECKOUT_POST,
        'uses' => 'BookingController@checkout'
    ]);

    Route::post('/{lang}/bookings/{id}/pay', [
        'as' => Constants::R_USERS_BOOKING_PAY,
        'uses' => 'BookingController@pay'
    ]);

    Route::post('/{lang}/coupon', [
        'as' => Constants::R_USERS_COUPON_CHECK,
        'uses' => 'CouponController@apply'
    ]);

    // CAMPAIGNS
    Route::get('/{lang}/opt-in', [
        'as' => Constants::R_USERS_OPT_IN,
        'uses' => 'CampaignController@getOptInView'
    ]);
    Route::get('/{lang}/rules', [
        'as' => Constants::R_USERS_CAMPAIGNS_RULES,
        'uses' => 'CampaignController@getCampaignRules'
    ]);

    // Static routes
/*    Route::get('/it/come-funziona', [
        'as' => 'users.how-it-works-it',
        'uses' => 'StaticPagesController@getHowItWorks'
    ]);
    Route::get('/en/how-it-works', [
        'as' => 'users.how-it-works-en',
        'uses' => 'StaticPagesController@getHowItWorks'
    ]);*/

    Route::get('/it/termini-e-condizioni', [
        'as' => 'users.terms-of-service-it',
        'uses' => 'StaticPagesController@getTermsOfService'
    ]);
    Route::get('/en/terms-of-service', [
        'as' => 'users.terms-of-service-en',
        'uses' => 'StaticPagesController@getTermsOfService'
    ]);

    Route::get('/it/cookie-policy', [
        'as' => 'users.cookie-policy-it',
        'uses' => 'StaticPagesController@getCookiePolicy'
    ]);
    Route::get('/en/cookie-policy', [
        'as' => 'users.cookie-policy-en',
        'uses' => 'StaticPagesController@getCookiePolicy'
    ]);

    Route::get('/it/privacy-policy', [
        'as' => 'users.privacy-policy-it',
        'uses' => 'StaticPagesController@getPrivacyPolicy'
    ]);
    Route::get('/en/privacy-policy', [
        'as' => 'users.privacy-policy-en',
        'uses' => 'StaticPagesController@getPrivacyPolicy'
    ]);

    Route::get('/it/contattaci', [
        'as' => 'users.contact-us-it',
        'uses' => 'StaticPagesController@getContactUs'
    ]);
    Route::get('/en/contact-us', [
        'as' => 'users.contact-us-en',
        'uses' => 'StaticPagesController@getContactUs'
    ]);
    Route::get('/it/non-trovato', [
        'as' => 'users.not-found-it',
        'uses' => 'StaticPagesController@getNotFound'
    ]);
    Route::get('/en/not-found', [
        'as' => 'users.not-found-en',
        'uses' => 'StaticPagesController@getNotFound'
    ]);
    Route::get('/it/non-autorizzato', [
        'as' => 'users.not-authorized-it',
        'uses' => 'StaticPagesController@getNotAuthorized'
    ]);
    Route::get('/en/not-authorized', [
        'as' => 'users.not-authorized-en',
        'uses' => 'StaticPagesController@getNotAuthorized'
    ]);
    Route::get('/it/disiscriviti', [
        'as' => 'users.unsubscribe-it',
        'uses' => 'StaticPagesController@getUnsubscribe'
    ]);
    Route::get('/en/unsubscribe', [
        'as' => 'users.unsubscribe-en',
        'uses' => 'StaticPagesController@getUnsubscribe'
    ]);


    // Old experiences
    Route::get('/{lang}/experiences/{slug_url}', [
        'as' => Constants::R_USERS_OLD_EXPERIENCE,
        'uses' => 'ListingController@getNewFromOld'
    ]);
});
