webpackJsonp([3],{

/***/ "./resources/assets/backoffice/js/libraries/jquery.nestable.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery) {/*!
 * Nestable jQuery Plugin - Copyright (c) 2012 David Bushell - http://dbushell.com/
 * Dual-licensed under the BSD or MIT licenses
 */
;(function ($, window, document, undefined) {
    var hasTouch = 'ontouchstart' in document;

    /**
     * Detect CSS pointer-events property
     * events are normally disabled on the dragging element to avoid conflicts
     * https://github.com/ausi/Feature-detection-technique-for-pointer-events/blob/master/modernizr-pointerevents.js
     */
    var hasPointerEvents = function () {
        var el = document.createElement('div'),
            docEl = document.documentElement;
        if (!('pointerEvents' in el.style)) {
            return false;
        }
        el.style.pointerEvents = 'auto';
        el.style.pointerEvents = 'x';
        docEl.appendChild(el);
        var supports = window.getComputedStyle && window.getComputedStyle(el, '').pointerEvents === 'auto';
        docEl.removeChild(el);
        return !!supports;
    }();

    var defaults = {
        listNodeName: 'ol',
        itemNodeName: 'li',
        rootClass: 'dd',
        listClass: 'dd-list',
        itemClass: 'dd-item',
        dragClass: 'dd-dragel',
        handleClass: 'dd-handle',
        collapsedClass: 'dd-collapsed',
        placeClass: 'dd-placeholder',
        noDragClass: 'dd-nodrag',
        emptyClass: 'dd-empty',
        expandBtnHTML: '<button data-action="expand" type="button">Expand</button>',
        collapseBtnHTML: '<button data-action="collapse" type="button">Collapse</button>',
        group: 0,
        maxDepth: 5,
        threshold: 20
    };

    function Plugin(element, options) {
        this.w = $(document);
        this.el = $(element);
        this.options = $.extend({}, defaults, options);
        this.init();
    }

    Plugin.prototype = {

        init: function init() {
            var list = this;

            list.reset();

            list.el.data('nestable-group', this.options.group);

            list.placeEl = $('<div class="' + list.options.placeClass + '"/>');

            $.each(this.el.find(list.options.itemNodeName), function (k, el) {
                list.setParent($(el));
            });

            list.el.on('click', 'button', function (e) {
                if (list.dragEl) {
                    return;
                }
                var target = $(e.currentTarget),
                    action = target.data('action'),
                    item = target.parent(list.options.itemNodeName);
                if (action === 'collapse') {
                    list.collapseItem(item);
                }
                if (action === 'expand') {
                    list.expandItem(item);
                }
            });

            var onStartEvent = function onStartEvent(e) {
                var handle = $(e.target);
                if (!handle.hasClass(list.options.handleClass)) {
                    if (handle.closest('.' + list.options.noDragClass).length) {
                        return;
                    }
                    handle = handle.closest('.' + list.options.handleClass);
                }

                if (!handle.length || list.dragEl) {
                    return;
                }

                list.isTouch = /^touch/.test(e.type);
                if (list.isTouch && e.touches.length !== 1) {
                    return;
                }

                e.preventDefault();
                list.dragStart(e.touches ? e.touches[0] : e);
            };

            var onMoveEvent = function onMoveEvent(e) {
                if (list.dragEl) {
                    e.preventDefault();
                    list.dragMove(e.touches ? e.touches[0] : e);
                }
            };

            var onEndEvent = function onEndEvent(e) {
                if (list.dragEl) {
                    e.preventDefault();
                    list.dragStop(e.touches ? e.touches[0] : e);
                }
            };

            if (hasTouch) {
                list.el[0].addEventListener('touchstart', onStartEvent, false);
                window.addEventListener('touchmove', onMoveEvent, false);
                window.addEventListener('touchend', onEndEvent, false);
                window.addEventListener('touchcancel', onEndEvent, false);
            }

            list.el.on('mousedown', onStartEvent);
            list.w.on('mousemove', onMoveEvent);
            list.w.on('mouseup', onEndEvent);
        },

        serialize: function serialize() {
            var data,
                depth = 0,
                list = this;
            step = function (_step) {
                function step(_x, _x2) {
                    return _step.apply(this, arguments);
                }

                step.toString = function () {
                    return _step.toString();
                };

                return step;
            }(function (level, depth) {
                var array = [],
                    items = level.children(list.options.itemNodeName);
                items.each(function () {
                    var li = $(this),
                        item = $.extend({}, li.data()),
                        sub = li.children(list.options.listNodeName);
                    if (sub.length) {
                        item.children = step(sub, depth + 1);
                    }
                    array.push(item);
                });
                return array;
            });
            data = step(list.el.find(list.options.listNodeName).first(), depth);
            return data;
        },

        serialise: function serialise() {
            return this.serialize();
        },

        reset: function reset() {
            this.mouse = {
                offsetX: 0,
                offsetY: 0,
                startX: 0,
                startY: 0,
                lastX: 0,
                lastY: 0,
                nowX: 0,
                nowY: 0,
                distX: 0,
                distY: 0,
                dirAx: 0,
                dirX: 0,
                dirY: 0,
                lastDirX: 0,
                lastDirY: 0,
                distAxX: 0,
                distAxY: 0
            };
            this.isTouch = false;
            this.moving = false;
            this.dragEl = null;
            this.dragRootEl = null;
            this.dragDepth = 0;
            this.hasNewRoot = false;
            this.pointEl = null;
        },

        expandItem: function expandItem(li) {
            li.removeClass(this.options.collapsedClass);
            li.children('[data-action="expand"]').hide();
            li.children('[data-action="collapse"]').show();
            li.children(this.options.listNodeName).show();
        },

        collapseItem: function collapseItem(li) {
            var lists = li.children(this.options.listNodeName);
            if (lists.length) {
                li.addClass(this.options.collapsedClass);
                li.children('[data-action="collapse"]').hide();
                li.children('[data-action="expand"]').show();
                li.children(this.options.listNodeName).hide();
            }
        },

        expandAll: function expandAll() {
            var list = this;
            list.el.find(list.options.itemNodeName).each(function () {
                list.expandItem($(this));
            });
        },

        collapseAll: function collapseAll() {
            var list = this;
            list.el.find(list.options.itemNodeName).each(function () {
                list.collapseItem($(this));
            });
        },

        setParent: function setParent(li) {
            if (li.children(this.options.listNodeName).length) {
                li.prepend($(this.options.expandBtnHTML));
                li.prepend($(this.options.collapseBtnHTML));
            }
            li.children('[data-action="expand"]').hide();
        },

        unsetParent: function unsetParent(li) {
            li.removeClass(this.options.collapsedClass);
            li.children('[data-action]').remove();
            li.children(this.options.listNodeName).remove();
        },

        dragStart: function dragStart(e) {
            var mouse = this.mouse,
                target = $(e.target),
                dragItem = target.closest(this.options.itemNodeName);

            this.placeEl.css('height', dragItem.height());

            mouse.offsetX = e.offsetX !== undefined ? e.offsetX : e.pageX - target.offset().left;
            mouse.offsetY = e.offsetY !== undefined ? e.offsetY : e.pageY - target.offset().top;
            mouse.startX = mouse.lastX = e.pageX;
            mouse.startY = mouse.lastY = e.pageY;

            this.dragRootEl = this.el;

            this.dragEl = $(document.createElement(this.options.listNodeName)).addClass(this.options.listClass + ' ' + this.options.dragClass);
            this.dragEl.css('width', dragItem.width());

            dragItem.after(this.placeEl);
            dragItem[0].parentNode.removeChild(dragItem[0]);
            dragItem.appendTo(this.dragEl);

            $(document.body).append(this.dragEl);
            this.dragEl.css({
                'left': e.pageX - mouse.offsetX,
                'top': e.pageY - mouse.offsetY
            });
            // total depth of dragging item
            var i,
                depth,
                items = this.dragEl.find(this.options.itemNodeName);
            for (i = 0; i < items.length; i++) {
                depth = $(items[i]).parents(this.options.listNodeName).length;
                if (depth > this.dragDepth) {
                    this.dragDepth = depth;
                }
            }
        },

        dragStop: function dragStop(e) {
            var el = this.dragEl.children(this.options.itemNodeName).first();
            el[0].parentNode.removeChild(el[0]);
            this.placeEl.replaceWith(el);

            this.dragEl.remove();
            this.el.trigger('change');
            if (this.hasNewRoot) {
                this.dragRootEl.trigger('change');
            }
            this.reset();
        },

        dragMove: function dragMove(e) {
            var list,
                parent,
                prev,
                next,
                depth,
                opt = this.options,
                mouse = this.mouse;

            this.dragEl.css({
                'left': e.pageX - mouse.offsetX,
                'top': e.pageY - mouse.offsetY
            });

            // mouse position last events
            mouse.lastX = mouse.nowX;
            mouse.lastY = mouse.nowY;
            // mouse position this events
            mouse.nowX = e.pageX;
            mouse.nowY = e.pageY;
            // distance mouse moved between events
            mouse.distX = mouse.nowX - mouse.lastX;
            mouse.distY = mouse.nowY - mouse.lastY;
            // direction mouse was moving
            mouse.lastDirX = mouse.dirX;
            mouse.lastDirY = mouse.dirY;
            // direction mouse is now moving (on both axis)
            mouse.dirX = mouse.distX === 0 ? 0 : mouse.distX > 0 ? 1 : -1;
            mouse.dirY = mouse.distY === 0 ? 0 : mouse.distY > 0 ? 1 : -1;
            // axis mouse is now moving on
            var newAx = Math.abs(mouse.distX) > Math.abs(mouse.distY) ? 1 : 0;

            // do nothing on first move
            if (!mouse.moving) {
                mouse.dirAx = newAx;
                mouse.moving = true;
                return;
            }

            // calc distance moved on this axis (and direction)
            if (mouse.dirAx !== newAx) {
                mouse.distAxX = 0;
                mouse.distAxY = 0;
            } else {
                mouse.distAxX += Math.abs(mouse.distX);
                if (mouse.dirX !== 0 && mouse.dirX !== mouse.lastDirX) {
                    mouse.distAxX = 0;
                }
                mouse.distAxY += Math.abs(mouse.distY);
                if (mouse.dirY !== 0 && mouse.dirY !== mouse.lastDirY) {
                    mouse.distAxY = 0;
                }
            }
            mouse.dirAx = newAx;

            /**
             * move horizontal
             */
            if (mouse.dirAx && mouse.distAxX >= opt.threshold) {
                // reset move distance on x-axis for new phase
                mouse.distAxX = 0;
                prev = this.placeEl.prev(opt.itemNodeName);
                // increase horizontal level if previous sibling exists and is not collapsed
                if (mouse.distX > 0 && prev.length && !prev.hasClass(opt.collapsedClass)) {
                    // cannot increase level when item above is collapsed
                    list = prev.find(opt.listNodeName).last();
                    // check if depth limit has reached
                    depth = this.placeEl.parents(opt.listNodeName).length;
                    if (depth + this.dragDepth <= opt.maxDepth) {
                        // create new sub-level if one doesn't exist
                        if (!list.length) {
                            list = $('<' + opt.listNodeName + '/>').addClass(opt.listClass);
                            list.append(this.placeEl);
                            prev.append(list);
                            this.setParent(prev);
                        } else {
                            // else append to next level up
                            list = prev.children(opt.listNodeName).last();
                            list.append(this.placeEl);
                        }
                    }
                }
                // decrease horizontal level
                if (mouse.distX < 0) {
                    // we can't decrease a level if an item preceeds the current one
                    next = this.placeEl.next(opt.itemNodeName);
                    if (!next.length) {
                        parent = this.placeEl.parent();
                        this.placeEl.closest(opt.itemNodeName).after(this.placeEl);
                        if (!parent.children().length) {
                            this.unsetParent(parent.parent());
                        }
                    }
                }
            }

            var isEmpty = false;

            // find list item under cursor
            if (!hasPointerEvents) {
                this.dragEl[0].style.visibility = 'hidden';
            }
            this.pointEl = $(document.elementFromPoint(e.pageX - document.body.scrollLeft, e.pageY - (window.pageYOffset || document.documentElement.scrollTop)));
            if (!hasPointerEvents) {
                this.dragEl[0].style.visibility = 'visible';
            }
            if (this.pointEl.hasClass(opt.handleClass)) {
                this.pointEl = this.pointEl.parent(opt.itemNodeName);
            }
            if (this.pointEl.hasClass(opt.emptyClass)) {
                isEmpty = true;
            } else if (!this.pointEl.length || !this.pointEl.hasClass(opt.itemClass)) {
                return;
            }

            // find parent list of item under cursor
            var pointElRoot = this.pointEl.closest('.' + opt.rootClass),
                isNewRoot = this.dragRootEl.data('nestable-id') !== pointElRoot.data('nestable-id');

            /**
             * move vertical
             */
            if (!mouse.dirAx || isNewRoot || isEmpty) {
                // check if groups match if dragging over new root
                if (isNewRoot && opt.group !== pointElRoot.data('nestable-group')) {
                    return;
                }
                // check depth limit
                depth = this.dragDepth - 1 + this.pointEl.parents(opt.listNodeName).length;
                if (depth > opt.maxDepth) {
                    return;
                }
                var before = e.pageY < this.pointEl.offset().top + this.pointEl.height() / 2;
                parent = this.placeEl.parent();
                // if empty create new list to replace empty placeholder
                if (isEmpty) {
                    list = $(document.createElement(opt.listNodeName)).addClass(opt.listClass);
                    list.append(this.placeEl);
                    this.pointEl.replaceWith(list);
                } else if (before) {
                    this.pointEl.before(this.placeEl);
                } else {
                    this.pointEl.after(this.placeEl);
                }
                if (!parent.children().length) {
                    this.unsetParent(parent.parent());
                }
                if (!this.dragRootEl.find(opt.itemNodeName).length) {
                    this.dragRootEl.append('<div class="' + opt.emptyClass + '"/>');
                }
                // parent root list has changed
                if (isNewRoot) {
                    this.dragRootEl = pointElRoot;
                    this.hasNewRoot = this.el[0] !== this.dragRootEl[0];
                }
            }
        }

    };

    $.fn.nestable = function (params) {
        var lists = this,
            retval = this;

        lists.each(function () {
            var plugin = $(this).data("nestable");

            if (!plugin) {
                $(this).data("nestable", new Plugin(this, params));
                $(this).data("nestable-id", new Date().getTime());
            } else {
                if (typeof params === 'string' && typeof plugin[params] === 'function') {
                    retval = plugin[params]();
                }
            }
        });

        return retval || lists;
    };
})(__webpack_provided_window_dot_jQuery || window.Zepto, window, document);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/backoffice/js/libraries/jquery.nestable.js");


/***/ })

},[2]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2JhY2tvZmZpY2UvanMvbGlicmFyaWVzL2pxdWVyeS5uZXN0YWJsZS5qcyJdLCJuYW1lcyI6WyIkIiwid2luZG93IiwiZG9jdW1lbnQiLCJ1bmRlZmluZWQiLCJoYXNUb3VjaCIsImhhc1BvaW50ZXJFdmVudHMiLCJlbCIsImNyZWF0ZUVsZW1lbnQiLCJkb2NFbCIsImRvY3VtZW50RWxlbWVudCIsInN0eWxlIiwicG9pbnRlckV2ZW50cyIsImFwcGVuZENoaWxkIiwic3VwcG9ydHMiLCJnZXRDb21wdXRlZFN0eWxlIiwicmVtb3ZlQ2hpbGQiLCJkZWZhdWx0cyIsImxpc3ROb2RlTmFtZSIsIml0ZW1Ob2RlTmFtZSIsInJvb3RDbGFzcyIsImxpc3RDbGFzcyIsIml0ZW1DbGFzcyIsImRyYWdDbGFzcyIsImhhbmRsZUNsYXNzIiwiY29sbGFwc2VkQ2xhc3MiLCJwbGFjZUNsYXNzIiwibm9EcmFnQ2xhc3MiLCJlbXB0eUNsYXNzIiwiZXhwYW5kQnRuSFRNTCIsImNvbGxhcHNlQnRuSFRNTCIsImdyb3VwIiwibWF4RGVwdGgiLCJ0aHJlc2hvbGQiLCJQbHVnaW4iLCJlbGVtZW50Iiwib3B0aW9ucyIsInciLCJleHRlbmQiLCJpbml0IiwicHJvdG90eXBlIiwibGlzdCIsInJlc2V0IiwiZGF0YSIsInBsYWNlRWwiLCJlYWNoIiwiZmluZCIsImsiLCJzZXRQYXJlbnQiLCJvbiIsImUiLCJkcmFnRWwiLCJ0YXJnZXQiLCJjdXJyZW50VGFyZ2V0IiwiYWN0aW9uIiwiaXRlbSIsInBhcmVudCIsImNvbGxhcHNlSXRlbSIsImV4cGFuZEl0ZW0iLCJvblN0YXJ0RXZlbnQiLCJoYW5kbGUiLCJoYXNDbGFzcyIsImNsb3Nlc3QiLCJsZW5ndGgiLCJpc1RvdWNoIiwidGVzdCIsInR5cGUiLCJ0b3VjaGVzIiwicHJldmVudERlZmF1bHQiLCJkcmFnU3RhcnQiLCJvbk1vdmVFdmVudCIsImRyYWdNb3ZlIiwib25FbmRFdmVudCIsImRyYWdTdG9wIiwiYWRkRXZlbnRMaXN0ZW5lciIsInNlcmlhbGl6ZSIsImRlcHRoIiwic3RlcCIsImxldmVsIiwiYXJyYXkiLCJpdGVtcyIsImNoaWxkcmVuIiwibGkiLCJzdWIiLCJwdXNoIiwiZmlyc3QiLCJzZXJpYWxpc2UiLCJtb3VzZSIsIm9mZnNldFgiLCJvZmZzZXRZIiwic3RhcnRYIiwic3RhcnRZIiwibGFzdFgiLCJsYXN0WSIsIm5vd1giLCJub3dZIiwiZGlzdFgiLCJkaXN0WSIsImRpckF4IiwiZGlyWCIsImRpclkiLCJsYXN0RGlyWCIsImxhc3REaXJZIiwiZGlzdEF4WCIsImRpc3RBeFkiLCJtb3ZpbmciLCJkcmFnUm9vdEVsIiwiZHJhZ0RlcHRoIiwiaGFzTmV3Um9vdCIsInBvaW50RWwiLCJyZW1vdmVDbGFzcyIsImhpZGUiLCJzaG93IiwibGlzdHMiLCJhZGRDbGFzcyIsImV4cGFuZEFsbCIsImNvbGxhcHNlQWxsIiwicHJlcGVuZCIsInVuc2V0UGFyZW50IiwicmVtb3ZlIiwiZHJhZ0l0ZW0iLCJjc3MiLCJoZWlnaHQiLCJwYWdlWCIsIm9mZnNldCIsImxlZnQiLCJwYWdlWSIsInRvcCIsIndpZHRoIiwiYWZ0ZXIiLCJwYXJlbnROb2RlIiwiYXBwZW5kVG8iLCJib2R5IiwiYXBwZW5kIiwiaSIsInBhcmVudHMiLCJyZXBsYWNlV2l0aCIsInRyaWdnZXIiLCJwcmV2IiwibmV4dCIsIm9wdCIsIm5ld0F4IiwiTWF0aCIsImFicyIsImxhc3QiLCJpc0VtcHR5IiwidmlzaWJpbGl0eSIsImVsZW1lbnRGcm9tUG9pbnQiLCJzY3JvbGxMZWZ0IiwicGFnZVlPZmZzZXQiLCJzY3JvbGxUb3AiLCJwb2ludEVsUm9vdCIsImlzTmV3Um9vdCIsImJlZm9yZSIsImZuIiwibmVzdGFibGUiLCJwYXJhbXMiLCJyZXR2YWwiLCJwbHVnaW4iLCJEYXRlIiwiZ2V0VGltZSIsIlplcHRvIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBOzs7O0FBSUEsQ0FBQyxDQUFDLFVBQVNBLENBQVQsRUFBWUMsTUFBWixFQUFvQkMsUUFBcEIsRUFBOEJDLFNBQTlCLEVBQ0Y7QUFDSSxRQUFJQyxXQUFXLGtCQUFrQkYsUUFBakM7O0FBRUE7Ozs7O0FBS0EsUUFBSUcsbUJBQW9CLFlBQ3hCO0FBQ0ksWUFBSUMsS0FBUUosU0FBU0ssYUFBVCxDQUF1QixLQUF2QixDQUFaO0FBQUEsWUFDSUMsUUFBUU4sU0FBU08sZUFEckI7QUFFQSxZQUFJLEVBQUUsbUJBQW1CSCxHQUFHSSxLQUF4QixDQUFKLEVBQW9DO0FBQ2hDLG1CQUFPLEtBQVA7QUFDSDtBQUNESixXQUFHSSxLQUFILENBQVNDLGFBQVQsR0FBeUIsTUFBekI7QUFDQUwsV0FBR0ksS0FBSCxDQUFTQyxhQUFULEdBQXlCLEdBQXpCO0FBQ0FILGNBQU1JLFdBQU4sQ0FBa0JOLEVBQWxCO0FBQ0EsWUFBSU8sV0FBV1osT0FBT2EsZ0JBQVAsSUFBMkJiLE9BQU9hLGdCQUFQLENBQXdCUixFQUF4QixFQUE0QixFQUE1QixFQUFnQ0ssYUFBaEMsS0FBa0QsTUFBNUY7QUFDQUgsY0FBTU8sV0FBTixDQUFrQlQsRUFBbEI7QUFDQSxlQUFPLENBQUMsQ0FBQ08sUUFBVDtBQUNILEtBYnNCLEVBQXZCOztBQWVBLFFBQUlHLFdBQVc7QUFDUEMsc0JBQWtCLElBRFg7QUFFUEMsc0JBQWtCLElBRlg7QUFHUEMsbUJBQWtCLElBSFg7QUFJUEMsbUJBQWtCLFNBSlg7QUFLUEMsbUJBQWtCLFNBTFg7QUFNUEMsbUJBQWtCLFdBTlg7QUFPUEMscUJBQWtCLFdBUFg7QUFRUEMsd0JBQWtCLGNBUlg7QUFTUEMsb0JBQWtCLGdCQVRYO0FBVVBDLHFCQUFrQixXQVZYO0FBV1BDLG9CQUFrQixVQVhYO0FBWVBDLHVCQUFrQiw0REFaWDtBQWFQQyx5QkFBa0IsZ0VBYlg7QUFjUEMsZUFBa0IsQ0FkWDtBQWVQQyxrQkFBa0IsQ0FmWDtBQWdCUEMsbUJBQWtCO0FBaEJYLEtBQWY7O0FBbUJBLGFBQVNDLE1BQVQsQ0FBZ0JDLE9BQWhCLEVBQXlCQyxPQUF6QixFQUNBO0FBQ0ksYUFBS0MsQ0FBTCxHQUFVcEMsRUFBRUUsUUFBRixDQUFWO0FBQ0EsYUFBS0ksRUFBTCxHQUFVTixFQUFFa0MsT0FBRixDQUFWO0FBQ0EsYUFBS0MsT0FBTCxHQUFlbkMsRUFBRXFDLE1BQUYsQ0FBUyxFQUFULEVBQWFyQixRQUFiLEVBQXVCbUIsT0FBdkIsQ0FBZjtBQUNBLGFBQUtHLElBQUw7QUFDSDs7QUFFREwsV0FBT00sU0FBUCxHQUFtQjs7QUFFZkQsY0FBTSxnQkFDTjtBQUNJLGdCQUFJRSxPQUFPLElBQVg7O0FBRUFBLGlCQUFLQyxLQUFMOztBQUVBRCxpQkFBS2xDLEVBQUwsQ0FBUW9DLElBQVIsQ0FBYSxnQkFBYixFQUErQixLQUFLUCxPQUFMLENBQWFMLEtBQTVDOztBQUVBVSxpQkFBS0csT0FBTCxHQUFlM0MsRUFBRSxpQkFBaUJ3QyxLQUFLTCxPQUFMLENBQWFWLFVBQTlCLEdBQTJDLEtBQTdDLENBQWY7O0FBRUF6QixjQUFFNEMsSUFBRixDQUFPLEtBQUt0QyxFQUFMLENBQVF1QyxJQUFSLENBQWFMLEtBQUtMLE9BQUwsQ0FBYWpCLFlBQTFCLENBQVAsRUFBZ0QsVUFBUzRCLENBQVQsRUFBWXhDLEVBQVosRUFBZ0I7QUFDNURrQyxxQkFBS08sU0FBTCxDQUFlL0MsRUFBRU0sRUFBRixDQUFmO0FBQ0gsYUFGRDs7QUFJQWtDLGlCQUFLbEMsRUFBTCxDQUFRMEMsRUFBUixDQUFXLE9BQVgsRUFBb0IsUUFBcEIsRUFBOEIsVUFBU0MsQ0FBVCxFQUFZO0FBQ3RDLG9CQUFJVCxLQUFLVSxNQUFULEVBQWlCO0FBQ2I7QUFDSDtBQUNELG9CQUFJQyxTQUFTbkQsRUFBRWlELEVBQUVHLGFBQUosQ0FBYjtBQUFBLG9CQUNJQyxTQUFTRixPQUFPVCxJQUFQLENBQVksUUFBWixDQURiO0FBQUEsb0JBRUlZLE9BQVNILE9BQU9JLE1BQVAsQ0FBY2YsS0FBS0wsT0FBTCxDQUFhakIsWUFBM0IsQ0FGYjtBQUdBLG9CQUFJbUMsV0FBVyxVQUFmLEVBQTJCO0FBQ3ZCYix5QkFBS2dCLFlBQUwsQ0FBa0JGLElBQWxCO0FBQ0g7QUFDRCxvQkFBSUQsV0FBVyxRQUFmLEVBQXlCO0FBQ3JCYix5QkFBS2lCLFVBQUwsQ0FBZ0JILElBQWhCO0FBQ0g7QUFDSixhQWJEOztBQWVBLGdCQUFJSSxlQUFlLFNBQWZBLFlBQWUsQ0FBU1QsQ0FBVCxFQUNuQjtBQUNJLG9CQUFJVSxTQUFTM0QsRUFBRWlELEVBQUVFLE1BQUosQ0FBYjtBQUNBLG9CQUFJLENBQUNRLE9BQU9DLFFBQVAsQ0FBZ0JwQixLQUFLTCxPQUFMLENBQWFaLFdBQTdCLENBQUwsRUFBZ0Q7QUFDNUMsd0JBQUlvQyxPQUFPRSxPQUFQLENBQWUsTUFBTXJCLEtBQUtMLE9BQUwsQ0FBYVQsV0FBbEMsRUFBK0NvQyxNQUFuRCxFQUEyRDtBQUN2RDtBQUNIO0FBQ0RILDZCQUFTQSxPQUFPRSxPQUFQLENBQWUsTUFBTXJCLEtBQUtMLE9BQUwsQ0FBYVosV0FBbEMsQ0FBVDtBQUNIOztBQUVELG9CQUFJLENBQUNvQyxPQUFPRyxNQUFSLElBQWtCdEIsS0FBS1UsTUFBM0IsRUFBbUM7QUFDL0I7QUFDSDs7QUFFRFYscUJBQUt1QixPQUFMLEdBQWUsU0FBU0MsSUFBVCxDQUFjZixFQUFFZ0IsSUFBaEIsQ0FBZjtBQUNBLG9CQUFJekIsS0FBS3VCLE9BQUwsSUFBZ0JkLEVBQUVpQixPQUFGLENBQVVKLE1BQVYsS0FBcUIsQ0FBekMsRUFBNEM7QUFDeEM7QUFDSDs7QUFFRGIsa0JBQUVrQixjQUFGO0FBQ0EzQixxQkFBSzRCLFNBQUwsQ0FBZW5CLEVBQUVpQixPQUFGLEdBQVlqQixFQUFFaUIsT0FBRixDQUFVLENBQVYsQ0FBWixHQUEyQmpCLENBQTFDO0FBQ0gsYUFyQkQ7O0FBdUJBLGdCQUFJb0IsY0FBYyxTQUFkQSxXQUFjLENBQVNwQixDQUFULEVBQ2xCO0FBQ0ksb0JBQUlULEtBQUtVLE1BQVQsRUFBaUI7QUFDYkQsc0JBQUVrQixjQUFGO0FBQ0EzQix5QkFBSzhCLFFBQUwsQ0FBY3JCLEVBQUVpQixPQUFGLEdBQVlqQixFQUFFaUIsT0FBRixDQUFVLENBQVYsQ0FBWixHQUEyQmpCLENBQXpDO0FBQ0g7QUFDSixhQU5EOztBQVFBLGdCQUFJc0IsYUFBYSxTQUFiQSxVQUFhLENBQVN0QixDQUFULEVBQ2pCO0FBQ0ksb0JBQUlULEtBQUtVLE1BQVQsRUFBaUI7QUFDYkQsc0JBQUVrQixjQUFGO0FBQ0EzQix5QkFBS2dDLFFBQUwsQ0FBY3ZCLEVBQUVpQixPQUFGLEdBQVlqQixFQUFFaUIsT0FBRixDQUFVLENBQVYsQ0FBWixHQUEyQmpCLENBQXpDO0FBQ0g7QUFDSixhQU5EOztBQVFBLGdCQUFJN0MsUUFBSixFQUFjO0FBQ1ZvQyxxQkFBS2xDLEVBQUwsQ0FBUSxDQUFSLEVBQVdtRSxnQkFBWCxDQUE0QixZQUE1QixFQUEwQ2YsWUFBMUMsRUFBd0QsS0FBeEQ7QUFDQXpELHVCQUFPd0UsZ0JBQVAsQ0FBd0IsV0FBeEIsRUFBcUNKLFdBQXJDLEVBQWtELEtBQWxEO0FBQ0FwRSx1QkFBT3dFLGdCQUFQLENBQXdCLFVBQXhCLEVBQW9DRixVQUFwQyxFQUFnRCxLQUFoRDtBQUNBdEUsdUJBQU93RSxnQkFBUCxDQUF3QixhQUF4QixFQUF1Q0YsVUFBdkMsRUFBbUQsS0FBbkQ7QUFDSDs7QUFFRC9CLGlCQUFLbEMsRUFBTCxDQUFRMEMsRUFBUixDQUFXLFdBQVgsRUFBd0JVLFlBQXhCO0FBQ0FsQixpQkFBS0osQ0FBTCxDQUFPWSxFQUFQLENBQVUsV0FBVixFQUF1QnFCLFdBQXZCO0FBQ0E3QixpQkFBS0osQ0FBTCxDQUFPWSxFQUFQLENBQVUsU0FBVixFQUFxQnVCLFVBQXJCO0FBRUgsU0FqRmM7O0FBbUZmRyxtQkFBVyxxQkFDWDtBQUNJLGdCQUFJaEMsSUFBSjtBQUFBLGdCQUNJaUMsUUFBUSxDQURaO0FBQUEsZ0JBRUluQyxPQUFRLElBRlo7QUFHSW9DO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBLGNBQVEsVUFBU0MsS0FBVCxFQUFnQkYsS0FBaEIsRUFDUjtBQUNJLG9CQUFJRyxRQUFRLEVBQVo7QUFBQSxvQkFDSUMsUUFBUUYsTUFBTUcsUUFBTixDQUFleEMsS0FBS0wsT0FBTCxDQUFhakIsWUFBNUIsQ0FEWjtBQUVBNkQsc0JBQU1uQyxJQUFOLENBQVcsWUFDWDtBQUNJLHdCQUFJcUMsS0FBT2pGLEVBQUUsSUFBRixDQUFYO0FBQUEsd0JBQ0lzRCxPQUFPdEQsRUFBRXFDLE1BQUYsQ0FBUyxFQUFULEVBQWE0QyxHQUFHdkMsSUFBSCxFQUFiLENBRFg7QUFBQSx3QkFFSXdDLE1BQU9ELEdBQUdELFFBQUgsQ0FBWXhDLEtBQUtMLE9BQUwsQ0FBYWxCLFlBQXpCLENBRlg7QUFHQSx3QkFBSWlFLElBQUlwQixNQUFSLEVBQWdCO0FBQ1pSLDZCQUFLMEIsUUFBTCxHQUFnQkosS0FBS00sR0FBTCxFQUFVUCxRQUFRLENBQWxCLENBQWhCO0FBQ0g7QUFDREcsMEJBQU1LLElBQU4sQ0FBVzdCLElBQVg7QUFDSCxpQkFURDtBQVVBLHVCQUFPd0IsS0FBUDtBQUNILGFBZkQ7QUFnQkpwQyxtQkFBT2tDLEtBQUtwQyxLQUFLbEMsRUFBTCxDQUFRdUMsSUFBUixDQUFhTCxLQUFLTCxPQUFMLENBQWFsQixZQUExQixFQUF3Q21FLEtBQXhDLEVBQUwsRUFBc0RULEtBQXRELENBQVA7QUFDQSxtQkFBT2pDLElBQVA7QUFDSCxTQTFHYzs7QUE0R2YyQyxtQkFBVyxxQkFDWDtBQUNJLG1CQUFPLEtBQUtYLFNBQUwsRUFBUDtBQUNILFNBL0djOztBQWlIZmpDLGVBQU8saUJBQ1A7QUFDSSxpQkFBSzZDLEtBQUwsR0FBYTtBQUNUQyx5QkFBWSxDQURIO0FBRVRDLHlCQUFZLENBRkg7QUFHVEMsd0JBQVksQ0FISDtBQUlUQyx3QkFBWSxDQUpIO0FBS1RDLHVCQUFZLENBTEg7QUFNVEMsdUJBQVksQ0FOSDtBQU9UQyxzQkFBWSxDQVBIO0FBUVRDLHNCQUFZLENBUkg7QUFTVEMsdUJBQVksQ0FUSDtBQVVUQyx1QkFBWSxDQVZIO0FBV1RDLHVCQUFZLENBWEg7QUFZVEMsc0JBQVksQ0FaSDtBQWFUQyxzQkFBWSxDQWJIO0FBY1RDLDBCQUFZLENBZEg7QUFlVEMsMEJBQVksQ0FmSDtBQWdCVEMseUJBQVksQ0FoQkg7QUFpQlRDLHlCQUFZO0FBakJILGFBQWI7QUFtQkEsaUJBQUt4QyxPQUFMLEdBQWtCLEtBQWxCO0FBQ0EsaUJBQUt5QyxNQUFMLEdBQWtCLEtBQWxCO0FBQ0EsaUJBQUt0RCxNQUFMLEdBQWtCLElBQWxCO0FBQ0EsaUJBQUt1RCxVQUFMLEdBQWtCLElBQWxCO0FBQ0EsaUJBQUtDLFNBQUwsR0FBa0IsQ0FBbEI7QUFDQSxpQkFBS0MsVUFBTCxHQUFrQixLQUFsQjtBQUNBLGlCQUFLQyxPQUFMLEdBQWtCLElBQWxCO0FBQ0gsU0E3SWM7O0FBK0lmbkQsb0JBQVksb0JBQVN3QixFQUFULEVBQ1o7QUFDSUEsZUFBRzRCLFdBQUgsQ0FBZSxLQUFLMUUsT0FBTCxDQUFhWCxjQUE1QjtBQUNBeUQsZUFBR0QsUUFBSCxDQUFZLHdCQUFaLEVBQXNDOEIsSUFBdEM7QUFDQTdCLGVBQUdELFFBQUgsQ0FBWSwwQkFBWixFQUF3QytCLElBQXhDO0FBQ0E5QixlQUFHRCxRQUFILENBQVksS0FBSzdDLE9BQUwsQ0FBYWxCLFlBQXpCLEVBQXVDOEYsSUFBdkM7QUFDSCxTQXJKYzs7QUF1SmZ2RCxzQkFBYyxzQkFBU3lCLEVBQVQsRUFDZDtBQUNJLGdCQUFJK0IsUUFBUS9CLEdBQUdELFFBQUgsQ0FBWSxLQUFLN0MsT0FBTCxDQUFhbEIsWUFBekIsQ0FBWjtBQUNBLGdCQUFJK0YsTUFBTWxELE1BQVYsRUFBa0I7QUFDZG1CLG1CQUFHZ0MsUUFBSCxDQUFZLEtBQUs5RSxPQUFMLENBQWFYLGNBQXpCO0FBQ0F5RCxtQkFBR0QsUUFBSCxDQUFZLDBCQUFaLEVBQXdDOEIsSUFBeEM7QUFDQTdCLG1CQUFHRCxRQUFILENBQVksd0JBQVosRUFBc0MrQixJQUF0QztBQUNBOUIsbUJBQUdELFFBQUgsQ0FBWSxLQUFLN0MsT0FBTCxDQUFhbEIsWUFBekIsRUFBdUM2RixJQUF2QztBQUNIO0FBQ0osU0FoS2M7O0FBa0tmSSxtQkFBVyxxQkFDWDtBQUNJLGdCQUFJMUUsT0FBTyxJQUFYO0FBQ0FBLGlCQUFLbEMsRUFBTCxDQUFRdUMsSUFBUixDQUFhTCxLQUFLTCxPQUFMLENBQWFqQixZQUExQixFQUF3QzBCLElBQXhDLENBQTZDLFlBQVc7QUFDcERKLHFCQUFLaUIsVUFBTCxDQUFnQnpELEVBQUUsSUFBRixDQUFoQjtBQUNILGFBRkQ7QUFHSCxTQXhLYzs7QUEwS2ZtSCxxQkFBYSx1QkFDYjtBQUNJLGdCQUFJM0UsT0FBTyxJQUFYO0FBQ0FBLGlCQUFLbEMsRUFBTCxDQUFRdUMsSUFBUixDQUFhTCxLQUFLTCxPQUFMLENBQWFqQixZQUExQixFQUF3QzBCLElBQXhDLENBQTZDLFlBQVc7QUFDcERKLHFCQUFLZ0IsWUFBTCxDQUFrQnhELEVBQUUsSUFBRixDQUFsQjtBQUNILGFBRkQ7QUFHSCxTQWhMYzs7QUFrTGYrQyxtQkFBVyxtQkFBU2tDLEVBQVQsRUFDWDtBQUNJLGdCQUFJQSxHQUFHRCxRQUFILENBQVksS0FBSzdDLE9BQUwsQ0FBYWxCLFlBQXpCLEVBQXVDNkMsTUFBM0MsRUFBbUQ7QUFDL0NtQixtQkFBR21DLE9BQUgsQ0FBV3BILEVBQUUsS0FBS21DLE9BQUwsQ0FBYVAsYUFBZixDQUFYO0FBQ0FxRCxtQkFBR21DLE9BQUgsQ0FBV3BILEVBQUUsS0FBS21DLE9BQUwsQ0FBYU4sZUFBZixDQUFYO0FBQ0g7QUFDRG9ELGVBQUdELFFBQUgsQ0FBWSx3QkFBWixFQUFzQzhCLElBQXRDO0FBQ0gsU0F6TGM7O0FBMkxmTyxxQkFBYSxxQkFBU3BDLEVBQVQsRUFDYjtBQUNJQSxlQUFHNEIsV0FBSCxDQUFlLEtBQUsxRSxPQUFMLENBQWFYLGNBQTVCO0FBQ0F5RCxlQUFHRCxRQUFILENBQVksZUFBWixFQUE2QnNDLE1BQTdCO0FBQ0FyQyxlQUFHRCxRQUFILENBQVksS0FBSzdDLE9BQUwsQ0FBYWxCLFlBQXpCLEVBQXVDcUcsTUFBdkM7QUFDSCxTQWhNYzs7QUFrTWZsRCxtQkFBVyxtQkFBU25CLENBQVQsRUFDWDtBQUNJLGdCQUFJcUMsUUFBVyxLQUFLQSxLQUFwQjtBQUFBLGdCQUNJbkMsU0FBV25ELEVBQUVpRCxFQUFFRSxNQUFKLENBRGY7QUFBQSxnQkFFSW9FLFdBQVdwRSxPQUFPVSxPQUFQLENBQWUsS0FBSzFCLE9BQUwsQ0FBYWpCLFlBQTVCLENBRmY7O0FBSUEsaUJBQUt5QixPQUFMLENBQWE2RSxHQUFiLENBQWlCLFFBQWpCLEVBQTJCRCxTQUFTRSxNQUFULEVBQTNCOztBQUVBbkMsa0JBQU1DLE9BQU4sR0FBZ0J0QyxFQUFFc0MsT0FBRixLQUFjcEYsU0FBZCxHQUEwQjhDLEVBQUVzQyxPQUE1QixHQUFzQ3RDLEVBQUV5RSxLQUFGLEdBQVV2RSxPQUFPd0UsTUFBUCxHQUFnQkMsSUFBaEY7QUFDQXRDLGtCQUFNRSxPQUFOLEdBQWdCdkMsRUFBRXVDLE9BQUYsS0FBY3JGLFNBQWQsR0FBMEI4QyxFQUFFdUMsT0FBNUIsR0FBc0N2QyxFQUFFNEUsS0FBRixHQUFVMUUsT0FBT3dFLE1BQVAsR0FBZ0JHLEdBQWhGO0FBQ0F4QyxrQkFBTUcsTUFBTixHQUFlSCxNQUFNSyxLQUFOLEdBQWMxQyxFQUFFeUUsS0FBL0I7QUFDQXBDLGtCQUFNSSxNQUFOLEdBQWVKLE1BQU1NLEtBQU4sR0FBYzNDLEVBQUU0RSxLQUEvQjs7QUFFQSxpQkFBS3BCLFVBQUwsR0FBa0IsS0FBS25HLEVBQXZCOztBQUVBLGlCQUFLNEMsTUFBTCxHQUFjbEQsRUFBRUUsU0FBU0ssYUFBVCxDQUF1QixLQUFLNEIsT0FBTCxDQUFhbEIsWUFBcEMsQ0FBRixFQUFxRGdHLFFBQXJELENBQThELEtBQUs5RSxPQUFMLENBQWFmLFNBQWIsR0FBeUIsR0FBekIsR0FBK0IsS0FBS2UsT0FBTCxDQUFhYixTQUExRyxDQUFkO0FBQ0EsaUJBQUs0QixNQUFMLENBQVlzRSxHQUFaLENBQWdCLE9BQWhCLEVBQXlCRCxTQUFTUSxLQUFULEVBQXpCOztBQUVBUixxQkFBU1MsS0FBVCxDQUFlLEtBQUtyRixPQUFwQjtBQUNBNEUscUJBQVMsQ0FBVCxFQUFZVSxVQUFaLENBQXVCbEgsV0FBdkIsQ0FBbUN3RyxTQUFTLENBQVQsQ0FBbkM7QUFDQUEscUJBQVNXLFFBQVQsQ0FBa0IsS0FBS2hGLE1BQXZCOztBQUVBbEQsY0FBRUUsU0FBU2lJLElBQVgsRUFBaUJDLE1BQWpCLENBQXdCLEtBQUtsRixNQUE3QjtBQUNBLGlCQUFLQSxNQUFMLENBQVlzRSxHQUFaLENBQWdCO0FBQ1osd0JBQVN2RSxFQUFFeUUsS0FBRixHQUFVcEMsTUFBTUMsT0FEYjtBQUVaLHVCQUFTdEMsRUFBRTRFLEtBQUYsR0FBVXZDLE1BQU1FO0FBRmIsYUFBaEI7QUFJQTtBQUNBLGdCQUFJNkMsQ0FBSjtBQUFBLGdCQUFPMUQsS0FBUDtBQUFBLGdCQUNJSSxRQUFRLEtBQUs3QixNQUFMLENBQVlMLElBQVosQ0FBaUIsS0FBS1YsT0FBTCxDQUFhakIsWUFBOUIsQ0FEWjtBQUVBLGlCQUFLbUgsSUFBSSxDQUFULEVBQVlBLElBQUl0RCxNQUFNakIsTUFBdEIsRUFBOEJ1RSxHQUE5QixFQUFtQztBQUMvQjFELHdCQUFRM0UsRUFBRStFLE1BQU1zRCxDQUFOLENBQUYsRUFBWUMsT0FBWixDQUFvQixLQUFLbkcsT0FBTCxDQUFhbEIsWUFBakMsRUFBK0M2QyxNQUF2RDtBQUNBLG9CQUFJYSxRQUFRLEtBQUsrQixTQUFqQixFQUE0QjtBQUN4Qix5QkFBS0EsU0FBTCxHQUFpQi9CLEtBQWpCO0FBQ0g7QUFDSjtBQUNKLFNBdE9jOztBQXdPZkgsa0JBQVUsa0JBQVN2QixDQUFULEVBQ1Y7QUFDSSxnQkFBSTNDLEtBQUssS0FBSzRDLE1BQUwsQ0FBWThCLFFBQVosQ0FBcUIsS0FBSzdDLE9BQUwsQ0FBYWpCLFlBQWxDLEVBQWdEa0UsS0FBaEQsRUFBVDtBQUNBOUUsZUFBRyxDQUFILEVBQU0ySCxVQUFOLENBQWlCbEgsV0FBakIsQ0FBNkJULEdBQUcsQ0FBSCxDQUE3QjtBQUNBLGlCQUFLcUMsT0FBTCxDQUFhNEYsV0FBYixDQUF5QmpJLEVBQXpCOztBQUVBLGlCQUFLNEMsTUFBTCxDQUFZb0UsTUFBWjtBQUNBLGlCQUFLaEgsRUFBTCxDQUFRa0ksT0FBUixDQUFnQixRQUFoQjtBQUNBLGdCQUFJLEtBQUs3QixVQUFULEVBQXFCO0FBQ2pCLHFCQUFLRixVQUFMLENBQWdCK0IsT0FBaEIsQ0FBd0IsUUFBeEI7QUFDSDtBQUNELGlCQUFLL0YsS0FBTDtBQUNILFNBcFBjOztBQXNQZjZCLGtCQUFVLGtCQUFTckIsQ0FBVCxFQUNWO0FBQ0ksZ0JBQUlULElBQUo7QUFBQSxnQkFBVWUsTUFBVjtBQUFBLGdCQUFrQmtGLElBQWxCO0FBQUEsZ0JBQXdCQyxJQUF4QjtBQUFBLGdCQUE4Qi9ELEtBQTlCO0FBQUEsZ0JBQ0lnRSxNQUFRLEtBQUt4RyxPQURqQjtBQUFBLGdCQUVJbUQsUUFBUSxLQUFLQSxLQUZqQjs7QUFJQSxpQkFBS3BDLE1BQUwsQ0FBWXNFLEdBQVosQ0FBZ0I7QUFDWix3QkFBU3ZFLEVBQUV5RSxLQUFGLEdBQVVwQyxNQUFNQyxPQURiO0FBRVosdUJBQVN0QyxFQUFFNEUsS0FBRixHQUFVdkMsTUFBTUU7QUFGYixhQUFoQjs7QUFLQTtBQUNBRixrQkFBTUssS0FBTixHQUFjTCxNQUFNTyxJQUFwQjtBQUNBUCxrQkFBTU0sS0FBTixHQUFjTixNQUFNUSxJQUFwQjtBQUNBO0FBQ0FSLGtCQUFNTyxJQUFOLEdBQWM1QyxFQUFFeUUsS0FBaEI7QUFDQXBDLGtCQUFNUSxJQUFOLEdBQWM3QyxFQUFFNEUsS0FBaEI7QUFDQTtBQUNBdkMsa0JBQU1TLEtBQU4sR0FBY1QsTUFBTU8sSUFBTixHQUFhUCxNQUFNSyxLQUFqQztBQUNBTCxrQkFBTVUsS0FBTixHQUFjVixNQUFNUSxJQUFOLEdBQWFSLE1BQU1NLEtBQWpDO0FBQ0E7QUFDQU4sa0JBQU1jLFFBQU4sR0FBaUJkLE1BQU1ZLElBQXZCO0FBQ0FaLGtCQUFNZSxRQUFOLEdBQWlCZixNQUFNYSxJQUF2QjtBQUNBO0FBQ0FiLGtCQUFNWSxJQUFOLEdBQWFaLE1BQU1TLEtBQU4sS0FBZ0IsQ0FBaEIsR0FBb0IsQ0FBcEIsR0FBd0JULE1BQU1TLEtBQU4sR0FBYyxDQUFkLEdBQWtCLENBQWxCLEdBQXNCLENBQUMsQ0FBNUQ7QUFDQVQsa0JBQU1hLElBQU4sR0FBYWIsTUFBTVUsS0FBTixLQUFnQixDQUFoQixHQUFvQixDQUFwQixHQUF3QlYsTUFBTVUsS0FBTixHQUFjLENBQWQsR0FBa0IsQ0FBbEIsR0FBc0IsQ0FBQyxDQUE1RDtBQUNBO0FBQ0EsZ0JBQUk0QyxRQUFVQyxLQUFLQyxHQUFMLENBQVN4RCxNQUFNUyxLQUFmLElBQXdCOEMsS0FBS0MsR0FBTCxDQUFTeEQsTUFBTVUsS0FBZixDQUF4QixHQUFnRCxDQUFoRCxHQUFvRCxDQUFsRTs7QUFFQTtBQUNBLGdCQUFJLENBQUNWLE1BQU1rQixNQUFYLEVBQW1CO0FBQ2ZsQixzQkFBTVcsS0FBTixHQUFlMkMsS0FBZjtBQUNBdEQsc0JBQU1rQixNQUFOLEdBQWUsSUFBZjtBQUNBO0FBQ0g7O0FBRUQ7QUFDQSxnQkFBSWxCLE1BQU1XLEtBQU4sS0FBZ0IyQyxLQUFwQixFQUEyQjtBQUN2QnRELHNCQUFNZ0IsT0FBTixHQUFnQixDQUFoQjtBQUNBaEIsc0JBQU1pQixPQUFOLEdBQWdCLENBQWhCO0FBQ0gsYUFIRCxNQUdPO0FBQ0hqQixzQkFBTWdCLE9BQU4sSUFBaUJ1QyxLQUFLQyxHQUFMLENBQVN4RCxNQUFNUyxLQUFmLENBQWpCO0FBQ0Esb0JBQUlULE1BQU1ZLElBQU4sS0FBZSxDQUFmLElBQW9CWixNQUFNWSxJQUFOLEtBQWVaLE1BQU1jLFFBQTdDLEVBQXVEO0FBQ25EZCwwQkFBTWdCLE9BQU4sR0FBZ0IsQ0FBaEI7QUFDSDtBQUNEaEIsc0JBQU1pQixPQUFOLElBQWlCc0MsS0FBS0MsR0FBTCxDQUFTeEQsTUFBTVUsS0FBZixDQUFqQjtBQUNBLG9CQUFJVixNQUFNYSxJQUFOLEtBQWUsQ0FBZixJQUFvQmIsTUFBTWEsSUFBTixLQUFlYixNQUFNZSxRQUE3QyxFQUF1RDtBQUNuRGYsMEJBQU1pQixPQUFOLEdBQWdCLENBQWhCO0FBQ0g7QUFDSjtBQUNEakIsa0JBQU1XLEtBQU4sR0FBYzJDLEtBQWQ7O0FBRUE7OztBQUdBLGdCQUFJdEQsTUFBTVcsS0FBTixJQUFlWCxNQUFNZ0IsT0FBTixJQUFpQnFDLElBQUkzRyxTQUF4QyxFQUFtRDtBQUMvQztBQUNBc0Qsc0JBQU1nQixPQUFOLEdBQWdCLENBQWhCO0FBQ0FtQyx1QkFBTyxLQUFLOUYsT0FBTCxDQUFhOEYsSUFBYixDQUFrQkUsSUFBSXpILFlBQXRCLENBQVA7QUFDQTtBQUNBLG9CQUFJb0UsTUFBTVMsS0FBTixHQUFjLENBQWQsSUFBbUIwQyxLQUFLM0UsTUFBeEIsSUFBa0MsQ0FBQzJFLEtBQUs3RSxRQUFMLENBQWMrRSxJQUFJbkgsY0FBbEIsQ0FBdkMsRUFBMEU7QUFDdEU7QUFDQWdCLDJCQUFPaUcsS0FBSzVGLElBQUwsQ0FBVThGLElBQUkxSCxZQUFkLEVBQTRCOEgsSUFBNUIsRUFBUDtBQUNBO0FBQ0FwRSw0QkFBUSxLQUFLaEMsT0FBTCxDQUFhMkYsT0FBYixDQUFxQkssSUFBSTFILFlBQXpCLEVBQXVDNkMsTUFBL0M7QUFDQSx3QkFBSWEsUUFBUSxLQUFLK0IsU0FBYixJQUEwQmlDLElBQUk1RyxRQUFsQyxFQUE0QztBQUN4QztBQUNBLDRCQUFJLENBQUNTLEtBQUtzQixNQUFWLEVBQWtCO0FBQ2R0QixtQ0FBT3hDLEVBQUUsTUFBTTJJLElBQUkxSCxZQUFWLEdBQXlCLElBQTNCLEVBQWlDZ0csUUFBakMsQ0FBMEMwQixJQUFJdkgsU0FBOUMsQ0FBUDtBQUNBb0IsaUNBQUs0RixNQUFMLENBQVksS0FBS3pGLE9BQWpCO0FBQ0E4RixpQ0FBS0wsTUFBTCxDQUFZNUYsSUFBWjtBQUNBLGlDQUFLTyxTQUFMLENBQWUwRixJQUFmO0FBQ0gseUJBTEQsTUFLTztBQUNIO0FBQ0FqRyxtQ0FBT2lHLEtBQUt6RCxRQUFMLENBQWMyRCxJQUFJMUgsWUFBbEIsRUFBZ0M4SCxJQUFoQyxFQUFQO0FBQ0F2RyxpQ0FBSzRGLE1BQUwsQ0FBWSxLQUFLekYsT0FBakI7QUFDSDtBQUNKO0FBQ0o7QUFDRDtBQUNBLG9CQUFJMkMsTUFBTVMsS0FBTixHQUFjLENBQWxCLEVBQXFCO0FBQ2pCO0FBQ0EyQywyQkFBTyxLQUFLL0YsT0FBTCxDQUFhK0YsSUFBYixDQUFrQkMsSUFBSXpILFlBQXRCLENBQVA7QUFDQSx3QkFBSSxDQUFDd0gsS0FBSzVFLE1BQVYsRUFBa0I7QUFDZFAsaUNBQVMsS0FBS1osT0FBTCxDQUFhWSxNQUFiLEVBQVQ7QUFDQSw2QkFBS1osT0FBTCxDQUFha0IsT0FBYixDQUFxQjhFLElBQUl6SCxZQUF6QixFQUF1QzhHLEtBQXZDLENBQTZDLEtBQUtyRixPQUFsRDtBQUNBLDRCQUFJLENBQUNZLE9BQU95QixRQUFQLEdBQWtCbEIsTUFBdkIsRUFBK0I7QUFDM0IsaUNBQUt1RCxXQUFMLENBQWlCOUQsT0FBT0EsTUFBUCxFQUFqQjtBQUNIO0FBQ0o7QUFDSjtBQUNKOztBQUVELGdCQUFJeUYsVUFBVSxLQUFkOztBQUVBO0FBQ0EsZ0JBQUksQ0FBQzNJLGdCQUFMLEVBQXVCO0FBQ25CLHFCQUFLNkMsTUFBTCxDQUFZLENBQVosRUFBZXhDLEtBQWYsQ0FBcUJ1SSxVQUFyQixHQUFrQyxRQUFsQztBQUNIO0FBQ0QsaUJBQUtyQyxPQUFMLEdBQWU1RyxFQUFFRSxTQUFTZ0osZ0JBQVQsQ0FBMEJqRyxFQUFFeUUsS0FBRixHQUFVeEgsU0FBU2lJLElBQVQsQ0FBY2dCLFVBQWxELEVBQThEbEcsRUFBRTRFLEtBQUYsSUFBVzVILE9BQU9tSixXQUFQLElBQXNCbEosU0FBU08sZUFBVCxDQUF5QjRJLFNBQTFELENBQTlELENBQUYsQ0FBZjtBQUNBLGdCQUFJLENBQUNoSixnQkFBTCxFQUF1QjtBQUNuQixxQkFBSzZDLE1BQUwsQ0FBWSxDQUFaLEVBQWV4QyxLQUFmLENBQXFCdUksVUFBckIsR0FBa0MsU0FBbEM7QUFDSDtBQUNELGdCQUFJLEtBQUtyQyxPQUFMLENBQWFoRCxRQUFiLENBQXNCK0UsSUFBSXBILFdBQTFCLENBQUosRUFBNEM7QUFDeEMscUJBQUtxRixPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhckQsTUFBYixDQUFvQm9GLElBQUl6SCxZQUF4QixDQUFmO0FBQ0g7QUFDRCxnQkFBSSxLQUFLMEYsT0FBTCxDQUFhaEQsUUFBYixDQUFzQitFLElBQUloSCxVQUExQixDQUFKLEVBQTJDO0FBQ3ZDcUgsMEJBQVUsSUFBVjtBQUNILGFBRkQsTUFHSyxJQUFJLENBQUMsS0FBS3BDLE9BQUwsQ0FBYTlDLE1BQWQsSUFBd0IsQ0FBQyxLQUFLOEMsT0FBTCxDQUFhaEQsUUFBYixDQUFzQitFLElBQUl0SCxTQUExQixDQUE3QixFQUFtRTtBQUNwRTtBQUNIOztBQUVEO0FBQ0EsZ0JBQUlpSSxjQUFjLEtBQUsxQyxPQUFMLENBQWEvQyxPQUFiLENBQXFCLE1BQU04RSxJQUFJeEgsU0FBL0IsQ0FBbEI7QUFBQSxnQkFDSW9JLFlBQWMsS0FBSzlDLFVBQUwsQ0FBZ0IvRCxJQUFoQixDQUFxQixhQUFyQixNQUF3QzRHLFlBQVk1RyxJQUFaLENBQWlCLGFBQWpCLENBRDFEOztBQUdBOzs7QUFHQSxnQkFBSSxDQUFDNEMsTUFBTVcsS0FBUCxJQUFnQnNELFNBQWhCLElBQTZCUCxPQUFqQyxFQUEwQztBQUN0QztBQUNBLG9CQUFJTyxhQUFhWixJQUFJN0csS0FBSixLQUFjd0gsWUFBWTVHLElBQVosQ0FBaUIsZ0JBQWpCLENBQS9CLEVBQW1FO0FBQy9EO0FBQ0g7QUFDRDtBQUNBaUMsd0JBQVEsS0FBSytCLFNBQUwsR0FBaUIsQ0FBakIsR0FBcUIsS0FBS0UsT0FBTCxDQUFhMEIsT0FBYixDQUFxQkssSUFBSTFILFlBQXpCLEVBQXVDNkMsTUFBcEU7QUFDQSxvQkFBSWEsUUFBUWdFLElBQUk1RyxRQUFoQixFQUEwQjtBQUN0QjtBQUNIO0FBQ0Qsb0JBQUl5SCxTQUFTdkcsRUFBRTRFLEtBQUYsR0FBVyxLQUFLakIsT0FBTCxDQUFhZSxNQUFiLEdBQXNCRyxHQUF0QixHQUE0QixLQUFLbEIsT0FBTCxDQUFhYSxNQUFiLEtBQXdCLENBQTVFO0FBQ0lsRSx5QkFBUyxLQUFLWixPQUFMLENBQWFZLE1BQWIsRUFBVDtBQUNKO0FBQ0Esb0JBQUl5RixPQUFKLEVBQWE7QUFDVHhHLDJCQUFPeEMsRUFBRUUsU0FBU0ssYUFBVCxDQUF1Qm9JLElBQUkxSCxZQUEzQixDQUFGLEVBQTRDZ0csUUFBNUMsQ0FBcUQwQixJQUFJdkgsU0FBekQsQ0FBUDtBQUNBb0IseUJBQUs0RixNQUFMLENBQVksS0FBS3pGLE9BQWpCO0FBQ0EseUJBQUtpRSxPQUFMLENBQWEyQixXQUFiLENBQXlCL0YsSUFBekI7QUFDSCxpQkFKRCxNQUtLLElBQUlnSCxNQUFKLEVBQVk7QUFDYix5QkFBSzVDLE9BQUwsQ0FBYTRDLE1BQWIsQ0FBb0IsS0FBSzdHLE9BQXpCO0FBQ0gsaUJBRkksTUFHQTtBQUNELHlCQUFLaUUsT0FBTCxDQUFhb0IsS0FBYixDQUFtQixLQUFLckYsT0FBeEI7QUFDSDtBQUNELG9CQUFJLENBQUNZLE9BQU95QixRQUFQLEdBQWtCbEIsTUFBdkIsRUFBK0I7QUFDM0IseUJBQUt1RCxXQUFMLENBQWlCOUQsT0FBT0EsTUFBUCxFQUFqQjtBQUNIO0FBQ0Qsb0JBQUksQ0FBQyxLQUFLa0QsVUFBTCxDQUFnQjVELElBQWhCLENBQXFCOEYsSUFBSXpILFlBQXpCLEVBQXVDNEMsTUFBNUMsRUFBb0Q7QUFDaEQseUJBQUsyQyxVQUFMLENBQWdCMkIsTUFBaEIsQ0FBdUIsaUJBQWlCTyxJQUFJaEgsVUFBckIsR0FBa0MsS0FBekQ7QUFDSDtBQUNEO0FBQ0Esb0JBQUk0SCxTQUFKLEVBQWU7QUFDWCx5QkFBSzlDLFVBQUwsR0FBa0I2QyxXQUFsQjtBQUNBLHlCQUFLM0MsVUFBTCxHQUFrQixLQUFLckcsRUFBTCxDQUFRLENBQVIsTUFBZSxLQUFLbUcsVUFBTCxDQUFnQixDQUFoQixDQUFqQztBQUNIO0FBQ0o7QUFDSjs7QUFsWmMsS0FBbkI7O0FBc1pBekcsTUFBRXlKLEVBQUYsQ0FBS0MsUUFBTCxHQUFnQixVQUFTQyxNQUFULEVBQ2hCO0FBQ0ksWUFBSTNDLFFBQVMsSUFBYjtBQUFBLFlBQ0k0QyxTQUFTLElBRGI7O0FBR0E1QyxjQUFNcEUsSUFBTixDQUFXLFlBQ1g7QUFDSSxnQkFBSWlILFNBQVM3SixFQUFFLElBQUYsRUFBUTBDLElBQVIsQ0FBYSxVQUFiLENBQWI7O0FBRUEsZ0JBQUksQ0FBQ21ILE1BQUwsRUFBYTtBQUNUN0osa0JBQUUsSUFBRixFQUFRMEMsSUFBUixDQUFhLFVBQWIsRUFBeUIsSUFBSVQsTUFBSixDQUFXLElBQVgsRUFBaUIwSCxNQUFqQixDQUF6QjtBQUNBM0osa0JBQUUsSUFBRixFQUFRMEMsSUFBUixDQUFhLGFBQWIsRUFBNEIsSUFBSW9ILElBQUosR0FBV0MsT0FBWCxFQUE1QjtBQUNILGFBSEQsTUFHTztBQUNILG9CQUFJLE9BQU9KLE1BQVAsS0FBa0IsUUFBbEIsSUFBOEIsT0FBT0UsT0FBT0YsTUFBUCxDQUFQLEtBQTBCLFVBQTVELEVBQXdFO0FBQ3BFQyw2QkFBU0MsT0FBT0YsTUFBUCxHQUFUO0FBQ0g7QUFDSjtBQUNKLFNBWkQ7O0FBY0EsZUFBT0MsVUFBVTVDLEtBQWpCO0FBQ0gsS0FwQkQ7QUFzQkgsQ0EvZEEsRUErZEUsd0NBQWlCL0csT0FBTytKLEtBL2QxQixFQStkaUMvSixNQS9kakMsRUErZHlDQyxRQS9kekMsRSIsImZpbGUiOiIvanMvanF1ZXJ5Lm5lc3RhYmxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiBOZXN0YWJsZSBqUXVlcnkgUGx1Z2luIC0gQ29weXJpZ2h0IChjKSAyMDEyIERhdmlkIEJ1c2hlbGwgLSBodHRwOi8vZGJ1c2hlbGwuY29tL1xuICogRHVhbC1saWNlbnNlZCB1bmRlciB0aGUgQlNEIG9yIE1JVCBsaWNlbnNlc1xuICovXG47KGZ1bmN0aW9uKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZClcbntcbiAgICB2YXIgaGFzVG91Y2ggPSAnb250b3VjaHN0YXJ0JyBpbiBkb2N1bWVudDtcblxuICAgIC8qKlxuICAgICAqIERldGVjdCBDU1MgcG9pbnRlci1ldmVudHMgcHJvcGVydHlcbiAgICAgKiBldmVudHMgYXJlIG5vcm1hbGx5IGRpc2FibGVkIG9uIHRoZSBkcmFnZ2luZyBlbGVtZW50IHRvIGF2b2lkIGNvbmZsaWN0c1xuICAgICAqIGh0dHBzOi8vZ2l0aHViLmNvbS9hdXNpL0ZlYXR1cmUtZGV0ZWN0aW9uLXRlY2huaXF1ZS1mb3ItcG9pbnRlci1ldmVudHMvYmxvYi9tYXN0ZXIvbW9kZXJuaXpyLXBvaW50ZXJldmVudHMuanNcbiAgICAgKi9cbiAgICB2YXIgaGFzUG9pbnRlckV2ZW50cyA9IChmdW5jdGlvbigpXG4gICAge1xuICAgICAgICB2YXIgZWwgICAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKSxcbiAgICAgICAgICAgIGRvY0VsID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xuICAgICAgICBpZiAoISgncG9pbnRlckV2ZW50cycgaW4gZWwuc3R5bGUpKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgZWwuc3R5bGUucG9pbnRlckV2ZW50cyA9ICdhdXRvJztcbiAgICAgICAgZWwuc3R5bGUucG9pbnRlckV2ZW50cyA9ICd4JztcbiAgICAgICAgZG9jRWwuYXBwZW5kQ2hpbGQoZWwpO1xuICAgICAgICB2YXIgc3VwcG9ydHMgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSAmJiB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShlbCwgJycpLnBvaW50ZXJFdmVudHMgPT09ICdhdXRvJztcbiAgICAgICAgZG9jRWwucmVtb3ZlQ2hpbGQoZWwpO1xuICAgICAgICByZXR1cm4gISFzdXBwb3J0cztcbiAgICB9KSgpO1xuXG4gICAgdmFyIGRlZmF1bHRzID0ge1xuICAgICAgICAgICAgbGlzdE5vZGVOYW1lICAgIDogJ29sJyxcbiAgICAgICAgICAgIGl0ZW1Ob2RlTmFtZSAgICA6ICdsaScsXG4gICAgICAgICAgICByb290Q2xhc3MgICAgICAgOiAnZGQnLFxuICAgICAgICAgICAgbGlzdENsYXNzICAgICAgIDogJ2RkLWxpc3QnLFxuICAgICAgICAgICAgaXRlbUNsYXNzICAgICAgIDogJ2RkLWl0ZW0nLFxuICAgICAgICAgICAgZHJhZ0NsYXNzICAgICAgIDogJ2RkLWRyYWdlbCcsXG4gICAgICAgICAgICBoYW5kbGVDbGFzcyAgICAgOiAnZGQtaGFuZGxlJyxcbiAgICAgICAgICAgIGNvbGxhcHNlZENsYXNzICA6ICdkZC1jb2xsYXBzZWQnLFxuICAgICAgICAgICAgcGxhY2VDbGFzcyAgICAgIDogJ2RkLXBsYWNlaG9sZGVyJyxcbiAgICAgICAgICAgIG5vRHJhZ0NsYXNzICAgICA6ICdkZC1ub2RyYWcnLFxuICAgICAgICAgICAgZW1wdHlDbGFzcyAgICAgIDogJ2RkLWVtcHR5JyxcbiAgICAgICAgICAgIGV4cGFuZEJ0bkhUTUwgICA6ICc8YnV0dG9uIGRhdGEtYWN0aW9uPVwiZXhwYW5kXCIgdHlwZT1cImJ1dHRvblwiPkV4cGFuZDwvYnV0dG9uPicsXG4gICAgICAgICAgICBjb2xsYXBzZUJ0bkhUTUwgOiAnPGJ1dHRvbiBkYXRhLWFjdGlvbj1cImNvbGxhcHNlXCIgdHlwZT1cImJ1dHRvblwiPkNvbGxhcHNlPC9idXR0b24+JyxcbiAgICAgICAgICAgIGdyb3VwICAgICAgICAgICA6IDAsXG4gICAgICAgICAgICBtYXhEZXB0aCAgICAgICAgOiA1LFxuICAgICAgICAgICAgdGhyZXNob2xkICAgICAgIDogMjBcbiAgICAgICAgfTtcblxuICAgIGZ1bmN0aW9uIFBsdWdpbihlbGVtZW50LCBvcHRpb25zKVxuICAgIHtcbiAgICAgICAgdGhpcy53ICA9ICQoZG9jdW1lbnQpO1xuICAgICAgICB0aGlzLmVsID0gJChlbGVtZW50KTtcbiAgICAgICAgdGhpcy5vcHRpb25zID0gJC5leHRlbmQoe30sIGRlZmF1bHRzLCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuXG4gICAgUGx1Z2luLnByb3RvdHlwZSA9IHtcblxuICAgICAgICBpbml0OiBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBsaXN0ID0gdGhpcztcblxuICAgICAgICAgICAgbGlzdC5yZXNldCgpO1xuXG4gICAgICAgICAgICBsaXN0LmVsLmRhdGEoJ25lc3RhYmxlLWdyb3VwJywgdGhpcy5vcHRpb25zLmdyb3VwKTtcblxuICAgICAgICAgICAgbGlzdC5wbGFjZUVsID0gJCgnPGRpdiBjbGFzcz1cIicgKyBsaXN0Lm9wdGlvbnMucGxhY2VDbGFzcyArICdcIi8+Jyk7XG5cbiAgICAgICAgICAgICQuZWFjaCh0aGlzLmVsLmZpbmQobGlzdC5vcHRpb25zLml0ZW1Ob2RlTmFtZSksIGZ1bmN0aW9uKGssIGVsKSB7XG4gICAgICAgICAgICAgICAgbGlzdC5zZXRQYXJlbnQoJChlbCkpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGxpc3QuZWwub24oJ2NsaWNrJywgJ2J1dHRvbicsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgICAgICBpZiAobGlzdC5kcmFnRWwpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB2YXIgdGFyZ2V0ID0gJChlLmN1cnJlbnRUYXJnZXQpLFxuICAgICAgICAgICAgICAgICAgICBhY3Rpb24gPSB0YXJnZXQuZGF0YSgnYWN0aW9uJyksXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gICA9IHRhcmdldC5wYXJlbnQobGlzdC5vcHRpb25zLml0ZW1Ob2RlTmFtZSk7XG4gICAgICAgICAgICAgICAgaWYgKGFjdGlvbiA9PT0gJ2NvbGxhcHNlJykge1xuICAgICAgICAgICAgICAgICAgICBsaXN0LmNvbGxhcHNlSXRlbShpdGVtKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGFjdGlvbiA9PT0gJ2V4cGFuZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgbGlzdC5leHBhbmRJdGVtKGl0ZW0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB2YXIgb25TdGFydEV2ZW50ID0gZnVuY3Rpb24oZSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB2YXIgaGFuZGxlID0gJChlLnRhcmdldCk7XG4gICAgICAgICAgICAgICAgaWYgKCFoYW5kbGUuaGFzQ2xhc3MobGlzdC5vcHRpb25zLmhhbmRsZUNsYXNzKSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoaGFuZGxlLmNsb3Nlc3QoJy4nICsgbGlzdC5vcHRpb25zLm5vRHJhZ0NsYXNzKS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBoYW5kbGUgPSBoYW5kbGUuY2xvc2VzdCgnLicgKyBsaXN0Lm9wdGlvbnMuaGFuZGxlQ2xhc3MpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICghaGFuZGxlLmxlbmd0aCB8fCBsaXN0LmRyYWdFbCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgbGlzdC5pc1RvdWNoID0gL150b3VjaC8udGVzdChlLnR5cGUpO1xuICAgICAgICAgICAgICAgIGlmIChsaXN0LmlzVG91Y2ggJiYgZS50b3VjaGVzLmxlbmd0aCAhPT0gMSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIGxpc3QuZHJhZ1N0YXJ0KGUudG91Y2hlcyA/IGUudG91Y2hlc1swXSA6IGUpO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgdmFyIG9uTW92ZUV2ZW50ID0gZnVuY3Rpb24oZSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZiAobGlzdC5kcmFnRWwpIHtcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICBsaXN0LmRyYWdNb3ZlKGUudG91Y2hlcyA/IGUudG91Y2hlc1swXSA6IGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHZhciBvbkVuZEV2ZW50ID0gZnVuY3Rpb24oZSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZiAobGlzdC5kcmFnRWwpIHtcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICBsaXN0LmRyYWdTdG9wKGUudG91Y2hlcyA/IGUudG91Y2hlc1swXSA6IGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIGlmIChoYXNUb3VjaCkge1xuICAgICAgICAgICAgICAgIGxpc3QuZWxbMF0uYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hzdGFydCcsIG9uU3RhcnRFdmVudCwgZmFsc2UpO1xuICAgICAgICAgICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCBvbk1vdmVFdmVudCwgZmFsc2UpO1xuICAgICAgICAgICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCd0b3VjaGVuZCcsIG9uRW5kRXZlbnQsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hjYW5jZWwnLCBvbkVuZEV2ZW50LCBmYWxzZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGxpc3QuZWwub24oJ21vdXNlZG93bicsIG9uU3RhcnRFdmVudCk7XG4gICAgICAgICAgICBsaXN0Lncub24oJ21vdXNlbW92ZScsIG9uTW92ZUV2ZW50KTtcbiAgICAgICAgICAgIGxpc3Qudy5vbignbW91c2V1cCcsIG9uRW5kRXZlbnQpO1xuXG4gICAgICAgIH0sXG5cbiAgICAgICAgc2VyaWFsaXplOiBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBkYXRhLFxuICAgICAgICAgICAgICAgIGRlcHRoID0gMCxcbiAgICAgICAgICAgICAgICBsaXN0ICA9IHRoaXM7XG4gICAgICAgICAgICAgICAgc3RlcCAgPSBmdW5jdGlvbihsZXZlbCwgZGVwdGgpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICB2YXIgYXJyYXkgPSBbIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICBpdGVtcyA9IGxldmVsLmNoaWxkcmVuKGxpc3Qub3B0aW9ucy5pdGVtTm9kZU5hbWUpO1xuICAgICAgICAgICAgICAgICAgICBpdGVtcy5lYWNoKGZ1bmN0aW9uKClcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGxpICAgPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0gPSAkLmV4dGVuZCh7fSwgbGkuZGF0YSgpKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWIgID0gbGkuY2hpbGRyZW4obGlzdC5vcHRpb25zLmxpc3ROb2RlTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3ViLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0uY2hpbGRyZW4gPSBzdGVwKHN1YiwgZGVwdGggKyAxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGFycmF5LnB1c2goaXRlbSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYXJyYXk7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGRhdGEgPSBzdGVwKGxpc3QuZWwuZmluZChsaXN0Lm9wdGlvbnMubGlzdE5vZGVOYW1lKS5maXJzdCgpLCBkZXB0aCk7XG4gICAgICAgICAgICByZXR1cm4gZGF0YTtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXJpYWxpc2U6IGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuc2VyaWFsaXplKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVzZXQ6IGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdGhpcy5tb3VzZSA9IHtcbiAgICAgICAgICAgICAgICBvZmZzZXRYICAgOiAwLFxuICAgICAgICAgICAgICAgIG9mZnNldFkgICA6IDAsXG4gICAgICAgICAgICAgICAgc3RhcnRYICAgIDogMCxcbiAgICAgICAgICAgICAgICBzdGFydFkgICAgOiAwLFxuICAgICAgICAgICAgICAgIGxhc3RYICAgICA6IDAsXG4gICAgICAgICAgICAgICAgbGFzdFkgICAgIDogMCxcbiAgICAgICAgICAgICAgICBub3dYICAgICAgOiAwLFxuICAgICAgICAgICAgICAgIG5vd1kgICAgICA6IDAsXG4gICAgICAgICAgICAgICAgZGlzdFggICAgIDogMCxcbiAgICAgICAgICAgICAgICBkaXN0WSAgICAgOiAwLFxuICAgICAgICAgICAgICAgIGRpckF4ICAgICA6IDAsXG4gICAgICAgICAgICAgICAgZGlyWCAgICAgIDogMCxcbiAgICAgICAgICAgICAgICBkaXJZICAgICAgOiAwLFxuICAgICAgICAgICAgICAgIGxhc3REaXJYICA6IDAsXG4gICAgICAgICAgICAgICAgbGFzdERpclkgIDogMCxcbiAgICAgICAgICAgICAgICBkaXN0QXhYICAgOiAwLFxuICAgICAgICAgICAgICAgIGRpc3RBeFkgICA6IDBcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICB0aGlzLmlzVG91Y2ggICAgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMubW92aW5nICAgICA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5kcmFnRWwgICAgID0gbnVsbDtcbiAgICAgICAgICAgIHRoaXMuZHJhZ1Jvb3RFbCA9IG51bGw7XG4gICAgICAgICAgICB0aGlzLmRyYWdEZXB0aCAgPSAwO1xuICAgICAgICAgICAgdGhpcy5oYXNOZXdSb290ID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLnBvaW50RWwgICAgPSBudWxsO1xuICAgICAgICB9LFxuXG4gICAgICAgIGV4cGFuZEl0ZW06IGZ1bmN0aW9uKGxpKVxuICAgICAgICB7XG4gICAgICAgICAgICBsaS5yZW1vdmVDbGFzcyh0aGlzLm9wdGlvbnMuY29sbGFwc2VkQ2xhc3MpO1xuICAgICAgICAgICAgbGkuY2hpbGRyZW4oJ1tkYXRhLWFjdGlvbj1cImV4cGFuZFwiXScpLmhpZGUoKTtcbiAgICAgICAgICAgIGxpLmNoaWxkcmVuKCdbZGF0YS1hY3Rpb249XCJjb2xsYXBzZVwiXScpLnNob3coKTtcbiAgICAgICAgICAgIGxpLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5saXN0Tm9kZU5hbWUpLnNob3coKTtcbiAgICAgICAgfSxcblxuICAgICAgICBjb2xsYXBzZUl0ZW06IGZ1bmN0aW9uKGxpKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgbGlzdHMgPSBsaS5jaGlsZHJlbih0aGlzLm9wdGlvbnMubGlzdE5vZGVOYW1lKTtcbiAgICAgICAgICAgIGlmIChsaXN0cy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBsaS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuY29sbGFwc2VkQ2xhc3MpO1xuICAgICAgICAgICAgICAgIGxpLmNoaWxkcmVuKCdbZGF0YS1hY3Rpb249XCJjb2xsYXBzZVwiXScpLmhpZGUoKTtcbiAgICAgICAgICAgICAgICBsaS5jaGlsZHJlbignW2RhdGEtYWN0aW9uPVwiZXhwYW5kXCJdJykuc2hvdygpO1xuICAgICAgICAgICAgICAgIGxpLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5saXN0Tm9kZU5hbWUpLmhpZGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBleHBhbmRBbGw6IGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGxpc3QgPSB0aGlzO1xuICAgICAgICAgICAgbGlzdC5lbC5maW5kKGxpc3Qub3B0aW9ucy5pdGVtTm9kZU5hbWUpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgbGlzdC5leHBhbmRJdGVtKCQodGhpcykpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY29sbGFwc2VBbGw6IGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGxpc3QgPSB0aGlzO1xuICAgICAgICAgICAgbGlzdC5lbC5maW5kKGxpc3Qub3B0aW9ucy5pdGVtTm9kZU5hbWUpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgbGlzdC5jb2xsYXBzZUl0ZW0oJCh0aGlzKSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXRQYXJlbnQ6IGZ1bmN0aW9uKGxpKVxuICAgICAgICB7XG4gICAgICAgICAgICBpZiAobGkuY2hpbGRyZW4odGhpcy5vcHRpb25zLmxpc3ROb2RlTmFtZSkubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgbGkucHJlcGVuZCgkKHRoaXMub3B0aW9ucy5leHBhbmRCdG5IVE1MKSk7XG4gICAgICAgICAgICAgICAgbGkucHJlcGVuZCgkKHRoaXMub3B0aW9ucy5jb2xsYXBzZUJ0bkhUTUwpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxpLmNoaWxkcmVuKCdbZGF0YS1hY3Rpb249XCJleHBhbmRcIl0nKS5oaWRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgdW5zZXRQYXJlbnQ6IGZ1bmN0aW9uKGxpKVxuICAgICAgICB7XG4gICAgICAgICAgICBsaS5yZW1vdmVDbGFzcyh0aGlzLm9wdGlvbnMuY29sbGFwc2VkQ2xhc3MpO1xuICAgICAgICAgICAgbGkuY2hpbGRyZW4oJ1tkYXRhLWFjdGlvbl0nKS5yZW1vdmUoKTtcbiAgICAgICAgICAgIGxpLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5saXN0Tm9kZU5hbWUpLnJlbW92ZSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRyYWdTdGFydDogZnVuY3Rpb24oZSlcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIG1vdXNlICAgID0gdGhpcy5tb3VzZSxcbiAgICAgICAgICAgICAgICB0YXJnZXQgICA9ICQoZS50YXJnZXQpLFxuICAgICAgICAgICAgICAgIGRyYWdJdGVtID0gdGFyZ2V0LmNsb3Nlc3QodGhpcy5vcHRpb25zLml0ZW1Ob2RlTmFtZSk7XG5cbiAgICAgICAgICAgIHRoaXMucGxhY2VFbC5jc3MoJ2hlaWdodCcsIGRyYWdJdGVtLmhlaWdodCgpKTtcblxuICAgICAgICAgICAgbW91c2Uub2Zmc2V0WCA9IGUub2Zmc2V0WCAhPT0gdW5kZWZpbmVkID8gZS5vZmZzZXRYIDogZS5wYWdlWCAtIHRhcmdldC5vZmZzZXQoKS5sZWZ0O1xuICAgICAgICAgICAgbW91c2Uub2Zmc2V0WSA9IGUub2Zmc2V0WSAhPT0gdW5kZWZpbmVkID8gZS5vZmZzZXRZIDogZS5wYWdlWSAtIHRhcmdldC5vZmZzZXQoKS50b3A7XG4gICAgICAgICAgICBtb3VzZS5zdGFydFggPSBtb3VzZS5sYXN0WCA9IGUucGFnZVg7XG4gICAgICAgICAgICBtb3VzZS5zdGFydFkgPSBtb3VzZS5sYXN0WSA9IGUucGFnZVk7XG5cbiAgICAgICAgICAgIHRoaXMuZHJhZ1Jvb3RFbCA9IHRoaXMuZWw7XG5cbiAgICAgICAgICAgIHRoaXMuZHJhZ0VsID0gJChkb2N1bWVudC5jcmVhdGVFbGVtZW50KHRoaXMub3B0aW9ucy5saXN0Tm9kZU5hbWUpKS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMubGlzdENsYXNzICsgJyAnICsgdGhpcy5vcHRpb25zLmRyYWdDbGFzcyk7XG4gICAgICAgICAgICB0aGlzLmRyYWdFbC5jc3MoJ3dpZHRoJywgZHJhZ0l0ZW0ud2lkdGgoKSk7XG5cbiAgICAgICAgICAgIGRyYWdJdGVtLmFmdGVyKHRoaXMucGxhY2VFbCk7XG4gICAgICAgICAgICBkcmFnSXRlbVswXS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGRyYWdJdGVtWzBdKTtcbiAgICAgICAgICAgIGRyYWdJdGVtLmFwcGVuZFRvKHRoaXMuZHJhZ0VsKTtcblxuICAgICAgICAgICAgJChkb2N1bWVudC5ib2R5KS5hcHBlbmQodGhpcy5kcmFnRWwpO1xuICAgICAgICAgICAgdGhpcy5kcmFnRWwuY3NzKHtcbiAgICAgICAgICAgICAgICAnbGVmdCcgOiBlLnBhZ2VYIC0gbW91c2Uub2Zmc2V0WCxcbiAgICAgICAgICAgICAgICAndG9wJyAgOiBlLnBhZ2VZIC0gbW91c2Uub2Zmc2V0WVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAvLyB0b3RhbCBkZXB0aCBvZiBkcmFnZ2luZyBpdGVtXG4gICAgICAgICAgICB2YXIgaSwgZGVwdGgsXG4gICAgICAgICAgICAgICAgaXRlbXMgPSB0aGlzLmRyYWdFbC5maW5kKHRoaXMub3B0aW9ucy5pdGVtTm9kZU5hbWUpO1xuICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGl0ZW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgZGVwdGggPSAkKGl0ZW1zW2ldKS5wYXJlbnRzKHRoaXMub3B0aW9ucy5saXN0Tm9kZU5hbWUpLmxlbmd0aDtcbiAgICAgICAgICAgICAgICBpZiAoZGVwdGggPiB0aGlzLmRyYWdEZXB0aCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmRyYWdEZXB0aCA9IGRlcHRoO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBkcmFnU3RvcDogZnVuY3Rpb24oZSlcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGVsID0gdGhpcy5kcmFnRWwuY2hpbGRyZW4odGhpcy5vcHRpb25zLml0ZW1Ob2RlTmFtZSkuZmlyc3QoKTtcbiAgICAgICAgICAgIGVsWzBdLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoZWxbMF0pO1xuICAgICAgICAgICAgdGhpcy5wbGFjZUVsLnJlcGxhY2VXaXRoKGVsKTtcblxuICAgICAgICAgICAgdGhpcy5kcmFnRWwucmVtb3ZlKCk7XG4gICAgICAgICAgICB0aGlzLmVsLnRyaWdnZXIoJ2NoYW5nZScpO1xuICAgICAgICAgICAgaWYgKHRoaXMuaGFzTmV3Um9vdCkge1xuICAgICAgICAgICAgICAgIHRoaXMuZHJhZ1Jvb3RFbC50cmlnZ2VyKCdjaGFuZ2UnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBkcmFnTW92ZTogZnVuY3Rpb24oZSlcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGxpc3QsIHBhcmVudCwgcHJldiwgbmV4dCwgZGVwdGgsXG4gICAgICAgICAgICAgICAgb3B0ICAgPSB0aGlzLm9wdGlvbnMsXG4gICAgICAgICAgICAgICAgbW91c2UgPSB0aGlzLm1vdXNlO1xuXG4gICAgICAgICAgICB0aGlzLmRyYWdFbC5jc3Moe1xuICAgICAgICAgICAgICAgICdsZWZ0JyA6IGUucGFnZVggLSBtb3VzZS5vZmZzZXRYLFxuICAgICAgICAgICAgICAgICd0b3AnICA6IGUucGFnZVkgLSBtb3VzZS5vZmZzZXRZXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgLy8gbW91c2UgcG9zaXRpb24gbGFzdCBldmVudHNcbiAgICAgICAgICAgIG1vdXNlLmxhc3RYID0gbW91c2Uubm93WDtcbiAgICAgICAgICAgIG1vdXNlLmxhc3RZID0gbW91c2Uubm93WTtcbiAgICAgICAgICAgIC8vIG1vdXNlIHBvc2l0aW9uIHRoaXMgZXZlbnRzXG4gICAgICAgICAgICBtb3VzZS5ub3dYICA9IGUucGFnZVg7XG4gICAgICAgICAgICBtb3VzZS5ub3dZICA9IGUucGFnZVk7XG4gICAgICAgICAgICAvLyBkaXN0YW5jZSBtb3VzZSBtb3ZlZCBiZXR3ZWVuIGV2ZW50c1xuICAgICAgICAgICAgbW91c2UuZGlzdFggPSBtb3VzZS5ub3dYIC0gbW91c2UubGFzdFg7XG4gICAgICAgICAgICBtb3VzZS5kaXN0WSA9IG1vdXNlLm5vd1kgLSBtb3VzZS5sYXN0WTtcbiAgICAgICAgICAgIC8vIGRpcmVjdGlvbiBtb3VzZSB3YXMgbW92aW5nXG4gICAgICAgICAgICBtb3VzZS5sYXN0RGlyWCA9IG1vdXNlLmRpclg7XG4gICAgICAgICAgICBtb3VzZS5sYXN0RGlyWSA9IG1vdXNlLmRpclk7XG4gICAgICAgICAgICAvLyBkaXJlY3Rpb24gbW91c2UgaXMgbm93IG1vdmluZyAob24gYm90aCBheGlzKVxuICAgICAgICAgICAgbW91c2UuZGlyWCA9IG1vdXNlLmRpc3RYID09PSAwID8gMCA6IG1vdXNlLmRpc3RYID4gMCA/IDEgOiAtMTtcbiAgICAgICAgICAgIG1vdXNlLmRpclkgPSBtb3VzZS5kaXN0WSA9PT0gMCA/IDAgOiBtb3VzZS5kaXN0WSA+IDAgPyAxIDogLTE7XG4gICAgICAgICAgICAvLyBheGlzIG1vdXNlIGlzIG5vdyBtb3Zpbmcgb25cbiAgICAgICAgICAgIHZhciBuZXdBeCAgID0gTWF0aC5hYnMobW91c2UuZGlzdFgpID4gTWF0aC5hYnMobW91c2UuZGlzdFkpID8gMSA6IDA7XG5cbiAgICAgICAgICAgIC8vIGRvIG5vdGhpbmcgb24gZmlyc3QgbW92ZVxuICAgICAgICAgICAgaWYgKCFtb3VzZS5tb3ZpbmcpIHtcbiAgICAgICAgICAgICAgICBtb3VzZS5kaXJBeCAgPSBuZXdBeDtcbiAgICAgICAgICAgICAgICBtb3VzZS5tb3ZpbmcgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gY2FsYyBkaXN0YW5jZSBtb3ZlZCBvbiB0aGlzIGF4aXMgKGFuZCBkaXJlY3Rpb24pXG4gICAgICAgICAgICBpZiAobW91c2UuZGlyQXggIT09IG5ld0F4KSB7XG4gICAgICAgICAgICAgICAgbW91c2UuZGlzdEF4WCA9IDA7XG4gICAgICAgICAgICAgICAgbW91c2UuZGlzdEF4WSA9IDA7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIG1vdXNlLmRpc3RBeFggKz0gTWF0aC5hYnMobW91c2UuZGlzdFgpO1xuICAgICAgICAgICAgICAgIGlmIChtb3VzZS5kaXJYICE9PSAwICYmIG1vdXNlLmRpclggIT09IG1vdXNlLmxhc3REaXJYKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vdXNlLmRpc3RBeFggPSAwO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBtb3VzZS5kaXN0QXhZICs9IE1hdGguYWJzKG1vdXNlLmRpc3RZKTtcbiAgICAgICAgICAgICAgICBpZiAobW91c2UuZGlyWSAhPT0gMCAmJiBtb3VzZS5kaXJZICE9PSBtb3VzZS5sYXN0RGlyWSkge1xuICAgICAgICAgICAgICAgICAgICBtb3VzZS5kaXN0QXhZID0gMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb3VzZS5kaXJBeCA9IG5ld0F4O1xuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIG1vdmUgaG9yaXpvbnRhbFxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBpZiAobW91c2UuZGlyQXggJiYgbW91c2UuZGlzdEF4WCA+PSBvcHQudGhyZXNob2xkKSB7XG4gICAgICAgICAgICAgICAgLy8gcmVzZXQgbW92ZSBkaXN0YW5jZSBvbiB4LWF4aXMgZm9yIG5ldyBwaGFzZVxuICAgICAgICAgICAgICAgIG1vdXNlLmRpc3RBeFggPSAwO1xuICAgICAgICAgICAgICAgIHByZXYgPSB0aGlzLnBsYWNlRWwucHJldihvcHQuaXRlbU5vZGVOYW1lKTtcbiAgICAgICAgICAgICAgICAvLyBpbmNyZWFzZSBob3Jpem9udGFsIGxldmVsIGlmIHByZXZpb3VzIHNpYmxpbmcgZXhpc3RzIGFuZCBpcyBub3QgY29sbGFwc2VkXG4gICAgICAgICAgICAgICAgaWYgKG1vdXNlLmRpc3RYID4gMCAmJiBwcmV2Lmxlbmd0aCAmJiAhcHJldi5oYXNDbGFzcyhvcHQuY29sbGFwc2VkQ2xhc3MpKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNhbm5vdCBpbmNyZWFzZSBsZXZlbCB3aGVuIGl0ZW0gYWJvdmUgaXMgY29sbGFwc2VkXG4gICAgICAgICAgICAgICAgICAgIGxpc3QgPSBwcmV2LmZpbmQob3B0Lmxpc3ROb2RlTmFtZSkubGFzdCgpO1xuICAgICAgICAgICAgICAgICAgICAvLyBjaGVjayBpZiBkZXB0aCBsaW1pdCBoYXMgcmVhY2hlZFxuICAgICAgICAgICAgICAgICAgICBkZXB0aCA9IHRoaXMucGxhY2VFbC5wYXJlbnRzKG9wdC5saXN0Tm9kZU5hbWUpLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRlcHRoICsgdGhpcy5kcmFnRGVwdGggPD0gb3B0Lm1heERlcHRoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjcmVhdGUgbmV3IHN1Yi1sZXZlbCBpZiBvbmUgZG9lc24ndCBleGlzdFxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFsaXN0Lmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpc3QgPSAkKCc8JyArIG9wdC5saXN0Tm9kZU5hbWUgKyAnLz4nKS5hZGRDbGFzcyhvcHQubGlzdENsYXNzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaXN0LmFwcGVuZCh0aGlzLnBsYWNlRWwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZXYuYXBwZW5kKGxpc3QpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0UGFyZW50KHByZXYpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBlbHNlIGFwcGVuZCB0byBuZXh0IGxldmVsIHVwXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGlzdCA9IHByZXYuY2hpbGRyZW4ob3B0Lmxpc3ROb2RlTmFtZSkubGFzdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpc3QuYXBwZW5kKHRoaXMucGxhY2VFbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy8gZGVjcmVhc2UgaG9yaXpvbnRhbCBsZXZlbFxuICAgICAgICAgICAgICAgIGlmIChtb3VzZS5kaXN0WCA8IDApIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gd2UgY2FuJ3QgZGVjcmVhc2UgYSBsZXZlbCBpZiBhbiBpdGVtIHByZWNlZWRzIHRoZSBjdXJyZW50IG9uZVxuICAgICAgICAgICAgICAgICAgICBuZXh0ID0gdGhpcy5wbGFjZUVsLm5leHQob3B0Lml0ZW1Ob2RlTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgIGlmICghbmV4dC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudCA9IHRoaXMucGxhY2VFbC5wYXJlbnQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucGxhY2VFbC5jbG9zZXN0KG9wdC5pdGVtTm9kZU5hbWUpLmFmdGVyKHRoaXMucGxhY2VFbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXBhcmVudC5jaGlsZHJlbigpLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudW5zZXRQYXJlbnQocGFyZW50LnBhcmVudCgpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIGlzRW1wdHkgPSBmYWxzZTtcblxuICAgICAgICAgICAgLy8gZmluZCBsaXN0IGl0ZW0gdW5kZXIgY3Vyc29yXG4gICAgICAgICAgICBpZiAoIWhhc1BvaW50ZXJFdmVudHMpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmRyYWdFbFswXS5zdHlsZS52aXNpYmlsaXR5ID0gJ2hpZGRlbic7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnBvaW50RWwgPSAkKGRvY3VtZW50LmVsZW1lbnRGcm9tUG9pbnQoZS5wYWdlWCAtIGRvY3VtZW50LmJvZHkuc2Nyb2xsTGVmdCwgZS5wYWdlWSAtICh3aW5kb3cucGFnZVlPZmZzZXQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCkpKTtcbiAgICAgICAgICAgIGlmICghaGFzUG9pbnRlckV2ZW50cykge1xuICAgICAgICAgICAgICAgIHRoaXMuZHJhZ0VsWzBdLnN0eWxlLnZpc2liaWxpdHkgPSAndmlzaWJsZSc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5wb2ludEVsLmhhc0NsYXNzKG9wdC5oYW5kbGVDbGFzcykpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnBvaW50RWwgPSB0aGlzLnBvaW50RWwucGFyZW50KG9wdC5pdGVtTm9kZU5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMucG9pbnRFbC5oYXNDbGFzcyhvcHQuZW1wdHlDbGFzcykpIHtcbiAgICAgICAgICAgICAgICBpc0VtcHR5ID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKCF0aGlzLnBvaW50RWwubGVuZ3RoIHx8ICF0aGlzLnBvaW50RWwuaGFzQ2xhc3Mob3B0Lml0ZW1DbGFzcykpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGZpbmQgcGFyZW50IGxpc3Qgb2YgaXRlbSB1bmRlciBjdXJzb3JcbiAgICAgICAgICAgIHZhciBwb2ludEVsUm9vdCA9IHRoaXMucG9pbnRFbC5jbG9zZXN0KCcuJyArIG9wdC5yb290Q2xhc3MpLFxuICAgICAgICAgICAgICAgIGlzTmV3Um9vdCAgID0gdGhpcy5kcmFnUm9vdEVsLmRhdGEoJ25lc3RhYmxlLWlkJykgIT09IHBvaW50RWxSb290LmRhdGEoJ25lc3RhYmxlLWlkJyk7XG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogbW92ZSB2ZXJ0aWNhbFxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBpZiAoIW1vdXNlLmRpckF4IHx8IGlzTmV3Um9vdCB8fCBpc0VtcHR5KSB7XG4gICAgICAgICAgICAgICAgLy8gY2hlY2sgaWYgZ3JvdXBzIG1hdGNoIGlmIGRyYWdnaW5nIG92ZXIgbmV3IHJvb3RcbiAgICAgICAgICAgICAgICBpZiAoaXNOZXdSb290ICYmIG9wdC5ncm91cCAhPT0gcG9pbnRFbFJvb3QuZGF0YSgnbmVzdGFibGUtZ3JvdXAnKSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIGNoZWNrIGRlcHRoIGxpbWl0XG4gICAgICAgICAgICAgICAgZGVwdGggPSB0aGlzLmRyYWdEZXB0aCAtIDEgKyB0aGlzLnBvaW50RWwucGFyZW50cyhvcHQubGlzdE5vZGVOYW1lKS5sZW5ndGg7XG4gICAgICAgICAgICAgICAgaWYgKGRlcHRoID4gb3B0Lm1heERlcHRoKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIGJlZm9yZSA9IGUucGFnZVkgPCAodGhpcy5wb2ludEVsLm9mZnNldCgpLnRvcCArIHRoaXMucG9pbnRFbC5oZWlnaHQoKSAvIDIpO1xuICAgICAgICAgICAgICAgICAgICBwYXJlbnQgPSB0aGlzLnBsYWNlRWwucGFyZW50KCk7XG4gICAgICAgICAgICAgICAgLy8gaWYgZW1wdHkgY3JlYXRlIG5ldyBsaXN0IHRvIHJlcGxhY2UgZW1wdHkgcGxhY2Vob2xkZXJcbiAgICAgICAgICAgICAgICBpZiAoaXNFbXB0eSkge1xuICAgICAgICAgICAgICAgICAgICBsaXN0ID0gJChkb2N1bWVudC5jcmVhdGVFbGVtZW50KG9wdC5saXN0Tm9kZU5hbWUpKS5hZGRDbGFzcyhvcHQubGlzdENsYXNzKTtcbiAgICAgICAgICAgICAgICAgICAgbGlzdC5hcHBlbmQodGhpcy5wbGFjZUVsKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wb2ludEVsLnJlcGxhY2VXaXRoKGxpc3QpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmIChiZWZvcmUpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wb2ludEVsLmJlZm9yZSh0aGlzLnBsYWNlRWwpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wb2ludEVsLmFmdGVyKHRoaXMucGxhY2VFbCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICghcGFyZW50LmNoaWxkcmVuKCkubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudW5zZXRQYXJlbnQocGFyZW50LnBhcmVudCgpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLmRyYWdSb290RWwuZmluZChvcHQuaXRlbU5vZGVOYW1lKS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kcmFnUm9vdEVsLmFwcGVuZCgnPGRpdiBjbGFzcz1cIicgKyBvcHQuZW1wdHlDbGFzcyArICdcIi8+Jyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIHBhcmVudCByb290IGxpc3QgaGFzIGNoYW5nZWRcbiAgICAgICAgICAgICAgICBpZiAoaXNOZXdSb290KSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZHJhZ1Jvb3RFbCA9IHBvaW50RWxSb290O1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc05ld1Jvb3QgPSB0aGlzLmVsWzBdICE9PSB0aGlzLmRyYWdSb290RWxbMF07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9O1xuXG4gICAgJC5mbi5uZXN0YWJsZSA9IGZ1bmN0aW9uKHBhcmFtcylcbiAgICB7XG4gICAgICAgIHZhciBsaXN0cyAgPSB0aGlzLFxuICAgICAgICAgICAgcmV0dmFsID0gdGhpcztcblxuICAgICAgICBsaXN0cy5lYWNoKGZ1bmN0aW9uKClcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIHBsdWdpbiA9ICQodGhpcykuZGF0YShcIm5lc3RhYmxlXCIpO1xuXG4gICAgICAgICAgICBpZiAoIXBsdWdpbikge1xuICAgICAgICAgICAgICAgICQodGhpcykuZGF0YShcIm5lc3RhYmxlXCIsIG5ldyBQbHVnaW4odGhpcywgcGFyYW1zKSk7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5kYXRhKFwibmVzdGFibGUtaWRcIiwgbmV3IERhdGUoKS5nZXRUaW1lKCkpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHBhcmFtcyA9PT0gJ3N0cmluZycgJiYgdHlwZW9mIHBsdWdpbltwYXJhbXNdID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHZhbCA9IHBsdWdpbltwYXJhbXNdKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gcmV0dmFsIHx8IGxpc3RzO1xuICAgIH07XG5cbn0pKHdpbmRvdy5qUXVlcnkgfHwgd2luZG93LlplcHRvLCB3aW5kb3csIGRvY3VtZW50KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvYmFja29mZmljZS9qcy9saWJyYXJpZXMvanF1ZXJ5Lm5lc3RhYmxlLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==