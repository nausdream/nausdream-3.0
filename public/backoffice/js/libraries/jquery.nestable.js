webpackJsonp([3],{

/***/ "./resources/assets/backoffice/js/libraries/jquery.nestable.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery) {/*!
 * Nestable jQuery Plugin - Copyright (c) 2012 David Bushell - http://dbushell.com/
 * Dual-licensed under the BSD or MIT licenses
 */
;(function ($, window, document, undefined) {
    var hasTouch = 'ontouchstart' in document;

    /**
     * Detect CSS pointer-events property
     * events are normally disabled on the dragging element to avoid conflicts
     * https://github.com/ausi/Feature-detection-technique-for-pointer-events/blob/master/modernizr-pointerevents.js
     */
    var hasPointerEvents = function () {
        var el = document.createElement('div'),
            docEl = document.documentElement;
        if (!('pointerEvents' in el.style)) {
            return false;
        }
        el.style.pointerEvents = 'auto';
        el.style.pointerEvents = 'x';
        docEl.appendChild(el);
        var supports = window.getComputedStyle && window.getComputedStyle(el, '').pointerEvents === 'auto';
        docEl.removeChild(el);
        return !!supports;
    }();

    var defaults = {
        listNodeName: 'ol',
        itemNodeName: 'li',
        rootClass: 'dd',
        listClass: 'dd-list',
        itemClass: 'dd-item',
        dragClass: 'dd-dragel',
        handleClass: 'dd-handle',
        collapsedClass: 'dd-collapsed',
        placeClass: 'dd-placeholder',
        noDragClass: 'dd-nodrag',
        emptyClass: 'dd-empty',
        expandBtnHTML: '<button data-action="expand" type="button">Expand</button>',
        collapseBtnHTML: '<button data-action="collapse" type="button">Collapse</button>',
        group: 0,
        maxDepth: 5,
        threshold: 20
    };

    function Plugin(element, options) {
        this.w = $(document);
        this.el = $(element);
        this.options = $.extend({}, defaults, options);
        this.init();
    }

    Plugin.prototype = {

        init: function init() {
            var list = this;

            list.reset();

            list.el.data('nestable-group', this.options.group);

            list.placeEl = $('<div class="' + list.options.placeClass + '"/>');

            $.each(this.el.find(list.options.itemNodeName), function (k, el) {
                list.setParent($(el));
            });

            list.el.on('click', 'button', function (e) {
                if (list.dragEl) {
                    return;
                }
                var target = $(e.currentTarget),
                    action = target.data('action'),
                    item = target.parent(list.options.itemNodeName);
                if (action === 'collapse') {
                    list.collapseItem(item);
                }
                if (action === 'expand') {
                    list.expandItem(item);
                }
            });

            var onStartEvent = function onStartEvent(e) {
                var handle = $(e.target);
                if (!handle.hasClass(list.options.handleClass)) {
                    if (handle.closest('.' + list.options.noDragClass).length) {
                        return;
                    }
                    handle = handle.closest('.' + list.options.handleClass);
                }

                if (!handle.length || list.dragEl) {
                    return;
                }

                list.isTouch = /^touch/.test(e.type);
                if (list.isTouch && e.touches.length !== 1) {
                    return;
                }

                e.preventDefault();
                list.dragStart(e.touches ? e.touches[0] : e);
            };

            var onMoveEvent = function onMoveEvent(e) {
                if (list.dragEl) {
                    e.preventDefault();
                    list.dragMove(e.touches ? e.touches[0] : e);
                }
            };

            var onEndEvent = function onEndEvent(e) {
                if (list.dragEl) {
                    e.preventDefault();
                    list.dragStop(e.touches ? e.touches[0] : e);
                }
            };

            if (hasTouch) {
                list.el[0].addEventListener('touchstart', onStartEvent, false);
                window.addEventListener('touchmove', onMoveEvent, false);
                window.addEventListener('touchend', onEndEvent, false);
                window.addEventListener('touchcancel', onEndEvent, false);
            }

            list.el.on('mousedown', onStartEvent);
            list.w.on('mousemove', onMoveEvent);
            list.w.on('mouseup', onEndEvent);
        },

        serialize: function serialize() {
            var data,
                depth = 0,
                list = this;
            step = function (_step) {
                function step(_x, _x2) {
                    return _step.apply(this, arguments);
                }

                step.toString = function () {
                    return _step.toString();
                };

                return step;
            }(function (level, depth) {
                var array = [],
                    items = level.children(list.options.itemNodeName);
                items.each(function () {
                    var li = $(this),
                        item = $.extend({}, li.data()),
                        sub = li.children(list.options.listNodeName);
                    if (sub.length) {
                        item.children = step(sub, depth + 1);
                    }
                    array.push(item);
                });
                return array;
            });
            data = step(list.el.find(list.options.listNodeName).first(), depth);
            return data;
        },

        serialise: function serialise() {
            return this.serialize();
        },

        reset: function reset() {
            this.mouse = {
                offsetX: 0,
                offsetY: 0,
                startX: 0,
                startY: 0,
                lastX: 0,
                lastY: 0,
                nowX: 0,
                nowY: 0,
                distX: 0,
                distY: 0,
                dirAx: 0,
                dirX: 0,
                dirY: 0,
                lastDirX: 0,
                lastDirY: 0,
                distAxX: 0,
                distAxY: 0
            };
            this.isTouch = false;
            this.moving = false;
            this.dragEl = null;
            this.dragRootEl = null;
            this.dragDepth = 0;
            this.hasNewRoot = false;
            this.pointEl = null;
        },

        expandItem: function expandItem(li) {
            li.removeClass(this.options.collapsedClass);
            li.children('[data-action="expand"]').hide();
            li.children('[data-action="collapse"]').show();
            li.children(this.options.listNodeName).show();
        },

        collapseItem: function collapseItem(li) {
            var lists = li.children(this.options.listNodeName);
            if (lists.length) {
                li.addClass(this.options.collapsedClass);
                li.children('[data-action="collapse"]').hide();
                li.children('[data-action="expand"]').show();
                li.children(this.options.listNodeName).hide();
            }
        },

        expandAll: function expandAll() {
            var list = this;
            list.el.find(list.options.itemNodeName).each(function () {
                list.expandItem($(this));
            });
        },

        collapseAll: function collapseAll() {
            var list = this;
            list.el.find(list.options.itemNodeName).each(function () {
                list.collapseItem($(this));
            });
        },

        setParent: function setParent(li) {
            if (li.children(this.options.listNodeName).length) {
                li.prepend($(this.options.expandBtnHTML));
                li.prepend($(this.options.collapseBtnHTML));
            }
            li.children('[data-action="expand"]').hide();
        },

        unsetParent: function unsetParent(li) {
            li.removeClass(this.options.collapsedClass);
            li.children('[data-action]').remove();
            li.children(this.options.listNodeName).remove();
        },

        dragStart: function dragStart(e) {
            var mouse = this.mouse,
                target = $(e.target),
                dragItem = target.closest(this.options.itemNodeName);

            this.placeEl.css('height', dragItem.height());

            mouse.offsetX = e.offsetX !== undefined ? e.offsetX : e.pageX - target.offset().left;
            mouse.offsetY = e.offsetY !== undefined ? e.offsetY : e.pageY - target.offset().top;
            mouse.startX = mouse.lastX = e.pageX;
            mouse.startY = mouse.lastY = e.pageY;

            this.dragRootEl = this.el;

            this.dragEl = $(document.createElement(this.options.listNodeName)).addClass(this.options.listClass + ' ' + this.options.dragClass);
            this.dragEl.css('width', dragItem.width());

            dragItem.after(this.placeEl);
            dragItem[0].parentNode.removeChild(dragItem[0]);
            dragItem.appendTo(this.dragEl);

            $(document.body).append(this.dragEl);
            this.dragEl.css({
                'left': e.pageX - mouse.offsetX,
                'top': e.pageY - mouse.offsetY
            });
            // total depth of dragging item
            var i,
                depth,
                items = this.dragEl.find(this.options.itemNodeName);
            for (i = 0; i < items.length; i++) {
                depth = $(items[i]).parents(this.options.listNodeName).length;
                if (depth > this.dragDepth) {
                    this.dragDepth = depth;
                }
            }
        },

        dragStop: function dragStop(e) {
            var el = this.dragEl.children(this.options.itemNodeName).first();
            el[0].parentNode.removeChild(el[0]);
            this.placeEl.replaceWith(el);

            this.dragEl.remove();
            this.el.trigger('change');
            if (this.hasNewRoot) {
                this.dragRootEl.trigger('change');
            }
            this.reset();
        },

        dragMove: function dragMove(e) {
            var list,
                parent,
                prev,
                next,
                depth,
                opt = this.options,
                mouse = this.mouse;

            this.dragEl.css({
                'left': e.pageX - mouse.offsetX,
                'top': e.pageY - mouse.offsetY
            });

            // mouse position last events
            mouse.lastX = mouse.nowX;
            mouse.lastY = mouse.nowY;
            // mouse position this events
            mouse.nowX = e.pageX;
            mouse.nowY = e.pageY;
            // distance mouse moved between events
            mouse.distX = mouse.nowX - mouse.lastX;
            mouse.distY = mouse.nowY - mouse.lastY;
            // direction mouse was moving
            mouse.lastDirX = mouse.dirX;
            mouse.lastDirY = mouse.dirY;
            // direction mouse is now moving (on both axis)
            mouse.dirX = mouse.distX === 0 ? 0 : mouse.distX > 0 ? 1 : -1;
            mouse.dirY = mouse.distY === 0 ? 0 : mouse.distY > 0 ? 1 : -1;
            // axis mouse is now moving on
            var newAx = Math.abs(mouse.distX) > Math.abs(mouse.distY) ? 1 : 0;

            // do nothing on first move
            if (!mouse.moving) {
                mouse.dirAx = newAx;
                mouse.moving = true;
                return;
            }

            // calc distance moved on this axis (and direction)
            if (mouse.dirAx !== newAx) {
                mouse.distAxX = 0;
                mouse.distAxY = 0;
            } else {
                mouse.distAxX += Math.abs(mouse.distX);
                if (mouse.dirX !== 0 && mouse.dirX !== mouse.lastDirX) {
                    mouse.distAxX = 0;
                }
                mouse.distAxY += Math.abs(mouse.distY);
                if (mouse.dirY !== 0 && mouse.dirY !== mouse.lastDirY) {
                    mouse.distAxY = 0;
                }
            }
            mouse.dirAx = newAx;

            /**
             * move horizontal
             */
            if (mouse.dirAx && mouse.distAxX >= opt.threshold) {
                // reset move distance on x-axis for new phase
                mouse.distAxX = 0;
                prev = this.placeEl.prev(opt.itemNodeName);
                // increase horizontal level if previous sibling exists and is not collapsed
                if (mouse.distX > 0 && prev.length && !prev.hasClass(opt.collapsedClass)) {
                    // cannot increase level when item above is collapsed
                    list = prev.find(opt.listNodeName).last();
                    // check if depth limit has reached
                    depth = this.placeEl.parents(opt.listNodeName).length;
                    if (depth + this.dragDepth <= opt.maxDepth) {
                        // create new sub-level if one doesn't exist
                        if (!list.length) {
                            list = $('<' + opt.listNodeName + '/>').addClass(opt.listClass);
                            list.append(this.placeEl);
                            prev.append(list);
                            this.setParent(prev);
                        } else {
                            // else append to next level up
                            list = prev.children(opt.listNodeName).last();
                            list.append(this.placeEl);
                        }
                    }
                }
                // decrease horizontal level
                if (mouse.distX < 0) {
                    // we can't decrease a level if an item preceeds the current one
                    next = this.placeEl.next(opt.itemNodeName);
                    if (!next.length) {
                        parent = this.placeEl.parent();
                        this.placeEl.closest(opt.itemNodeName).after(this.placeEl);
                        if (!parent.children().length) {
                            this.unsetParent(parent.parent());
                        }
                    }
                }
            }

            var isEmpty = false;

            // find list item under cursor
            if (!hasPointerEvents) {
                this.dragEl[0].style.visibility = 'hidden';
            }
            this.pointEl = $(document.elementFromPoint(e.pageX - document.body.scrollLeft, e.pageY - (window.pageYOffset || document.documentElement.scrollTop)));
            if (!hasPointerEvents) {
                this.dragEl[0].style.visibility = 'visible';
            }
            if (this.pointEl.hasClass(opt.handleClass)) {
                this.pointEl = this.pointEl.parent(opt.itemNodeName);
            }
            if (this.pointEl.hasClass(opt.emptyClass)) {
                isEmpty = true;
            } else if (!this.pointEl.length || !this.pointEl.hasClass(opt.itemClass)) {
                return;
            }

            // find parent list of item under cursor
            var pointElRoot = this.pointEl.closest('.' + opt.rootClass),
                isNewRoot = this.dragRootEl.data('nestable-id') !== pointElRoot.data('nestable-id');

            /**
             * move vertical
             */
            if (!mouse.dirAx || isNewRoot || isEmpty) {
                // check if groups match if dragging over new root
                if (isNewRoot && opt.group !== pointElRoot.data('nestable-group')) {
                    return;
                }
                // check depth limit
                depth = this.dragDepth - 1 + this.pointEl.parents(opt.listNodeName).length;
                if (depth > opt.maxDepth) {
                    return;
                }
                var before = e.pageY < this.pointEl.offset().top + this.pointEl.height() / 2;
                parent = this.placeEl.parent();
                // if empty create new list to replace empty placeholder
                if (isEmpty) {
                    list = $(document.createElement(opt.listNodeName)).addClass(opt.listClass);
                    list.append(this.placeEl);
                    this.pointEl.replaceWith(list);
                } else if (before) {
                    this.pointEl.before(this.placeEl);
                } else {
                    this.pointEl.after(this.placeEl);
                }
                if (!parent.children().length) {
                    this.unsetParent(parent.parent());
                }
                if (!this.dragRootEl.find(opt.itemNodeName).length) {
                    this.dragRootEl.append('<div class="' + opt.emptyClass + '"/>');
                }
                // parent root list has changed
                if (isNewRoot) {
                    this.dragRootEl = pointElRoot;
                    this.hasNewRoot = this.el[0] !== this.dragRootEl[0];
                }
            }
        }

    };

    $.fn.nestable = function (params) {
        var lists = this,
            retval = this;

        lists.each(function () {
            var plugin = $(this).data("nestable");

            if (!plugin) {
                $(this).data("nestable", new Plugin(this, params));
                $(this).data("nestable-id", new Date().getTime());
            } else {
                if (typeof params === 'string' && typeof plugin[params] === 'function') {
                    retval = plugin[params]();
                }
            }
        });

        return retval || lists;
    };
})(__webpack_provided_window_dot_jQuery || window.Zepto, window, document);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/backoffice/js/libraries/jquery.nestable.js");


/***/ })

},[2]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2JhY2tvZmZpY2UvanMvbGlicmFyaWVzL2pxdWVyeS5uZXN0YWJsZS5qcyJdLCJuYW1lcyI6WyIkIiwid2luZG93IiwiZG9jdW1lbnQiLCJ1bmRlZmluZWQiLCJoYXNUb3VjaCIsImhhc1BvaW50ZXJFdmVudHMiLCJlbCIsImNyZWF0ZUVsZW1lbnQiLCJkb2NFbCIsImRvY3VtZW50RWxlbWVudCIsInN0eWxlIiwicG9pbnRlckV2ZW50cyIsImFwcGVuZENoaWxkIiwic3VwcG9ydHMiLCJnZXRDb21wdXRlZFN0eWxlIiwicmVtb3ZlQ2hpbGQiLCJkZWZhdWx0cyIsImxpc3ROb2RlTmFtZSIsIml0ZW1Ob2RlTmFtZSIsInJvb3RDbGFzcyIsImxpc3RDbGFzcyIsIml0ZW1DbGFzcyIsImRyYWdDbGFzcyIsImhhbmRsZUNsYXNzIiwiY29sbGFwc2VkQ2xhc3MiLCJwbGFjZUNsYXNzIiwibm9EcmFnQ2xhc3MiLCJlbXB0eUNsYXNzIiwiZXhwYW5kQnRuSFRNTCIsImNvbGxhcHNlQnRuSFRNTCIsImdyb3VwIiwibWF4RGVwdGgiLCJ0aHJlc2hvbGQiLCJQbHVnaW4iLCJlbGVtZW50Iiwib3B0aW9ucyIsInciLCJleHRlbmQiLCJpbml0IiwicHJvdG90eXBlIiwibGlzdCIsInJlc2V0IiwiZGF0YSIsInBsYWNlRWwiLCJlYWNoIiwiZmluZCIsImsiLCJzZXRQYXJlbnQiLCJvbiIsImUiLCJkcmFnRWwiLCJ0YXJnZXQiLCJjdXJyZW50VGFyZ2V0IiwiYWN0aW9uIiwiaXRlbSIsInBhcmVudCIsImNvbGxhcHNlSXRlbSIsImV4cGFuZEl0ZW0iLCJvblN0YXJ0RXZlbnQiLCJoYW5kbGUiLCJoYXNDbGFzcyIsImNsb3Nlc3QiLCJsZW5ndGgiLCJpc1RvdWNoIiwidGVzdCIsInR5cGUiLCJ0b3VjaGVzIiwicHJldmVudERlZmF1bHQiLCJkcmFnU3RhcnQiLCJvbk1vdmVFdmVudCIsImRyYWdNb3ZlIiwib25FbmRFdmVudCIsImRyYWdTdG9wIiwiYWRkRXZlbnRMaXN0ZW5lciIsInNlcmlhbGl6ZSIsImRlcHRoIiwic3RlcCIsImxldmVsIiwiYXJyYXkiLCJpdGVtcyIsImNoaWxkcmVuIiwibGkiLCJzdWIiLCJwdXNoIiwiZmlyc3QiLCJzZXJpYWxpc2UiLCJtb3VzZSIsIm9mZnNldFgiLCJvZmZzZXRZIiwic3RhcnRYIiwic3RhcnRZIiwibGFzdFgiLCJsYXN0WSIsIm5vd1giLCJub3dZIiwiZGlzdFgiLCJkaXN0WSIsImRpckF4IiwiZGlyWCIsImRpclkiLCJsYXN0RGlyWCIsImxhc3REaXJZIiwiZGlzdEF4WCIsImRpc3RBeFkiLCJtb3ZpbmciLCJkcmFnUm9vdEVsIiwiZHJhZ0RlcHRoIiwiaGFzTmV3Um9vdCIsInBvaW50RWwiLCJyZW1vdmVDbGFzcyIsImhpZGUiLCJzaG93IiwibGlzdHMiLCJhZGRDbGFzcyIsImV4cGFuZEFsbCIsImNvbGxhcHNlQWxsIiwicHJlcGVuZCIsInVuc2V0UGFyZW50IiwicmVtb3ZlIiwiZHJhZ0l0ZW0iLCJjc3MiLCJoZWlnaHQiLCJwYWdlWCIsIm9mZnNldCIsImxlZnQiLCJwYWdlWSIsInRvcCIsIndpZHRoIiwiYWZ0ZXIiLCJwYXJlbnROb2RlIiwiYXBwZW5kVG8iLCJib2R5IiwiYXBwZW5kIiwiaSIsInBhcmVudHMiLCJyZXBsYWNlV2l0aCIsInRyaWdnZXIiLCJwcmV2IiwibmV4dCIsIm9wdCIsIm5ld0F4IiwiTWF0aCIsImFicyIsImxhc3QiLCJpc0VtcHR5IiwidmlzaWJpbGl0eSIsImVsZW1lbnRGcm9tUG9pbnQiLCJzY3JvbGxMZWZ0IiwicGFnZVlPZmZzZXQiLCJzY3JvbGxUb3AiLCJwb2ludEVsUm9vdCIsImlzTmV3Um9vdCIsImJlZm9yZSIsImZuIiwibmVzdGFibGUiLCJwYXJhbXMiLCJyZXR2YWwiLCJwbHVnaW4iLCJEYXRlIiwiZ2V0VGltZSIsIlplcHRvIl0sIm1hcHBpbmdzIjoiOzs7OztBQUFBOzs7O0FBSUEsQ0FBQyxDQUFDLFVBQVNBLENBQVQsRUFBWUMsTUFBWixFQUFvQkMsUUFBcEIsRUFBOEJDLFNBQTlCLEVBQ0Y7QUFDSSxRQUFJQyxXQUFXLGtCQUFrQkYsUUFBakM7O0FBRUE7Ozs7O0FBS0EsUUFBSUcsbUJBQW9CLFlBQ3hCO0FBQ0ksWUFBSUMsS0FBUUosU0FBU0ssYUFBVCxDQUF1QixLQUF2QixDQUFaO0FBQUEsWUFDSUMsUUFBUU4sU0FBU08sZUFEckI7QUFFQSxZQUFJLEVBQUUsbUJBQW1CSCxHQUFHSSxLQUF4QixDQUFKLEVBQW9DO0FBQ2hDLG1CQUFPLEtBQVA7QUFDSDtBQUNESixXQUFHSSxLQUFILENBQVNDLGFBQVQsR0FBeUIsTUFBekI7QUFDQUwsV0FBR0ksS0FBSCxDQUFTQyxhQUFULEdBQXlCLEdBQXpCO0FBQ0FILGNBQU1JLFdBQU4sQ0FBa0JOLEVBQWxCO0FBQ0EsWUFBSU8sV0FBV1osT0FBT2EsZ0JBQVAsSUFBMkJiLE9BQU9hLGdCQUFQLENBQXdCUixFQUF4QixFQUE0QixFQUE1QixFQUFnQ0ssYUFBaEMsS0FBa0QsTUFBNUY7QUFDQUgsY0FBTU8sV0FBTixDQUFrQlQsRUFBbEI7QUFDQSxlQUFPLENBQUMsQ0FBQ08sUUFBVDtBQUNILEtBYnNCLEVBQXZCOztBQWVBLFFBQUlHLFdBQVc7QUFDUEMsc0JBQWtCLElBRFg7QUFFUEMsc0JBQWtCLElBRlg7QUFHUEMsbUJBQWtCLElBSFg7QUFJUEMsbUJBQWtCLFNBSlg7QUFLUEMsbUJBQWtCLFNBTFg7QUFNUEMsbUJBQWtCLFdBTlg7QUFPUEMscUJBQWtCLFdBUFg7QUFRUEMsd0JBQWtCLGNBUlg7QUFTUEMsb0JBQWtCLGdCQVRYO0FBVVBDLHFCQUFrQixXQVZYO0FBV1BDLG9CQUFrQixVQVhYO0FBWVBDLHVCQUFrQiw0REFaWDtBQWFQQyx5QkFBa0IsZ0VBYlg7QUFjUEMsZUFBa0IsQ0FkWDtBQWVQQyxrQkFBa0IsQ0FmWDtBQWdCUEMsbUJBQWtCO0FBaEJYLEtBQWY7O0FBbUJBLGFBQVNDLE1BQVQsQ0FBZ0JDLE9BQWhCLEVBQXlCQyxPQUF6QixFQUNBO0FBQ0ksYUFBS0MsQ0FBTCxHQUFVcEMsRUFBRUUsUUFBRixDQUFWO0FBQ0EsYUFBS0ksRUFBTCxHQUFVTixFQUFFa0MsT0FBRixDQUFWO0FBQ0EsYUFBS0MsT0FBTCxHQUFlbkMsRUFBRXFDLE1BQUYsQ0FBUyxFQUFULEVBQWFyQixRQUFiLEVBQXVCbUIsT0FBdkIsQ0FBZjtBQUNBLGFBQUtHLElBQUw7QUFDSDs7QUFFREwsV0FBT00sU0FBUCxHQUFtQjs7QUFFZkQsY0FBTSxnQkFDTjtBQUNJLGdCQUFJRSxPQUFPLElBQVg7O0FBRUFBLGlCQUFLQyxLQUFMOztBQUVBRCxpQkFBS2xDLEVBQUwsQ0FBUW9DLElBQVIsQ0FBYSxnQkFBYixFQUErQixLQUFLUCxPQUFMLENBQWFMLEtBQTVDOztBQUVBVSxpQkFBS0csT0FBTCxHQUFlM0MsRUFBRSxpQkFBaUJ3QyxLQUFLTCxPQUFMLENBQWFWLFVBQTlCLEdBQTJDLEtBQTdDLENBQWY7O0FBRUF6QixjQUFFNEMsSUFBRixDQUFPLEtBQUt0QyxFQUFMLENBQVF1QyxJQUFSLENBQWFMLEtBQUtMLE9BQUwsQ0FBYWpCLFlBQTFCLENBQVAsRUFBZ0QsVUFBUzRCLENBQVQsRUFBWXhDLEVBQVosRUFBZ0I7QUFDNURrQyxxQkFBS08sU0FBTCxDQUFlL0MsRUFBRU0sRUFBRixDQUFmO0FBQ0gsYUFGRDs7QUFJQWtDLGlCQUFLbEMsRUFBTCxDQUFRMEMsRUFBUixDQUFXLE9BQVgsRUFBb0IsUUFBcEIsRUFBOEIsVUFBU0MsQ0FBVCxFQUFZO0FBQ3RDLG9CQUFJVCxLQUFLVSxNQUFULEVBQWlCO0FBQ2I7QUFDSDtBQUNELG9CQUFJQyxTQUFTbkQsRUFBRWlELEVBQUVHLGFBQUosQ0FBYjtBQUFBLG9CQUNJQyxTQUFTRixPQUFPVCxJQUFQLENBQVksUUFBWixDQURiO0FBQUEsb0JBRUlZLE9BQVNILE9BQU9JLE1BQVAsQ0FBY2YsS0FBS0wsT0FBTCxDQUFhakIsWUFBM0IsQ0FGYjtBQUdBLG9CQUFJbUMsV0FBVyxVQUFmLEVBQTJCO0FBQ3ZCYix5QkFBS2dCLFlBQUwsQ0FBa0JGLElBQWxCO0FBQ0g7QUFDRCxvQkFBSUQsV0FBVyxRQUFmLEVBQXlCO0FBQ3JCYix5QkFBS2lCLFVBQUwsQ0FBZ0JILElBQWhCO0FBQ0g7QUFDSixhQWJEOztBQWVBLGdCQUFJSSxlQUFlLFNBQWZBLFlBQWUsQ0FBU1QsQ0FBVCxFQUNuQjtBQUNJLG9CQUFJVSxTQUFTM0QsRUFBRWlELEVBQUVFLE1BQUosQ0FBYjtBQUNBLG9CQUFJLENBQUNRLE9BQU9DLFFBQVAsQ0FBZ0JwQixLQUFLTCxPQUFMLENBQWFaLFdBQTdCLENBQUwsRUFBZ0Q7QUFDNUMsd0JBQUlvQyxPQUFPRSxPQUFQLENBQWUsTUFBTXJCLEtBQUtMLE9BQUwsQ0FBYVQsV0FBbEMsRUFBK0NvQyxNQUFuRCxFQUEyRDtBQUN2RDtBQUNIO0FBQ0RILDZCQUFTQSxPQUFPRSxPQUFQLENBQWUsTUFBTXJCLEtBQUtMLE9BQUwsQ0FBYVosV0FBbEMsQ0FBVDtBQUNIOztBQUVELG9CQUFJLENBQUNvQyxPQUFPRyxNQUFSLElBQWtCdEIsS0FBS1UsTUFBM0IsRUFBbUM7QUFDL0I7QUFDSDs7QUFFRFYscUJBQUt1QixPQUFMLEdBQWUsU0FBU0MsSUFBVCxDQUFjZixFQUFFZ0IsSUFBaEIsQ0FBZjtBQUNBLG9CQUFJekIsS0FBS3VCLE9BQUwsSUFBZ0JkLEVBQUVpQixPQUFGLENBQVVKLE1BQVYsS0FBcUIsQ0FBekMsRUFBNEM7QUFDeEM7QUFDSDs7QUFFRGIsa0JBQUVrQixjQUFGO0FBQ0EzQixxQkFBSzRCLFNBQUwsQ0FBZW5CLEVBQUVpQixPQUFGLEdBQVlqQixFQUFFaUIsT0FBRixDQUFVLENBQVYsQ0FBWixHQUEyQmpCLENBQTFDO0FBQ0gsYUFyQkQ7O0FBdUJBLGdCQUFJb0IsY0FBYyxTQUFkQSxXQUFjLENBQVNwQixDQUFULEVBQ2xCO0FBQ0ksb0JBQUlULEtBQUtVLE1BQVQsRUFBaUI7QUFDYkQsc0JBQUVrQixjQUFGO0FBQ0EzQix5QkFBSzhCLFFBQUwsQ0FBY3JCLEVBQUVpQixPQUFGLEdBQVlqQixFQUFFaUIsT0FBRixDQUFVLENBQVYsQ0FBWixHQUEyQmpCLENBQXpDO0FBQ0g7QUFDSixhQU5EOztBQVFBLGdCQUFJc0IsYUFBYSxTQUFiQSxVQUFhLENBQVN0QixDQUFULEVBQ2pCO0FBQ0ksb0JBQUlULEtBQUtVLE1BQVQsRUFBaUI7QUFDYkQsc0JBQUVrQixjQUFGO0FBQ0EzQix5QkFBS2dDLFFBQUwsQ0FBY3ZCLEVBQUVpQixPQUFGLEdBQVlqQixFQUFFaUIsT0FBRixDQUFVLENBQVYsQ0FBWixHQUEyQmpCLENBQXpDO0FBQ0g7QUFDSixhQU5EOztBQVFBLGdCQUFJN0MsUUFBSixFQUFjO0FBQ1ZvQyxxQkFBS2xDLEVBQUwsQ0FBUSxDQUFSLEVBQVdtRSxnQkFBWCxDQUE0QixZQUE1QixFQUEwQ2YsWUFBMUMsRUFBd0QsS0FBeEQ7QUFDQXpELHVCQUFPd0UsZ0JBQVAsQ0FBd0IsV0FBeEIsRUFBcUNKLFdBQXJDLEVBQWtELEtBQWxEO0FBQ0FwRSx1QkFBT3dFLGdCQUFQLENBQXdCLFVBQXhCLEVBQW9DRixVQUFwQyxFQUFnRCxLQUFoRDtBQUNBdEUsdUJBQU93RSxnQkFBUCxDQUF3QixhQUF4QixFQUF1Q0YsVUFBdkMsRUFBbUQsS0FBbkQ7QUFDSDs7QUFFRC9CLGlCQUFLbEMsRUFBTCxDQUFRMEMsRUFBUixDQUFXLFdBQVgsRUFBd0JVLFlBQXhCO0FBQ0FsQixpQkFBS0osQ0FBTCxDQUFPWSxFQUFQLENBQVUsV0FBVixFQUF1QnFCLFdBQXZCO0FBQ0E3QixpQkFBS0osQ0FBTCxDQUFPWSxFQUFQLENBQVUsU0FBVixFQUFxQnVCLFVBQXJCO0FBRUgsU0FqRmM7O0FBbUZmRyxtQkFBVyxxQkFDWDtBQUNJLGdCQUFJaEMsSUFBSjtBQUFBLGdCQUNJaUMsUUFBUSxDQURaO0FBQUEsZ0JBRUluQyxPQUFRLElBRlo7QUFHSW9DO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBLGNBQVEsVUFBU0MsS0FBVCxFQUFnQkYsS0FBaEIsRUFDUjtBQUNJLG9CQUFJRyxRQUFRLEVBQVo7QUFBQSxvQkFDSUMsUUFBUUYsTUFBTUcsUUFBTixDQUFleEMsS0FBS0wsT0FBTCxDQUFhakIsWUFBNUIsQ0FEWjtBQUVBNkQsc0JBQU1uQyxJQUFOLENBQVcsWUFDWDtBQUNJLHdCQUFJcUMsS0FBT2pGLEVBQUUsSUFBRixDQUFYO0FBQUEsd0JBQ0lzRCxPQUFPdEQsRUFBRXFDLE1BQUYsQ0FBUyxFQUFULEVBQWE0QyxHQUFHdkMsSUFBSCxFQUFiLENBRFg7QUFBQSx3QkFFSXdDLE1BQU9ELEdBQUdELFFBQUgsQ0FBWXhDLEtBQUtMLE9BQUwsQ0FBYWxCLFlBQXpCLENBRlg7QUFHQSx3QkFBSWlFLElBQUlwQixNQUFSLEVBQWdCO0FBQ1pSLDZCQUFLMEIsUUFBTCxHQUFnQkosS0FBS00sR0FBTCxFQUFVUCxRQUFRLENBQWxCLENBQWhCO0FBQ0g7QUFDREcsMEJBQU1LLElBQU4sQ0FBVzdCLElBQVg7QUFDSCxpQkFURDtBQVVBLHVCQUFPd0IsS0FBUDtBQUNILGFBZkQ7QUFnQkpwQyxtQkFBT2tDLEtBQUtwQyxLQUFLbEMsRUFBTCxDQUFRdUMsSUFBUixDQUFhTCxLQUFLTCxPQUFMLENBQWFsQixZQUExQixFQUF3Q21FLEtBQXhDLEVBQUwsRUFBc0RULEtBQXRELENBQVA7QUFDQSxtQkFBT2pDLElBQVA7QUFDSCxTQTFHYzs7QUE0R2YyQyxtQkFBVyxxQkFDWDtBQUNJLG1CQUFPLEtBQUtYLFNBQUwsRUFBUDtBQUNILFNBL0djOztBQWlIZmpDLGVBQU8saUJBQ1A7QUFDSSxpQkFBSzZDLEtBQUwsR0FBYTtBQUNUQyx5QkFBWSxDQURIO0FBRVRDLHlCQUFZLENBRkg7QUFHVEMsd0JBQVksQ0FISDtBQUlUQyx3QkFBWSxDQUpIO0FBS1RDLHVCQUFZLENBTEg7QUFNVEMsdUJBQVksQ0FOSDtBQU9UQyxzQkFBWSxDQVBIO0FBUVRDLHNCQUFZLENBUkg7QUFTVEMsdUJBQVksQ0FUSDtBQVVUQyx1QkFBWSxDQVZIO0FBV1RDLHVCQUFZLENBWEg7QUFZVEMsc0JBQVksQ0FaSDtBQWFUQyxzQkFBWSxDQWJIO0FBY1RDLDBCQUFZLENBZEg7QUFlVEMsMEJBQVksQ0FmSDtBQWdCVEMseUJBQVksQ0FoQkg7QUFpQlRDLHlCQUFZO0FBakJILGFBQWI7QUFtQkEsaUJBQUt4QyxPQUFMLEdBQWtCLEtBQWxCO0FBQ0EsaUJBQUt5QyxNQUFMLEdBQWtCLEtBQWxCO0FBQ0EsaUJBQUt0RCxNQUFMLEdBQWtCLElBQWxCO0FBQ0EsaUJBQUt1RCxVQUFMLEdBQWtCLElBQWxCO0FBQ0EsaUJBQUtDLFNBQUwsR0FBa0IsQ0FBbEI7QUFDQSxpQkFBS0MsVUFBTCxHQUFrQixLQUFsQjtBQUNBLGlCQUFLQyxPQUFMLEdBQWtCLElBQWxCO0FBQ0gsU0E3SWM7O0FBK0lmbkQsb0JBQVksb0JBQVN3QixFQUFULEVBQ1o7QUFDSUEsZUFBRzRCLFdBQUgsQ0FBZSxLQUFLMUUsT0FBTCxDQUFhWCxjQUE1QjtBQUNBeUQsZUFBR0QsUUFBSCxDQUFZLHdCQUFaLEVBQXNDOEIsSUFBdEM7QUFDQTdCLGVBQUdELFFBQUgsQ0FBWSwwQkFBWixFQUF3QytCLElBQXhDO0FBQ0E5QixlQUFHRCxRQUFILENBQVksS0FBSzdDLE9BQUwsQ0FBYWxCLFlBQXpCLEVBQXVDOEYsSUFBdkM7QUFDSCxTQXJKYzs7QUF1SmZ2RCxzQkFBYyxzQkFBU3lCLEVBQVQsRUFDZDtBQUNJLGdCQUFJK0IsUUFBUS9CLEdBQUdELFFBQUgsQ0FBWSxLQUFLN0MsT0FBTCxDQUFhbEIsWUFBekIsQ0FBWjtBQUNBLGdCQUFJK0YsTUFBTWxELE1BQVYsRUFBa0I7QUFDZG1CLG1CQUFHZ0MsUUFBSCxDQUFZLEtBQUs5RSxPQUFMLENBQWFYLGNBQXpCO0FBQ0F5RCxtQkFBR0QsUUFBSCxDQUFZLDBCQUFaLEVBQXdDOEIsSUFBeEM7QUFDQTdCLG1CQUFHRCxRQUFILENBQVksd0JBQVosRUFBc0MrQixJQUF0QztBQUNBOUIsbUJBQUdELFFBQUgsQ0FBWSxLQUFLN0MsT0FBTCxDQUFhbEIsWUFBekIsRUFBdUM2RixJQUF2QztBQUNIO0FBQ0osU0FoS2M7O0FBa0tmSSxtQkFBVyxxQkFDWDtBQUNJLGdCQUFJMUUsT0FBTyxJQUFYO0FBQ0FBLGlCQUFLbEMsRUFBTCxDQUFRdUMsSUFBUixDQUFhTCxLQUFLTCxPQUFMLENBQWFqQixZQUExQixFQUF3QzBCLElBQXhDLENBQTZDLFlBQVc7QUFDcERKLHFCQUFLaUIsVUFBTCxDQUFnQnpELEVBQUUsSUFBRixDQUFoQjtBQUNILGFBRkQ7QUFHSCxTQXhLYzs7QUEwS2ZtSCxxQkFBYSx1QkFDYjtBQUNJLGdCQUFJM0UsT0FBTyxJQUFYO0FBQ0FBLGlCQUFLbEMsRUFBTCxDQUFRdUMsSUFBUixDQUFhTCxLQUFLTCxPQUFMLENBQWFqQixZQUExQixFQUF3QzBCLElBQXhDLENBQTZDLFlBQVc7QUFDcERKLHFCQUFLZ0IsWUFBTCxDQUFrQnhELEVBQUUsSUFBRixDQUFsQjtBQUNILGFBRkQ7QUFHSCxTQWhMYzs7QUFrTGYrQyxtQkFBVyxtQkFBU2tDLEVBQVQsRUFDWDtBQUNJLGdCQUFJQSxHQUFHRCxRQUFILENBQVksS0FBSzdDLE9BQUwsQ0FBYWxCLFlBQXpCLEVBQXVDNkMsTUFBM0MsRUFBbUQ7QUFDL0NtQixtQkFBR21DLE9BQUgsQ0FBV3BILEVBQUUsS0FBS21DLE9BQUwsQ0FBYVAsYUFBZixDQUFYO0FBQ0FxRCxtQkFBR21DLE9BQUgsQ0FBV3BILEVBQUUsS0FBS21DLE9BQUwsQ0FBYU4sZUFBZixDQUFYO0FBQ0g7QUFDRG9ELGVBQUdELFFBQUgsQ0FBWSx3QkFBWixFQUFzQzhCLElBQXRDO0FBQ0gsU0F6TGM7O0FBMkxmTyxxQkFBYSxxQkFBU3BDLEVBQVQsRUFDYjtBQUNJQSxlQUFHNEIsV0FBSCxDQUFlLEtBQUsxRSxPQUFMLENBQWFYLGNBQTVCO0FBQ0F5RCxlQUFHRCxRQUFILENBQVksZUFBWixFQUE2QnNDLE1BQTdCO0FBQ0FyQyxlQUFHRCxRQUFILENBQVksS0FBSzdDLE9BQUwsQ0FBYWxCLFlBQXpCLEVBQXVDcUcsTUFBdkM7QUFDSCxTQWhNYzs7QUFrTWZsRCxtQkFBVyxtQkFBU25CLENBQVQsRUFDWDtBQUNJLGdCQUFJcUMsUUFBVyxLQUFLQSxLQUFwQjtBQUFBLGdCQUNJbkMsU0FBV25ELEVBQUVpRCxFQUFFRSxNQUFKLENBRGY7QUFBQSxnQkFFSW9FLFdBQVdwRSxPQUFPVSxPQUFQLENBQWUsS0FBSzFCLE9BQUwsQ0FBYWpCLFlBQTVCLENBRmY7O0FBSUEsaUJBQUt5QixPQUFMLENBQWE2RSxHQUFiLENBQWlCLFFBQWpCLEVBQTJCRCxTQUFTRSxNQUFULEVBQTNCOztBQUVBbkMsa0JBQU1DLE9BQU4sR0FBZ0J0QyxFQUFFc0MsT0FBRixLQUFjcEYsU0FBZCxHQUEwQjhDLEVBQUVzQyxPQUE1QixHQUFzQ3RDLEVBQUV5RSxLQUFGLEdBQVV2RSxPQUFPd0UsTUFBUCxHQUFnQkMsSUFBaEY7QUFDQXRDLGtCQUFNRSxPQUFOLEdBQWdCdkMsRUFBRXVDLE9BQUYsS0FBY3JGLFNBQWQsR0FBMEI4QyxFQUFFdUMsT0FBNUIsR0FBc0N2QyxFQUFFNEUsS0FBRixHQUFVMUUsT0FBT3dFLE1BQVAsR0FBZ0JHLEdBQWhGO0FBQ0F4QyxrQkFBTUcsTUFBTixHQUFlSCxNQUFNSyxLQUFOLEdBQWMxQyxFQUFFeUUsS0FBL0I7QUFDQXBDLGtCQUFNSSxNQUFOLEdBQWVKLE1BQU1NLEtBQU4sR0FBYzNDLEVBQUU0RSxLQUEvQjs7QUFFQSxpQkFBS3BCLFVBQUwsR0FBa0IsS0FBS25HLEVBQXZCOztBQUVBLGlCQUFLNEMsTUFBTCxHQUFjbEQsRUFBRUUsU0FBU0ssYUFBVCxDQUF1QixLQUFLNEIsT0FBTCxDQUFhbEIsWUFBcEMsQ0FBRixFQUFxRGdHLFFBQXJELENBQThELEtBQUs5RSxPQUFMLENBQWFmLFNBQWIsR0FBeUIsR0FBekIsR0FBK0IsS0FBS2UsT0FBTCxDQUFhYixTQUExRyxDQUFkO0FBQ0EsaUJBQUs0QixNQUFMLENBQVlzRSxHQUFaLENBQWdCLE9BQWhCLEVBQXlCRCxTQUFTUSxLQUFULEVBQXpCOztBQUVBUixxQkFBU1MsS0FBVCxDQUFlLEtBQUtyRixPQUFwQjtBQUNBNEUscUJBQVMsQ0FBVCxFQUFZVSxVQUFaLENBQXVCbEgsV0FBdkIsQ0FBbUN3RyxTQUFTLENBQVQsQ0FBbkM7QUFDQUEscUJBQVNXLFFBQVQsQ0FBa0IsS0FBS2hGLE1BQXZCOztBQUVBbEQsY0FBRUUsU0FBU2lJLElBQVgsRUFBaUJDLE1BQWpCLENBQXdCLEtBQUtsRixNQUE3QjtBQUNBLGlCQUFLQSxNQUFMLENBQVlzRSxHQUFaLENBQWdCO0FBQ1osd0JBQVN2RSxFQUFFeUUsS0FBRixHQUFVcEMsTUFBTUMsT0FEYjtBQUVaLHVCQUFTdEMsRUFBRTRFLEtBQUYsR0FBVXZDLE1BQU1FO0FBRmIsYUFBaEI7QUFJQTtBQUNBLGdCQUFJNkMsQ0FBSjtBQUFBLGdCQUFPMUQsS0FBUDtBQUFBLGdCQUNJSSxRQUFRLEtBQUs3QixNQUFMLENBQVlMLElBQVosQ0FBaUIsS0FBS1YsT0FBTCxDQUFhakIsWUFBOUIsQ0FEWjtBQUVBLGlCQUFLbUgsSUFBSSxDQUFULEVBQVlBLElBQUl0RCxNQUFNakIsTUFBdEIsRUFBOEJ1RSxHQUE5QixFQUFtQztBQUMvQjFELHdCQUFRM0UsRUFBRStFLE1BQU1zRCxDQUFOLENBQUYsRUFBWUMsT0FBWixDQUFvQixLQUFLbkcsT0FBTCxDQUFhbEIsWUFBakMsRUFBK0M2QyxNQUF2RDtBQUNBLG9CQUFJYSxRQUFRLEtBQUsrQixTQUFqQixFQUE0QjtBQUN4Qix5QkFBS0EsU0FBTCxHQUFpQi9CLEtBQWpCO0FBQ0g7QUFDSjtBQUNKLFNBdE9jOztBQXdPZkgsa0JBQVUsa0JBQVN2QixDQUFULEVBQ1Y7QUFDSSxnQkFBSTNDLEtBQUssS0FBSzRDLE1BQUwsQ0FBWThCLFFBQVosQ0FBcUIsS0FBSzdDLE9BQUwsQ0FBYWpCLFlBQWxDLEVBQWdEa0UsS0FBaEQsRUFBVDtBQUNBOUUsZUFBRyxDQUFILEVBQU0ySCxVQUFOLENBQWlCbEgsV0FBakIsQ0FBNkJULEdBQUcsQ0FBSCxDQUE3QjtBQUNBLGlCQUFLcUMsT0FBTCxDQUFhNEYsV0FBYixDQUF5QmpJLEVBQXpCOztBQUVBLGlCQUFLNEMsTUFBTCxDQUFZb0UsTUFBWjtBQUNBLGlCQUFLaEgsRUFBTCxDQUFRa0ksT0FBUixDQUFnQixRQUFoQjtBQUNBLGdCQUFJLEtBQUs3QixVQUFULEVBQXFCO0FBQ2pCLHFCQUFLRixVQUFMLENBQWdCK0IsT0FBaEIsQ0FBd0IsUUFBeEI7QUFDSDtBQUNELGlCQUFLL0YsS0FBTDtBQUNILFNBcFBjOztBQXNQZjZCLGtCQUFVLGtCQUFTckIsQ0FBVCxFQUNWO0FBQ0ksZ0JBQUlULElBQUo7QUFBQSxnQkFBVWUsTUFBVjtBQUFBLGdCQUFrQmtGLElBQWxCO0FBQUEsZ0JBQXdCQyxJQUF4QjtBQUFBLGdCQUE4Qi9ELEtBQTlCO0FBQUEsZ0JBQ0lnRSxNQUFRLEtBQUt4RyxPQURqQjtBQUFBLGdCQUVJbUQsUUFBUSxLQUFLQSxLQUZqQjs7QUFJQSxpQkFBS3BDLE1BQUwsQ0FBWXNFLEdBQVosQ0FBZ0I7QUFDWix3QkFBU3ZFLEVBQUV5RSxLQUFGLEdBQVVwQyxNQUFNQyxPQURiO0FBRVosdUJBQVN0QyxFQUFFNEUsS0FBRixHQUFVdkMsTUFBTUU7QUFGYixhQUFoQjs7QUFLQTtBQUNBRixrQkFBTUssS0FBTixHQUFjTCxNQUFNTyxJQUFwQjtBQUNBUCxrQkFBTU0sS0FBTixHQUFjTixNQUFNUSxJQUFwQjtBQUNBO0FBQ0FSLGtCQUFNTyxJQUFOLEdBQWM1QyxFQUFFeUUsS0FBaEI7QUFDQXBDLGtCQUFNUSxJQUFOLEdBQWM3QyxFQUFFNEUsS0FBaEI7QUFDQTtBQUNBdkMsa0JBQU1TLEtBQU4sR0FBY1QsTUFBTU8sSUFBTixHQUFhUCxNQUFNSyxLQUFqQztBQUNBTCxrQkFBTVUsS0FBTixHQUFjVixNQUFNUSxJQUFOLEdBQWFSLE1BQU1NLEtBQWpDO0FBQ0E7QUFDQU4sa0JBQU1jLFFBQU4sR0FBaUJkLE1BQU1ZLElBQXZCO0FBQ0FaLGtCQUFNZSxRQUFOLEdBQWlCZixNQUFNYSxJQUF2QjtBQUNBO0FBQ0FiLGtCQUFNWSxJQUFOLEdBQWFaLE1BQU1TLEtBQU4sS0FBZ0IsQ0FBaEIsR0FBb0IsQ0FBcEIsR0FBd0JULE1BQU1TLEtBQU4sR0FBYyxDQUFkLEdBQWtCLENBQWxCLEdBQXNCLENBQUMsQ0FBNUQ7QUFDQVQsa0JBQU1hLElBQU4sR0FBYWIsTUFBTVUsS0FBTixLQUFnQixDQUFoQixHQUFvQixDQUFwQixHQUF3QlYsTUFBTVUsS0FBTixHQUFjLENBQWQsR0FBa0IsQ0FBbEIsR0FBc0IsQ0FBQyxDQUE1RDtBQUNBO0FBQ0EsZ0JBQUk0QyxRQUFVQyxLQUFLQyxHQUFMLENBQVN4RCxNQUFNUyxLQUFmLElBQXdCOEMsS0FBS0MsR0FBTCxDQUFTeEQsTUFBTVUsS0FBZixDQUF4QixHQUFnRCxDQUFoRCxHQUFvRCxDQUFsRTs7QUFFQTtBQUNBLGdCQUFJLENBQUNWLE1BQU1rQixNQUFYLEVBQW1CO0FBQ2ZsQixzQkFBTVcsS0FBTixHQUFlMkMsS0FBZjtBQUNBdEQsc0JBQU1rQixNQUFOLEdBQWUsSUFBZjtBQUNBO0FBQ0g7O0FBRUQ7QUFDQSxnQkFBSWxCLE1BQU1XLEtBQU4sS0FBZ0IyQyxLQUFwQixFQUEyQjtBQUN2QnRELHNCQUFNZ0IsT0FBTixHQUFnQixDQUFoQjtBQUNBaEIsc0JBQU1pQixPQUFOLEdBQWdCLENBQWhCO0FBQ0gsYUFIRCxNQUdPO0FBQ0hqQixzQkFBTWdCLE9BQU4sSUFBaUJ1QyxLQUFLQyxHQUFMLENBQVN4RCxNQUFNUyxLQUFmLENBQWpCO0FBQ0Esb0JBQUlULE1BQU1ZLElBQU4sS0FBZSxDQUFmLElBQW9CWixNQUFNWSxJQUFOLEtBQWVaLE1BQU1jLFFBQTdDLEVBQXVEO0FBQ25EZCwwQkFBTWdCLE9BQU4sR0FBZ0IsQ0FBaEI7QUFDSDtBQUNEaEIsc0JBQU1pQixPQUFOLElBQWlCc0MsS0FBS0MsR0FBTCxDQUFTeEQsTUFBTVUsS0FBZixDQUFqQjtBQUNBLG9CQUFJVixNQUFNYSxJQUFOLEtBQWUsQ0FBZixJQUFvQmIsTUFBTWEsSUFBTixLQUFlYixNQUFNZSxRQUE3QyxFQUF1RDtBQUNuRGYsMEJBQU1pQixPQUFOLEdBQWdCLENBQWhCO0FBQ0g7QUFDSjtBQUNEakIsa0JBQU1XLEtBQU4sR0FBYzJDLEtBQWQ7O0FBRUE7OztBQUdBLGdCQUFJdEQsTUFBTVcsS0FBTixJQUFlWCxNQUFNZ0IsT0FBTixJQUFpQnFDLElBQUkzRyxTQUF4QyxFQUFtRDtBQUMvQztBQUNBc0Qsc0JBQU1nQixPQUFOLEdBQWdCLENBQWhCO0FBQ0FtQyx1QkFBTyxLQUFLOUYsT0FBTCxDQUFhOEYsSUFBYixDQUFrQkUsSUFBSXpILFlBQXRCLENBQVA7QUFDQTtBQUNBLG9CQUFJb0UsTUFBTVMsS0FBTixHQUFjLENBQWQsSUFBbUIwQyxLQUFLM0UsTUFBeEIsSUFBa0MsQ0FBQzJFLEtBQUs3RSxRQUFMLENBQWMrRSxJQUFJbkgsY0FBbEIsQ0FBdkMsRUFBMEU7QUFDdEU7QUFDQWdCLDJCQUFPaUcsS0FBSzVGLElBQUwsQ0FBVThGLElBQUkxSCxZQUFkLEVBQTRCOEgsSUFBNUIsRUFBUDtBQUNBO0FBQ0FwRSw0QkFBUSxLQUFLaEMsT0FBTCxDQUFhMkYsT0FBYixDQUFxQkssSUFBSTFILFlBQXpCLEVBQXVDNkMsTUFBL0M7QUFDQSx3QkFBSWEsUUFBUSxLQUFLK0IsU0FBYixJQUEwQmlDLElBQUk1RyxRQUFsQyxFQUE0QztBQUN4QztBQUNBLDRCQUFJLENBQUNTLEtBQUtzQixNQUFWLEVBQWtCO0FBQ2R0QixtQ0FBT3hDLEVBQUUsTUFBTTJJLElBQUkxSCxZQUFWLEdBQXlCLElBQTNCLEVBQWlDZ0csUUFBakMsQ0FBMEMwQixJQUFJdkgsU0FBOUMsQ0FBUDtBQUNBb0IsaUNBQUs0RixNQUFMLENBQVksS0FBS3pGLE9BQWpCO0FBQ0E4RixpQ0FBS0wsTUFBTCxDQUFZNUYsSUFBWjtBQUNBLGlDQUFLTyxTQUFMLENBQWUwRixJQUFmO0FBQ0gseUJBTEQsTUFLTztBQUNIO0FBQ0FqRyxtQ0FBT2lHLEtBQUt6RCxRQUFMLENBQWMyRCxJQUFJMUgsWUFBbEIsRUFBZ0M4SCxJQUFoQyxFQUFQO0FBQ0F2RyxpQ0FBSzRGLE1BQUwsQ0FBWSxLQUFLekYsT0FBakI7QUFDSDtBQUNKO0FBQ0o7QUFDRDtBQUNBLG9CQUFJMkMsTUFBTVMsS0FBTixHQUFjLENBQWxCLEVBQXFCO0FBQ2pCO0FBQ0EyQywyQkFBTyxLQUFLL0YsT0FBTCxDQUFhK0YsSUFBYixDQUFrQkMsSUFBSXpILFlBQXRCLENBQVA7QUFDQSx3QkFBSSxDQUFDd0gsS0FBSzVFLE1BQVYsRUFBa0I7QUFDZFAsaUNBQVMsS0FBS1osT0FBTCxDQUFhWSxNQUFiLEVBQVQ7QUFDQSw2QkFBS1osT0FBTCxDQUFha0IsT0FBYixDQUFxQjhFLElBQUl6SCxZQUF6QixFQUF1QzhHLEtBQXZDLENBQTZDLEtBQUtyRixPQUFsRDtBQUNBLDRCQUFJLENBQUNZLE9BQU95QixRQUFQLEdBQWtCbEIsTUFBdkIsRUFBK0I7QUFDM0IsaUNBQUt1RCxXQUFMLENBQWlCOUQsT0FBT0EsTUFBUCxFQUFqQjtBQUNIO0FBQ0o7QUFDSjtBQUNKOztBQUVELGdCQUFJeUYsVUFBVSxLQUFkOztBQUVBO0FBQ0EsZ0JBQUksQ0FBQzNJLGdCQUFMLEVBQXVCO0FBQ25CLHFCQUFLNkMsTUFBTCxDQUFZLENBQVosRUFBZXhDLEtBQWYsQ0FBcUJ1SSxVQUFyQixHQUFrQyxRQUFsQztBQUNIO0FBQ0QsaUJBQUtyQyxPQUFMLEdBQWU1RyxFQUFFRSxTQUFTZ0osZ0JBQVQsQ0FBMEJqRyxFQUFFeUUsS0FBRixHQUFVeEgsU0FBU2lJLElBQVQsQ0FBY2dCLFVBQWxELEVBQThEbEcsRUFBRTRFLEtBQUYsSUFBVzVILE9BQU9tSixXQUFQLElBQXNCbEosU0FBU08sZUFBVCxDQUF5QjRJLFNBQTFELENBQTlELENBQUYsQ0FBZjtBQUNBLGdCQUFJLENBQUNoSixnQkFBTCxFQUF1QjtBQUNuQixxQkFBSzZDLE1BQUwsQ0FBWSxDQUFaLEVBQWV4QyxLQUFmLENBQXFCdUksVUFBckIsR0FBa0MsU0FBbEM7QUFDSDtBQUNELGdCQUFJLEtBQUtyQyxPQUFMLENBQWFoRCxRQUFiLENBQXNCK0UsSUFBSXBILFdBQTFCLENBQUosRUFBNEM7QUFDeEMscUJBQUtxRixPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhckQsTUFBYixDQUFvQm9GLElBQUl6SCxZQUF4QixDQUFmO0FBQ0g7QUFDRCxnQkFBSSxLQUFLMEYsT0FBTCxDQUFhaEQsUUFBYixDQUFzQitFLElBQUloSCxVQUExQixDQUFKLEVBQTJDO0FBQ3ZDcUgsMEJBQVUsSUFBVjtBQUNILGFBRkQsTUFHSyxJQUFJLENBQUMsS0FBS3BDLE9BQUwsQ0FBYTlDLE1BQWQsSUFBd0IsQ0FBQyxLQUFLOEMsT0FBTCxDQUFhaEQsUUFBYixDQUFzQitFLElBQUl0SCxTQUExQixDQUE3QixFQUFtRTtBQUNwRTtBQUNIOztBQUVEO0FBQ0EsZ0JBQUlpSSxjQUFjLEtBQUsxQyxPQUFMLENBQWEvQyxPQUFiLENBQXFCLE1BQU04RSxJQUFJeEgsU0FBL0IsQ0FBbEI7QUFBQSxnQkFDSW9JLFlBQWMsS0FBSzlDLFVBQUwsQ0FBZ0IvRCxJQUFoQixDQUFxQixhQUFyQixNQUF3QzRHLFlBQVk1RyxJQUFaLENBQWlCLGFBQWpCLENBRDFEOztBQUdBOzs7QUFHQSxnQkFBSSxDQUFDNEMsTUFBTVcsS0FBUCxJQUFnQnNELFNBQWhCLElBQTZCUCxPQUFqQyxFQUEwQztBQUN0QztBQUNBLG9CQUFJTyxhQUFhWixJQUFJN0csS0FBSixLQUFjd0gsWUFBWTVHLElBQVosQ0FBaUIsZ0JBQWpCLENBQS9CLEVBQW1FO0FBQy9EO0FBQ0g7QUFDRDtBQUNBaUMsd0JBQVEsS0FBSytCLFNBQUwsR0FBaUIsQ0FBakIsR0FBcUIsS0FBS0UsT0FBTCxDQUFhMEIsT0FBYixDQUFxQkssSUFBSTFILFlBQXpCLEVBQXVDNkMsTUFBcEU7QUFDQSxvQkFBSWEsUUFBUWdFLElBQUk1RyxRQUFoQixFQUEwQjtBQUN0QjtBQUNIO0FBQ0Qsb0JBQUl5SCxTQUFTdkcsRUFBRTRFLEtBQUYsR0FBVyxLQUFLakIsT0FBTCxDQUFhZSxNQUFiLEdBQXNCRyxHQUF0QixHQUE0QixLQUFLbEIsT0FBTCxDQUFhYSxNQUFiLEtBQXdCLENBQTVFO0FBQ0lsRSx5QkFBUyxLQUFLWixPQUFMLENBQWFZLE1BQWIsRUFBVDtBQUNKO0FBQ0Esb0JBQUl5RixPQUFKLEVBQWE7QUFDVHhHLDJCQUFPeEMsRUFBRUUsU0FBU0ssYUFBVCxDQUF1Qm9JLElBQUkxSCxZQUEzQixDQUFGLEVBQTRDZ0csUUFBNUMsQ0FBcUQwQixJQUFJdkgsU0FBekQsQ0FBUDtBQUNBb0IseUJBQUs0RixNQUFMLENBQVksS0FBS3pGLE9BQWpCO0FBQ0EseUJBQUtpRSxPQUFMLENBQWEyQixXQUFiLENBQXlCL0YsSUFBekI7QUFDSCxpQkFKRCxNQUtLLElBQUlnSCxNQUFKLEVBQVk7QUFDYix5QkFBSzVDLE9BQUwsQ0FBYTRDLE1BQWIsQ0FBb0IsS0FBSzdHLE9BQXpCO0FBQ0gsaUJBRkksTUFHQTtBQUNELHlCQUFLaUUsT0FBTCxDQUFhb0IsS0FBYixDQUFtQixLQUFLckYsT0FBeEI7QUFDSDtBQUNELG9CQUFJLENBQUNZLE9BQU95QixRQUFQLEdBQWtCbEIsTUFBdkIsRUFBK0I7QUFDM0IseUJBQUt1RCxXQUFMLENBQWlCOUQsT0FBT0EsTUFBUCxFQUFqQjtBQUNIO0FBQ0Qsb0JBQUksQ0FBQyxLQUFLa0QsVUFBTCxDQUFnQjVELElBQWhCLENBQXFCOEYsSUFBSXpILFlBQXpCLEVBQXVDNEMsTUFBNUMsRUFBb0Q7QUFDaEQseUJBQUsyQyxVQUFMLENBQWdCMkIsTUFBaEIsQ0FBdUIsaUJBQWlCTyxJQUFJaEgsVUFBckIsR0FBa0MsS0FBekQ7QUFDSDtBQUNEO0FBQ0Esb0JBQUk0SCxTQUFKLEVBQWU7QUFDWCx5QkFBSzlDLFVBQUwsR0FBa0I2QyxXQUFsQjtBQUNBLHlCQUFLM0MsVUFBTCxHQUFrQixLQUFLckcsRUFBTCxDQUFRLENBQVIsTUFBZSxLQUFLbUcsVUFBTCxDQUFnQixDQUFoQixDQUFqQztBQUNIO0FBQ0o7QUFDSjs7QUFsWmMsS0FBbkI7O0FBc1pBekcsTUFBRXlKLEVBQUYsQ0FBS0MsUUFBTCxHQUFnQixVQUFTQyxNQUFULEVBQ2hCO0FBQ0ksWUFBSTNDLFFBQVMsSUFBYjtBQUFBLFlBQ0k0QyxTQUFTLElBRGI7O0FBR0E1QyxjQUFNcEUsSUFBTixDQUFXLFlBQ1g7QUFDSSxnQkFBSWlILFNBQVM3SixFQUFFLElBQUYsRUFBUTBDLElBQVIsQ0FBYSxVQUFiLENBQWI7O0FBRUEsZ0JBQUksQ0FBQ21ILE1BQUwsRUFBYTtBQUNUN0osa0JBQUUsSUFBRixFQUFRMEMsSUFBUixDQUFhLFVBQWIsRUFBeUIsSUFBSVQsTUFBSixDQUFXLElBQVgsRUFBaUIwSCxNQUFqQixDQUF6QjtBQUNBM0osa0JBQUUsSUFBRixFQUFRMEMsSUFBUixDQUFhLGFBQWIsRUFBNEIsSUFBSW9ILElBQUosR0FBV0MsT0FBWCxFQUE1QjtBQUNILGFBSEQsTUFHTztBQUNILG9CQUFJLE9BQU9KLE1BQVAsS0FBa0IsUUFBbEIsSUFBOEIsT0FBT0UsT0FBT0YsTUFBUCxDQUFQLEtBQTBCLFVBQTVELEVBQXdFO0FBQ3BFQyw2QkFBU0MsT0FBT0YsTUFBUCxHQUFUO0FBQ0g7QUFDSjtBQUNKLFNBWkQ7O0FBY0EsZUFBT0MsVUFBVTVDLEtBQWpCO0FBQ0gsS0FwQkQ7QUFzQkgsQ0EvZEEsRUErZEUsd0NBQWlCL0csT0FBTytKLEtBL2QxQixFQStkaUMvSixNQS9kakMsRUErZHlDQyxRQS9kekMsRSIsImZpbGUiOiIvanMvbGlicmFyaWVzL2pxdWVyeS5uZXN0YWJsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogTmVzdGFibGUgalF1ZXJ5IFBsdWdpbiAtIENvcHlyaWdodCAoYykgMjAxMiBEYXZpZCBCdXNoZWxsIC0gaHR0cDovL2RidXNoZWxsLmNvbS9cbiAqIER1YWwtbGljZW5zZWQgdW5kZXIgdGhlIEJTRCBvciBNSVQgbGljZW5zZXNcbiAqL1xuOyhmdW5jdGlvbigkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpXG57XG4gICAgdmFyIGhhc1RvdWNoID0gJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQ7XG5cbiAgICAvKipcbiAgICAgKiBEZXRlY3QgQ1NTIHBvaW50ZXItZXZlbnRzIHByb3BlcnR5XG4gICAgICogZXZlbnRzIGFyZSBub3JtYWxseSBkaXNhYmxlZCBvbiB0aGUgZHJhZ2dpbmcgZWxlbWVudCB0byBhdm9pZCBjb25mbGljdHNcbiAgICAgKiBodHRwczovL2dpdGh1Yi5jb20vYXVzaS9GZWF0dXJlLWRldGVjdGlvbi10ZWNobmlxdWUtZm9yLXBvaW50ZXItZXZlbnRzL2Jsb2IvbWFzdGVyL21vZGVybml6ci1wb2ludGVyZXZlbnRzLmpzXG4gICAgICovXG4gICAgdmFyIGhhc1BvaW50ZXJFdmVudHMgPSAoZnVuY3Rpb24oKVxuICAgIHtcbiAgICAgICAgdmFyIGVsICAgID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JyksXG4gICAgICAgICAgICBkb2NFbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcbiAgICAgICAgaWYgKCEoJ3BvaW50ZXJFdmVudHMnIGluIGVsLnN0eWxlKSkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGVsLnN0eWxlLnBvaW50ZXJFdmVudHMgPSAnYXV0byc7XG4gICAgICAgIGVsLnN0eWxlLnBvaW50ZXJFdmVudHMgPSAneCc7XG4gICAgICAgIGRvY0VsLmFwcGVuZENoaWxkKGVsKTtcbiAgICAgICAgdmFyIHN1cHBvcnRzID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUgJiYgd2luZG93LmdldENvbXB1dGVkU3R5bGUoZWwsICcnKS5wb2ludGVyRXZlbnRzID09PSAnYXV0byc7XG4gICAgICAgIGRvY0VsLnJlbW92ZUNoaWxkKGVsKTtcbiAgICAgICAgcmV0dXJuICEhc3VwcG9ydHM7XG4gICAgfSkoKTtcblxuICAgIHZhciBkZWZhdWx0cyA9IHtcbiAgICAgICAgICAgIGxpc3ROb2RlTmFtZSAgICA6ICdvbCcsXG4gICAgICAgICAgICBpdGVtTm9kZU5hbWUgICAgOiAnbGknLFxuICAgICAgICAgICAgcm9vdENsYXNzICAgICAgIDogJ2RkJyxcbiAgICAgICAgICAgIGxpc3RDbGFzcyAgICAgICA6ICdkZC1saXN0JyxcbiAgICAgICAgICAgIGl0ZW1DbGFzcyAgICAgICA6ICdkZC1pdGVtJyxcbiAgICAgICAgICAgIGRyYWdDbGFzcyAgICAgICA6ICdkZC1kcmFnZWwnLFxuICAgICAgICAgICAgaGFuZGxlQ2xhc3MgICAgIDogJ2RkLWhhbmRsZScsXG4gICAgICAgICAgICBjb2xsYXBzZWRDbGFzcyAgOiAnZGQtY29sbGFwc2VkJyxcbiAgICAgICAgICAgIHBsYWNlQ2xhc3MgICAgICA6ICdkZC1wbGFjZWhvbGRlcicsXG4gICAgICAgICAgICBub0RyYWdDbGFzcyAgICAgOiAnZGQtbm9kcmFnJyxcbiAgICAgICAgICAgIGVtcHR5Q2xhc3MgICAgICA6ICdkZC1lbXB0eScsXG4gICAgICAgICAgICBleHBhbmRCdG5IVE1MICAgOiAnPGJ1dHRvbiBkYXRhLWFjdGlvbj1cImV4cGFuZFwiIHR5cGU9XCJidXR0b25cIj5FeHBhbmQ8L2J1dHRvbj4nLFxuICAgICAgICAgICAgY29sbGFwc2VCdG5IVE1MIDogJzxidXR0b24gZGF0YS1hY3Rpb249XCJjb2xsYXBzZVwiIHR5cGU9XCJidXR0b25cIj5Db2xsYXBzZTwvYnV0dG9uPicsXG4gICAgICAgICAgICBncm91cCAgICAgICAgICAgOiAwLFxuICAgICAgICAgICAgbWF4RGVwdGggICAgICAgIDogNSxcbiAgICAgICAgICAgIHRocmVzaG9sZCAgICAgICA6IDIwXG4gICAgICAgIH07XG5cbiAgICBmdW5jdGlvbiBQbHVnaW4oZWxlbWVudCwgb3B0aW9ucylcbiAgICB7XG4gICAgICAgIHRoaXMudyAgPSAkKGRvY3VtZW50KTtcbiAgICAgICAgdGhpcy5lbCA9ICQoZWxlbWVudCk7XG4gICAgICAgIHRoaXMub3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCBkZWZhdWx0cywgb3B0aW9ucyk7XG4gICAgICAgIHRoaXMuaW5pdCgpO1xuICAgIH1cblxuICAgIFBsdWdpbi5wcm90b3R5cGUgPSB7XG5cbiAgICAgICAgaW5pdDogZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgbGlzdCA9IHRoaXM7XG5cbiAgICAgICAgICAgIGxpc3QucmVzZXQoKTtcblxuICAgICAgICAgICAgbGlzdC5lbC5kYXRhKCduZXN0YWJsZS1ncm91cCcsIHRoaXMub3B0aW9ucy5ncm91cCk7XG5cbiAgICAgICAgICAgIGxpc3QucGxhY2VFbCA9ICQoJzxkaXYgY2xhc3M9XCInICsgbGlzdC5vcHRpb25zLnBsYWNlQ2xhc3MgKyAnXCIvPicpO1xuXG4gICAgICAgICAgICAkLmVhY2godGhpcy5lbC5maW5kKGxpc3Qub3B0aW9ucy5pdGVtTm9kZU5hbWUpLCBmdW5jdGlvbihrLCBlbCkge1xuICAgICAgICAgICAgICAgIGxpc3Quc2V0UGFyZW50KCQoZWwpKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBsaXN0LmVsLm9uKCdjbGljaycsICdidXR0b24nLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgICAgaWYgKGxpc3QuZHJhZ0VsKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIHRhcmdldCA9ICQoZS5jdXJyZW50VGFyZ2V0KSxcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uID0gdGFyZ2V0LmRhdGEoJ2FjdGlvbicpLFxuICAgICAgICAgICAgICAgICAgICBpdGVtICAgPSB0YXJnZXQucGFyZW50KGxpc3Qub3B0aW9ucy5pdGVtTm9kZU5hbWUpO1xuICAgICAgICAgICAgICAgIGlmIChhY3Rpb24gPT09ICdjb2xsYXBzZScpIHtcbiAgICAgICAgICAgICAgICAgICAgbGlzdC5jb2xsYXBzZUl0ZW0oaXRlbSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChhY3Rpb24gPT09ICdleHBhbmQnKSB7XG4gICAgICAgICAgICAgICAgICAgIGxpc3QuZXhwYW5kSXRlbShpdGVtKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgdmFyIG9uU3RhcnRFdmVudCA9IGZ1bmN0aW9uKGUpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFyIGhhbmRsZSA9ICQoZS50YXJnZXQpO1xuICAgICAgICAgICAgICAgIGlmICghaGFuZGxlLmhhc0NsYXNzKGxpc3Qub3B0aW9ucy5oYW5kbGVDbGFzcykpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGhhbmRsZS5jbG9zZXN0KCcuJyArIGxpc3Qub3B0aW9ucy5ub0RyYWdDbGFzcykubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlID0gaGFuZGxlLmNsb3Nlc3QoJy4nICsgbGlzdC5vcHRpb25zLmhhbmRsZUNsYXNzKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoIWhhbmRsZS5sZW5ndGggfHwgbGlzdC5kcmFnRWwpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGxpc3QuaXNUb3VjaCA9IC9edG91Y2gvLnRlc3QoZS50eXBlKTtcbiAgICAgICAgICAgICAgICBpZiAobGlzdC5pc1RvdWNoICYmIGUudG91Y2hlcy5sZW5ndGggIT09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICBsaXN0LmRyYWdTdGFydChlLnRvdWNoZXMgPyBlLnRvdWNoZXNbMF0gOiBlKTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHZhciBvbk1vdmVFdmVudCA9IGZ1bmN0aW9uKGUpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWYgKGxpc3QuZHJhZ0VsKSB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgbGlzdC5kcmFnTW92ZShlLnRvdWNoZXMgPyBlLnRvdWNoZXNbMF0gOiBlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICB2YXIgb25FbmRFdmVudCA9IGZ1bmN0aW9uKGUpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWYgKGxpc3QuZHJhZ0VsKSB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgbGlzdC5kcmFnU3RvcChlLnRvdWNoZXMgPyBlLnRvdWNoZXNbMF0gOiBlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBpZiAoaGFzVG91Y2gpIHtcbiAgICAgICAgICAgICAgICBsaXN0LmVsWzBdLmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoc3RhcnQnLCBvblN0YXJ0RXZlbnQsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2htb3ZlJywgb25Nb3ZlRXZlbnQsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBvbkVuZEV2ZW50LCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoY2FuY2VsJywgb25FbmRFdmVudCwgZmFsc2UpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsaXN0LmVsLm9uKCdtb3VzZWRvd24nLCBvblN0YXJ0RXZlbnQpO1xuICAgICAgICAgICAgbGlzdC53Lm9uKCdtb3VzZW1vdmUnLCBvbk1vdmVFdmVudCk7XG4gICAgICAgICAgICBsaXN0Lncub24oJ21vdXNldXAnLCBvbkVuZEV2ZW50KTtcblxuICAgICAgICB9LFxuXG4gICAgICAgIHNlcmlhbGl6ZTogZnVuY3Rpb24oKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgZGF0YSxcbiAgICAgICAgICAgICAgICBkZXB0aCA9IDAsXG4gICAgICAgICAgICAgICAgbGlzdCAgPSB0aGlzO1xuICAgICAgICAgICAgICAgIHN0ZXAgID0gZnVuY3Rpb24obGV2ZWwsIGRlcHRoKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGFycmF5ID0gWyBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgaXRlbXMgPSBsZXZlbC5jaGlsZHJlbihsaXN0Lm9wdGlvbnMuaXRlbU5vZGVOYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgaXRlbXMuZWFjaChmdW5jdGlvbigpXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBsaSAgID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtID0gJC5leHRlbmQoe30sIGxpLmRhdGEoKSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3ViICA9IGxpLmNoaWxkcmVuKGxpc3Qub3B0aW9ucy5saXN0Tm9kZU5hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN1Yi5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtLmNoaWxkcmVuID0gc3RlcChzdWIsIGRlcHRoICsgMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBhcnJheS5wdXNoKGl0ZW0pO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFycmF5O1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICBkYXRhID0gc3RlcChsaXN0LmVsLmZpbmQobGlzdC5vcHRpb25zLmxpc3ROb2RlTmFtZSkuZmlyc3QoKSwgZGVwdGgpO1xuICAgICAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2VyaWFsaXNlOiBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnNlcmlhbGl6ZSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlc2V0OiBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHRoaXMubW91c2UgPSB7XG4gICAgICAgICAgICAgICAgb2Zmc2V0WCAgIDogMCxcbiAgICAgICAgICAgICAgICBvZmZzZXRZICAgOiAwLFxuICAgICAgICAgICAgICAgIHN0YXJ0WCAgICA6IDAsXG4gICAgICAgICAgICAgICAgc3RhcnRZICAgIDogMCxcbiAgICAgICAgICAgICAgICBsYXN0WCAgICAgOiAwLFxuICAgICAgICAgICAgICAgIGxhc3RZICAgICA6IDAsXG4gICAgICAgICAgICAgICAgbm93WCAgICAgIDogMCxcbiAgICAgICAgICAgICAgICBub3dZICAgICAgOiAwLFxuICAgICAgICAgICAgICAgIGRpc3RYICAgICA6IDAsXG4gICAgICAgICAgICAgICAgZGlzdFkgICAgIDogMCxcbiAgICAgICAgICAgICAgICBkaXJBeCAgICAgOiAwLFxuICAgICAgICAgICAgICAgIGRpclggICAgICA6IDAsXG4gICAgICAgICAgICAgICAgZGlyWSAgICAgIDogMCxcbiAgICAgICAgICAgICAgICBsYXN0RGlyWCAgOiAwLFxuICAgICAgICAgICAgICAgIGxhc3REaXJZICA6IDAsXG4gICAgICAgICAgICAgICAgZGlzdEF4WCAgIDogMCxcbiAgICAgICAgICAgICAgICBkaXN0QXhZICAgOiAwXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgdGhpcy5pc1RvdWNoICAgID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLm1vdmluZyAgICAgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMuZHJhZ0VsICAgICA9IG51bGw7XG4gICAgICAgICAgICB0aGlzLmRyYWdSb290RWwgPSBudWxsO1xuICAgICAgICAgICAgdGhpcy5kcmFnRGVwdGggID0gMDtcbiAgICAgICAgICAgIHRoaXMuaGFzTmV3Um9vdCA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5wb2ludEVsICAgID0gbnVsbDtcbiAgICAgICAgfSxcblxuICAgICAgICBleHBhbmRJdGVtOiBmdW5jdGlvbihsaSlcbiAgICAgICAge1xuICAgICAgICAgICAgbGkucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLmNvbGxhcHNlZENsYXNzKTtcbiAgICAgICAgICAgIGxpLmNoaWxkcmVuKCdbZGF0YS1hY3Rpb249XCJleHBhbmRcIl0nKS5oaWRlKCk7XG4gICAgICAgICAgICBsaS5jaGlsZHJlbignW2RhdGEtYWN0aW9uPVwiY29sbGFwc2VcIl0nKS5zaG93KCk7XG4gICAgICAgICAgICBsaS5jaGlsZHJlbih0aGlzLm9wdGlvbnMubGlzdE5vZGVOYW1lKS5zaG93KCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY29sbGFwc2VJdGVtOiBmdW5jdGlvbihsaSlcbiAgICAgICAge1xuICAgICAgICAgICAgdmFyIGxpc3RzID0gbGkuY2hpbGRyZW4odGhpcy5vcHRpb25zLmxpc3ROb2RlTmFtZSk7XG4gICAgICAgICAgICBpZiAobGlzdHMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgbGkuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmNvbGxhcHNlZENsYXNzKTtcbiAgICAgICAgICAgICAgICBsaS5jaGlsZHJlbignW2RhdGEtYWN0aW9uPVwiY29sbGFwc2VcIl0nKS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgbGkuY2hpbGRyZW4oJ1tkYXRhLWFjdGlvbj1cImV4cGFuZFwiXScpLnNob3coKTtcbiAgICAgICAgICAgICAgICBsaS5jaGlsZHJlbih0aGlzLm9wdGlvbnMubGlzdE5vZGVOYW1lKS5oaWRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZXhwYW5kQWxsOiBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBsaXN0ID0gdGhpcztcbiAgICAgICAgICAgIGxpc3QuZWwuZmluZChsaXN0Lm9wdGlvbnMuaXRlbU5vZGVOYW1lKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGxpc3QuZXhwYW5kSXRlbSgkKHRoaXMpKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNvbGxhcHNlQWxsOiBmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBsaXN0ID0gdGhpcztcbiAgICAgICAgICAgIGxpc3QuZWwuZmluZChsaXN0Lm9wdGlvbnMuaXRlbU5vZGVOYW1lKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGxpc3QuY29sbGFwc2VJdGVtKCQodGhpcykpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0UGFyZW50OiBmdW5jdGlvbihsaSlcbiAgICAgICAge1xuICAgICAgICAgICAgaWYgKGxpLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5saXN0Tm9kZU5hbWUpLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgIGxpLnByZXBlbmQoJCh0aGlzLm9wdGlvbnMuZXhwYW5kQnRuSFRNTCkpO1xuICAgICAgICAgICAgICAgIGxpLnByZXBlbmQoJCh0aGlzLm9wdGlvbnMuY29sbGFwc2VCdG5IVE1MKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBsaS5jaGlsZHJlbignW2RhdGEtYWN0aW9uPVwiZXhwYW5kXCJdJykuaGlkZSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHVuc2V0UGFyZW50OiBmdW5jdGlvbihsaSlcbiAgICAgICAge1xuICAgICAgICAgICAgbGkucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLmNvbGxhcHNlZENsYXNzKTtcbiAgICAgICAgICAgIGxpLmNoaWxkcmVuKCdbZGF0YS1hY3Rpb25dJykucmVtb3ZlKCk7XG4gICAgICAgICAgICBsaS5jaGlsZHJlbih0aGlzLm9wdGlvbnMubGlzdE5vZGVOYW1lKS5yZW1vdmUoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBkcmFnU3RhcnQ6IGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBtb3VzZSAgICA9IHRoaXMubW91c2UsXG4gICAgICAgICAgICAgICAgdGFyZ2V0ICAgPSAkKGUudGFyZ2V0KSxcbiAgICAgICAgICAgICAgICBkcmFnSXRlbSA9IHRhcmdldC5jbG9zZXN0KHRoaXMub3B0aW9ucy5pdGVtTm9kZU5hbWUpO1xuXG4gICAgICAgICAgICB0aGlzLnBsYWNlRWwuY3NzKCdoZWlnaHQnLCBkcmFnSXRlbS5oZWlnaHQoKSk7XG5cbiAgICAgICAgICAgIG1vdXNlLm9mZnNldFggPSBlLm9mZnNldFggIT09IHVuZGVmaW5lZCA/IGUub2Zmc2V0WCA6IGUucGFnZVggLSB0YXJnZXQub2Zmc2V0KCkubGVmdDtcbiAgICAgICAgICAgIG1vdXNlLm9mZnNldFkgPSBlLm9mZnNldFkgIT09IHVuZGVmaW5lZCA/IGUub2Zmc2V0WSA6IGUucGFnZVkgLSB0YXJnZXQub2Zmc2V0KCkudG9wO1xuICAgICAgICAgICAgbW91c2Uuc3RhcnRYID0gbW91c2UubGFzdFggPSBlLnBhZ2VYO1xuICAgICAgICAgICAgbW91c2Uuc3RhcnRZID0gbW91c2UubGFzdFkgPSBlLnBhZ2VZO1xuXG4gICAgICAgICAgICB0aGlzLmRyYWdSb290RWwgPSB0aGlzLmVsO1xuXG4gICAgICAgICAgICB0aGlzLmRyYWdFbCA9ICQoZG9jdW1lbnQuY3JlYXRlRWxlbWVudCh0aGlzLm9wdGlvbnMubGlzdE5vZGVOYW1lKSkuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmxpc3RDbGFzcyArICcgJyArIHRoaXMub3B0aW9ucy5kcmFnQ2xhc3MpO1xuICAgICAgICAgICAgdGhpcy5kcmFnRWwuY3NzKCd3aWR0aCcsIGRyYWdJdGVtLndpZHRoKCkpO1xuXG4gICAgICAgICAgICBkcmFnSXRlbS5hZnRlcih0aGlzLnBsYWNlRWwpO1xuICAgICAgICAgICAgZHJhZ0l0ZW1bMF0ucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChkcmFnSXRlbVswXSk7XG4gICAgICAgICAgICBkcmFnSXRlbS5hcHBlbmRUbyh0aGlzLmRyYWdFbCk7XG5cbiAgICAgICAgICAgICQoZG9jdW1lbnQuYm9keSkuYXBwZW5kKHRoaXMuZHJhZ0VsKTtcbiAgICAgICAgICAgIHRoaXMuZHJhZ0VsLmNzcyh7XG4gICAgICAgICAgICAgICAgJ2xlZnQnIDogZS5wYWdlWCAtIG1vdXNlLm9mZnNldFgsXG4gICAgICAgICAgICAgICAgJ3RvcCcgIDogZS5wYWdlWSAtIG1vdXNlLm9mZnNldFlcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy8gdG90YWwgZGVwdGggb2YgZHJhZ2dpbmcgaXRlbVxuICAgICAgICAgICAgdmFyIGksIGRlcHRoLFxuICAgICAgICAgICAgICAgIGl0ZW1zID0gdGhpcy5kcmFnRWwuZmluZCh0aGlzLm9wdGlvbnMuaXRlbU5vZGVOYW1lKTtcbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBpdGVtcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGRlcHRoID0gJChpdGVtc1tpXSkucGFyZW50cyh0aGlzLm9wdGlvbnMubGlzdE5vZGVOYW1lKS5sZW5ndGg7XG4gICAgICAgICAgICAgICAgaWYgKGRlcHRoID4gdGhpcy5kcmFnRGVwdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kcmFnRGVwdGggPSBkZXB0aDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZHJhZ1N0b3A6IGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBlbCA9IHRoaXMuZHJhZ0VsLmNoaWxkcmVuKHRoaXMub3B0aW9ucy5pdGVtTm9kZU5hbWUpLmZpcnN0KCk7XG4gICAgICAgICAgICBlbFswXS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGVsWzBdKTtcbiAgICAgICAgICAgIHRoaXMucGxhY2VFbC5yZXBsYWNlV2l0aChlbCk7XG5cbiAgICAgICAgICAgIHRoaXMuZHJhZ0VsLnJlbW92ZSgpO1xuICAgICAgICAgICAgdGhpcy5lbC50cmlnZ2VyKCdjaGFuZ2UnKTtcbiAgICAgICAgICAgIGlmICh0aGlzLmhhc05ld1Jvb3QpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmRyYWdSb290RWwudHJpZ2dlcignY2hhbmdlJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnJlc2V0KCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZHJhZ01vdmU6IGZ1bmN0aW9uKGUpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBsaXN0LCBwYXJlbnQsIHByZXYsIG5leHQsIGRlcHRoLFxuICAgICAgICAgICAgICAgIG9wdCAgID0gdGhpcy5vcHRpb25zLFxuICAgICAgICAgICAgICAgIG1vdXNlID0gdGhpcy5tb3VzZTtcblxuICAgICAgICAgICAgdGhpcy5kcmFnRWwuY3NzKHtcbiAgICAgICAgICAgICAgICAnbGVmdCcgOiBlLnBhZ2VYIC0gbW91c2Uub2Zmc2V0WCxcbiAgICAgICAgICAgICAgICAndG9wJyAgOiBlLnBhZ2VZIC0gbW91c2Uub2Zmc2V0WVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIC8vIG1vdXNlIHBvc2l0aW9uIGxhc3QgZXZlbnRzXG4gICAgICAgICAgICBtb3VzZS5sYXN0WCA9IG1vdXNlLm5vd1g7XG4gICAgICAgICAgICBtb3VzZS5sYXN0WSA9IG1vdXNlLm5vd1k7XG4gICAgICAgICAgICAvLyBtb3VzZSBwb3NpdGlvbiB0aGlzIGV2ZW50c1xuICAgICAgICAgICAgbW91c2Uubm93WCAgPSBlLnBhZ2VYO1xuICAgICAgICAgICAgbW91c2Uubm93WSAgPSBlLnBhZ2VZO1xuICAgICAgICAgICAgLy8gZGlzdGFuY2UgbW91c2UgbW92ZWQgYmV0d2VlbiBldmVudHNcbiAgICAgICAgICAgIG1vdXNlLmRpc3RYID0gbW91c2Uubm93WCAtIG1vdXNlLmxhc3RYO1xuICAgICAgICAgICAgbW91c2UuZGlzdFkgPSBtb3VzZS5ub3dZIC0gbW91c2UubGFzdFk7XG4gICAgICAgICAgICAvLyBkaXJlY3Rpb24gbW91c2Ugd2FzIG1vdmluZ1xuICAgICAgICAgICAgbW91c2UubGFzdERpclggPSBtb3VzZS5kaXJYO1xuICAgICAgICAgICAgbW91c2UubGFzdERpclkgPSBtb3VzZS5kaXJZO1xuICAgICAgICAgICAgLy8gZGlyZWN0aW9uIG1vdXNlIGlzIG5vdyBtb3ZpbmcgKG9uIGJvdGggYXhpcylcbiAgICAgICAgICAgIG1vdXNlLmRpclggPSBtb3VzZS5kaXN0WCA9PT0gMCA/IDAgOiBtb3VzZS5kaXN0WCA+IDAgPyAxIDogLTE7XG4gICAgICAgICAgICBtb3VzZS5kaXJZID0gbW91c2UuZGlzdFkgPT09IDAgPyAwIDogbW91c2UuZGlzdFkgPiAwID8gMSA6IC0xO1xuICAgICAgICAgICAgLy8gYXhpcyBtb3VzZSBpcyBub3cgbW92aW5nIG9uXG4gICAgICAgICAgICB2YXIgbmV3QXggICA9IE1hdGguYWJzKG1vdXNlLmRpc3RYKSA+IE1hdGguYWJzKG1vdXNlLmRpc3RZKSA/IDEgOiAwO1xuXG4gICAgICAgICAgICAvLyBkbyBub3RoaW5nIG9uIGZpcnN0IG1vdmVcbiAgICAgICAgICAgIGlmICghbW91c2UubW92aW5nKSB7XG4gICAgICAgICAgICAgICAgbW91c2UuZGlyQXggID0gbmV3QXg7XG4gICAgICAgICAgICAgICAgbW91c2UubW92aW5nID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGNhbGMgZGlzdGFuY2UgbW92ZWQgb24gdGhpcyBheGlzIChhbmQgZGlyZWN0aW9uKVxuICAgICAgICAgICAgaWYgKG1vdXNlLmRpckF4ICE9PSBuZXdBeCkge1xuICAgICAgICAgICAgICAgIG1vdXNlLmRpc3RBeFggPSAwO1xuICAgICAgICAgICAgICAgIG1vdXNlLmRpc3RBeFkgPSAwO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb3VzZS5kaXN0QXhYICs9IE1hdGguYWJzKG1vdXNlLmRpc3RYKTtcbiAgICAgICAgICAgICAgICBpZiAobW91c2UuZGlyWCAhPT0gMCAmJiBtb3VzZS5kaXJYICE9PSBtb3VzZS5sYXN0RGlyWCkge1xuICAgICAgICAgICAgICAgICAgICBtb3VzZS5kaXN0QXhYID0gMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbW91c2UuZGlzdEF4WSArPSBNYXRoLmFicyhtb3VzZS5kaXN0WSk7XG4gICAgICAgICAgICAgICAgaWYgKG1vdXNlLmRpclkgIT09IDAgJiYgbW91c2UuZGlyWSAhPT0gbW91c2UubGFzdERpclkpIHtcbiAgICAgICAgICAgICAgICAgICAgbW91c2UuZGlzdEF4WSA9IDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW91c2UuZGlyQXggPSBuZXdBeDtcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBtb3ZlIGhvcml6b250YWxcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaWYgKG1vdXNlLmRpckF4ICYmIG1vdXNlLmRpc3RBeFggPj0gb3B0LnRocmVzaG9sZCkge1xuICAgICAgICAgICAgICAgIC8vIHJlc2V0IG1vdmUgZGlzdGFuY2Ugb24geC1heGlzIGZvciBuZXcgcGhhc2VcbiAgICAgICAgICAgICAgICBtb3VzZS5kaXN0QXhYID0gMDtcbiAgICAgICAgICAgICAgICBwcmV2ID0gdGhpcy5wbGFjZUVsLnByZXYob3B0Lml0ZW1Ob2RlTmFtZSk7XG4gICAgICAgICAgICAgICAgLy8gaW5jcmVhc2UgaG9yaXpvbnRhbCBsZXZlbCBpZiBwcmV2aW91cyBzaWJsaW5nIGV4aXN0cyBhbmQgaXMgbm90IGNvbGxhcHNlZFxuICAgICAgICAgICAgICAgIGlmIChtb3VzZS5kaXN0WCA+IDAgJiYgcHJldi5sZW5ndGggJiYgIXByZXYuaGFzQ2xhc3Mob3B0LmNvbGxhcHNlZENsYXNzKSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBjYW5ub3QgaW5jcmVhc2UgbGV2ZWwgd2hlbiBpdGVtIGFib3ZlIGlzIGNvbGxhcHNlZFxuICAgICAgICAgICAgICAgICAgICBsaXN0ID0gcHJldi5maW5kKG9wdC5saXN0Tm9kZU5hbWUpLmxhc3QoKTtcbiAgICAgICAgICAgICAgICAgICAgLy8gY2hlY2sgaWYgZGVwdGggbGltaXQgaGFzIHJlYWNoZWRcbiAgICAgICAgICAgICAgICAgICAgZGVwdGggPSB0aGlzLnBsYWNlRWwucGFyZW50cyhvcHQubGlzdE5vZGVOYW1lKS5sZW5ndGg7XG4gICAgICAgICAgICAgICAgICAgIGlmIChkZXB0aCArIHRoaXMuZHJhZ0RlcHRoIDw9IG9wdC5tYXhEZXB0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY3JlYXRlIG5ldyBzdWItbGV2ZWwgaWYgb25lIGRvZXNuJ3QgZXhpc3RcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghbGlzdC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaXN0ID0gJCgnPCcgKyBvcHQubGlzdE5vZGVOYW1lICsgJy8+JykuYWRkQ2xhc3Mob3B0Lmxpc3RDbGFzcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGlzdC5hcHBlbmQodGhpcy5wbGFjZUVsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmV2LmFwcGVuZChsaXN0KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFBhcmVudChwcmV2KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gZWxzZSBhcHBlbmQgdG8gbmV4dCBsZXZlbCB1cFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpc3QgPSBwcmV2LmNoaWxkcmVuKG9wdC5saXN0Tm9kZU5hbWUpLmxhc3QoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaXN0LmFwcGVuZCh0aGlzLnBsYWNlRWwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIGRlY3JlYXNlIGhvcml6b250YWwgbGV2ZWxcbiAgICAgICAgICAgICAgICBpZiAobW91c2UuZGlzdFggPCAwKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIHdlIGNhbid0IGRlY3JlYXNlIGEgbGV2ZWwgaWYgYW4gaXRlbSBwcmVjZWVkcyB0aGUgY3VycmVudCBvbmVcbiAgICAgICAgICAgICAgICAgICAgbmV4dCA9IHRoaXMucGxhY2VFbC5uZXh0KG9wdC5pdGVtTm9kZU5hbWUpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoIW5leHQubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnQgPSB0aGlzLnBsYWNlRWwucGFyZW50KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnBsYWNlRWwuY2xvc2VzdChvcHQuaXRlbU5vZGVOYW1lKS5hZnRlcih0aGlzLnBsYWNlRWwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFwYXJlbnQuY2hpbGRyZW4oKS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVuc2V0UGFyZW50KHBhcmVudC5wYXJlbnQoKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZhciBpc0VtcHR5ID0gZmFsc2U7XG5cbiAgICAgICAgICAgIC8vIGZpbmQgbGlzdCBpdGVtIHVuZGVyIGN1cnNvclxuICAgICAgICAgICAgaWYgKCFoYXNQb2ludGVyRXZlbnRzKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kcmFnRWxbMF0uc3R5bGUudmlzaWJpbGl0eSA9ICdoaWRkZW4nO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5wb2ludEVsID0gJChkb2N1bWVudC5lbGVtZW50RnJvbVBvaW50KGUucGFnZVggLSBkb2N1bWVudC5ib2R5LnNjcm9sbExlZnQsIGUucGFnZVkgLSAod2luZG93LnBhZ2VZT2Zmc2V0IHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3ApKSk7XG4gICAgICAgICAgICBpZiAoIWhhc1BvaW50ZXJFdmVudHMpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmRyYWdFbFswXS5zdHlsZS52aXNpYmlsaXR5ID0gJ3Zpc2libGUnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMucG9pbnRFbC5oYXNDbGFzcyhvcHQuaGFuZGxlQ2xhc3MpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wb2ludEVsID0gdGhpcy5wb2ludEVsLnBhcmVudChvcHQuaXRlbU5vZGVOYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh0aGlzLnBvaW50RWwuaGFzQ2xhc3Mob3B0LmVtcHR5Q2xhc3MpKSB7XG4gICAgICAgICAgICAgICAgaXNFbXB0eSA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICghdGhpcy5wb2ludEVsLmxlbmd0aCB8fCAhdGhpcy5wb2ludEVsLmhhc0NsYXNzKG9wdC5pdGVtQ2xhc3MpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBmaW5kIHBhcmVudCBsaXN0IG9mIGl0ZW0gdW5kZXIgY3Vyc29yXG4gICAgICAgICAgICB2YXIgcG9pbnRFbFJvb3QgPSB0aGlzLnBvaW50RWwuY2xvc2VzdCgnLicgKyBvcHQucm9vdENsYXNzKSxcbiAgICAgICAgICAgICAgICBpc05ld1Jvb3QgICA9IHRoaXMuZHJhZ1Jvb3RFbC5kYXRhKCduZXN0YWJsZS1pZCcpICE9PSBwb2ludEVsUm9vdC5kYXRhKCduZXN0YWJsZS1pZCcpO1xuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIG1vdmUgdmVydGljYWxcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgaWYgKCFtb3VzZS5kaXJBeCB8fCBpc05ld1Jvb3QgfHwgaXNFbXB0eSkge1xuICAgICAgICAgICAgICAgIC8vIGNoZWNrIGlmIGdyb3VwcyBtYXRjaCBpZiBkcmFnZ2luZyBvdmVyIG5ldyByb290XG4gICAgICAgICAgICAgICAgaWYgKGlzTmV3Um9vdCAmJiBvcHQuZ3JvdXAgIT09IHBvaW50RWxSb290LmRhdGEoJ25lc3RhYmxlLWdyb3VwJykpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBjaGVjayBkZXB0aCBsaW1pdFxuICAgICAgICAgICAgICAgIGRlcHRoID0gdGhpcy5kcmFnRGVwdGggLSAxICsgdGhpcy5wb2ludEVsLnBhcmVudHMob3B0Lmxpc3ROb2RlTmFtZSkubGVuZ3RoO1xuICAgICAgICAgICAgICAgIGlmIChkZXB0aCA+IG9wdC5tYXhEZXB0aCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHZhciBiZWZvcmUgPSBlLnBhZ2VZIDwgKHRoaXMucG9pbnRFbC5vZmZzZXQoKS50b3AgKyB0aGlzLnBvaW50RWwuaGVpZ2h0KCkgLyAyKTtcbiAgICAgICAgICAgICAgICAgICAgcGFyZW50ID0gdGhpcy5wbGFjZUVsLnBhcmVudCgpO1xuICAgICAgICAgICAgICAgIC8vIGlmIGVtcHR5IGNyZWF0ZSBuZXcgbGlzdCB0byByZXBsYWNlIGVtcHR5IHBsYWNlaG9sZGVyXG4gICAgICAgICAgICAgICAgaWYgKGlzRW1wdHkpIHtcbiAgICAgICAgICAgICAgICAgICAgbGlzdCA9ICQoZG9jdW1lbnQuY3JlYXRlRWxlbWVudChvcHQubGlzdE5vZGVOYW1lKSkuYWRkQ2xhc3Mob3B0Lmxpc3RDbGFzcyk7XG4gICAgICAgICAgICAgICAgICAgIGxpc3QuYXBwZW5kKHRoaXMucGxhY2VFbCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucG9pbnRFbC5yZXBsYWNlV2l0aChsaXN0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoYmVmb3JlKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucG9pbnRFbC5iZWZvcmUodGhpcy5wbGFjZUVsKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucG9pbnRFbC5hZnRlcih0aGlzLnBsYWNlRWwpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoIXBhcmVudC5jaGlsZHJlbigpLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnVuc2V0UGFyZW50KHBhcmVudC5wYXJlbnQoKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5kcmFnUm9vdEVsLmZpbmQob3B0Lml0ZW1Ob2RlTmFtZSkubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZHJhZ1Jvb3RFbC5hcHBlbmQoJzxkaXYgY2xhc3M9XCInICsgb3B0LmVtcHR5Q2xhc3MgKyAnXCIvPicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBwYXJlbnQgcm9vdCBsaXN0IGhhcyBjaGFuZ2VkXG4gICAgICAgICAgICAgICAgaWYgKGlzTmV3Um9vdCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmRyYWdSb290RWwgPSBwb2ludEVsUm9vdDtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5oYXNOZXdSb290ID0gdGhpcy5lbFswXSAhPT0gdGhpcy5kcmFnUm9vdEVsWzBdO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgICQuZm4ubmVzdGFibGUgPSBmdW5jdGlvbihwYXJhbXMpXG4gICAge1xuICAgICAgICB2YXIgbGlzdHMgID0gdGhpcyxcbiAgICAgICAgICAgIHJldHZhbCA9IHRoaXM7XG5cbiAgICAgICAgbGlzdHMuZWFjaChmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHZhciBwbHVnaW4gPSAkKHRoaXMpLmRhdGEoXCJuZXN0YWJsZVwiKTtcblxuICAgICAgICAgICAgaWYgKCFwbHVnaW4pIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmRhdGEoXCJuZXN0YWJsZVwiLCBuZXcgUGx1Z2luKHRoaXMsIHBhcmFtcykpO1xuICAgICAgICAgICAgICAgICQodGhpcykuZGF0YShcIm5lc3RhYmxlLWlkXCIsIG5ldyBEYXRlKCkuZ2V0VGltZSgpKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBwYXJhbXMgPT09ICdzdHJpbmcnICYmIHR5cGVvZiBwbHVnaW5bcGFyYW1zXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgICAgICByZXR2YWwgPSBwbHVnaW5bcGFyYW1zXSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIHJldHZhbCB8fCBsaXN0cztcbiAgICB9O1xuXG59KSh3aW5kb3cualF1ZXJ5IHx8IHdpbmRvdy5aZXB0bywgd2luZG93LCBkb2N1bWVudCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2JhY2tvZmZpY2UvanMvbGlicmFyaWVzL2pxdWVyeS5uZXN0YWJsZS5qcyJdLCJzb3VyY2VSb290IjoiIn0=