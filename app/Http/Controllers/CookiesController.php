<?php
/**
 * User: Luca Puddu
 * Date: 19/03/2018
 * Time: 18:11
 */

namespace App\Http\Controllers;

use App\Models\CookieConsent;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CookiesController
{
    public function saveCookieConsent(Request $request)
    {
        $necessary = true;
        $preferences = $request->preferences_checkbox == "on";
        $statistics = $request->statistics_checkbox == "on";
        $marketing = $request->marketing_checkbox == "on";

        CookieConsent::updateOrCreate(['ip' => $request->ip()], [
            'ip' => $request->ip(),
            'necessary' => $necessary,
            'preferences' => $preferences,
            'statistics' => $statistics,
            'marketing' => $marketing
        ]);

        return "";
    }
}