<?php
/**
 * User: Luca Puddu
 * Date: 01/03/2018
 * Time: 18:54
 */

namespace App\Http\Controllers\Users;

use App\Constants;
use App\Helpers\HarbourCentersHelper;
use App\Helpers\StaticPagesHelper;
use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\Seo;
use App\Models\Token;
use App\Models\User;
use App\Services\TokenService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

class StaticPagesController extends Controller
{
    public function getHome(Request $request)
    {
        $language = \App::getLocale();

        // Get harbour centers in this form: [area_name => [business_id => query]]
        $harbourCenters = HarbourCentersHelper::withAreas($language);

        $guestsOnBoard = Constants::GUESTS_ON_BOARD;
        $experiencesCount = Constants::EXPERIENCES_COUNT;

        $slugUrls = Seo::where('language', \App::getLocale())
            ->where('boat_type_id', null)
            ->where('duration', null)->with('harbour_center')->get();

        $topDestinations = [
            /*'nap' => [
                'destination' => 'nap',
                'photo_url' => 'assets/img/harbour_centers/nap',
                'versions' => [],
                'cloudinary_version' => 'v1529924056',
                'def' => 'c_scale,w_528,h_220/c_crop,w_528,h_220/q_65',
                'slug_url' => ''
            ],*/
            'cag' => [
                'destination' => 'cag',
                'photo_url' => 'assets/img/harbour_centers/cag',
                'versions' => [],
                'cloudinary_version' => 'v1525616812',
                'def' => 'c_scale,w_528/c_crop,w_528,h_220,y_120/q_65',
                'slug_url' => ''
            ],
            'csm' => [
                'destination' => 'csm',
                'photo_url' => 'assets/img/harbour_centers/csm',
                'versions' => [],
                'cloudinary_version' => 'v1529924059',
                'def' => 'c_scale,w_528,h_220/c_crop,w_528,h_220/q_65',
                'slug_url' => ''
            ],
            /*'std' => [
                'destination' => 'std',
                'photo_url' => 'assets/img/harbour_centers/std',
                'versions' => [],
                'cloudinary_version' => 'v1525616823',
                'def' => 'c_scale,w_528,h_220/c_crop,w_528,h_220/q_65',
                'slug_url' => ''
            ],*/
            'ast' => [
                'destination' => 'ast',
                'photo_url' => 'assets/img/harbour_centers/ast',
                'versions' => [],
                'cloudinary_version' => 'v1526837554',
                'def' => 'c_scale,w_528,h_220/c_crop,w_528,h_220/q_65',
                'slug_url' => ''
            ],
            /*'mar' => [
                'destination' => 'mar',
                'photo_url' => 'assets/img/harbour_centers/mar',
                'versions' => [],
                'cloudinary_version' => 'v1528232028',
                'def' => 'c_scale,h_500/c_crop,w_528,h_220,x_250,y_60/q_65',
                'slug_url' => ''
            ]*/
        ];
        foreach ($slugUrls as $slugUrl) {
            if (!isset($slugUrl->harbour_center)) continue;
            if (!isset($topDestinations[$slugUrl->harbour_center->business_id])) continue;

            $topDestinations[$slugUrl->harbour_center->business_id]['slug_url'] = HarbourCentersHelper::getUrl(\App::getLocale(), $slugUrl['query']);
        }

        return view(Constants::R_USERS_HOME, [
            'top_destinations' => $topDestinations,
            'harbour_centers' => $harbourCenters,
            'guests_on_board' => $guestsOnBoard,
            'experiences_count' => $experiencesCount,
            'seo' => StaticPagesHelper::getSeo('home')
        ]);
    }

    /*public function getHowItWorks(Request $request)
    {
        return view('users.how-it-works', [
            'seo' => StaticPagesHelper::getSeo('how_it_works')
        ]);
    }*/

    public function getTermsOfService(Request $request)
    {
        return view('users.terms-of-service', [
            'seo' => StaticPagesHelper::getSeo('terms_of_service')
        ]);
    }

    public function getCookiePolicy(Request $request)
    {
        return view('users.cookie-policy', [
            'seo' => StaticPagesHelper::getSeo('cookie_policy')
        ]);
    }

    public function getPrivacyPolicy(Request $request)
    {
        return view('users.privacy-policy', [
            'seo' => StaticPagesHelper::getSeo('privacy_policy')
        ]);
    }

    public function getContactUs(Request $request)
    {
        return view('users.contact-us', [
            'phone' => str_replace(" ", "", Constants::TELEPHONE_NUMBER),
            'fb_profile_link' => Constants::FB_PROFILE_LINK,
            'instagram_profile_link' => Constants::INSTAGRAM_PROFILE_LINK,
            'seo' => StaticPagesHelper::getSeo('contact_us')
        ]);
    }

    public function getNotFound(Request $request)
    {
        return view('errors.404');
    }

    public function getNotAuthorized(Request $request)
    {
        return view('errors.401');
    }

    public function getUnsubscribe(Request $request)
    {
        if (!TokenService::isValidForUser($request->token, $request->uid)) {
            abort(ResponseCodes::HTTP_UNAUTHORIZED);
        }

        // Unsub user
        $user = User::findOrFail($request->uid);
        $user->marketing_accepted = false;
        $user->save();

        // Delete token
        $token = Token::where('token', $request->token)->get()->first();
        if (!isset($token)) {
            abort(ResponseCodes::HTTP_UNAUTHORIZED);
        }
        $token->delete();

        return view('users.unsubscribe', ['email' => $user->email, 'fb' => Constants::FB_PROFILE_LINK, 'ig' => Constants::INSTAGRAM_PROFILE_LINK]);
    }
}
