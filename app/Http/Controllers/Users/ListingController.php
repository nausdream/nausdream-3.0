<?php
/**
 * User: Luca Puddu
 * Date: 19/03/2018
 * Time: 18:11
 */

namespace App\Http\Controllers\Users;

use App\Constants;
use App\Helpers\PhotoHelper;
use App\Helpers\WeekdayHelper;
use App\Jobs\SendMail;
use App\Mail\BookingRequest;
use App\Mail\CustomBookingRequest;
use App\Models\ExperienceCopy;
use App\Models\Lead;
use App\Models\Redirect;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

class ListingController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get(Request $request)
    {
        $language = \App::getLocale();

        // Eager load all needed relationships
        $listingSEO = ExperienceCopy::where('language', $language)
            ->where('slug_url', $request->slug_url)
            ->with(
                'experience.harbour_center.experiences',
                'experience.experience_feedbacks',
                'experience.experience_photos',
                'experience.sentiments',
                'experience.spoken_languages',
                'experience.included_services',
                'experience.schedules',
                'experience.stops'
            )
            ->first();

        // Check if listing exists
        if (!isset($listingSEO)) {
            // Check if there's a redirect for this slug url
            $redirectFrom = '/' . $language . '/' . Constants::LISTING_PREFIX . '/' . $request->slug_url;
            $redirect = Redirect::where('old_slug_url', $redirectFrom)->first();
            if (isset($redirect) && isset($redirect->new_slug_url)) {
                return redirect($redirect->new_slug_url, ResponseCodes::HTTP_MOVED_PERMANENTLY);
            }

            // Return 404
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        $listing = $listingSEO->experience()
            ->with('difficulty')
            ->with('sentiments')
            ->with('experience_copies')->first();

        // If experience is not published, return 404
        if (!$listing->is_finished && !$request->get('isAdmin')) {
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        // Get related listings
        $related = $listing->harbour_center->experiences
            ->where('is_finished', true)
            ->where('is_searchable', true)
            ->where('id', '!=', $listing->id)
            ->map('App\Transformers\ListingTransformer::transform');

        // Get photos ids
        $photos = $listing->experience_photos->pluck('is_cover', 'id');

        // Get photos links
        $photosUrls = $listing->experience_photos->map(function ($photo, $index) {
            return PhotoHelper::getLink($photo);
        });

        // Difficulty
        $difficulty = $listing->difficulty->name;

        // Sentiments
        $translatedSentiments = $listing->sentiments->map(function ($sentiment, $index) {
            return trans('sentiments.' . $sentiment->name);
        });
        $translatedSentiments = $translatedSentiments->implode(", ");

        // Spoken languages
        $translatedLanguages = $listing->spoken_languages->map(function ($language) {
            return trans('spoken_languages.' . $language->language);
        });
        $translatedLanguages = $translatedLanguages->implode(", ");

        // Get slug urls in other languages
        $copies = $listing->experience_copies->pluck('slug_url', 'language');
        $copies = $copies->map(function ($slug, $language) {
            return "/{$language}/" . Constants::LISTING_PREFIX . "/{$slug}";
        });
        $alternates = [
            'x-default' => $copies[Constants::USERS_DEFAULT_LANGUAGE]];
        $alternates = array_merge($alternates, $copies->toArray());

        $seo = [
            'title' => $listingSEO->meta_title . " " . Constants::SEO_SEPARATOR . " " . Constants::SEO_DOMAIN_NAME,
            'description' => $listingSEO->meta_description,
            'alternates' => $alternates,
            'og_type' => 'website',
            'og_url' => $request->url(),
            'og_images' => $photosUrls,
        ];

        $features = explode(',', $listingSEO->features);
        if ($features[0] == "") {
            // To prevent templating from displaying empty feature
            $features = [];
        }

        //0 = not set, 1 = only to is set, 2 = from and to are set
        if (!isset($listing->boat_length_min) && !isset($listing->boat_length_max)) {
            $boat_length = 0;
        } else if (isset($listing->boat_length_min) && isset($listing->boat_length_max)) {
            $boat_length = 2;
        } else {
            $boat_length = 1;
        }
        $params = [
            'email_sent' => $request->email_sent ?? false,
            'seo' => $seo,
            'listing' => null,
            'photos' => $photosUrls,
            'feedbacks' => $listing->experience_feedbacks,
            'sentiments' => $translatedSentiments,
            'spoken_languages' => $translatedLanguages,
            'schedules' => $listing->schedules->sortBy('departure_time'),
            'stops' => $listing->stops->where('language', \App::getLocale()),
            'features' => $features,
            'services' => $listing->included_services()->withTrashed()->get(),
            'related' => $related,
            'map' => [
                'image' => "https://maps.googleapis.com/maps/api/staticmap?center={$listing->lat},{$listing->lng}&zoom=15&markers=color:red%7C{$listing->lat},{$listing->lng}&size=720x200&maptype=roadmap&key=" . config('services.google.maps.api_key'),
                'link' => "https://www.google.com/maps/search/?api=1&query={$listing->lat},{$listing->lng}"
            ],
            'phone_formatted' => Constants::TELEPHONE_NUMBER,
            'phone' => str_replace(' ', '', Constants::TELEPHONE_NUMBER),
            'action' => isset($listing['trekksoft_id']) ? 'book' : 'request',
            'boat_length' => $boat_length
        ];
        // Merge experience copy with experience to pass it to the view
        $listingArray = array_merge($listing->getAttributes(), $listingSEO->getAttributes());
        $listingArray['harbour_center'] = $listing->harbour_center->business_id;
        $listingArray['photos'] = $photos;
        $listingArray['difficulty'] = $difficulty;
        $listingArray['weekdays'] = explode(',', $listingArray['weekdays']);

        // Set disabled weekdays in "x,y,z" format where x, y and z are weekdays numbers starting from sunday
        $listingArray['disabled_weekdays'] = WeekdayHelper::getOtherWeekdays($listingArray['weekdays']);

        // Set start date. This is the date from where we start accepting requests, and depends on the cutoff time
        // and the first available tour date and time
        $startDate = Carbon::now(Constants::TIMEZONE)->addMinutes(Constants::CUT_OFF_MINUTES);

        // If startDate time is greater than the last scheduled trip departure_time, add one day to the start date
        $lastSchedule = $params['schedules']->last();
        if ($startDate->greaterThan($startDate->copy()->setTimeFromTimeString($lastSchedule->departure_time))) {
            $startDate->addDay();
        }

        $listingArray['start_date'] = $startDate->toDateTimeString();

        $params['listing'] = $listingArray;

        return view(Constants::R_USERS_LISTING, $params);
    }

    /**
     * Send booking request via the listing page form
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function contact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "experience_business_id" => "exists:experiences,business_id",
            "name" => "required|string|max:" . Constants::LISTING_FORM_NAME_MAX_LENGTH,
            "email" => "required|email",
            "phone" => "required|string",
            "guests" => "required|integer|min:1",
            "date" => "required|date_format:Y-m-d",
            "notes" => "nullable|string",
            "coupon" => "nullable|string|max:" . Constants::LISTING_FORM_COUPON_MAX_LENGTH,
        ]);

        $lang = \App::getLocale();

        if ($validator->fails()) {
            return redirect()
                ->route(Constants::R_USERS_LISTING, [
                    'lang' => $lang, 'slug_url' => $request->slug_url
                ])
                ->withErrors($validator)
                ->withInput();
        }

        // Add lead to db
        $lead = Lead::create([
            'experience_business_id' => $request->experience_business_id,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'date' => $request->date,
            'notes' => $request->notes,
            'coupon' => $request->coupon,
            'guests' => $request->guests,
            'language' => $lang,
            'marketing_accepted' => $request->marketing == "1"
        ]);

        // Register or update user
        UserService::createOrUpdateFromLead($lead);

        // Request is valid, send email
        dispatch((new SendMail(new BookingRequest([
            'experience_business_id' => $request->experience_business_id,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'guests' => $request->guests,
            'date' => $request->date,
            'notes' => $request->notes,
            'coupon' => $request->coupon,
            'language' => $lang,
            'marketing' => $request->marketing
        ])))->onQueue(config('queue.connections.sqs.mail')));

        return redirect()->route(Constants::R_USERS_LISTING, [
            'lang' => $lang, 'slug_url' => $request->slug_url, 'email_sent' => true
        ]);
    }

    public function contactCustom(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required|string|max:" . Constants::LISTING_FORM_NAME_MAX_LENGTH,
            "email" => "required|email",
            "phone" => "required|string",
            "guests" => "required|integer|min:1",
            "date" => "required|date_format:Y-m-d",
            "location" => "required|string",
            "like_to_do" => "required|string",
            "coupon" => "nullable|string|max:" . Constants::LISTING_FORM_COUPON_MAX_LENGTH,
        ]);

        $lang = \App::getLocale();

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Add lead to db
        $lead = Lead::create(['name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'date' => $request->date,
            'location' => $request->location,
            'coupon' => $request->coupon,
            'guests' => $request->guests,
            'like_to_do' =>$request->like_to_do,
            'language' => $lang,
            'marketing_accepted' => $request->marketing == "1"
        ]);

        // Register or update user
        UserService::createOrUpdateFromLead($lead);

        // Request is valid, send email
        dispatch((new SendMail(new CustomBookingRequest([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'guests' => $request->guests,
            'date' => $request->date,
            'location' => $request->location,
            'like_to_do' => $request->like_to_do,
            'coupon' => $request->coupon,
            'language' => $lang,
            'marketing' => $request->marketing
        ])))->onQueue(config('queue.connections.sqs.mail')));

        return redirect()->back()->with(['success' => true]);
    }


    /**
     * Redirect to new experience
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getNewFromOld(Request $request)
    {
        $old = DB::table('old_experience_links')
            ->where([
                ['language', '=', \App::getLocale()],
                ['slug_url', '=', $request->slug_url],
            ])
            ->get()->first();

        if (!isset($old)) {
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        $new = ExperienceCopy::find($old->experience_copy_id);

        if (!isset($new)) {
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        return $new->goTo(ResponseCodes::HTTP_MOVED_PERMANENTLY);
    }
}