<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 07/07/2018
 * Time: 15:07
 */

namespace App\Http\Controllers\Users;


use App\Models\Booking;
use App\Models\Campaign;
use App\Models\Token;
use App\Services\TokenService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

class CampaignController
{
    /**
     * Get view for opt-in of a specific campaign.
     *
     * @param Request $request
     * @return null|string
     */
    public function getOptInView(Request $request)
    {
        // Verify token and user
        if (!isset($request->token) || !isset($request->buid) || !isset($request->campaign)) {
            abort(ResponseCodes::HTTP_UNAUTHORIZED);
        }
        $booking = Booking::findOrFail($request->buid);

        if (!TokenService::isValidForUser($request->token, $booking->user->id)) {
            abort(ResponseCodes::HTTP_UNAUTHORIZED);
        }

        // Verify if campaign exists and is active
        $campaign = Campaign::find($request->campaign);
        if (!$campaign->is_active) {
            abort(ResponseCodes::HTTP_UNAUTHORIZED);
        }

        // Opt-in user
        $booking->user->marketing_accepted = true;
        $booking->user->save();

        // Delete token
        $token = Token::where('token', $request->token)->get()->first();
        if (isset($token)) {
            try {
                $token->delete();
            } catch (\Exception $exception) {

            }
        }

        return $campaign->optIn($booking);
    }

    /**
     * Return campaign rules view.
     *
     * @param Request $request
     * @return mixed
     */
    public function getCampaignRules(Request $request){
        if (!isset($request->c)){
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        $campaign = Campaign::where('name', $request->c)->get()->first();
        if (!isset($campaign)){
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        return $campaign->rules();
    }
}