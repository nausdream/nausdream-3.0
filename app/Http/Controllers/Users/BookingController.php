<?php
/**
 * User: Luca Puddu
 * Date: 19/03/2018
 * Time: 18:11
 */

namespace App\Http\Controllers\Users;

use App\Constants;
use App\Helpers\CouponHelper;
use App\Helpers\PriceHelper;
use App\Helpers\DateHelper;
use App\Mail\BookingPaidToAdmin;
use App\Mail\BookingPaidToGuest;
use App\Models\Booking;
use App\Models\Campaign;
use App\Models\Guest;
use App\Models\PaymentMethod;
use App\Services\CouponService;
use Carbon\Carbon;
use http\Exception;
use Illuminate\Http\Request;
use Mail;
use Stripe\Stripe;

class BookingController
{
    private $validationRules;

    /**
     * BookingController constructor.
     */
    public function __construct()
    {
        $this->validationRules = [
            'experience_description' => [
                'max' => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            'experience_title' => [
                'max' => Constants::EXPERIENCE_TITLE_LENGTH
            ],
            'experience_link' => [
                'max' => Constants::BOOKING_EXPERIENCE_LINK_LENGTH
            ],
            'user_first_name' => [
                'max' => Constants::FIRST_NAME_LENGTH
            ],
            'user_last_name' => [
                'max' => Constants::LAST_NAME_LENGTH
            ],
            'user_email' => [
                'max' => Constants::EMAIL_LENGTH
            ],
            'user_phone' => [
                'max' => Constants::PHONE_LENGTH
            ],
            'user_city' => [
                'max' => Constants::CITY_NAME_LENGTH
            ],
            'user_address' => [
                'max' => Constants::ADDRESS_LENGTH
            ],
            'user_zip_code' => [
                'max' => Constants::ZIP_CODE_LENGTH
            ],
            'meeting_point' => [
                'max' => Constants::EXPERIENCE_MEETING_POINT_LENGTH
            ],
            'captain_name' => [
                'max' => Constants::FIRST_NAME_LENGTH + Constants::LAST_NAME_LENGTH
            ],
            'captain_email' => [
                'max' => Constants::EMAIL_LENGTH
            ],
            'captain_phone' => [
                'max' => Constants::PHONE_LENGTH
            ],
            'deleted_for' => [
                'max' => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            'guest_first_name' => [
                'max' => Constants::FIRST_NAME_LENGTH
            ],
            'guest_last_name' => [
                'max' => Constants::LAST_NAME_LENGTH
            ],
            'guest_place_of_birth' => [
                'max' => Constants::PLACE_OF_BIRTH_LENGTH
            ],
        ];
    }

    public function getCheckoutView(Request $request)
    {
        $request->validate([
            'id' => 'exists:bookings,id'
        ]);

        $booking = Booking::find($request->id);

        $seo = [
            'title' => trans("booking.meta_title"),
            'description' => trans("booking.meta_description"),
            'og_type' => 'website',
            'og_url' => request()->url(),
            'alternates' => [
                'x-default' => route(Constants::R_USERS_BOOKING_CHECKOUT, ['lang' => Constants::USERS_DEFAULT_LANGUAGE, 'id' => $booking->id]),
                'it' => route(Constants::R_USERS_BOOKING_CHECKOUT, ['lang' => 'it', 'id' => $booking->id]),
                'en' => route(Constants::R_USERS_BOOKING_CHECKOUT, ['lang' => 'en', 'id' => $booking->id])
            ],
            'og_images' => []
        ];

        if ($booking->status == Constants::BOOKING_PAYMENT_LINK_SENT && $booking->expired) {
            $booking->status = Constants::BOOKING_EXPIRED;
            $booking->save();
        }

        // Format date according to user locale
        if (isset($booking->experience_date)) {
            $booking->experience_date = Carbon::parse($booking->experience_date)->format(DateHelper::getDateFormat());
        }

        $contactEndDateOfBirth = Carbon::now(Constants::TIMEZONE)->addYears(-Constants::CONTACT_MIN_AGE)->toDateTimeString();
        $adultEndDateOfBirth = Carbon::now(Constants::TIMEZONE)->addYears(-Constants::ADULT_MIN_AGE)->toDateTimeString();
        $childStartDateOfBirth = Carbon::now(Constants::TIMEZONE)->addYears(-Constants::CHILD_MAX_AGE)->addDay(1)->toDateTimeString();
        $childEndDateOfBirth = Carbon::now(Constants::TIMEZONE)->addYears(-Constants::CHILD_MIN_AGE)->toDateTimeString();

        return view(Constants::R_USERS_BOOKING_CHECKOUT, [
            'booking' => $booking,
            'adults' => $booking->adults()->get(),
            'children' => $booking->children()->get(),
            'rules' => $this->validationRules,
            'seo' => $seo,
            'channels' => Constants::ACQUISITION_CHANNELS,
            'contact_end_date' => $contactEndDateOfBirth,
            'adult_end_date' => $adultEndDateOfBirth,
            'child_start_date' => $childStartDateOfBirth,
            'child_end_date' => $childEndDateOfBirth,
        ]);
    }

    /*
     * Validate checkout data and go to pay page
     */
    public function checkout(Request $request)
    {
        $rules = $this->validationRules;

        $request->validate([
            'privacy' => 'required|accepted',
            'user_first_name' => 'required|max:' . $rules['user_first_name']['max'],
            'user_last_name' => 'required|max:' . $rules['user_last_name']['max'],
            'user_email' => 'required|email|max:' . $rules['user_email']['max'],
            'user_phone' => 'required|max:' . $rules['user_phone']['max'],
            'user_place_of_birth' => 'required|max:' . $rules['guest_place_of_birth']['max'],
            'user_birth_date' => 'required|date_format:Y-m-d',
            'user_city' => 'required|max:' . $rules['user_city']['max'],
            'user_address' => 'required|max:' . $rules['user_address']['max'],
            'user_zip_code' => 'required|max:' . $rules['user_zip_code']['max'],
            'guests.*.id' => 'required|exists:guests,id',
            'guests.*.first_name' => 'required|max:' . $rules['guest_first_name']['max'],
            'guests.*.last_name' => 'required|max:' . $rules['guest_first_name']['max'],
            'guests.*.birth_date' => 'required|date_format:Y-m-d',
            'guests.*.place_of_birth' => 'required|max:' . $rules['guest_place_of_birth']['max'],
            'acquisition_channel' => 'required|in:' . implode(",", Constants::ACQUISITION_CHANNELS),
        ]);

        // Save booking data
        $booking = Booking::find($request->id);
        $booking->fill($this->getBookingParams($request));
        $booking->save();

        // Save guests
        foreach ($request->guests as $guest) {
            $g = Guest::find($guest['id']);
            $g->fill([
                'first_name' => $guest['first_name'],
                'last_name' => $guest['last_name'],
                'birth_date' => $guest['birth_date'],
                'place_of_birth' => $guest['place_of_birth']
            ]);
            $g->save();
        }

        $couponValue = null;
        $couponSymbol = null;
        $couponCode = null;

        if (isset($booking->coupon)){
            $couponValue = $booking->coupon->value;
            $couponSymbol = CouponHelper::getTypeSymbol($booking->coupon);
            $couponCode = $booking->coupon->code;
        }

        return view(Constants::R_USERS_BOOKING_PAY, [
            'price_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->price, $booking->currency, 2),
            'discounted_price_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->discounted_price, $booking->currency, 2),
            'discount_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), PriceHelper::getRoundedPrice($booking->discounted_price - $booking->price), $booking->currency, 2),
            'payment_methods' => $booking->payment_methods()->pluck('name')->all(),
            'booking' => $booking,
            'coupon_code' => $couponCode,
            'coupon_value' => $couponValue,
            'coupon_symbol' => $couponSymbol
        ]);
    }

    public function pay(Request $request)
    {
        $request->validate([
            'booking' => 'exists:bookings,id',
            'stripeToken' => 'required',
            'stripeEmail' => 'required'
        ]);

        $booking = Booking::find($request->booking);

        if ($booking->status != Constants::BOOKING_PAYMENT_LINK_SENT) {
            return view(Constants::R_USERS_BOOKING_PAY, [
                'booking' => $booking,
                'payment_methods' => $booking->payment_methods()->pluck('name')->all(),
                'custom_errors' => 'booking_status'
            ]);
        }

        // TODO set paid if price < 0.50 cents (as Stripe cannot accept such amounts).

        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        Stripe::setApiKey(config('services.stripe.secret'));

        try {
            $charge = \Stripe\Charge::create([
                'amount' => $booking->discounted_price * 100,
                'currency' => $booking->currency,
                'description' => $booking->user_email . ", uscita del " . $booking->experience_date,
                'source' => $request->stripeToken,
            ]);
        } catch (Exception $e) {
            return view(Constants::R_USERS_BOOKING_PAY, [
                'booking' => $booking,
                'payment_methods' => $booking->payment_methods()->pluck('name')->all(),
                'custom_errors' => 'payment'
            ]);
        }

        if (!$charge->paid) {
            return view(Constants::R_USERS_BOOKING_PAY, [
                'booking' => $booking,
                'payment_methods' => $booking->payment_methods()->pluck('name')->all(),
                'custom_errors' => 'payment'
            ]);
        }

        // Clear coupon from booking if it was not valid
        if ($booking->price == $booking->discounted_price) {
            $booking->coupon()->dissociate();
        } else {
            $booking->coupon->stock--;
            $booking->coupon->save();
        }

        $booking->status = Constants::BOOKING_PAID;
        $booking->payment_datetime = Carbon::now(Constants::TIMEZONE);
        $booking->payment_method()->associate(PaymentMethod::where('name', 'stripe')->first());
        $booking->save();

        $booking->campaigns()->each(function (Campaign $campaign) use ($booking) {
            $campaign->onBookingPaid($booking);
        });

        \App::setLocale($booking->language);

        // Send email with voucher to user
        Mail::queue((new BookingPaidToGuest($booking))->onQueue(config('queue.connections.sqs.mail')));

        // Send email to administration and support
        Mail::queue((new BookingPaidToAdmin($booking))->onQueue(config('queue.connections.sqs.mail')));

        return view(Constants::R_USERS_BOOKING_THANK_YOU, ['booking' => $booking]);
    }

    public function getBookingParams(Request $request)
    {
        return [
            'user_first_name' => $request->user_first_name,
            'user_last_name' => $request->user_last_name,
            'user_email' => $request->user_email,
            'user_phone' => $request->user_phone,
            'user_place_of_birth' => $request->user_place_of_birth,
            'user_birth_date' => $request->user_birth_date,
            'user_city' => $request->user_city,
            'user_address' => $request->user_address,
            'user_zip_code' => $request->user_zip_code,
            'acquisition_channel' => $request->acquisition_channel
        ];
    }
}