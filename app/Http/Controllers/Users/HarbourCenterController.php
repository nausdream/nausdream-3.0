<?php
/**
 * User: Luca Puddu
 * Date: 01/03/2018
 * Time: 15:35
 */

namespace App\Http\Controllers\Users;

use App\Constants;
use App\Helpers\StaticPagesHelper;
use App\Http\Controllers\Controller;
use App\Models\HarbourCenter;
use App\Models\Redirect;
use App\Models\Seo;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

class HarbourCenterController extends Controller
{
    public function get(Request $request)
    {
        $language = $request->lang;

        $query = Seo::with('harbour_center.experiences')
            ->where('language', $language)
            ->where('query', $request->slug)
            ->first();

        if (!isset($query)) {
            // Search a redirect
            $redirectModel = Redirect::where('old_slug_url', '/' . $language . '/' . Constants::HARBOUR_CENTER_PREFIX . '/' . $request->slug)->first();
            if (isset($redirectModel)) {
                return redirect($redirectModel->new_slug_url, ResponseCodes::HTTP_MOVED_PERMANENTLY);
            }

            abort(ResponseCodes::HTTP_NOT_FOUND);
        }
        // Return 404 if harbour center is deleted (Admin can still see)
        if (!isset($query->harbour_center) || !isset($query->harbour_center->area)) {
            if (!$request->get('isAdmin')) {
                // User is not admin
                abort(ResponseCodes::HTTP_NOT_FOUND);
            }
        }

        // Get harbour center business id
        $businessId = $query->harbour_center->business_id;

        // Get alternate seos for the same harbour center
        $harbourCenterSeos = Seo::where('harbour_center_id', $query->harbour_center->id)
            ->where('duration', $query->duration)
            ->where('boat_type_id', $query->boat_type_id)->get();

        $seo = StaticPagesHelper::getSeo($businessId);

        // Set alternates
        $seo['alternates'] = [];
        $seo['alternates']['x-default'] = '/' . config('app.fallback_locale') . '/' . Constants::HARBOUR_CENTER_PREFIX . '/' . $harbourCenterSeos->where('language', config('app.fallback_locale'))
                ->first()->query;
        foreach ($harbourCenterSeos as $harbourCenterSeo) {
            $seo['alternates'][$harbourCenterSeo->language] = '/' . $harbourCenterSeo->language . '/' . Constants::HARBOUR_CENTER_PREFIX . '/' . $harbourCenterSeo->query;
        }

        // Set og_images
        $images = explode(',', $query->og_images);

        // If db field is empty
        if ($images[0] == "") {
            $seo['og_images'] = [];
        } else {
            $seo['og_images'] = $images;
        }

        // Cover photos
        $covers = [
            'bcn' => [
                'versions' => [
                    '420' => 'b_rgb:000000,o_90,q_60/c_scale,h_500/c_crop,w_420,h_850,x_200,y_100/v1529924058',
                    '800' => 'b_rgb:000000,o_90,q_65,w_800/v1529924058',
                    '1440' => 'b_rgb:000000,o_90,q_65/c_scale,w_1440/c_crop,w_1440,h_500/v1529924058'
                ],
                'cloudinary_version' => '',
                'def' => 'b_rgb:000000,o_90,q_65/c_scale,w_1920/c_crop,w_1920,h_500,g_center,y_100/v1529924058',
            ],
            'cag' => [
                'versions' => [
                    '420' => 'c_scale,h_850/c_crop,w_420,h_457,x_400,y_350,q_80/v1528227967',
                    '768' => 'c_scale,h_850/c_crop,h_457,x_150,y_470,q_80/v1528227967',
                    '1024' => 'c_scale,h_850/c_crop,h_557,y_300,q_80/v1528227967',
                    '1280' => 'c_scale,w_1267/c_crop,h_508,w_1267,y_300,q_80/v1528227967',
                ],
                'cloudinary_version' => '',
                'def' => 'c_scale,w_1903/c_crop,h_508,w_1903,y_600,q_80/v1528227967',
            ],
            'nap' => [
                'versions' => [
                    '420' => 'c_scale,h_500/c_crop,w_420,h_850,x_450,y_135,q_60/v1529924056',
                    '768' => 'c_scale,h_500/c_crop,w_420,h_850,x_450,y_135,q_60/v1529924056',
                    '1024' => 'c_fill,g_center,w_1024,h_650,q_80/v1529924056',
                    '1280' => 'c_scale,w_1280/c_crop,w_1280,x_100,q_60/v1529924056',
                ],
                'cloudinary_version' => '',
                'def' => 'c_scale,w_1920/c_crop,w_1920,x_100,y_100,q_60/v1529924056',
            ],
            'std' => [
                'versions' => [
                    '420' => 'c_fill,g_center,w_420,h_450,q_80/v1525616823',
                    '768' => 'c_fill,g_center,w_768,h_650,q_80/v1525616823',
                    '1024' => 'c_fill,g_center,w_1024,h_650,q_80/v1525616823',
                    '1441' => 'c_fill,g_center,w_1280,h_650,q_80/v1525616823',
                ],
                'cloudinary_version' => '',
                'def' => 'c_scale,w_1920/c_crop,w_1920,x_100,y_325,q_60/v1525616823',
            ],
            'vcr' => [
                'versions' => [
                    '420' => 'c_scale,h_500/c_crop,w_420,h_850,x_450,y_135,q_80/v1529924055',
                    '768' => 'c_scale,w_768/c_crop,w_768,q_80/v1529924055',
                    '1280' => 'c_scale,w_1280/c_crop,w_1280,q_80/v1529924055',
                ],
                'cloudinary_version' => '',
                'def' => 'c_scale,w_1920/c_crop,w_1920,y_50,q_80/v1529924055',
            ],
            'csm' => [
                'versions' => [
                    '420' => 'c_scale,h_550/c_crop,w_420,h_500,g_center,x_350,y_50/q_65/v1529924059',
                    '768' => 'c_scale,w_1024/c_crop,w_768,h_450,g_center/q_65/v1529924059',
                    '1280' => 'c_scale,w_1920/c_crop,w_1280,h_650,g_center,x_200,y_20/q_65/v1529924059',
                ],
                'cloudinary_version' => '',
                'def' => 'c_scale,w_2500/c_crop,w_1920,y_50,q_80,g_center/v1529924059',
            ],
            'ast' => [
                'versions' => [
                    '420' => 'c_fill,g_center,w_420,h_450,q_80/v1526837554',
                    '768' => 'c_fill,g_center,w_768,h_650,q_80/v1526837554',
                    '1024' => 'c_fill,g_center,w_1024,h_650,q_80/v1526837554',
                    '1280' => 'c_fill,g_center,w_1280,h_650,q_80/v1526837554',
                ],
                'cloudinary_version' => '',
                'def' => 'c_fit,w_1920/c_crop,g_xy_center,h_600,w_1920,x_0,y_580,q_65/v1526837554',
            ],
            'spz' => [
                'versions' => [
                    '420' => 'c_scale,h_500/c_crop,w_600,h_850,g_east,x_125,q_80/v1529924058',
                    '769' => 'c_scale,h_500/c_crop,w_768,h_500,g_east,x_125,q_80/v1529924058',
                    '1280' => 'c_scale,w_1280/c_crop,w_1280,h_600,q_80/v1529924058',
                ],
                'cloudinary_version' => '',
                'def' => 'c_scale,w_1920/c_crop,w_1920,y_100,q_80/v1529924058',
            ],
            'tri' => [
                'versions' => [
                    '420' => 'b_rgb:000000,o_90,c_fill,g_center,w_420,h_350,q_80/v1525616824',
                    '768' => 'b_rgb:000000,o_90,c_fill,g_center,w_768,h_350,q_80/v1525616824',
                    '1024' => 'b_rgb:000000,o_90,c_fill,g_center,w_1024,h_650,q_80/v1525616824',
                    '1280' => 'b_rgb:000000,o_90,c_fill,g_center,w_1280,h_650,q_80/v1525616824',
                ],
                'cloudinary_version' => '',
                'def' => 'b_rgb:000000,o_90,c_fill,g_center,w_1920,h_750,q_80/v1525616824',
            ],
            'mar' => [
                'versions' => [
                    '420' => 'b_rgb:000000,o_90,c_scale,h_850/c_crop,w_420,h_457,x_400,y_75,q_80/v1528232028',
                    '768' => 'b_rgb:000000,o_90,c_scale,h_850/c_crop,h_457,x_150,y_100,q_80/v1528232028',
                    '1024' => 'b_rgb:000000,o_90,c_scale,h_950/c_crop,h_557,y_80,x_600,q_80/v1528232028',
                    '1280' => 'b_rgb:000000,o_90,c_scale,w_1267/c_crop,h_508,w_1267,y_75,q_80/v1528232028',
                ],
                'cloudinary_version' => '',
                'def' => 'b_rgb:000000,o_90,c_scale,w_1903/c_crop,h_508,w_1903,y_175,q_80/v1528232028',
            ],
            'phu' => [
                'versions' => [
                    '420' => 'b_rgb:000000,o_95,c_scale,h_457/c_crop,w_420,h_457,x_350,y_75,q_80/v1528231996',
                    '768' => 'c_scale,h_850/c_crop,h_457,x_525,y_100,q_80/v1528231996',
                    '1024' => 'c_scale,h_950/c_crop,h_557,y_80,x_650,q_80/v1528231996',
                    '1280' => 'c_scale,w_1440/c_crop,h_508,w_1267,y_0,q_80/v1528231996',
                ],
                'cloudinary_version' => '',
                'def' => 'c_scale,w_1903/c_crop,h_508,w_1903,y_75,q_80/v1528231996',
            ],
            'kos' => [
                'versions' => [
                    '420' => 'b_rgb:000000,o_90,c_scale,h_850/c_crop,w_420,h_457,x_725,y_250,q_80/v1528232016',
                    '768' => 'b_rgb:000000,o_90,c_scale,h_850/c_crop,h_457,x_650,y_250,q_80/v1528232016',
                    '1024' => 'b_rgb:000000,o_90,c_scale,h_950/c_crop,h_557,y_250,x_700,q_80/v1528232016',
                    '1280' => 'b_rgb:000000,o_90,c_scale,w_1267/c_crop,h_508,w_1267,y_75,q_80/v1528232016',
                ],
                'cloudinary_version' => '',
                'def' => 'b_rgb:000000,o_90,c_scale,w_2440/c_crop,w_1903,h_508,x_600,y_400,q_80/v1528232016',
            ]
        ];

        $covers = $covers[$query->harbour_center->business_id] ?? [
                'versions' => [],
                'cloudinary_version' => '',
                'def' => 'c_fill,g_center,w_1920,h_750,q_80/v1',
            ];

        // Get listings for harbour center
        $listings = $query->harbour_center->experiences
            ->where('is_finished', true)
            ->where('is_searchable', true)
            ->map('App\Transformers\ListingTransformer::transform');

        // Get count of free sale products (where trekksoft id is set)
        $freeSale = 0;
        foreach ($listings as $listing) {
            if ($listing['top']) {
                $freeSale++;
            }
        }
        $notFreeSale = count($listings) - $freeSale;

        return view(Constants::R_USERS_HARBOUR_CENTER, [
            'harbour_center' => $query->harbour_center,
            'listings' => $listings,
            'seo' => $seo,
            'covers' => $covers,
            'free_sale_count' => $freeSale,
            'not_free_sale_count' => $notFreeSale
        ]);
    }

    public function getFromName(Request $request)
    {
        // Validate request
        if (!isset($request->business_id)) {
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        // Get harbour center from business id
        $harbourCenter = HarbourCenter::where('business_id', $request->business_id)->first();

        if (!isset($harbourCenter)) {
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        $lang = \App::getLocale();

        // Get slug url of harbour center
        $seo = $harbourCenter->seos()
            ->where('language', $lang)
            ->where('duration', null)
            ->whereDoesntHave('boat_type')
            ->first();

        if (!isset($seo)) {
            abort(ResponseCodes::HTTP_NOT_FOUND);
        }

        return redirect("{$lang}/" . Constants::HARBOUR_CENTER_PREFIX . "/{$seo->query}");
    }
}