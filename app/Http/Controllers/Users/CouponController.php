<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 07/07/2018
 * Time: 15:07
 */

namespace App\Http\Controllers\Users;

use App\Constants;
use App\Helpers\CouponHelper;
use App\Helpers\PriceHelper;
use App\Models\Booking;
use App\Models\Coupon;
use Illuminate\Http\Request;

class CouponController
{
    public function apply(Request $request)
    {
        $booking = Booking::findOrFail($request->booking);

        if (!isset($request->coupon)) {
            return view(Constants::R_USERS_BOOKING_PAY, [
                'price_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->price, $booking->currency, 2),
                'discounted_price_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->discounted_price, $booking->currency, 2),
                'discount_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->discounted_price - $booking->price, $booking->currency, 2),
                'payment_methods' => $booking->payment_methods()->pluck('name')->all(),
                'booking' => $booking,
                'coupon_code' => null,
                'coupon_value' => null,
                'coupon_symbol' => null,
                'coupon_error' => true,
                'page' => 'booking'
            ]);
        }

        $coupon = Coupon::where('code', $request->coupon)->get()->first();
        if (!isset($coupon)) {
            $booking->coupon()->dissociate();
            $booking->save();
            return view(Constants::R_USERS_BOOKING_PAY, [
                'price_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->price, $booking->currency, 2),
                'discounted_price_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->discounted_price, $booking->currency, 2),
                'discount_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->discounted_price - $booking->price, $booking->currency, 2),
                'payment_methods' => $booking->payment_methods()->pluck('name')->all(),
                'booking' => $booking,
                'coupon_code' => null,
                'coupon_value' => null,
                'coupon_symbol' => null,
                'coupon_error' => true,
                'page' => 'booking'
            ]);
        }

        $booking->coupon()->associate($coupon);
        $booking->save();

        $couponValue = null;
        $couponSymbol = null;
        $couponCode = null;

        if (isset($booking->coupon)) {
            $couponValue = $booking->coupon->value;
            $couponSymbol = CouponHelper::getTypeSymbol($booking->coupon);
            $couponCode = $booking->coupon->code;
        }

        return view(Constants::R_USERS_BOOKING_PAY, [
            'price_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->price, $booking->currency, 2),
            'discounted_price_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->discounted_price, $booking->currency, 2),
            'discount_formatted' => PriceHelper::getFormattedPrice(\App::getLocale(), $booking->discounted_price - $booking->price, $booking->currency, 2),
            'payment_methods' => $booking->payment_methods()->pluck('name')->all(),
            'booking' => $booking,
            'coupon_code' => $couponCode,
            'coupon_value' => $couponValue,
            'coupon_symbol' => $couponSymbol,
            'page' => 'booking'
        ]);
    }
}