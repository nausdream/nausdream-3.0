<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Helpers\LanguageHelper;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Experience;
use App\Models\HarbourCenter;
use App\Models\Redirect;
use App\Models\Seo;
use App\TranslationModels\Language;
use Illuminate\Http\Request;

class AreasController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function getView()
    {
        $areasWithHcs = Area::with([
            'harbour_centers' => function ($q) {
                $q->orderBy('order');
            },
            'harbour_centers.seos'
        ])
            ->orderBy('order')
            ->get();
        $languages = Language::all();

        return view('admin.areas', [
            'areas' => $areasWithHcs,
            'languages' => $languages
        ]);
    }

    public function edit(Request $request)
    {
        $rules = [
            'areas.*.id' => 'exists:areas,id',
            'areas.*.hidden' => 'boolean|nullable',
            'areas.*.order' => 'integer|nullable',
            'seos.*.id' => 'exists:seos,id',
            'seos.*.url' => 'required|max:' . Constants::QUERY_LENGTH,
            'harbour_centers.*.id' => 'exists:harbour_centers,id',
            'harbour_centers.*.hidden' => 'boolean|nullable',
            'harbour_centers.*.order' => 'integer|nullable'
        ];

        // Validate request
        $request->validate($rules);

        // Make urls lowercase, trim spaces and replace spaces with dashes
        foreach ($request->seos as $seo) {
            $seo['url'] = strtolower(preg_replace('/_+|\s+/', '-', trim($seo['url'])));
        }

        // Update areas
        foreach ($request->areas as $id => $area) {
            $newArea = Area::find($id);
            $newArea->order = $area['order'] ?? null;
            $newArea->hidden = $area['hidden'] ?? false;
            $newArea->save();
        }

        // Update harbour_centers
        foreach ($request->harbour_centers as $id => $center) {
            $newCenter = HarbourCenter::find($id);
            $newCenter->order = $center['order'] ?? null;
            $newCenter->hidden = $center['hidden'] ?? false;
            $newCenter->save();
        }

        // Update seos
        foreach ($request->seos as $id => $seo) {
            $newSeo = Seo::find($id);

            $oldQuery = $newSeo->query;

            $newSeo->query = $seo['url'] ?? null;

            // Redirect old url to new one
            if ($oldQuery != $newSeo->query) {
                Redirect::create([
                    'old_slug_url' => '/' . $seo['lang'] . '/' . Constants::HARBOUR_CENTER_PREFIX . '/' . $oldQuery,
                    'new_slug_url' => '/' . $seo['lang'] . '/' . Constants::HARBOUR_CENTER_PREFIX . '/' . $newSeo->query
                ]);
            }

            $newSeo->save();
        }

        // Delete Harbour centers
        if (isset($request->deleted_hcs)) {
            foreach ($request->deleted_hcs as $id) {
                $hc = HarbourCenter::find($id);
                if (isset($hc)){
                    $hc->delete();
                }
            }
        }


        // Delete Areas
        if (isset($request->deleted_areas)) {
            foreach ($request->deleted_areas as $id) {
                $area = Area::find($id);
                if (isset($area)){
                    $area->delete();
                }
            }
        }

        return redirect()->route(Constants::R_ADMIN_AREAS);
    }

    public function addArea(Request $request)
    {
        $request->validate([
            'name' => 'unique:areas,name'
        ]);

        $request->name = strtolower(preg_replace('/-+|\s+/', '_', trim($request->name)));

        Area::create(['name' => $request->name, 'hidden' => true]);

        return redirect()->route(Constants::R_ADMIN_AREAS);
    }

    public function addHarbourCenter(Request $request)
    {
        $request->validate([
            'area_id' => 'exists:areas,id',
            'business_id' => 'unique:harbour_centers,business_id|max:' . Constants::BUSINESS_ID_HARBOUR_CENTER_LENGTH,
            'name' => 'unique:harbour_centers,name|max:' . Constants::HARBOUR_CENTER_NAME_LENGTH,
        ]);

        $request->business_id = strtolower(preg_replace('/-+|\s+/', '_', trim($request->business_id)));
        $request->name = strtolower(preg_replace('/_+|\s+/', '-', trim($request->name)));

        $newHc = HarbourCenter::make([
            'point_a_lat' => 0,
            'point_b_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lng' => 0,
            'business_id' => $request->business_id,
            'name' => $request->name,
            'hidden' => true
        ]);
        $newHc->area()->associate($request->area_id);
        $newHc->save();

        // Create empty seos
        $languages = LanguageHelper::getLanguages();
        foreach ($languages as $language) {
            $seo = Seo::make([
                'query' => 'boat_exp_' . $request->name,
                'boat_type_id' => null,
                'og_images' => null,
                'duration' => null,
                'language' => $language
            ]);
            $seo->harbour_center()->associate($newHc->id);
            $seo->save();
        }

        return redirect()->route(Constants::R_ADMIN_AREAS);
    }
}
