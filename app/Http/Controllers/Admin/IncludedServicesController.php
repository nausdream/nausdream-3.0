<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Http\Controllers\Controller;
use App\Models\IncludedService;
use Illuminate\Http\Request;

class IncludedServicesController extends Controller
{
    public function getView(Request $request)
    {
        return view(Constants::R_ADMIN_INCLUDED_SERVICES, [
            'services' => IncludedService::all(),
            'newServicePath' => route(Constants::R_ADMIN_INCLUDED_SERVICES_ADD),
            'deleteServicePath' => route(Constants::R_ADMIN_INCLUDED_SERVICES_DELETE),
            'newServiceMinLength' => 1,
            'newServiceMaxLength' => Constants::INCLUDED_SERVICE_LENGTH
        ]);
    }

    public function add(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:included_services,name|max:'.Constants::INCLUDED_SERVICE_LENGTH
        ]);

        $service = IncludedService::create(['name' => $request->name]);

        return response(['id' => $service->id, 'name' => $service->name]);
    }

    public function delete(Request $request)
    {
        // Delete service with requested id
        $request->validate([
            "id" => "exists:included_services,id"
        ]);

        $service = IncludedService::findOrFail($request->id);
        $service->delete();

        return response('ok');
    }
}
