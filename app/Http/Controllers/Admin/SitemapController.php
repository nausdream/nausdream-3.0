<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\SitemapHelper;
use App\Http\Controllers\Controller;

class SitemapController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function getView(){
        return view('admin.sitemap.sitemap', ['create' => false]);
    }

    public function generate(){
        SitemapHelper::generate();

        return view('admin.sitemap.sitemap', ['create' => true]);
    }

    public function download(){
        return response()->download(\Storage::path('public/sitemap.xml'));
    }

    public function getXML(){
        return response()->file(\Storage::path('public/sitemap.xml'));
    }
}
