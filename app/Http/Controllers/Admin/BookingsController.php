<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Helpers\PriceHelper;
use App\Http\Controllers\Controller;
use App\Mail\Cancel;
use App\Mail\Quotation;
use App\Models\BoatType;
use App\Models\Booking;
use App\Models\Campaign;
use App\Models\Currency;
use App\Models\Experience;
use App\Models\Guest;
use App\Models\IncludedService;
use App\Models\PaymentMethod;
use App\Models\User;
use App\Services\UserService;
use App\TranslationModels\Language;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;

class BookingsController extends Controller
{
    private $validationRules;
    private $languages;
    private $activeCampaigns;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->languages = Language::all()->pluck('language');
        $this->validationRules = [
            'experience_description' => [
                'max' => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            'experience_title' => [
                'max' => Constants::EXPERIENCE_TITLE_LENGTH
            ],
            'experience_link' => [
                'max' => Constants::BOOKING_EXPERIENCE_LINK_LENGTH
            ],
            'user_first_name' => [
                'max' => Constants::FIRST_NAME_LENGTH
            ],
            'user_last_name' => [
                'max' => Constants::LAST_NAME_LENGTH
            ],
            'user_email' => [
                'max' => Constants::EMAIL_LENGTH
            ],
            'user_phone' => [
                'max' => Constants::PHONE_LENGTH
            ],
            'user_city' => [
                'max' => Constants::CITY_NAME_LENGTH
            ],
            'user_address' => [
                'max' => Constants::ADDRESS_LENGTH
            ],
            'user_zip_code' => [
                'max' => Constants::ZIP_CODE_LENGTH
            ],
            'meeting_point' => [
                'max' => Constants::EXPERIENCE_MEETING_POINT_LENGTH
            ],
            'return_point' => [
                'max' => Constants::EXPERIENCE_MEETING_POINT_LENGTH
            ],
            'captain_name' => [
                'max' => Constants::FIRST_NAME_LENGTH + Constants::LAST_NAME_LENGTH
            ],
            'captain_email' => [
                'max' => Constants::EMAIL_LENGTH
            ],
            'captain_phone' => [
                'max' => Constants::PHONE_LENGTH
            ],
            'deleted_for' => [
                'max' => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            'guest_first_name' => [
                'max' => Constants::FIRST_NAME_LENGTH
            ],
            'guest_last_name' => [
                'max' => Constants::LAST_NAME_LENGTH
            ],
            'guest_place_of_birth' => [
                'max' => Constants::PLACE_OF_BIRTH_LENGTH
            ],
            'notes' => [
                'max' => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            'operator' => [
                'max' => Constants::FIRST_NAME_LENGTH + Constants::LAST_NAME_LENGTH
            ]
        ];
        $this->activeCampaigns = Campaign::where('is_active', 1)->get()->pluck('name', 'id');
    }

    public function add(Request $request)
    {
        $sendEmail = $request->send_email;
        $request->validate($this->getRules());

        // Create new offsite booking
        $booking = Booking::create($this->getBookingParamsFromRequest($request));

        if (!$sendEmail) {
            $booking->status = Constants::BOOKING_DRAFT;
            $booking->save();
        }

        if (isset($booking->experience)) {
            // Save captain data on experience
            if (!isset($booking->experience->captain_name)) {
                $booking->experience->captain_name = $booking->captain_name;
            }
            if (!isset($booking->experience->captain_email)) {
                $booking->experience->captain_email = $booking->captain_email;
            }
            if (!isset($booking->experience->captain_phone)) {
                $booking->experience->captain_phone = $booking->captain_phone;
            }
            $booking->experience->save();

            // Save short description to experience copy
            $experienceCopy = $booking->experience->experience_copies()->where('language', $request->language)->first();
            $experienceCopy->short_description = $booking->experience_description;
            $experienceCopy->save();
        }

        // Save related records
        $booking->included_services()->sync($request->included_services);
        $booking->payment_methods()->sync($request->payment_methods);
        $booking->campaigns()->sync($request->campaigns);

        // Create Adults
        if (isset($request->new_adults)) {
            foreach ($request->new_adults as $adult) {
                $adult['guest_type'] = Constants::TYPE_ADULT;
                $booking->guests()->save(new Guest($adult));
            }
        }
        // Create Children
        if (isset($request->new_children)) {
            foreach ($request->new_children as $child) {
                $child['guest_type'] = Constants::TYPE_CHILD;
                $booking->guests()->save(new Guest($child));
            }
        }

        $request->id = $booking->id;
        // Request is valid, send email
        if ($sendEmail) {
            Mail::queue($this->getQuoteEmail($request, $booking->language)->onQueue(config('queue.connections.sqs.mail')));
        }

        // Create or update user and associate him/her with this booking
        UserService::createOrUpdateFromBooking($booking);

        return view('admin.bookings.created', [
            'email_url' => route(Constants::R_ADMIN_BOOKINGS_EMAIL_BODY, ['id' => $booking->id]),
            'url' => route(Constants::R_ADMIN_BOOKINGS_EDIT_FORM, ['id' => $booking->id]),
            'email_sent' => $request->send_email
        ]);
    }

    /**
     * Display new booking form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAddView()
    {
        $preselectedPaymentMethods = ['stripe'];
        $booking = new Booking(
            [
                "experience_id" => null,
                "boat_type_id" => null,
            ]);

        $experiences = Experience::all()->sortBy('business_id');
        $boatTypes = BoatType::all()->pluck('name', 'id')->all();
        $includedServices = IncludedService::all();
        $paymentMethods = PaymentMethod::all();
        $currencies = Currency::all()->pluck('name', 'id')->all();
        $getProductInfoURL = route(Constants::R_ADMIN_PRODUCTS_GET_COPY);
        $getEmailBody = route(Constants::R_ADMIN_BOOKINGS_EMAIL_BODY_NEW);

        return view(Constants::R_ADMIN_BOOKINGS_NEW, [
            'getProductInfoURL' => $getProductInfoURL,
            'getEmailBody' => $getEmailBody,
            'booking' => $booking,
            'experiences' => $experiences,
            'boatTypes' => $boatTypes,
            'languages' => $this->languages,
            'includedServices' => $includedServices,
            'paymentMethods' => $paymentMethods,
            'preselectedPaymentMethods' => $preselectedPaymentMethods,
            'currencies' => $currencies,
            'validationRules' => $this->validationRules,
            'campaigns' => $this->activeCampaigns
        ]);
    }

    /**
     * Return quotation email
     *
     * @param Request $request
     * @return Quotation
     */
    public function getBookingEmail(Request $request)
    {
        $request->validate([
            'id' => 'exists:bookings'
        ]);

        $booking = Booking::find($request->id);

        $formattedPrice = PriceHelper::getFormattedPrice($booking->language, $booking->price, $booking->currency);

        if (isset($request->id)) {
            $paymentLink = route(Constants::R_USERS_BOOKING_CHECKOUT, ['lang' => $booking->language, 'id' => $request->id]);
        } else {
            $paymentLink = config('app.url');
        }

        $quoteExpirationDateTime = isset($booking->quote_expiration_datetime) ? \Carbon\Carbon::parse($booking->quote_expiration_datetime)->format('Y-m-d H:i') : null;

        $params = [
            'email_heading' => trans('emails_quote.heading', ['date' => $booking->created_at]),
            'experience_title' => $booking->experience_title,
            'experience_link' => $booking->experience_link,
            'user_first_name' => $booking->user_first_name,
            'user_last_name' => $booking->user_last_name,
            'created_at' => $booking->created_at,
            'quote_expiration_datetime' => $quoteExpirationDateTime,
            'experience_description' => $booking->experience_description,
            'experience_date' => $booking->experience_date,
            'adults' => $booking->adults()->get()->count(),
            'children' => $booking->children()->get()->count(),
            'departure_time' => $booking->departure_time,
            'return_time' => $booking->return_time,
            'meeting_point' => $booking->meeting_point,
            'included_services' => $booking->included_services->pluck('name'),
            'raw_price' => $booking->price,
            'price' => $formattedPrice,
            'payment_link' => $paymentLink
        ];

        \App::setLocale($booking->language);

        return new Quotation($params);
    }

    /**
     * Return quotation email for draft booking
     *
     * @param Request $request
     * @return Quotation
     */
    public function getBookingEmailDraft(Request $request)
    {
        return $this->getQuoteEmail($request);
    }

    /**
     * Display list of bookings
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $searchKey = $request->search ?? "";
        $orderBy = $request->order_by ?? "bookings.id";
        $direction = $request->direction ?? 'asc';

        $validOrderBy = [
            'bookings.id',
            'bookings.experience_date',
            'bookings.created_at',
            'bookings.updated_at',
            'bookings.user_first_name',
            'bookings.price',
            'bookings.status',
            'experiences.business_id'
        ];

        $request->validate([
            "search" => "nullable|string|max:30",
            "order_by" => "nullable|string|in:" . implode(",", $validOrderBy),
            "direction" => "nullable|string|in:asc,desc"
        ]);

        $bookings = Booking::where('user_first_name', 'like', '%' . $searchKey . '%')
            ->orWhere('user_last_name', 'like', '%' . $searchKey . '%')
            ->orWhere('bookings.captain_name', 'like', '%' . $searchKey . '%')
            ->orderBy($orderBy, $direction)
            ->paginate(Constants::ADMIN_BOOKINGS_PER_PAGE);

        return view(Constants::R_ADMIN_BOOKINGS_LIST, [
            "bookings" => $bookings,
            "validationRules" => $this->validationRules,
            "languages" => $this->languages
        ]);
    }

    /**
     * Edit booking
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $rules = $this->getRules();
        $rules['id'] = 'exists:bookings';

        $request->validate($this->getRules());
        // Edit booking
        $booking = Booking::find($request->id);

        // Update booking
        $booking->fill($this->getBookingParamsFromRequest($request));
        $booking->save();

        if (isset($booking->experience)) {
            // Save captain data on experience
            if (!isset($booking->experience->captain_name)) {
                $booking->experience->captain_name = $booking->captain_name;
            }
            if (!isset($booking->experience->captain_email)) {
                $booking->experience->captain_email = $booking->captain_email;
            }
            if (!isset($booking->experience->captain_phone)) {
                $booking->experience->captain_phone = $booking->captain_phone;
            }
            $booking->experience->save();

            // Save short description to experience copy
            $experienceCopy = $booking->experience->experience_copies()->where('language', $request->language)->first();
            $experienceCopy->short_description = $booking->experience_description;
            $experienceCopy->save();
        }

        // Save related records
        $booking->included_services()->sync($request->included_services);
        $booking->payment_methods()->sync($request->payment_methods);
        $booking->campaigns()->sync($request->campaigns);

        /*
         * Update/Create/Delete guests
         */
        // Update Adults
        if (isset($request->old_adults)) {
            foreach ($request->old_adults as $adult) {
                Guest::find($adult['id'])->fill($adult)->save();
            }
        }
        // Delete Adults
        if (isset($request->deleted_adults)) {
            Guest::destroy($request->deleted_adults);
        }
        // Create Adults
        if (isset($request->new_adults)) {
            foreach ($request->new_adults as $adult) {
                $adult['guest_type'] = Constants::TYPE_ADULT;
                $booking->guests()->save(new Guest($adult));
            }
        }
        // Update Children
        if (isset($request->old_children)) {
            foreach ($request->old_children as $child) {
                Guest::find($child['id'])->fill($child)->save();
            }
        }
        // Delete Children
        if (isset($request->deleted_children)) {
            Guest::destroy($request->deleted_children);
        }
        // Create Children
        if (isset($request->new_children)) {
            foreach ($request->new_children as $child) {
                $child['guest_type'] = Constants::TYPE_CHILD;
                $booking->guests()->save(new Guest($child));
            }
        }

        // Create or update user and associate him/her with this booking
        UserService::createOrUpdateFromBooking($booking);

        // Request is valid, send email
        if ($request->send_email != "0") {
            // Set status
            $booking->status = Constants::BOOKING_PAYMENT_LINK_SENT;
            $booking->save();

            Mail::queue($this->getQuoteEmail($request, $booking->language)->onQueue(config('queue.connections.sqs.mail')));

            return view('admin.bookings.created', [
                'email_url' => route(Constants::R_ADMIN_BOOKINGS_EMAIL_BODY, ['id' => $booking->id]),
                'url' => route(Constants::R_ADMIN_BOOKINGS_EDIT_FORM, ['id' => $booking->id]),
                'email_sent' => $request->send_email
            ]);
        }

        return redirect()->route(Constants::R_ADMIN_BOOKINGS_EDIT_FORM, ['id' => $booking->id, 'just_saved' => true]);
    }

    /**
     * Display edit page for a specific booking
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditView(Request $request)
    {
        $request->validate([
            'id' => 'exists:bookings'
        ]);

        $booking = Booking::find($request->id);

        $experiences = Experience::all()->sortBy('business_id');
        $boatTypes = BoatType::all()->pluck('name', 'id')->all();
        $includedServices = IncludedService::all();
        $paymentMethods = PaymentMethod::all();
        $currencies = Currency::all()->pluck('name', 'id')->all();
        $getProductInfoURL = route(Constants::R_ADMIN_PRODUCTS_GET_COPY);
        $getEmailBody = route(Constants::R_ADMIN_BOOKINGS_EMAIL_BODY_NEW);

        return view(Constants::R_ADMIN_BOOKINGS_EDIT_FORM, [
            'getProductInfoURL' => $getProductInfoURL,
            'getEmailBody' => $getEmailBody,
            'booking' => $booking,
            'experiences' => $experiences,
            'boatTypes' => $boatTypes,
            'languages' => $this->languages,
            'includedServices' => $includedServices,
            'paymentMethods' => $paymentMethods,
            'preselectedPaymentMethods' => [],
            'currencies' => $currencies,
            'validationRules' => $this->validationRules,
            'just_saved' => $request->just_saved,
            'status' => [
                Constants::BOOKING_PAYMENT_LINK_SENT,
                Constants::BOOKING_PAID,
                Constants::BOOKING_EXPIRED,
                Constants::BOOKING_CANCELED,
                Constants::BOOKING_PARTIALLY_REFUNDED,
                Constants::BOOKING_REFUNDED
            ],
            'campaigns' => $this->activeCampaigns
        ]);
    }

    /**
     * Delete one or more bookings
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $request->validate([
            'booking_delete.*' => 'exists:bookings,id',
            'deleted_for.*' => 'max:' . $this->validationRules['deleted_for']['max']
        ]);
        // Set refunded status on all bookings
        $toBeCanceled = Booking::findMany($request->booking_delete);
        $toBeCanceled->each(function (Booking $booking) use ($request) {
            $oldStatus = $booking->status;
            $booking->status = Constants::BOOKING_CANCELED;
            $booking->deleted_for = $request->deleted_for[$booking->language] ?? null;
            $booking->save();

            $price = PriceHelper::getFormattedPrice($booking->language, $booking->price, $booking->currency);

            // Send email only to paid bookings
            if ($oldStatus == Constants::BOOKING_PAID) {
                // Restock used coupon
                if (isset($booking->coupon)) {
                    $booking->coupon->stock++;
                    $booking->coupon->save();
                }

                \App::setLocale($booking->language);

                $params = [
                    'experience_title' => $booking->experience_title,
                    'experience_link' => $booking->experience_link,
                    'user_first_name' => $booking->user_first_name,
                    'user_last_name' => $booking->user_last_name,
                    'user_email' => $booking->user_email,
                    'updated_at' => $booking->updated_at,
                    'experience_date' => $booking->experience_date,
                    'departure_time' => $booking->departure_time,
                    'price' => $price,
                    'deleted_for' => $booking->deleted_for
                ];

                Mail::queue((new Cancel($params))->onQueue(config('queue.connections.sqs.mail')));
            }

            // Invoke onBookingCanceled event
            $booking->campaigns()->each(function (Campaign $campaign) use ($booking) {
                $campaign->onBookingCanceled($booking);
            });
        });

        return redirect()->route(Constants::R_ADMIN_BOOKINGS_LIST);
    }

    private function getBookingParamsFromRequest(Request $request)
    {
        return [
            'experience_id' => $request->experience_id,
            'experience_title' => $request->experience_title,
            'experience_link' => $request->experience_link,
            'user_first_name' => $request->user_first_name,
            'user_last_name' => $request->user_last_name,
            'user_email' => $request->user_email,
            'user_phone' => $request->user_phone,
            'user_place_of_birth' => $request->user_place_of_birth,
            'user_birth_date' => $request->user_birth_date,
            'user_city' => $request->user_city,
            'user_address' => $request->user_address,
            'user_zip_code' => $request->user_zip_code,
            'captain_name' => $request->captain_name,
            'captain_email' => $request->captain_email,
            'captain_phone' => $request->captain_phone,
            'updated_at' => Carbon::now(),
            'quote_expiration_datetime' => $request->quote_expiration_datetime,
            'experience_description' => $request->experience_description,
            'experience_date' => $request->experience_date,
            'meeting_safety_margin' => $request->experience_meeting_safety_margin,
            'departure_time' => $request->experience_departure_time,
            'return_time' => $request->experience_return_time,
            'meeting_point' => $request->experience_meeting_point,
            'return_point' => $request->experience_return_point,
            'included_services' => $request->included_services,
            'campaigns' => $request->campaigns,
            'price' => $request->price,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'language' => $request->language,
            'currency' => $request->currency,
            'status' => $request->status,
            'refund' => $request->refund,
            'payment_datetime' => $request->payment_datetime,
            'payment_method_id' => $request->payment_method_id,
            'notes' => $request->notes,
            'operator' => $request->operator,
            'contact_channel' => $request->contact_channel,
            'soft_complain' => $request->soft_complain,
            'hard_complain' => $request->hard_complain
        ];
    }

    private function getRules()
    {
        $rules = $this->validationRules;

        return [
            'experience_description' => 'nullable|max:' . $rules['experience_description']['max'],
            'experience_title' => 'nullable|max:' . $rules['experience_title']['max'],
            'experience_link' => 'nullable|max:' . $rules['experience_link']['max'],
            'user_first_name' => 'nullable|max:' . $rules['user_first_name']['max'],
            'user_last_name' => 'nullable|max:' . $rules['user_last_name']['max'],
            'user_email' => 'nullable|email|max:' . $rules['user_email']['max'],
            'user_phone' => 'nullable|max:' . $rules['user_phone']['max'],
            'user_place_of_birth' => 'nullable|max:' . $rules['guest_place_of_birth']['max'],
            'user_birth_date' => 'nullable|date_format:Y-m-d',
            'user_city' => 'nullable|max:' . $rules['user_city']['max'],
            'user_address' => 'nullable|max:' . $rules['user_address']['max'],
            'user_zip_code' => 'nullable|max:' . $rules['user_zip_code']['max'],
            'meeting_point' => 'nullable|max:' . $rules['meeting_point']['max'],
            'return_point' => 'nullable|max:' . $rules['meeting_point']['max'],
            'captain_name' => 'nullable|max:' . $rules['captain_name']['max'],
            'captain_email' => 'nullable|max:' . $rules['captain_email']['max'],
            'captain_phone' => 'nullable|max:' . $rules['captain_phone']['max'],
            'language' => 'exists:mysql_translation.languages,language',
            'currency' => 'exists:currencies,name',
            'boat_type_id' => 'nullable|exists:boat_types,id',
            'experience_id' => 'nullable|exists:experiences,id',
            'adults' => 'nullable|integer',
            'deleted_adults.*' => 'exists:guests,id',
            'old_adults.*.id' => 'exists:guests,id',
            'old_adults.*.first_name' => 'nullable|max:' . $rules['guest_first_name']['max'],
            'old_adults.*.last_name' => 'nullable|max:' . $rules['guest_first_name']['max'],
            'old_adults.*.birth_date' => 'nullable|date_format:Y-m-d',
            'old_adults.*.place_of_birth' => 'nullable|max:' . $rules['guest_place_of_birth']['max'],
            'new_adults.*.first_name' => 'nullable|max:' . $rules['guest_first_name']['max'],
            'new_adults.*.last_name' => 'nullable|max:' . $rules['guest_first_name']['max'],
            'new_adults.*.birth_date' => 'nullable|date_format:Y-m-d',
            'new_adults.*.place_of_birth' => 'nullable|max:' . $rules['guest_place_of_birth']['max'],
            'new_adults.*.guest_type' => 'in:' . Constants::TYPE_ADULT,
            'deleted_children.*' => 'exists:guests,id',
            'children' => 'nullable|integer',
            'old_children.*.id' => 'exists:guests,id',
            'old_children.*.first_name' => 'nullable|max:' . $rules['guest_first_name']['max'],
            'old_children.*.last_name' => 'nullable|max:' . $rules['guest_first_name']['max'],
            'old_children.*.birth_date' => 'nullable|date_format:Y-m-d',
            'old_children.*.place_of_birth' => 'nullable|max:' . $rules['guest_place_of_birth']['max'],
            'new_children.*.first_name' => 'nullable|max:' . $rules['guest_first_name']['max'],
            'new_children.*.last_name' => 'nullable|max:' . $rules['guest_first_name']['max'],
            'new_children.*.birth_date' => 'nullable|date_format:Y-m-d',
            'new_children.*.place_of_birth' => 'nullable|max:' . $rules['guest_place_of_birth']['max'],
            'new_children.*.guest_type' => 'in:' . Constants::TYPE_CHILD,
            'deleted_for' => 'nullable',
            'payment_method_id' => 'exists:payment_methods,id',
            'payment_datetime' => 'nullable|date_format:Y-m-d\TH:i',
            'quote_expiration_datetime' => 'nullable|date_format:Y-m-d\TH:i',
            'experience_date' => 'nullable|date_format:Y-m-d',
            'meeting_safety_margin' => 'nullable|integer',
            'departure_time' => 'nullable|date_format:H:i:s',
            'return_time' => 'nullable|date_format:H:i:s',
            'price' => 'nullable|numeric',
            'lat' => 'nullable|numeric',
            'lng' => 'nullable|numeric',
            'status' => 'nullable|in:' .
                Constants::BOOKING_PAYMENT_LINK_SENT . ',' .
                Constants::BOOKING_PAID . ',' .
                Constants::BOOKING_EXPIRED . ',' .
                Constants::BOOKING_CANCELED . ',' .
                Constants::BOOKING_PARTIALLY_REFUNDED . ',' .
                Constants::BOOKING_REFUNDED,
            'notes' => 'nullable|max:' . $rules['notes']['max'],
            'operator' => 'nullable|max:' . $rules['notes']['max'],
            'contact_channel' => 'nullable|in:' . implode(",", array_keys(Constants::CONTACT_CHANNELS)),
            'soft_complain' => 'nullable|boolean',
            'hard_complain' => 'nullable|boolean'
        ];
    }

    private function getQuoteEmail(Request $request, $language = null)
    {
        $language = $language ?? $request->language ?? \App::getLocale();

        $formattedPrice = PriceHelper::getFormattedPrice($language, $request->price, $request->currency);
        $request->created_at = Carbon::now()->toDateTimeString();

        if (isset($request->included_services)) {
            $request->included_services = array_map(function ($id) {
                return IncludedService::withTrashed()->where('id', $id)->first()->name;
            }, $request->included_services);
        }

        if (isset($request->id)) {
            $paymentLink = route(Constants::R_USERS_BOOKING_CHECKOUT, ['lang' => $language, 'id' => $request->id]);
        } else {
            $paymentLink = config('app.url');
        }

        \App::setLocale($language);

        $params = [
            'experience_title' => $request->experience_title,
            'experience_link' => $request->experience_link,
            'user_first_name' => $request->user_first_name,
            'user_last_name' => $request->user_last_name,
            'user_email' => $request->user_email,
            'created_at' => $request->created_at,
            'quote_expiration_datetime' => $request->quote_expiration_datetime,
            'experience_description' => $request->experience_description,
            'experience_date' => $request->experience_date,
            'adults' => $request->adults,
            'children' => $request->children,
            'departure_time' => $request->experience_departure_time,
            'return_time' => $request->experience_return_time,
            'meeting_point' => $request->experience_meeting_point,
            'included_services' => $request->included_services,
            'raw_price' => $request->price,
            'price' => $formattedPrice,
            'payment_link' => $paymentLink
        ];

        return new Quotation($params);
    }
}
