<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Experience;

class ExperienceCopiesController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public static function getFromExperience(Experience $experience, String $lang){
        $copy = $experience->experience_copies()->where("language", $lang)->first();

        if (!isset($copy)){
            $copy = $experience->experience_copies()->first();
        }

        return $copy;
    }
}
