<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Helpers\PhotoHelper;
use App\Helpers\SeoHelper;
use App\Http\Controllers\Controller;
use App\Models\BoatType;
use App\Models\Currency;
use App\Models\Difficulty;
use App\Models\Experience;
use App\Models\ExperienceCopy;
use App\Models\ExperienceFeedback;
use App\Models\ExperiencePhoto;
use App\Models\HarbourCenter;
use App\Models\IncludedService;
use App\Models\Redirect;
use App\Models\Sentiment;
use App\Models\SpokenLanguage;
use App\Services\PhotoService;
use App\TranslationModels\Language;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductsController extends Controller
{
    private $validationRules;
    private $languages;
    private $weekdays;
    private $primaryLang;
    private $deleteConfirmationValue;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->validationRules = [
            "business_id" => [
                "max" => Constants::BUSINESS_ID_EXPERIENCE_LENGTH
            ],
            "trekksoft_id" => [
                "max" => Constants::TREKKSOFT_ID_LENGTH,
                "optional" => true
            ],
            "captain_name" => [
                "min" => 1,
                "max" => Constants::FIRST_NAME_LENGTH,
                "optional" => true
            ],
            "captain_email" => [
                "min" => 1,
                "max" => Constants::EMAIL_LENGTH,
                "optional" => true
            ],
            "captain_phone" => [
                "min" => 1,
                "max" => Constants::PHONE_LENGTH,
                "optional" => true
            ],
            "boat_length_min" => ["min" => 0, "optional" => true],
            "boat_length_max" => ["min" => 0, "optional" => true],
            "title" => [
                "min" => 10,
                "max" => Constants::EXPERIENCE_TITLE_LENGTH
            ],
            "description" => [
                "max" => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            "meeting_point" => [
                "max" => Constants::EXPERIENCE_MEETING_POINT_LENGTH
            ],
            "features" => [
                "max" => Constants::MYSQL_TEXT_MAX_LENGTH,
                "optional" => true
            ],
            "stop_text" => [
                "max" => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            "stop_duration" => [
                "min" => 0,
                "optional" => true
            ],
            "slug_url" => [
                "max" => Constants::SEO_SLUG_URL_LENGTH
            ],
            "meta_title" => [
                "max" => Constants::SEO_META_TITLE_LENGTH
            ],
            "meta_description" => [
                "max" => Constants::SEO_META_DESCRIPTION_LENGTH
            ],
            "departure_port" => [
                "max" => Constants::PORT_NAME_LENGTH
            ],
            "children_age" => ["min" => 0, "optional" => true],
            "meeting_safety_margin" => [
                "min" => 0
            ],
            "included_services" => ["optional" => true],
            "cover_photo" => [
                "optional" => true
            ],
            "photo" => [
                "max" => Constants::MAX_PHOTO_SIZE,
                "max_quantity" => Constants::MAX_PHOTOS_PER_LISTING
            ],
            "price" => ["min" => 0],
            "feedback_text" => [
                "max" => Constants::FEEDBACK_TEXT_LENGTH,
                "optional" => Constants::FEEDBACK_CUSTOMER_NAME_LENGTH
            ],
            "feedback_user_name" => [
                "max" => Constants::FEEDBACK_CUSTOMER_NAME_LENGTH,
                "optional" => Constants::FEEDBACK_CUSTOMER_NAME_LENGTH
            ]
            ,
            "feedback_score" => [
                "min" => Constants::FEEDBACK_SCORE_MIN,
                "max" => Constants::FEEDBACK_SCORE_MAX,
                "optional" => Constants::FEEDBACK_CUSTOMER_NAME_LENGTH
            ]
        ];

        // Get array of languages
        $this->languages = Language::all()->pluck('language', 'id')->all();

        $this->primaryLang = Constants::ADMIN_DEFAULT_LANGUAGE;

        // Set weekdays
        $this->weekdays = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];

        // Delete confirmation value
        $this->deleteConfirmationValue = 200;
    }

    public function add(Request $request)
    {
        $experience = new Experience();

        $this->editExperience($experience, $request);

        if (!$experience->is_finished) {
            return redirect()->route(Constants::R_ADMIN_PRODUCTS_EDIT_FORM, ["id" => $experience->id, "just_saved" => true]);
        }

        $validExperienceCopy = ExperienceCopiesController::getFromExperience($experience, $this->primaryLang);
        $slugUrl = config('app.url') . $this->primaryLang . "/" . Constants::LISTING_PREFIX . "/" . $validExperienceCopy->slug_url;
        return view('admin.products.created', ["id" => $experience->id,
            "url" => $slugUrl
        ]);
    }

    public function getAddView()
    {
        $experience = new Experience(
            [
                "harbour_center_id" => HarbourCenter::first()->id,
                "boat_type_id" => BoatType::first()->id,
                "difficulty_id" => Difficulty::first()->id
            ]);

        $boatTypes = BoatType::all()->pluck('name', 'id')->all();
        $harbourCenters = HarbourCenter::all()->pluck('business_id', 'id')->all();
        $difficulties = Difficulty::all()->pluck('name', 'id')->all();
        $sentiments = Sentiment::all();
        $includedServices = IncludedService::all();
        $spokenLanguages = SpokenLanguage::all();
        $currencies = Currency::all()->pluck('name', 'id')->all();

        return view(Constants::R_ADMIN_PRODUCTS_NEW, [
            'experience' => $experience,
            'experienceCopies' => $experience->experience_copies()->get(),
            'experienceStops' => $experience->stops()->get(),
            'photos' => $experience->experience_photos()->get(),
            'availableLanguages' => [],
            'boatTypes' => $boatTypes,
            'harbourCenters' => $harbourCenters,
            'languages' => $this->languages,
            'difficulties' => $difficulties,
            'sentiments' => $sentiments,
            'includedServices' => $includedServices,
            'spokenLanguages' => $spokenLanguages,
            'currencies' => $currencies,
            'validationRules' => $this->validationRules,
            'weekdays' => $this->weekdays
        ]);
    }

    public function getCopy(Request $request)
    {
        $request->validate([
            'id' => 'exists:experiences',
            'language' => 'exists:mysql_translation.languages,language'
        ]);

        $experience = Experience::find($request->id);
        if (!isset($experience)) {
            return response("error");
        }

        $copy = $experience->experience_copies()->where('language', $request->language)->first();
        if (!isset($copy)) {
            return response("error");
        }

        $copy->slug_url = config('app.url').$request->language.'/'.Constants::LISTING_PREFIX.'/'.$copy->slug_url;

        return [
            'experience' => $experience,
            'copy' => $copy,
            'services' => $experience->included_services,
            'schedules' => $experience->schedules
        ];
    }

    public function list(Request $request)
    {
        $searchKey = $request->search ?? "";
        $orderBy = $request->order_by ?? "business_id";
        $direction = $request->direction ?? 'asc';
        $language = $request->language ?? $this->primaryLang;

        $request->validate([
            "search" => "nullable|string|max:30",
            "order_by" => "nullable|string|in:business_id,title",
            "direction" => "nullable|string|in:asc,desc"
        ]);

        $experiences = DB::table('experiences')
            ->whereNull('experiences.deleted_at')
            ->leftJoin('experience_copies', function ($join) use ($language) {
                $join->on('experiences.id', '=', 'experience_copies.experience_id')
                    ->where('language', $language)
                    ->whereNull('experience_copies.deleted_at');
            })
            ->select('experiences.business_id', 'experiences.id', 'experiences.is_finished', 'experience_copies.language', 'experience_copies.title', 'experience_copies.slug_url')
            ->where('business_id', 'like', "%$searchKey%")
            ->orWhere('title', 'like', "%$searchKey%")
            ->orderBy($orderBy, $direction)
            ->paginate(Constants::ADMIN_PRODUCTS_PER_PAGE);

        return view(Constants::R_ADMIN_PRODUCTS_LIST, [
            "experiences" => $experiences->appends($request->except('page', 'deleted')),
            "languages" => $this->languages,
            "deleteConfirmationValue" => $this->deleteConfirmationValue
        ]);
    }

    public function delete(Request $request)
    {
        $request->validate([
            "delete_id" => "exists:experiences,id"
        ]);

        try {
            DB::transaction(function () use ($request) {
                $experience = Experience::find($request->delete_id);

                $experience->experience_feedbacks()->delete();
                $experience->experience_copies()->delete();
                $experience->schedules()->delete();
                $experience->stops()->delete();
                $experience->spoken_languages()->detach();
                $experience->included_services()->detach();
                // Delete photos from storage and db
                PhotoHelper::delete($experience->experience_photos()->get());
                $experience->experience_photos()->delete();

                // Delete experience
                $experience->delete();
            });
        } catch (\Exception $e) {
            $errors = [$e];
        }

        return redirect()->route(Constants::R_ADMIN_PRODUCTS_LIST, ["deleted" => true, "errors" => $errors ?? null]);
    }

    public function edit(Request $request)
    {
        if (!isset($request->id)) {
            return view(Constants::R_ADMIN_ERROR, ["e" => "Probabilmente url sbagliato (senza id)"]);
        }

        $experience = Experience::find($request->id);

        if (!isset($experience)) {
            return view(Constants::R_ADMIN_ERROR, ["e" => "L'esperienza che stai cercando di salvare non è più disponibile."]);
        }

        $this->editExperience($experience, $request);

        if (!$experience->is_finished) {
            return redirect()->route(Constants::R_ADMIN_PRODUCTS_EDIT_FORM, ["id" => $experience->id, "just_saved" => true]);
        }

        $validExperienceCopy = ExperienceCopiesController::getFromExperience($experience, $this->primaryLang);
        $slugUrl = config('app.url') . $this->primaryLang . "/" . Constants::LISTING_PREFIX . "/" . $validExperienceCopy->slug_url;
        return view('admin.products.created', ["id" => $experience->id,
            "url" => $slugUrl
        ]);
    }

    public function getEditView(Request $request)
    {
        $experience = Experience::find($request->id);

        if (is_null($experience)) {
            return view(Constants::R_ADMIN_ERROR, ["e" => "Experience not found"]);
        }

        $availableLanguages = $experience->experience_copies()->pluck("language")->all();

        $boatTypes = BoatType::all()->pluck('name', 'id')->all();
        $harbourCenters = HarbourCenter::all()->pluck('business_id', 'id')->all();
        $difficulties = Difficulty::all()->pluck('name', 'id')->all();
        $sentiments = Sentiment::all();
        $includedServices = IncludedService::all();
        $spokenLanguages = SpokenLanguage::all();
        $currencies = Currency::all()->pluck('name', 'id')->all();

        // Get photos with associated link
        $photos = $experience->experience_photos->map(function ($photo) use ($experience) {
            return ["id" => $photo->id, "link" => PhotoHelper::getLink($photo), "is_cover" => $photo->is_cover];
        });

        return view(Constants::R_ADMIN_PRODUCTS_EDIT_FORM, [
            'experience' => $experience,
            'experienceCopies' => $experience->experience_copies()->get(),
            'experienceStops' => $experience->stops()->get(),
            'photos' => $photos,
            'availableLanguages' => $availableLanguages,
            'boatTypes' => $boatTypes,
            'harbourCenters' => $harbourCenters,
            'languages' => $this->languages,
            'difficulties' => $difficulties,
            'sentiments' => $sentiments,
            'includedServices' => $includedServices,
            'spokenLanguages' => $spokenLanguages,
            'currencies' => $currencies,
            'validationRules' => $this->validationRules,
            'weekdays' => $this->weekdays,
            'just_saved' => $request->just_saved
        ]);
    }

    private function isFieldOptional($field)
    {
        return isset($this->validationRules[$field]['optional']) && $this->validationRules[$field]['optional'];
    }

    private function getRules(bool $draft)
    {
        // Define type/length rules
        $rules = [
            "business_id" => "string|max:" . $this->validationRules['business_id']['max'],
            "trekksoft_id" => "string|max:" . $this->validationRules['trekksoft_id']['max'],
            "harbour_center" => "exists:harbour_centers,business_id",
            "captain_name" => "string|min:".$this->validationRules['captain_name']['min'] . "|max:".$this->validationRules['captain_name']['max'],
            "captain_email" => "email|min:".$this->validationRules['captain_email']['min'] . "|max:".$this->validationRules['captain_email']['max'],
            "captain_phone" => "string|min:".$this->validationRules['captain_phone']['min'] . "|max:".$this->validationRules['captain_phone']['max'],
            "boat_type" => "exists:boat_types,name",
            "boat_length_min" => "integer|min:" . $this->validationRules['boat_length_min']['min'],
            "boat_length_max" => "integer|min:" . $this->validationRules['boat_length_min']['min'],
            "departure_port" => "string|max:" . $this->validationRules['departure_port']['max'],
            'lat' => 'numeric|between:-' . Constants::MAX_LATITUDE . ',' . Constants::MAX_LATITUDE,
            'lng' => 'numeric|between:-' . Constants::MAX_LONGITUDE . ',' . Constants::MAX_LONGITUDE,
            "meeting_safety_margin" => "integer|min:" . $this->validationRules['meeting_safety_margin']['min'],
            "schedules.*.departure_time" => "date_format:H:i",
            "schedules.*.arrival_time" => "date_format:H:i",
            "children_age" => "integer|min:" . $this->validationRules["children_age"]["min"],
            "difficulty" => "exists:difficulties,name",
            "sentiments.*" => "exists:sentiments,id",
            "included_services.*" => "exists:included_services,id",
            "spoken_languages.*" => "exists:spoken_languages,id",
            "weekdays.*" => [Rule::in($this->weekdays)],
            "cover_photo" => "integer",
            "cover_photo_type" => "in:old,new",
            "price" => "numeric|min:" . $this->validationRules["price"]["min"],
            "currency" => "exists:currencies,name",
            "photo.*" => "image|max:" . $this->validationRules["photo"]["max"],
            "feedback.*.user_name" => "string|max:" . $this->validationRules['feedback_user_name']['max'],
            "feedback.*.score" => "integer|min:" . $this->validationRules['feedback_score']['min'] . "|max:" . $this->validationRules['feedback_score']['max'],
            "feedback.*.text" => "string|max:" . $this->validationRules['feedback_score']['max']
        ];

        $requiredLanguages = [$this->primaryLang];
        // Add validation rules for localized fields
        foreach ($this->languages as $language) {
            $rules[$language . '.title'] = "string|max:" . $this->validationRules['title']['max'];
            $rules[$language . '.description'] = "string|max:" . $this->validationRules['description']['max'];
            $rules[$language . '.meeting_point'] = "string|max:" . $this->validationRules['meeting_point']['max'];
            $rules[$language . '.features'] = "nullable|string|max:" . $this->validationRules['features']['max'];
            $rules[$language . '.stops.*.text'] = "nullable|string|max:" . $this->validationRules['stop_text']['max'];
            $rules[$language . '.stops.*.duration'] = "nullable|integer|min:" . $this->validationRules['stop_duration']['min'];
            $rules[$language . '.slug_url'] = "string|max:" . $this->validationRules['slug_url']['max'];
            $rules[$language . '.meta_title'] = "string|max:" . $this->validationRules['meta_title']['max'];
            $rules[$language . '.meta_description'] = "string|max:" . $this->validationRules['meta_description']['max'];

            // If language is required and admin is saving in "publish mode"
            if (in_array($language, $requiredLanguages) && !$draft) {
                $rules[$language . '.stops.1.text'] = "required"; // Only the first stop for requiredLanguage is required
            } // If language is not required
            else {
                $rules[$language . '.title'] .= "|nullable";
                $rules[$language . '.description'] .= "|nullable";
                $rules[$language . '.meeting_point'] .= "|nullable";
                $rules[$language . '.slug_url'] .= "|nullable";
                $rules[$language . '.meta_title'] .= "|nullable";
                $rules[$language . '.meta_description'] .= "|nullable";
            }
        }

        // Make all fields nullable if "draft mode"
        if ($draft) {
            foreach ($rules as $ruleName => $rule) {
                // Rule may also be of array type
                if (is_string($rule)) {
                    $rules[$ruleName] .= "|nullable";
                } else {
                    $rules[] = "nullable";
                }

            }
        } else {
            $rules['business_id'] .= $this->isFieldOptional('business_id') ? '|nullable' : '|required';
            $rules['trekksoft_id'] .= $this->isFieldOptional('trekksoft_id') ? '|nullable' : '|required';
            $rules['harbour_center'] .= $this->isFieldOptional('harbour_center') ? '|nullable' : '|required';
            $rules['captain_name'] .= $this->isFieldOptional('captain_name') ? '|nullable' : '|required';
            $rules['captain_email'] .= $this->isFieldOptional('captain_email') ? '|nullable' : '|required';
            $rules['captain_phone'] .= $this->isFieldOptional('captain_phone') ? '|nullable' : '|required';
            $rules['boat_type'] .= $this->isFieldOptional('boat_type') ? '|nullable' : '|required';
            $rules['boat_length_min'] .= $this->isFieldOptional('boat_length_min') ? '|nullable' : '|required';
            $rules['boat_length_max'] .= $this->isFieldOptional('boat_length_max') ? '|nullable' : '|required';
            $rules['departure_port'] .= $this->isFieldOptional('departure_port') ? '|nullable' : '|required';
            $rules['lat'] .= $this->isFieldOptional('lat') ? '|nullable' : '|required';
            $rules['lng'] .= $this->isFieldOptional('lng') ? '|nullable' : '|required';
            $rules['meeting_safety_margin'] .= $this->isFieldOptional('meeting_safety_margin') ? '|nullable' : '|required';
            $rules['schedules.*.departure_time'] .= '|nullable';
            $rules['schedules.*.arrival_time'] .= '|nullable';
            $rules['schedules.1.departure_time'] = 'required'; // Only the first entry is required
            $rules['schedules.1.arrival_time'] = 'required'; // Only the first entry is required
            $rules['children_age'] .= $this->isFieldOptional('children_age') ? '|nullable' : '|required';
            $rules['difficulty'] .= $this->isFieldOptional('difficulty') ? '|nullable' : '|required';
            $rules['sentiments.*'] .= $this->isFieldOptional('sentiment') ? '|nullable' : '|required';
            $rules['included_services.*'] .= '|nullable';
            $rules['spoken_languages.*'] .= '|required';
            $rules['weekdays.*'][] = 'required';
            $rules['cover_photo'] .= $this->isFieldOptional('cover_photo') ? '|nullable' : '|required';
            $rules['cover_photo_type'] .= $this->isFieldOptional('cover_photo_type') ? '|nullable' : '|required';
            $rules['price'] .= $this->isFieldOptional('price') ? '|nullable' : '|required';
            $rules['currency'] .= $this->isFieldOptional('currency') ? '|nullable' : '|required';
            $rules['photo.*'] .= '|required';
            $rules['feedback.*.user_name'] .= '|nullable';
            $rules['feedback.*.score'] .= '|nullable';
            $rules['feedback.*.text'] .= '|nullable';
        }

        return $rules;
    }

    private function editExperience(Experience $experience, Request $request)
    {
        // Detect if saving in "draft mode" or "publishing mode"
        $draft = $request->draft == "true";

        $rules = $this->getRules($draft);

        // Validate request
        $request->validate($rules);

        // Request is valid
        $experience->save();

        // Request is valid, edit experience
        $experienceFields = ["business_id", "trekksoft_id", "captain_name", "captain_email", "captain_phone", "boat_length_min", "boat_length_max",
            "departure_port", "lat", "lng", "meeting_safety_margin", "children_age", "price", "currency"];
        foreach ($experienceFields as $experienceField) {
            if (isset($request[$experienceField])) {
                $experience->fill([$experienceField => $request[$experienceField]]);
            };
        }
        // Manual save
        if (isset($request->weekdays)) {
            $experience->weekdays = implode(",", $request->weekdays);
        }

        // Add relationships
        if (isset($request->harbour_center)) {
            $experience->harbour_center()->associate(HarbourCenter::where("business_id", $request->harbour_center)->first());
        }
        if (isset($request->boat_type)) {
            $experience->boat_type()->associate(BoatType::where("name", $request->boat_type)->first());
        }
        if (isset($request->difficulty)) {
            $experience->difficulty()->associate(Difficulty::where("name", $request->difficulty)->first());
        }
        $experience->administrator()->associate(Auth::guard(Constants::GUARD_ADMIN)->user());

        /**
         * Save localized experience in each language
         *
         * Create a copy of the ExperienceCopy only if the slug url is being modified on the old ExperienceCopy
         * or if no ExperienceCopies are present for that experience.
         *
         * Else, just overwrite the old copy.
         */
        foreach ($this->languages as $language) {
            // Get experience copy in $language. If it doesn't exist, and user is setting at least a field of the new language, create it
            $oldExperienceCopy = $experience->experience_copies()->where("language", $language)->first();
            $newSlug = false;
            $redirectToNew = false;

            // If user is not trying to create a new experienceCopy, don't create it and skip to the next language
            if (!isset($oldExperienceCopy)) {
                if (isset($request[$language]["title"]) || isset($request[$language]["description"]) || isset($request[$language]["meeting_point"])
                    || isset($request[$language]["features"]) || isset($request[$language]["slug_url"])
                    || isset($request[$language]["meta_title"]) || isset($request[$language]["meta_description"])) {
                    $newExperienceCopy = new ExperienceCopy(["experience_id" => $experience->id]);
                    $newSlug = true;
                } else {
                    continue;
                }
            } else if ($request[$language]["slug_url"] != $oldExperienceCopy->slug_url) {
                $newExperienceCopy = $oldExperienceCopy;
                $newSlug = true;
                $oldSlugCopy = $oldExperienceCopy->slug_url;

                if (isset($oldExperienceCopy->slug_url) && $oldExperienceCopy->slug_url != "" &&
                    isset($request[$language]["slug_url"]) && $request[$language]["slug_url"] != ""){
                    // Redirect old slug_url to new
                    $redirectToNew = true;
                }
            } else {
                $newExperienceCopy = $oldExperienceCopy;
            }

            // Save slug url appending number if already present in the db
            $i = null;
            if ($newSlug) {
                $newExperienceCopy->slug_url = SeoHelper::getSlugUrl($request[$language]["slug_url"], null);
                do {
                    try {
                        if (isset($i)) {
                            $newExperienceCopy->slug_url .= '-' . $i;
                        }
                        $newExperienceCopy->save();
                        $failed = false;
                    } catch (\Exception $e) {
                        !isset($i) ? $i = 1 : $i++;
                        $failed = true;
                    }
                } while ($failed);

                if ($redirectToNew) {
                    // Create new redirect
                    $oldSlugCopy = '/' . $language . '/' . Constants::LISTING_PREFIX . '/' . $oldSlugCopy;
                    $newSlugUrl = '/' . $language . '/' . Constants::LISTING_PREFIX . '/' . $newExperienceCopy->slug_url;
                    Redirect::create(['old_slug_url' => $oldSlugCopy, 'new_slug_url' => $newSlugUrl]);
                }
            }

            $newExperienceCopy->fill([
                "language" => $language,
                "title" => $request[$language]["title"],
                "description" => $request[$language]["description"],
                "meeting_point" => $request[$language]["meeting_point"],
                "features" => $request[$language]["features"],
                "meta_title" => $request[$language]["meta_title"],
                "meta_description" => $request[$language]["meta_description"]
            ]);
            $newExperienceCopy->save();
        }

        // Delete old stops
        $experience->stops()->forceDelete();
        // Add new stops
        foreach ($this->languages as $language) {
            // Save new stops
            $currentStopNumber = 1;
            $stops = $request[$language]['stops'];
            // Reset indices to 0, 1, ecc
            reset($stops);

            foreach ($stops as $stop) {
                if (isset($stop['text'])) {
                    $experience->stops()->create([
                        "language" => $language,
                        "stop_number" => $currentStopNumber,
                        "text" => $stop['text'],
                        "duration" => $stop['duration']
                    ]);

                    $currentStopNumber++;
                }
            }
        }

        // Delete old schedules
        $experience->schedules()->forceDelete();
        // Add schedules (departure/arrival times)
        foreach ($request->schedules as $schedule) {
            if (isset($schedule['departure_time']) && isset($schedule['arrival_time'])) {

                $experience->schedules()->create([
                    "departure_time" => $schedule['departure_time'],
                    "arrival_time" => $schedule['arrival_time']
                ]);
            }
        }

        // Delete old feedbacks
        $experience->experience_feedbacks()->forceDelete();
        // Add user feedbacks
        foreach ($request->feedbacks as $feedback) {
            if (isset($feedback['user_name']) && isset($feedback['score'])) {
                $newFeedback = new ExperienceFeedback([
                    "customer_name" => $feedback['user_name'],
                    "score" => $feedback['score']
                ]);

                if (isset($feedback['text'])) {
                    $newFeedback->text = $feedback['text'];
                }

                $experience->experience_feedbacks()->save($newFeedback);
            }
        }

        // Detach old included services, spoken languages and sentiments
        $experience->included_services()->detach();
        $experience->spoken_languages()->detach();
        $experience->sentiments()->detach();
        // Associate included services, spoken languages and sentiments
        if (isset($request->included_services)) {
            $experience->included_services()->attach($request->included_services);
        }
        if (isset($request->spoken_languages)) {
            $experience->spoken_languages()->attach($request->spoken_languages);
        }
        if (isset($request->sentiments)) {
            $experience->sentiments()->attach($request->sentiments);
        }

        // Photos
        // Get cover photo id
        $isCoverNew = $request->cover_photo_type == "new";
        // Remove deleted photos from storage
        PhotoHelper::delete($experience->experience_photos()->whereNotIn("id", $request->old_photos ?? [])->get());
        // Remove deleted photos from db
        $experience->experience_photos()->whereNotIn("id", $request->old_photos ?? [])->delete();

        // Create photo records and upload them
        if (isset($request->new_photos)) {
            // Reorder array key from 0 to n
            $resetPhotos = $request->new_photos;
            reset($resetPhotos);

            foreach ($resetPhotos as $count => $photo) {
                $newPhoto = ExperiencePhoto::create();

                // Photo may be cover only if the cover photo is among the new ones
                $newPhoto->is_cover = $isCoverNew && ($request->cover_photo == $count);

                $experience->experience_photos()->save($newPhoto);
                $publicId = PhotoHelper::getPublicId($newPhoto);
                PhotoService::upload($photo, $publicId);
            }
        }

        // Reset is_cover on all the other photo
        $experience->experience_photos()->update(["is_cover" => false]);

        // Set first photo as default
        $newCoverPhoto = ExperiencePhoto::find($request->cover_photo);
        if (!isset($newCoverPhoto)) {
            $newCoverPhoto = $experience->experience_photos()->first();
        }
        // Save new cover photo
        if (isset($newCoverPhoto)) {
            $newCoverPhoto->is_cover = true;
            $newCoverPhoto->save();
        }

        // If publishing
        $experience->is_finished = !$draft;

        $experience->save();
    }
}
