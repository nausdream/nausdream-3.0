<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Http\Controllers\Auth\AccountKitAuthenticatable;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Controller;
use App\Models\Administrator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdminAuthController extends Controller implements AccountKitAuthenticatable
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * @return Response
     */
    public function onFbResponseError()
    {
        return response()->view(Constants::R_ADMIN_LOGIN, ['e' => 'There was some problem with the request.'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return Response
     */
    public function onMissingModelError(){
        return response()->view(Constants::R_ADMIN_LOGIN, ['e' => 'Not authorized'], Response::HTTP_FORBIDDEN);
    }

    /**
     * @param string $phone
     * @return Administrator
     */
    public function getFromPhone(string $phone)
    {
        return Administrator::where('phone', $phone)->first();
    }

    /**
     * @return Response
     */
    public function onSuccess()
    {
        return redirect()->route(Constants::R_ADMIN_HOME);
    }

    /**
     * Get the named route where the user will be redirected upon login
     *
     * @return Response
     */
    public function getDefaultRoute()
    {
        return redirect()->route(Constants::R_ADMIN_HOME);
    }

    /**
     * @return string
     */
    public function getGuard()
    {
        return Constants::GUARD_ADMIN;
    }

    public function check(Request $request)
    {
        return AuthController::check($request, $this);
    }


}
