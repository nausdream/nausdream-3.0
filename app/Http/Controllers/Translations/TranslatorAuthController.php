<?php

namespace App\Http\Controllers\Translations;

use App\Constants;
use App\Http\Controllers\Auth\AccountKitAuthenticatable;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Controller;
use App\TranslationModels\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TranslatorAuthController extends Controller implements AccountKitAuthenticatable
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * @return Response
     */
    public function onFbResponseError()
    {
        return response()->view(Constants::R_TRANSLATOR_ERROR, ['e' => 'There was some problem with the request.'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return Response
     */
    public function onMissingModelError(){
        return response()->view(Constants::R_TRANSLATOR_ERROR, ['e' => 'Not authorized'], Response::HTTP_FORBIDDEN);
    }

    /**
     * @param string $phone
     * @return User
     */
    public function getFromPhone(string $phone)
    {
        return User::where('phone', $phone)->first();
    }

    /**
     * @return Response
     */
    public function onSuccess()
    {
        return redirect()->route(Constants::R_TRANSLATOR_HOME);
    }

    /**
     * Get the named route where the user will be redirected upon login
     *
     * @return Response
     */
    public function getDefaultRoute()
    {
        return redirect()->route(Constants::R_TRANSLATOR_HOME);
    }

    /**
     * @return string
     */
    public function getGuard()
    {
        return Constants::GUARD_TRANSLATOR;
    }

    public function check(Request $request)
    {
        return AuthController::check($request, $this);
    }
}
