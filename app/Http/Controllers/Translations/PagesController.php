<?php

namespace App\Http\Controllers\Translations;

use App\Constants;
use App\Helpers\TranslationHelper;
use App\Http\Controllers\Controller;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\SentenceTranslation;
use Gate;
use Illuminate\Http\Request;
use Auth;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;
use Illuminate\Support\Facades\URL;

class PagesController extends Controller
{

    private $languages;
    private $deleteConfirmationValue;
    private $pageNameRegex;
    private $validationRules;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Get array of languages
        $this->languages = Language::all()->pluck('language', 'id')->all();

        // Page name validator
        $this->pageNameRegex = '^[a-zA-Z0-9_ ]*$';

        // Delete confirmation value
        $this->deleteConfirmationValue = 200;

        // Validation rules
        $this->validationRules = [
            'sentence' => [
                'min' => 2,
                'max' => Constants::SENTENCE_NAME_LENGTH
            ],
            'sentence_translation' => [
                'max' => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            'page' => [
                'min' => 2,
                'max' => Constants::PAGE_NAME_LENGTH
            ]
        ];
    }

    /**
     * Add new page
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request)
    {
        // Only translator admin can add pages
        if (Gate::forUser(Auth::guard(Constants::GUARD_TRANSLATOR)->user())->denies('add-translations-page')) {
            return response("Not authorized.", ResponseCodes::HTTP_FORBIDDEN);
        }

        // Trim string, convert to lowercase, replace multiple spaces or undescores with single underscore
        $request->name = preg_replace('/_+/', '_', preg_replace('/ +/', "_", strtolower(trim($request->name))));

        $rules = [
            'name' => [
                'required',
                'string',
                "regex:/$this->pageNameRegex/", // Only letters, numbers, underscores, spaces
                'min:' . $this->validationRules['page']['min'],
                'max:' . $this->validationRules['page']['max'],
                'unique:mysql_translation.pages'
            ]
        ];

        $request->validate($rules);

        // Save new page
        $page = Page::create(['name' => $request->name]);

        // Redirect to just created page
        return redirect()->route(Constants::R_TRANSLATOR_PAGES_EDIT_FORM, ['id' => $page->id]);
    }

    /**
     * Get view for list of pages
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $searchKey = $request->search ?? "";
        $orderBy = $request->order_by ?? "name";
        $direction = $request->direction ?? 'asc';

        $pages = Page::where("name", "like", "%$searchKey%")
            ->orderBy($orderBy, $direction)
            ->paginate(Constants::ADMIN_PAGES_PER_PAGE);

        return view(Constants::R_TRANSLATOR_PAGES_LIST, [
            "pages" => $pages->appends($request->except('page', 'deleted')),
            "deleteConfirmationValue" => $this->deleteConfirmationValue,
            "admin" => Auth::guard(Constants::GUARD_TRANSLATOR)->user()->is_admin,
            "pageNameLength" => $this->validationRules['page']['max'],
            "pageNameRegex" => $this->pageNameRegex
        ]);
    }

    /**
     * Delete a page
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        // Only translator admin can delete a page
        if (Gate::forUser(Auth::guard(Constants::GUARD_TRANSLATOR)->user())->denies('delete-translations-page')) {
            return response("Not authorized.", ResponseCodes::HTTP_FORBIDDEN);
        }

        $rules = [
            "delete_id" => "exists:mysql_translation.pages,id"
        ];

        $page = Page::find($request->delete_id);
        $pageName = $page->name;

        // Validate request
        $request->validate($rules);

        // Delete all sentences for the page
        $page->sentences()->forceDelete();

        // Delete page
        $page->forceDelete();

        return redirect()->route(Constants::R_TRANSLATOR_PAGES_LIST, ["deleted" => $pageName]);
    }

    /**
     * Edit all sentences translations of a page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $rules = [
            'id' => 'exists:mysql_translation.pages',
            'language' => 'exists:mysql_translation.languages,id',
            'sentences' => 'nullable|page_sentences_exist:' . $request->id,
            'sentences.*' => 'nullable|string|max:' . $this->validationRules['sentence_translation']['max']
        ];

        $request->validate($rules);

        foreach ($request->sentences as $sentenceId => $newText) {
            if (!isset($newText)) continue;
            // Get pre-existing translation
            $sentenceTranslation = SentenceTranslation::where('sentence_id', $sentenceId)
                ->where('language_id', $request->language)->first();

            // Create new translation if it didn't exist yet
            if (!isset($sentenceTranslation)) {
                $sentenceTranslation = new SentenceTranslation();
            }

            $sentenceTranslation->sentence_id = $sentenceId;
            $sentenceTranslation->language_id = $request->language;
            $sentenceTranslation->text = $newText;
            $sentenceTranslation->save();
        }

        return redirect()->route(Constants::R_TRANSLATOR_PAGES_EDIT_FORM, [
            "id" => $request->id,
            "destination" => Language::find($request->language)->language
        ]);
    }

    /**
     * Get view for editing a page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditView(Request $request)
    {
        $defaultLanguage = Language::first()->language;

        $request->origin = $request->origin ?? $defaultLanguage;
        $request->destination = $request->destination ?? $defaultLanguage;

        $rules = [
            'origin' => 'exists:mysql_translation.languages,language',
            'destination' => 'exists:mysql_translation.languages,language',
        ];

        $request->validate($rules);

        $originLanguage = Language::where('language', $request->origin)->first()->id;
        $destinationLanguage = Language::where('language', $request->destination)->first()->id;

        $page = Page::find($request->id);

        if (!isset($page)) {
            return response("Not found", ResponseCodes::HTTP_NOT_FOUND);
        }

        // Get sentences translations in both origin and destination language
        $sentences = $page->sentences()->get();

        return view(Constants::R_TRANSLATOR_PAGES_EDIT_FORM, [
            'languages' => $this->languages,
            'sentences' => $sentences,
            'page' => $page,
            'origin' => $originLanguage,
            'destination' => $destinationLanguage,
            'newSentencePath' => URL::route("translator.sentences.new", [], false),
            'deleteSentencePath' => URL::route("translator.sentences.delete", [], false),
            'admin' => Auth::guard(Constants::GUARD_TRANSLATOR)->user()->is_admin,
            'newSentenceMinLength' => $this->validationRules['sentence']['min'],
            'newSentenceMaxLength' => $this->validationRules['sentence']['max'],
            'sentenceTranslationMaxLength' => $this->validationRules['sentence_translation']['max']
        ]);
    }

    /**
     * Add a new sentence to the page
     *
     * @param Request $request
     * @return \Illuminate\Support\MessageBag
     */
    public function addSentence(Request $request)
    {
        // Only translator admin can add a sentence
        if (Gate::forUser(Auth::guard(Constants::GUARD_TRANSLATOR)->user())->denies('add-sentence')) {
            return response("Not authorized.", ResponseCodes::HTTP_FORBIDDEN);
        }

        $sentenceHelper = new TranslationHelper();

        return $sentenceHelper->addSentence($request);
    }

    /**
     * Delete a sentence from a page
     *
     * @param Request $request
     * @return \Illuminate\Support\MessageBag
     */
    public function deleteSentence(Request $request)
    {
        // Only translator admin can delete a sentence
        if (Gate::forUser(Auth::guard(Constants::GUARD_TRANSLATOR)->user())->denies('delete-sentence')) {
            return response("Not authorized.", ResponseCodes::HTTP_FORBIDDEN);
        }

        $sentenceHelper = new TranslationHelper();

        return $sentenceHelper->deleteSentence($request);
    }
}
