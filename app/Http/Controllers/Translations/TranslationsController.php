<?php

namespace App\Http\Controllers\Translations;

use App\Http\Controllers\Controller;
use App\TranslationModels\Language;
use App\TranslationModels\Page;

class TranslationsController extends Controller
{
    private static $translations = [];

    /**
     * Get email translations
     *
     * @param string $pageName
     * @param string $languageName
     * @return array
     */

    public static function getTranslations(string $pageName, string $languageName)
    {
        // Fetch page and language objects
        $page = Page::where('name', $pageName)->first();

        if (isset(self::$translations[$pageName]) && isset(self::$translations[$pageName][$languageName])){
            return self::$translations[$pageName][$languageName];
        }

        $language = Language::where('language', $languageName)->first();
        $fallbackLanguage = Language::where('language', \Lang::getFallback())->first();

        $data = [];

        // Get all sentences for this page
        if (isset($page)) {

            // Get all sentenceTranslation for the provided page and language
            foreach ($page->sentences as $sentence) {

                $sentenceTranslation = $sentence->sentence_translations()->where('language_id', $language->id)->get()->first();

                if (!isset($sentenceTranslation)) {
                    $sentenceTranslation = $sentence->sentence_translations()->where('language_id', $fallbackLanguage->id)->get()->first();
                }

                if (isset($sentenceTranslation->text)) {
                    $text = $sentenceTranslation->text;
                } else {
                    $text = $sentence->name;
                }

                $data[$sentence->name] = $text;
            }
        }

        self::$translations[$pageName][$languageName] = $data;

        return $data;
    }
}
