<?php
/**
 * User: Luca Puddu
 * Date: 05/02/2018
 * Time: 15:23
 */

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;

class AuthController
{
    public static function check(Request $request, AccountKitAuthenticatable $accountKitUser)
    {
        $onSuccess = $request->input('to');
        $code = $request->input('code') ?? '';

        $accountKitController = new AccountKitController();

        $phone = $accountKitController->login($code);

        // If there was a problem with Fb request
        if ($phone instanceof Response) {
            return $accountKitUser->onFbResponseError();
        }

        // Check if user with phone exist
        $user = $accountKitUser->getFromPhone($phone);

        // Check if user exists
        if (!isset($user)) {
            return $accountKitUser->onMissingModelError();
        }

        // On successful login
        Auth::guard($accountKitUser->getGuard())->login($user);

        if (!isset($onSuccess)) {
            return $accountKitUser->getDefaultRoute();
        }

        return redirect($onSuccess);
    }
}