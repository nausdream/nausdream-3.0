<?php
/**
 * User: Luca Puddu
 * Date: 05/02/2018
 * Time: 15:16
 */

namespace App\Http\Controllers\Auth;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Response;

interface AccountKitAuthenticatable
{

    /**
     * @return string
     */
    public function getGuard();

    /**
     * @return Response
     */
    public function onFbResponseError();

    /**
     * @return Response
     */
    public function onMissingModelError();


    /**
     * @return Response
     */
    public function onSuccess();

    /**
     * Get the named route where the user will be redirected upon login
     *
     * @return Response
     */
    public function getDefaultRoute();

    /**
     * @param string $phone
     * @return Authenticatable
     */
    public function getFromPhone(string $phone);
}