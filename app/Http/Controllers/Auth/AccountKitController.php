<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 13/01/2018
 * Time: 20:39
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\RequestService;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class AccountKitController extends Controller
{
    protected $me_endpoint_base_url;
    protected $token_exchange_base_url;
    protected $app_access_token;
    protected $grant_type = 'authorization_code';

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        $this->me_endpoint_base_url = config('services.facebook.graph_url') . config('services.facebook.account_kit.app_version') . '/me';
        $this->token_exchange_base_url = config('services.facebook.graph_url') . config('services.facebook.account_kit.app_version') . '/access_token';
        $this->app_access_token = join('|', ['AA', config('services.facebook.app_id'), config('services.facebook.account_kit.app_secret')]);
    }

    /**
     * Get user id and token from user or register it on the database
     *
     * @param String $code
     *
     * @return Response
     */
    public function login(String $code)
    {
        /*
         * 1. Send authorization code to Fb /token endpoint and get an access token
         * 2. Send said token to Fb /me endpoint and retrieve user email/phone
         * 3. Create token from user data (fetch user from DB or create it as a new one)
         */

        // 1. Send authorization code to Fb /token endpoint and get an access token
        $request_body = [
            'grant_type' => $this->grant_type,
            'code' => $code,
            'access_token' => $this->app_access_token,
        ];

        $res = RequestService::get($this->token_exchange_base_url, $request_body);

        if (!isset($res) || $res->getStatusCode() != SymfonyResponse::HTTP_OK) {
            return $res;
        }
        $data = json_decode((string) $res->getBody());
        $user_access_token = $data->{'access_token'};


        // 2. Send said token to Fb /me endpoint and retrieve user email/phone
        $request_body = [
            'access_token' => $user_access_token,
        ];

        $res = RequestService::get($this->me_endpoint_base_url, $request_body);

        if (! isset($res) || $res->getStatusCode() != SymfonyResponse::HTTP_OK){
            return null;
        }

        $data = json_decode((string)$res->getBody());
        $phone = $data->{'phone'}->{'number'};

        return $phone;
    }
}