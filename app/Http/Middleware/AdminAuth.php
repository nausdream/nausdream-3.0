<?php

namespace App\Http\Middleware;

use App\Constants;
use App\Models\Administrator;
use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard(Constants::GUARD_ADMIN)->check()) {
            return $next($request);
        }

        return response()->view(
            Constants::R_ADMIN_LOGIN,
            ['to' => $request->path(), 'e' => 'Effettua l\'accesso per visualizzare questa risorsa'],
            ResponseCodes::HTTP_UNAUTHORIZED
        );
    }
}
