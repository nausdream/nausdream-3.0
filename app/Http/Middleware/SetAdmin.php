<?php

namespace App\Http\Middleware;

use App\Constants;
use Closure;

class SetAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isAdmin = \Auth::guard(Constants::GUARD_ADMIN)->check();
        $request->attributes->add(['isAdmin' => $isAdmin]);

        return $next($request);
    }
}
