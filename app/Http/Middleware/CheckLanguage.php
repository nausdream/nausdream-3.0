<?php

namespace App\Http\Middleware;

use App\Constants;
use App\Helpers\LanguageHelper;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;
use Closure;

class CheckLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = $request->lang ?? explode('/', $request->path())[0];

        $preferenceCookies = !isset($_COOKIE['cookie_preferences']) ||
            (isset($_COOKIE['cookie_preferences']) && $_COOKIE['cookie_preferences'] == "true");
        
        // Get user language in this order: session (if enabled by user), preferredLanguage
        if ($preferenceCookies) {
            $languageDetected = $request->cookie('language') ?? $request->getPreferredLanguage();
        } else {
            $languageDetected = $request->getPreferredLanguage();
        }

        // Check $languageDetected validity
        if (!LanguageHelper::isValid($languageDetected)) {
            // If not valid, use default app language
            $languageDetected = config('app.fallback_locale');
        }

        // If the path is "/"
        if (!strlen($language)) {
            // Set user cookie and app locale
            if ($preferenceCookies) {
                LanguageHelper::setLanguageCookie($languageDetected);
            }
            LanguageHelper::setAppLocale($languageDetected);

            // Redirect to home with correct language
            //return redirect()->route(Constants::R_USERS_HOME, ['lang' => $languageDetected]);

        } else {
            // Check if language is valid
            if (LanguageHelper::isValid($language)) {
                // Set user session and locale
                if ($preferenceCookies) {
                    LanguageHelper::setLanguageCookie($language);
                }
                LanguageHelper::setAppLocale($language);
            } else {
                // Set user session and locale with languageDetected
                if ($preferenceCookies) {
                    LanguageHelper::setLanguageCookie($languageDetected);
                }
                LanguageHelper::setAppLocale($languageDetected);

                // Return 404
                return response()->view('errors.404', ["page" => ''])->setStatusCode(ResponseCodes::HTTP_NOT_FOUND);
            }
        }

        return $next($request);
    }
}
