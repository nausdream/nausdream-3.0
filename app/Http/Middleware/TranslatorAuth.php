<?php

namespace App\Http\Middleware;

use App\Constants;
use App\TranslationModels\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class TranslatorAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard(Constants::GUARD_TRANSLATOR)->check()) {
            return $next($request);
        }

        return response()->view(
            Constants::R_TRANSLATOR_LOGIN,
            ['to' => $request->path(), 'e' => 'Effettua l\'accesso per visualizzare questa risorsa'],
            Response::HTTP_UNAUTHORIZED
        );
    }
}
