<?php
/**
 * User: Luca Puddu
 * Date: 02/03/2018
 * Time: 15:46
 */

namespace App\Http\ViewComposers;

use App\Constants;
use App\Helpers\StaticPagesHelper;
use Illuminate\View\View;

class ErrorsComposer
{

    /**
     * ErrorsComposer constructor.
     */
    public function __construct()
    {

    }

    public function notFound(View $view){
        $view->with('seo', StaticPagesHelper::getSeo('not_found'));
    }

    public function notAuthorized(View $view){
        $view->with('seo', StaticPagesHelper::getSeo('not_authorized'));
    }
}