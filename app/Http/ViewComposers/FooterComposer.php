<?php
/**
 * User: Luca Puddu
 * Date: 02/03/2018
 * Time: 15:46
 */

namespace App\Http\ViewComposers;

use App\Constants;
use Illuminate\View\View;

class FooterComposer
{
    protected $fb;
    protected $instagram;
    protected $blog;
    protected $language;

    /**
     * FooterComposer constructor.
     */
    public function __construct()
    {
        // Get locale
        $this->language = \App::getLocale();
        $this->fb = Constants::FB_PROFILE_LINK;
        $this->instagram = Constants::INSTAGRAM_PROFILE_LINK;
    }

    public function compose(View $view)
    {
        $view
            ->with('language', $this->language)
            ->with('instagram_profile_link', $this->instagram)
            ->with('fb_profile_link', $this->fb);
    }
}