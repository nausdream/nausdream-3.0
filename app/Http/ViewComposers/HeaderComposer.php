<?php
/**
 * User: Luca Puddu
 * Date: 02/03/2018
 * Time: 15:46
 */

namespace App\Http\ViewComposers;

use App\Helpers\HarbourCentersHelper;
use App\TranslationModels\Language;
use Illuminate\View\View;

class HeaderComposer
{
    protected $language;
    protected $languages;
    protected $harbourCenters;

    /**
     * HeaderComposer constructor.
     */
    public function __construct()
    {
        $this->languages = Language::pluck('language');

        // Get locale
        $this->language = \App::getLocale();

        // Get harbour centers in this form: [area_name => [business_id => query]]
        $this->harbourCenters = HarbourCentersHelper::withAreas($this->language);
    }

    public function compose(View $view)
    {
        $view
            ->with('language_flag',  config('services.cloudinary.secure_url') . "c_scale,h_20,q_80/" . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/lang/")
            ->with('logo_url', config('services.cloudinary.secure_url') . "c_scale,h_55/" . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/logo_nausdream.png")
            ->with('language', $this->language)
            ->with('languages', $this->languages)
            ->with('destinations', $this->harbourCenters);
    }
}
