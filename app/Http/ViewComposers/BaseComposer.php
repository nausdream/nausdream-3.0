<?php
/**
 * User: Luca Puddu
 * Date: 02/03/2018
 * Time: 15:46
 */

namespace App\Http\ViewComposers;

use App\Models\Campaign;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class BaseComposer
{
    protected $site_verification;
    protected $analytics_id;
    protected $live_chat_languages;

    /**
     * BaseComposer constructor.
     */
    public function __construct()
    {
        $this->live_chat_languages = ["it" => "it-IT", "en" => "en-US"];
        $this->analytics_id = config('services.google.analytics.tracking_id');
        $this->fb_pixel_id = config('services.facebook.pixel_id');
    }

    public function compose(View $view)
    {

        $page = $view->getData()['page'] ?? "";

        if (!strlen($page)) {
            $route = explode('.', Route::currentRouteName());
            if (isset($route[1])) {
                $page = $route[1];
            } else {
                $page = $route[0];
            }
        }

        // Live chat language
        $liveChatLanguage = $this->live_chat_languages[\App::getLocale()];

        // We need to access unencrypted cookie set by javascript, hence the reason for not using the Cookie facade
        $isTrafficInternal = isset($_COOKIE['internal_traffic']) && $_COOKIE['internal_traffic'] == "true";
        $statisticsConsent = !isset($_COOKIE['cookie_statistics']) || $_COOKIE['cookie_statistics'] == "true";
        $marketingConsent = !isset($_COOKIE['cookie_marketing']) || $_COOKIE['cookie_marketing'] == "true";

        // Get active campaigns
        $activeCampaigns = Campaign::where('is_active', true)->get();

        $view
            ->with('live_chat_language', $liveChatLanguage)
            ->with('block_analytics', $isTrafficInternal || !$statisticsConsent)
            ->with('block_marketing', !$marketingConsent)
            ->with('google_analytics_tracking_id', $this->analytics_id)
            ->with('active_campaigns', $activeCampaigns)
            ->with('page', $page);
    }
}