<?php
/**
 * User: Luca Puddu
 * Date: 15/03/2018
 * Time: 15:40
 */

namespace App\Transformers;

use App\Constants;
use App\Models\Experience;

class ListingTransformer
{
    /**
     * Get listing for listing preview
     *
     * @param Experience $listing
     * @return array
     */
    public static function transform(Experience $listing)
    {
        $copy = $listing->experience_copies->where('language', \App::getLocale())->first();
        return [
            'top' => isset($listing->trekksoft_id),
            'title' => $copy->title,
            'description' => str_limit(strip_tags(html_entity_decode($copy->description)), 350),
            'meta_description' => $copy->meta_description,
            'departure_port' => $listing->departure_port,
            'price' => $listing->getFormattedPrice(),
            'currency' => $listing->currency,
            'cover_photo' => $listing->id . '/' . $listing->topPhotoId(),
            'slug_url' => "/" . \App::getLocale() . "/". Constants::LISTING_PREFIX . "/".$copy->slug_url,
            'weekdays' => explode(',', $listing->weekdays)
        ];
    }
}