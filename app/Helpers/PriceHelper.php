<?php
/**
 * User: Luca Puddu
 * Date: 22/03/2018
 * Time: 19:13
 */

namespace App\Helpers;


use NumberFormatter;

class PriceHelper
{
    /**
     * @param string $locale
     * @param $price
     * @param string $currency
     * @param int $decimal
     * @return mixed|string
     */
    public static function getFormattedPrice(string $locale, $price, string $currency, int $decimal = 0)
    {
        $fmt = new NumberFormatter(LocaleHelper::getLocale($locale), NumberFormatter::CURRENCY);
        $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, $decimal);
        $fmt->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, $decimal);

        $price = $fmt->formatCurrency($price, $currency);
        $price = str_replace('.00', '', $price);

        return $price;
    }

    /**
     * @param $currency
     * @return null|string
     */
    public static function getSymbol($currency)
    {
        switch ($currency) {
            case 'EUR':
                return '€';
            case 'USD':
                return '$';
            default:
                return null;
        }
    }

    /**
     * Get a price in units (e.g. 117.123) and return the rounded price in cents (e.g. 11712)
     *
     * @param $price
     * @return float
     */
    public static function getRoundedPriceInCents($price){
        return floor($price * 100 + 0.5);
    }

    /**
     * Get a price in units (e.g. 117.126) and return the rounded price in units (e.g. 117.13)
     *
     * @param $price
     * @return float
     */
    public static function getRoundedPrice($price){
        return self::getRoundedPriceInCents($price) / 100;
    }
}