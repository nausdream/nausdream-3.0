<?php
/**
 * User: Luca Puddu
 * Date: 15/03/2018
 * Time: 18:51
 */

namespace App\Helpers;


class LocaleHelper
{
    public static function getLocale(string $lang)
    {
        $locales = [
            'it' => 'it-IT',
            'en' => 'en-US'
        ];

        if (!array_has($locales, $lang)) return $lang;

        return $locales[$lang];
    }
}