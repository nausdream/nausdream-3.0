<?php
/**
 * User: Luca Puddu
 * Date: 05/07/2018
 * Time: 12:39
 */

namespace App\Helpers;


class GoogleMapsHelper
{
    public static function getLinkFromCoordinates($lat,$lng) {
        if (!isset($lat) || !isset($lng)) {
            return null;
        }

        return "https://www.google.com/maps/?q=" . $lat . "," . $lng;
    }
}