<?php
/**
 * User: Luca Puddu
 * Date: 02/03/2018
 * Time: 20:47
 */

namespace App\Helpers;

use App\Constants;
use App\Jobs\SendMail;
use Illuminate\Support\Facades\Storage;
use Watson\Sitemap\Facades\Sitemap;

class SitemapHelper
{
    public static function generate()
    {
        $languages = \App\Helpers\LanguageHelper::getLanguages();

        // Static pages
        $pages = [
//            'users.how-it-works-',
            'users.contact-us-'
        ];
        foreach ($pages as $page) {
            $alternates = [];
            foreach ($languages as $language) {
                $alternates[$language] = route($page . $language);
            }
            Sitemap::addTag(new \Watson\Sitemap\Tags\MultilingualTag(
                route($page . config('app.fallback_locale')),
                null,
                null,
                null,
                $alternates
            ));
        }

        // Harbour centers pages
        $harbourCenters = \App\Models\HarbourCenter::all();
        foreach ($harbourCenters as $center) {
            $alternates = [];
            $seos = $center->seos()
                ->where('duration', null)
                ->where('boat_type_id', null)
                ->get();

            foreach ($seos as $seo) {
                if ($seo->language == config('app.fallback_locale')) {
                    $default = config('app.url').$seo->language.'/'.\App\Constants::HARBOUR_CENTER_PREFIX.'/'.$seo->query;
                }

                $alternates[$seo->language] = config('app.url').$seo->language.'/'.\App\Constants::HARBOUR_CENTER_PREFIX.'/'.$seo->query;
            }

            Sitemap::addTag(new \Watson\Sitemap\Tags\MultilingualTag(
                $default,
                null,
                null,
                null,
                $alternates
            ));

            // Experiences
            $experiences = $center->experiences()->get();
            foreach ($experiences as $experience) {
                $alternates = [];
                $experienceCopies = $experience->experience_copies()->get();

                foreach ($experienceCopies as $copy){
                    if ($copy->language == config('app.fallback_locale')) {
                        $default = config('app.url').$copy->language.'/'.\App\Constants::LISTING_PREFIX.'/'.$copy->slug_url;
                    }

                    $alternates[$copy->language] = config('app.url').$copy->language.'/'.\App\Constants::LISTING_PREFIX.'/'.$copy->slug_url;
                }

                Sitemap::addTag(new \Watson\Sitemap\Tags\MultilingualTag(
                    $default,
                    null,
                    null,
                    null,
                    $alternates
                ));
            }

        }

        // Return XML sitemap.
        $xml = Sitemap::xml();

        Storage::put('public/sitemap.xml', $xml);
    }
}
