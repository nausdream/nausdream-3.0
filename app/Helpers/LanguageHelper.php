<?php
/**
 * User: Luca Puddu
 * Date: 01/03/2018
 * Time: 18:13
 */

namespace App\Helpers;


use App\Constants;
use App\TranslationModels\Language;
use Illuminate\Support\Facades\Cookie;

class LanguageHelper
{
    /**
     * Return true if language is in db
     *
     * @param string|null $lang
     * @return bool
     */
    public static function isValid(string $lang = null)
    {
        $fetched = Language::where('language', $lang)->first();

        return isset($fetched);
    }

    /**
     * Change app and user language
     *
     * @param string|null $lang
     */
    public static function setLanguage(string $lang = null)
    {
        self::setAppLocale($lang);
        self::setLanguageCookie($lang);
    }

    /**
     * Change app locale
     *
     * @param string|null $lang
     */
    public static function setAppLocale(string $lang = null)
    {
        \App::setLocale($lang);
    }

    /**
     * Change app locale
     *
     * @param string|null $lang
     */
    public static function setLanguageCookie(string $lang = null)
    {
        Cookie::queue('language', $lang, Constants::LANGUAGE_COOKIE_DURATION);
    }

    /**
     * Return valid languages
     *
     * @return array
     */
    public static function getLanguages()
    {
        return Language::pluck('language')->all();
    }
}