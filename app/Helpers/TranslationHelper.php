<?php
/**
 * User: Luca Puddu
 * Date: 20/06/2018
 * Time: 20:17
 */

namespace App\Helpers;


use App\Constants;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class TranslationHelper
{
    private $languages;
    private $deleteConfirmationValue;
    private $pageNameRegex;
    private $validationRules;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Get array of languages
        $this->languages = Language::all()->pluck('language', 'id')->all();

        // Page name validator
        $this->pageNameRegex = '^[a-zA-Z0-9_ ]*$';

        // Delete confirmation value
        $this->deleteConfirmationValue = 200;

        // Validation rules
        $this->validationRules = [
            'sentence' => [
                'min' => 2,
                'max' => Constants::SENTENCE_NAME_LENGTH
            ],
            'sentence_translation' => [
                'max' => Constants::MYSQL_TEXT_MAX_LENGTH
            ],
            'page' => [
                'min' => 2,
                'max' => Constants::PAGE_NAME_LENGTH
            ]
        ];
    }


    /**
     * Add a new sentence to the page
     *
     * @param Request $request
     * @return \Illuminate\Support\MessageBag
     */
    public function addSentence(Request $request) {
        $rules = [
            "page_id" => "exists:mysql_translation.pages,id",
            "new_sentence" => [
                "required",
                "string",
                "alpha_dash",
                Rule::unique('mysql_translation.sentences', 'name')->where(function ($query) use ($request) {
                    return $query->where('page_id', $request->page_id);
                }),
                'min:' . $this->validationRules['sentence']['min'],
                'max:' . $this->validationRules['sentence']['max']
            ]
        ];

        $request->validate($rules);

        $page = Page::find($request->page_id);
        $newSentence = Sentence::create(["name" => $request->new_sentence]);
        $page->sentences()->save($newSentence);

        return response(["id" => $newSentence->id]);
    }

    /**
     * Delete a sentence from a page
     *
     * @param Request $request
     * @return \Illuminate\Support\MessageBag
     */
    public function deleteSentence(Request $request)
    {
        $rules = [
            "id" => "exists:mysql_translation.sentences,id"
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $sentence = Sentence::find($request->id);

        // Delete every translation for this sentence
        $sentence->sentence_translations()->forceDelete();

        // Delete sentence itself
        $sentence->forceDelete();

        return response("ok");
    }
}