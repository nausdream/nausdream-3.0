<?php
/**
 * User: Luca Puddu
 * Date: 18/07/2018
 * Time: 17:50
 */

namespace App\Helpers;


class WeekdayHelper
{
    /**
     * Get a string representing a day (E.g. "tue") and return the same day in a number format (E.g. 3)
     *
     * @param string|null $weekday
     * @return int
     */
    public static function getWeekdayNumber(string $weekday = null)
    {
        if (!isset($weekday)) return -1;

        $weekdays = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];

        $key = array_search($weekday, $weekdays);

        if ($key === false) {
            return -1;
        }

        return $key;
    }

    /**
     * Get an array of string representing weekdays (E.g. ["sun", "mon", "fri"])
     * and return a comma-separated string of the same day represented as numbers (E.g. "0,1,5")
     *
     * @param array $weekdays
     * @return string
     */
    public static function getWeekdaysNumbers(array $weekdays = [])
    {
        $weekdaysAsNumbers = [];

        foreach ($weekdays as $weekday) {
            $weekdaysAsNumbers[] = self::getWeekdayNumber($weekday);
        }

        return implode(",", $weekdaysAsNumbers);
    }

    /**
     * Return as array of ints only the days that are different
     * from the ones that were passed as inputs (["sun", "tue", "fri"])
     *
     * @param array $weekdays
     * @return array
     */
    public static function getOtherWeekdaysNumbers(array $weekdays = [])
    {
        $allDaysAsNumbers = [0, 1, 2, 3, 4, 5, 6];

        foreach ($weekdays as $weekday) {
            $allDaysAsNumbers = array_filter($allDaysAsNumbers, function(int $number) use ($weekday){
                return $number != self::getWeekdayNumber($weekday);
            });
        }

        return $allDaysAsNumbers;
    }

    /**
     * Return as comma-separated string only the days that are different
     * from the ones that were passed as inputs (["sun", "tue", "fri"])
     *
     * @param array $weekdays
     * @return string
     */
    public static function getOtherWeekdays(array $weekdays = []) {
        return implode(",", self::getOtherWeekdaysNumbers($weekdays));
    }
}