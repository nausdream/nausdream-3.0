<?php
/**
 * User: Luca Puddu
 * Date: 24/01/2018
 * Time: 15:57
 */

namespace App\Helpers;


use App\Constants;
use App\Models\Experience;
use App\Models\ExperiencePhoto;
use App\Services\PhotoService;
use Illuminate\Support\Collection;

class PhotoHelper
{
    /**
     * @param ExperiencePhoto $photo
     * @return string
     */
    public static function getPublicId(ExperiencePhoto $photo){
        return config('services.cloudinary.base_folder') . Constants::EXPERIENCES_FOLDER . $photo->experience->id . '/' . $photo->id;
    }

    /**
     * @param ExperiencePhoto $photo
     * @return string
     */
    public static function getLink(ExperiencePhoto $photo){
        return config('services.cloudinary.secure_url') . self::getPublicId($photo);
    }

    /**
     * @param ExperiencePhoto|Collection $photos
     * @param array $options
     */
    public static function delete($photos, array $options = []){
        if ($photos instanceof ExperiencePhoto) {
            PhotoService::delete(self::getPublicId($photos), $options);
        }
        else if ($photos instanceof Collection) {
            $photos->each(function($photo) use ($options) {
                PhotoService::delete(self::getPublicId($photo), $options);
            });
        }
    }
}