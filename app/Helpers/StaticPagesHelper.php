<?php
/**
 * User: Luca Puddu
 * Date: 02/03/2018
 * Time: 20:47
 */

namespace App\Helpers;

use App\Constants;

class StaticPagesHelper
{
    private static $seos;

    static function init()
    {
        self::$seos = [
            'home' => [
                'alternates' => [
                    'x-default' => "/" . config('app.fallback_locale'),
                    'it' => "/it",
                    'en' => "/en"
                ],
                'og_images' => [
                    config('services.cloudinary.base_url').'c_scale,h_536/v1525629442/'.config('services.cloudinary.base_folder').'assets/img/home/header.jpg'
                ]
            ],
            /*'how_it_works' => [
                'alternates' => [
                    'x-default' => route('users.how-it-works-' . config('app.fallback_locale')),
                    'it' => route('users.how-it-works-it'),
                    'en' => route('users.how-it-works-en')
                ]
            ],*/
            'terms_of_service' => [
                'alternates' => [
                    'x-default' => route('users.terms-of-service-' . config('app.fallback_locale')),
                    'it' => route('users.terms-of-service-it'),
                    'en' => route('users.terms-of-service-en')
                ]
            ],
            'contact_us' => [
                'description' => trans('contact_us.meta_description', ['number' => Constants::TELEPHONE_NUMBER, 'email' => Constants::SUPPORT_EMAIL]),
                'alternates' => [
                    'x-default' => route('users.contact-us-' . config('app.fallback_locale')),
                    'it' => route('users.contact-us-it'),
                    'en' => route('users.contact-us-en')
                ]
            ],
            'cookie_policy' => [
                'alternates' => [
                    'x-default' => route('users.cookie-policy-' . config('app.fallback_locale')),
                    'it' => route('users.cookie-policy-it'),
                    'en' => route('users.cookie-policy-en')
                ]
            ],
            'privacy_policy' => [
                'alternates' => [
                    'x-default' => route('users.privacy-policy-' . config('app.fallback_locale')),
                    'it' => route('users.privacy-policy-it'),
                    'en' => route('users.privacy-policy-en')
                ]
            ],
            'not_found' => [
                'title' => trans("errors.404_title"),
                'alternates' => [
                    'x-default' => '/en/not-found',
                    'it' => '/it/non-trovato',
                    'en' => '/en/not-found'
                ]
            ],
            'not_authorized' => [
                'title' => trans("errors.401_title"),
                'alternates' => [
                    'x-default' => '/en/not-authorized',
                    'it' => '/it/non-autorizzato',
                    'en' => '/en/not-authorized'
                ]
            ]
        ];
    }

    public static function getSeo(string $page = null)
    {
        $seo = [
            'title' => trans("$page.meta_title") . " " . Constants::SEO_SEPARATOR . " " . Constants::SEO_DOMAIN_NAME,
            'description' => trans("$page.meta_description"),
            'og_type' => 'website',
            'og_url' => request()->url(),
            'alternates' => [],
            'og_images' => [],
        ];

        if (isset(self::$seos[$page])) {
            $seo = self::$seos[$page] + $seo;
        }

        return $seo;
    }
}

// Initialize class fields
StaticPagesHelper::init();
