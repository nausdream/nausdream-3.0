<?php
/**
 * User: Luca Puddu
 * Date: 02/03/2018
 * Time: 16:47
 */

namespace App\Helpers;

use App\Constants;
use App\Models\Area;
use App\Models\HarbourCenter;
use App\Models\Seo;

class HarbourCentersHelper
{
    public static function withAreas($language)
    {
        $areas = [];
        // Get harbour centers in this form: [area_name => [business_id => query]]
        $queries = Seo::where('language', $language)
            ->where('boat_type_id', null)
            ->where('duration', null)
            ->get()
            ->pluck('query', 'harbour_center_id');

        $areasDb = Area::where('hidden', false)
            ->with(['harbour_centers' => function ($q) {
                $q->where('hidden', false)
                    ->orderBy('order');
            }])
            ->orderBy('order')
            ->get();

        foreach ($areasDb as $area) {
            foreach ($area->harbour_centers as $center) {
                $areas[$area->name][$center->business_id] = self::getUrl($language, $queries->get($center->id));
            }
        }

        return $areas;
    }

    public static function getUrl(string $language, string $slug)
    {
        return $language . '/' . Constants::HARBOUR_CENTER_PREFIX . '/' . $slug;
    }
}