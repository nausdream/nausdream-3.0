<?php
/**
 * User: Luca Puddu
 * Date: 04/06/2018
 * Time: 17:43
 */

namespace App\Helpers;

use App\Constants;

class BookingStatusHelper
{
    public static function getName(int $status){
        switch ($status) {
            case Constants::BOOKING_DRAFT:
                return 'bozza';
                break;
            case Constants::BOOKING_PAYMENT_LINK_SENT:
                return 'in attesa di pagamento';
                break;
            case Constants::BOOKING_PAID:
                return 'pagata';
                break;
            case Constants::BOOKING_EXPIRED:
                return 'scaduta';
                break;
            case Constants::BOOKING_CANCELED:
                return 'annullata';
                break;
            case Constants::BOOKING_REFUNDED:
                return 'rimborsata';
                break;
            case Constants::BOOKING_PARTIALLY_REFUNDED:
                return 'rimborso parziale';
                break;
            default:
                return 'error';
                break;
        }
    }
}