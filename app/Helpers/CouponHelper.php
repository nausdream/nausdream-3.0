<?php
/**
 * User: Luca Puddu
 * Date: 22/03/2018
 * Time: 19:13
 */

namespace App\Helpers;

use App\Constants;
use App\Models\Coupon;

class CouponHelper
{
    /**
     * Return coupon symbol base on coupon type (fixed or percentage)
     *
     * @param Coupon $coupon
     * @return null|string
     */
    public static function getTypeSymbol(Coupon $coupon)
    {
        if (!isset($coupon)) return null;

        switch ($coupon->type) {
            case Constants::COUPON_PERCENTAGE:
                return "%";
                break;
            case Constants::COUPON_FIXED:
                return PriceHelper::getSymbol($coupon->currency);
            default:
                return null;
        }
    }
}