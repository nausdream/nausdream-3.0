<?php
/**
 * User: Luca Puddu
 * Date: 25/06/2018
 * Time: 16:55
 */

namespace App\Helpers;


use Carbon\Carbon;

class DateHelper
{
    const dateFormats = [
        'it' => 'd/m/Y',
        'en' => 'Y/m/d'
    ];

    const timeFormats = [
        'it' => 'H:i',
        'en' => 'g:i A'
    ];

    const datetimeFormats = [
        'it' => 'd/m/Y H:i',
        'en' => 'Y/m/d g:i A'
    ];

    /**
     * Return date formatted according to provided locale. Fallback to app locale if no locale is provided.
     *
     * @param Carbon $date
     * @param string|null $locale
     * @return string
     */
    public static function format(Carbon $date, string $locale = null)
    {
        $format = self::getDateFormat($locale);

        return $date->format($format);
    }

    /**
     * Return date format according to provided locale. Fallback to app locale if no locale is provided.
     *
     * @param string|null $locale
     * @return string
     */
    public static function getDateFormat(string $locale = null)
    {
        if (!isset($locale) || !array_has(self::dateFormats, $locale)) {
            $locale = \App::getLocale();
        }

        return self::dateFormats[$locale];
    }

    /**
     * Return time format according to provided locale. Fallback to app locale if no locale is provided.
     *
     * @param string|null $locale
     * @return string
     */
    public static function getTimeFormat(string $locale = null)
    {
        if (!isset($locale) || !array_has(self::dateFormats, $locale)) {
            $locale = \App::getLocale();
        }

        return self::timeFormats[$locale];
    }

    /**
     * Return date format according to provided locale. Fallback to app locale if no locale is provided.
     *
     * @param string|null $locale
     * @return string
     */
    public static function getDatetimeFormat(string $locale = null)
    {
        if (!isset($locale) || !array_has(self::dateFormats, $locale)) {
            $locale = \App::getLocale();
        }

        return self::datetimeFormats[$locale];
    }

    /**
     * Get a string representation of a date and return a string representation of the same date in the provided format
     *
     * @param string|null $date
     * @param string|null $formatFrom
     * @param string|null $locale
     * @return string
     */
    public static function stringToFormat(string $date = null, string $formatFrom = null, string $locale = null){
        if (!isset($date)) return null;

        $date = Carbon::createFromFormat($formatFrom ?? 'Y-m-d', $date);

        return self::format($date, $locale);
    }
}