<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 22/01/2018
 * Time: 21:16
 */

namespace App\Helpers;


use App\Constants;

class SeoHelper
{
    /**
     * Get meta title
     *
     * @param string $title
     * @return string
     */
    public static function getMetaTitle(string $title)
    {
        // Create brand part of meta title
        $shortTitleLastPart = ' ' . Constants::SEO_SEPARATOR . ' ' . Constants::SEO_DOMAIN_NAME;

        // Check if input title is short enough to add shortTitleLastPart
        if (strlen($title) <= (Constants::SEO_META_TITLE_LENGTH - strlen($shortTitleLastPart))) {
            $title = $title . $shortTitleLastPart;
        }

        // Return cut input title if it's longer than SEO_TITLE_LENGTH
        return substr($title, 0, Constants::SEO_META_TITLE_LENGTH);
    }

    /**
     * Get meta description
     *
     * @param string $description
     * @return string
     */
    public static function getMetaDescription(string $description)
    {
        // Return input description cutted to SEO_DESCRIPTION_LENGTH length minus a delta for tolerance purposes in edge cases
        return substr($description, 0, Constants::SEO_META_DESCRIPTION_LENGTH - 10);
    }

    /**
     * Get slug url
     *
     * @param string $title
     * @param int|null $number
     * @return string
     */
    public static function getSlugUrl(string $title = null, int $number = null) {

        if ($title == null) {
            return null;
        }

        // Cut title to a shorter string before replace words
        $cutted_title = substr($title, 0, Constants::SEO_SLUG_URL_LENGTH);

        // Remove word with less then 3 characters from input title
        $replaced = preg_replace('/\b[A-z]{1,2}\b\s*/', '', $cutted_title);

        // Return slug url with random number at the end
        if (isset($number)) {
            return str_slug($replaced . '-' . $number);
        }

        // Return slug url
        return str_slug($replaced);
    }
}