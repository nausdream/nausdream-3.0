<?php

namespace App\Console\Commands;

use App\Constants;
use App\Mail\PostExperience;
use App\Mail\PreExperience;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendPrePostExperienceEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send_pre_post_experience';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send PreExperience and PostExperience Emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bookings = Booking::all();

        $bookings->each(function (Booking $booking) {
            if (($booking->paid || $booking->partially_refunded) && isset($booking->experience_date) && isset($booking->departure_time)) {
                $departureTime = explode(":", $booking->departure_time);
                $experienceDateTime = Carbon::parse($booking->experience_date);

                // Timezone must be set before hour and minute
                $experienceDateTime->setTimezone(Constants::TIMEZONE);
                $experienceDateTime->hour = $departureTime[0];
                $experienceDateTime->minute = $departureTime[1];

                $complaints = $booking->soft_complain || $booking->hard_complain;

                $now = Carbon::now(Constants::TIMEZONE);
                $hoursToExperienceDateTime = $now->diffInHours($experienceDateTime, false);

                // Pre-experience email
                if (!$booking->is_pre_experience_email_sent && $hoursToExperienceDateTime > 0 && $hoursToExperienceDateTime < Constants::PRE_EXPERIENCE_DELTA_HOURS) {
                    \App::setLocale($booking->language);

                    $attributes = $booking->getAttributes();
                    $attributes['meeting_safety_margin'] = $booking->meeting_safety_margin;

                    Mail::queue((new PreExperience($attributes))->onQueue(config('queue.connections.sqs.mail')));

                    $booking->is_pre_experience_email_sent = true;
                }

                // Post-experience email
                if (!$complaints && !$booking->is_post_experience_email_sent && $hoursToExperienceDateTime < 0 && $hoursToExperienceDateTime < -Constants::POST_EXPERIENCE_DELTA_HOURS) {
                    \App::setLocale($booking->language);

                    Mail::queue((new PostExperience($booking->getAttributes()))->onQueue(config('queue.connections.sqs.mail')));

                    $booking->is_post_experience_email_sent = true;
                }

                $booking->save();
            }
        });
    }
}
