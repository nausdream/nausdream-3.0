<?php

namespace App\Mail;

use App\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomBookingRequest extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;

    /**
     * BookingPaidToAdministrator constructor.
     *
     * @param $bookingData
     */
    public function __construct($bookingData)
    {
        $this->booking = $bookingData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to(Constants::SUPPORT_EMAIL, Constants::SUPPORT_NAME)
            ->subject("[Nuova richiesta personalizzata] ".$this->booking['name']." per il giorno ".$this->booking['date'])
            ->view('emails.admin.custombooking')
            ->with([
                'name' => $this->booking['name'],
                'email' => $this->booking['email'],
                'date' => $this->booking['date'],
                'phone' => $this->booking['phone'],
                'guests' => $this->booking['guests'],
                'location' => $this->booking['location'],
                'like_to_do' => $this->booking['like_to_do'],
                'coupon' => $this->booking['coupon'],
                'language' => $this->booking['language'],
                'marketing' => $this->booking['marketing']
            ]);

        return $email;
    }
}
