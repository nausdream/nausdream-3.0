<?php
/**
 * User: Luca Puddu
 * Date: 09/07/2018
 * Time: 18:26
 */

namespace App\Mail\Campaigns\ReferAFriend;

use App\Constants;
use App\Helpers\DateHelper;
use App\Mail\Mailable;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class FriendCoupon extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $unsubLink;
    protected $coupon;

    /**
     * FriendCoupon constructor.
     * @param Booking $booking
     * @param string $unsubLink
     * @param string $coupon
     */
    public function __construct(Booking $booking, string $unsubLink, string $coupon)
    {
        $this->booking = $booking;
        $this->unsubLink = $unsubLink;
        $this->coupon = $coupon;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = trans("emails_friend_coupon.subject", ['name' => $this->booking->user_first_name]);

        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to($this->booking->user_email ?? Constants::SUPPORT_EMAIL, $this->booking->user_first_name . " " . $this->booking->user_last_name)
            ->subject($subject)
            ->view('emails.users.campaigns.refer-a-friend.friend-coupon')
            ->with([
                'subject' => $subject,
                'email_heading' => trans('emails_friend_coupon.heading', ['date' => Carbon::now()->format(DateHelper::getDateFormat())]),
                'unsub_link' => $this->unsubLink,
                'booking' => $this->booking,
                'coupon' => $this->coupon,
                'rules' => route(Constants::R_USERS_CAMPAIGNS_RULES, ['lang' => \App::getLocale(), 'c' => 'refer-a-friend'])
            ]);

        return $email;
    }
}