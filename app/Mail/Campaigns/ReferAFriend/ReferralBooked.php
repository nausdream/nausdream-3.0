<?php
/**
 * User: Luca Puddu
 * Date: 09/07/2018
 * Time: 18:26
 */

namespace App\Mail\Campaigns\ReferAFriend;

use App\Constants;
use App\Helpers\DateHelper;
use App\Mail\Mailable;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ReferralBooked extends Mailable
{
    use Queueable, SerializesModels;

    protected $unsubLink;
    protected $friendName;
    protected $referrer;
    protected $coupon;

    /**
     * ReferralBooked constructor.
     *
     * @param string $unsubLink
     * @param string $friendName
     * @param User $referrer
     * @param string $coupon
     */
    public function __construct(string $unsubLink, string $friendName, User $referrer, string $coupon)
    {
        $this->unsubLink = $unsubLink;
        $this->friendName = $friendName;
        $this->referrer = $referrer;
        $this->coupon = $coupon;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = trans("emails_referral_booked.subject", ['name' => $this->referrer->first_name, 'friend' => $this->friendName]);

        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to($this->referrer->email ?? Constants::SUPPORT_EMAIL, $this->referrer->first_name . " " . $this->referrer->last_name)
            ->subject($subject)
            ->view('emails.users.campaigns.refer-a-friend.referral-booked')
            ->with([
                'email_heading' => trans('emails_referral_booked.heading', ['date' => Carbon::now()->format(DateHelper::getDateFormat())]),
                'subject' => $subject,
                'name' => $this->referrer->first_name,
                'friend' => $this->friendName,
                'unsub_link' => $this->unsubLink,
                'coupon' => $this->coupon,
                'rules' => Constants::R_USERS_CAMPAIGNS_RULES
            ]);

        return $email;
    }
}