<?php

namespace App\Mail;

use App\Constants;
use App\Helpers\DateHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Log;

class PreExperience extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $translationAddress;

    /**
     * Quotation constructor.
     *
     */
    public function __construct($bookingData)
    {
        $this->booking = $bookingData;

        if (isset($this->booking['experience_date'])){
            $this->booking['experience_date'] = Carbon::parse($this->booking['experience_date'])->format(DateHelper::getDateFormat());
        }
        if (isset($this->booking['departure_time'])){
            $this->booking['departure_time'] = Carbon::parse($this->booking['departure_time'])->format(DateHelper::getTimeFormat());
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to($this->booking['user_email'] ?? Constants::SUPPORT_EMAIL, $this->booking['user_first_name'] . " " . $this->booking['user_last_name'])
            ->subject(trans('emails_pre_experience.subject', ['name' => $this->booking['user_first_name']]))
            ->view('emails.users.pre-experience')
            ->with([
                'email_heading' => trans('emails_pre_experience.heading', ['date' => $this->booking['experience_date']]),
                'experience_title' => $this->booking['experience_title'],
                'experience_link' => $this->booking['experience_link'],
                'user_first_name' => $this->booking['user_first_name'],
                'experience_date' => $this->booking['experience_date'],
                'departure_time' => $this->booking['departure_time'],
                'meeting_point' => $this->booking['meeting_point'],
                'minutes' => $this->booking['meeting_safety_margin']
            ]);

        return $email;
    }
}
