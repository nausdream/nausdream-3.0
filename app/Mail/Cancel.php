<?php

namespace App\Mail;

use App\Constants;
use App\Helpers\DateHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Cancel extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $translationAddress;

    /**
     * Quotation constructor.
     *
     */
    public function __construct($bookingData)
    {
        $this->booking = $bookingData;

        if (isset($this->booking['experience_date'])){
            $this->booking['experience_date'] = Carbon::parse($this->booking['experience_date'])->format(DateHelper::getDateFormat());
        }
        if (isset($this->booking['updated_at'])){
            $this->booking['updated_at'] = Carbon::parse($this->booking['updated_at'])->format(DateHelper::getDatetimeFormat());
        }
        if (isset($this->booking['departure_time'])){
            $this->booking['departure_time'] = Carbon::parse($this->booking['departure_time'])->format(DateHelper::getTimeFormat());
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = trans('emails_cancel.subject', ['title' => $this->booking['experience_title'], 'date' => $this->booking['experience_date']]);
        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to($this->booking['user_email'] ?? Constants::SUPPORT_EMAIL, $this->booking['user_first_name'] . " " . $this->booking['user_last_name'])
            ->subject($subject)
            ->view('emails.users.cancel')
            ->with([
                'subject' => $subject,
                'email_heading' => trans('emails_cancel.heading', ['date' => $this->booking['updated_at']]),
                'experience_title' => $this->booking['experience_title'],
                'experience_link' => $this->booking['experience_link'],
                'user_first_name' => $this->booking['user_first_name'],
                'user_last_name' => $this->booking['user_last_name'],
                'experience_date' => $this->booking['experience_date'],
                'departure_time' => $this->booking['departure_time'],
                'price' => $this->booking['price'],
                'deleted_for' => $this->booking['deleted_for']
            ]);

        return $email;
    }
}
