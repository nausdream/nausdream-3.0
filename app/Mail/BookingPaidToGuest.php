<?php

namespace App\Mail;

use App\Constants;
use App\Helpers\DateHelper;
use App\Helpers\GoogleMapsHelper;
use App\Models\Booking;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class BookingPaidToGuest extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $additionalParams;

    /**
     * BookingPaidToGuest constructor.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceDate = null;
        $updatedAt = null;
        $departureTime = null;
        $returnTime = null;

        if (isset($this->booking->experience_date)){
            $experienceDate = Carbon::parse($this->booking->experience_date)->format(DateHelper::getDateFormat());
        }
        if (isset($this->booking->updated_at)){
            $updatedAt = Carbon::parse($this->booking->updated_at)->format(DateHelper::getDatetimeFormat());
        }
        if (isset($this->booking->departure_time)){
            $departureTime = Carbon::parse($this->booking->departure_time)->format(DateHelper::getTimeFormat());
        }
        if (isset($this->booking->return_time)){
            $returnTime = Carbon::parse($this->booking->return_time)->format(DateHelper::getTimeFormat());
        }
        $today = Carbon::today()->format(DateHelper::getDateFormat());

        // Generate pdf voucher
        $pdfData = [
            'company_name' => Constants::COMPANY_NAME,
            'company_booking_email' => Constants::BOOKING_EMAIL,
            'company_phone' => Constants::TELEPHONE_NUMBER,
            'company_vat' => Constants::VAT_NUMBER,
            'company_website' => Constants::COMPANY_WEBSITE,
            'company_address' => trans('voucher.company_address'),
            'title' => $this->booking->experience_title,
            'payment_date' => $today,
            'experience_date' => $experienceDate,
            'meeting_point' => $this->booking->meeting_point,
            'return_point' => $this->booking->return_point,
            'departure_time' => $departureTime,
            'return_time' => $returnTime,
            'adults' => $this->booking->adults()->get(),
            'children' => $this->booking->children()->get(),
            'included_services' => $this->booking->included_services,
            'user_first_name' => $this->booking->user_first_name,
            'user_last_name' => $this->booking->user_last_name,
            'captain_name' => $this->booking->captain_name,
            'captain_phone' => $this->booking->captain_phone,
            'minutes' => $this->booking->meeting_safety_margin
        ];
        $pdfName = str_replace(" ", "", strtolower($this->booking->user_first_name) . '_' . strtolower($this->booking->user_last_name) . '.pdf');

        $pdf = PDF::loadView('pdf.voucher', $pdfData);

        $email = $this->from('noreply@nausdream.com', trans('emails_voucher.from_name'))
            ->to($this->booking->user_email, $this->booking->user_first_name . " " . $this->booking->user_last_name)
            ->subject(trans('emails_voucher.subject', ['date' => $experienceDate]))
            ->view('emails.users.voucher')
            ->with([
                'email_heading' => trans('emails_voucher.heading', ['date' => $updatedAt]),
                'experience_title' => $this->booking->experience_title,
                'experience_link' => $this->booking->experience_link,
                'user_first_name' => $this->booking->user_first_name,
                'user_last_name' => $this->booking->user_last_name,
                'experience_date' => $experienceDate,
                'meeting_point' => $this->booking->meeting_point,
                'position' => GoogleMapsHelper::getLinkFromCoordinates($this->booking->lat, $this->booking->lng),
                'departure_time' => $departureTime,
                'price' => $this->booking->price,
                'deleted_for' => $this->booking->deleted_for,
                'booking' => $this->booking,
                'campaigns' => $this->booking->campaigns
            ])
            ->attachData($pdf->output(), $pdfName, [
                'mime' => 'application/pdf',
            ]);

        return $email;
    }
}
