<?php

namespace App\Mail;

use App\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class BookingRequest extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;

    /**
     * BookingPaidToAdministrator constructor.
     *
     * @param $bookingData
     */
    public function __construct($bookingData)
    {
        $this->booking = $bookingData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to(Constants::SUPPORT_EMAIL, Constants::SUPPORT_NAME)
            ->subject("[Nuova richiesta ".$this->booking['experience_business_id']."] ".$this->booking['name']." per il giorno ".$this->booking['date'])
            ->view('emails.admin.booking')
            ->with([
                'experience_business_id' => $this->booking['experience_business_id'],
                'name' => $this->booking['name'],
                'email' => $this->booking['email'],
                'date' => $this->booking['date'],
                'notes' => $this->booking['notes'],
                'phone' => $this->booking['phone'],
                'guests' => $this->booking['guests'],
                'coupon' => $this->booking['coupon'],
                'language' => $this->booking['language'],
                'marketing' => $this->booking['marketing']
            ]);

        return $email;
    }
}
