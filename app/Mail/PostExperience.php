<?php

namespace App\Mail;

use App\Constants;
use App\Helpers\DateHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Log;

class PostExperience extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $translationAddress;

    /**
     * Quotation constructor.
     *
     */
    public function __construct($bookingData)
    {
        $this->booking = $bookingData;

        if (isset($this->booking['experience_date'])) {
            $this->booking['experience_date'] = Carbon::parse($this->booking['experience_date'])->format(DateHelper::getDateFormat());
        }
        if (isset($this->booking['departure_time'])) {
            $this->booking['departure_time'] = Carbon::parse($this->booking['departure_time'])->format(DateHelper::getTimeFormat());
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to($this->booking['user_email'] ?? Constants::SUPPORT_EMAIL, $this->booking['user_first_name'] . " " . $this->booking['user_last_name'])
            ->subject(trans('emails_post_experience.subject', ['name' => $this->booking['user_first_name']]))
            ->view('emails.users.post-experience')
            ->with([
                'email_heading' => trans('emails_post_experience.heading', ['date' => Carbon::now()->format(DateHelper::getDateFormat())]),
                'experience_title' => $this->booking['experience_title'],
                'experience_link' => $this->booking['experience_link'],
                'user_first_name' => $this->booking['user_first_name'],
                'experience_date' => $this->booking['experience_date'],
                'departure_time' => $this->booking['departure_time'],
                'meeting_point' => $this->booking['meeting_point'],
                'social_image' => config('services.cloudinary.secure_url') . 'v1/' . config('services.cloudinary.base_folder') . 'assets/img/emails/post_exp'
            ]);

        return $email;
    }
}
