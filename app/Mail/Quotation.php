<?php

namespace App\Mail;

use App\Constants;
use App\Helpers\DateHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Log;

class Quotation extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $translationAddress;

    /**
     * Quotation constructor.
     *
     */
    public function __construct($bookingData)
    {
        $this->booking = $bookingData;

        if (isset($this->booking['experience_date'])) {
            $this->booking['experience_date'] = Carbon::parse($this->booking['experience_date'])->format(DateHelper::getDateFormat());
        }
        if (isset($this->booking['created_at'])) {
            $this->booking['created_at'] = Carbon::parse($this->booking['created_at'])->format(DateHelper::getDatetimeFormat());
        }
        if (isset($this->booking['departure_time'])) {
            $this->booking['departure_time'] = Carbon::parse($this->booking['departure_time'])->format(DateHelper::getTimeFormat());
        }
        if (isset($this->booking['return_time'])) {
            $this->booking['return_time'] = Carbon::parse($this->booking['return_time'])->format(DateHelper::getTimeFormat());
        }
        if (isset($this->booking['quote_expiration_datetime'])) {
            $this->booking['quote_expiration_datetime'] = Carbon::parse($this->booking['quote_expiration_datetime'])->format(DateHelper::getDatetimeFormat());
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = trans('emails_quote.subject', ['title' => $this->booking['experience_title'], 'date' => $this->booking['experience_date']]);

        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to($this->booking['user_email'] ?? Constants::SUPPORT_EMAIL, $this->booking['user_first_name'] . " " . $this->booking['user_last_name'])
            ->subject($subject)
            ->view('emails.users.quote')
            ->with([
                'subject' => $subject,
                'email_heading' => trans('emails_quote.heading', ['date' => $this->booking['created_at']]),
                'experience_title' => $this->booking['experience_title'],
                'experience_link' => $this->booking['experience_link'],
                'user_first_name' => $this->booking['user_first_name'],
                'user_last_name' => $this->booking['user_last_name'],
                'created_at' => $this->booking['created_at'],
                'quote_expiration_datetime' => $this->booking['quote_expiration_datetime'],
                'experience_description' => $this->booking['experience_description'],
                'experience_date' => $this->booking['experience_date'],
                'adults' => $this->booking['adults'],
                'children' => $this->booking['children'],
                'departure_time' => $this->booking['departure_time'],
                'return_time' => $this->booking['return_time'],
                'meeting_point' => $this->booking['meeting_point'],
                'included_services' => $this->booking['included_services'],
                'raw_price' => $this->booking['raw_price'],
                'price' => $this->booking['price'],
                'payment_link' => $this->booking['payment_link']
            ]);

        return $email;
    }
}
