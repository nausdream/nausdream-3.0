<?php

namespace App\Mail;

use App\Constants;
use App\Helpers\CouponHelper;
use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class BookingPaidToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;

    /**
     * BookingPaidToAdministrator constructor.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceBusinessId = isset($this->booking->experience) ? $this->booking->experience->business_id : "Esperienza personalizzata";
        $coupon = null;
        if (isset($this->booking->coupon)) {
            $coupon = $this->booking->coupon->value . CouponHelper::getTypeSymbol($this->booking->coupon);
        }

        $email = $this->from('noreply@nausdream.com', Constants::SUPPORT_NAME)
            ->to([Constants::SUPPORT_EMAIL, Constants::ADMINISTRATION_EMAIL], Constants::SUPPORT_NAME)
            ->subject("[Pagamento booking n.".$this->booking->id."] ".$this->booking->user_first_name." ".$this->booking->user_last_name." per il giorno ".$this->booking->experience_date)
            ->view('emails.admin.payment')
            ->with([
                'id' => $this->booking->id,
                'experience_business_id' => $experienceBusinessId,
                'first_name' => $this->booking->user_first_name,
                'last_name' => $this->booking->user_last_name,
                'birth_date' => $this->booking->user_birth_date,
                'place_of_birth' => $this->booking->user_place_of_birth,
                'city' => $this->booking->user_city,
                'address' => $this->booking->user_address,
                'zip' => $this->booking->user_zip_code,
                'email' => $this->booking->user_email,
                'date' => $this->booking->experience_date,
                'phone' => $this->booking->user_phone,
                'price' => $this->booking->price,
                'discounted_price' => $this->booking->discounted_price,
                'coupon' => $coupon,
                'adults' => $this->booking->adults()->get()->count(),
                'children' => $this->booking->children()->get()->count(),
            ]);

        return $email;
    }
}
