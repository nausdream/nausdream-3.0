<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class Guest
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property \Carbon\Carbon $birth_date
 * @property int $guest_type
 * @property string $place_of_birth
 *
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\Experience $experience
 * @property \App\Models\BoatType $boat_type
 * @property \Illuminate\Database\Eloquent\Collection $included_services
 *
 * @package App\Models
 */
class Guest extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [
        'first_name',
        'last_name',
        'birth_date',
        'guest_type',
        'place_of_birth'
    ];

    public function booking()
    {
        return $this->belongsTo(\App\Models\Booking::class);
    }
}
