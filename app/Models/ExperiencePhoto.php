<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class ExperiencePhoto
 * 
 * @property int $id
 * @property bool $is_cover
 * @property int $experience_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Experience $experience
 *
 * @package App\Models
 */
class ExperiencePhoto extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'is_cover' => 'bool',
		'experience_id' => 'int'
	];

	protected $fillable = [
		'is_cover',
		'experience_id'
	];

	public function experience()
	{
		return $this->belongsTo(\App\Models\Experience::class);
	}
}
