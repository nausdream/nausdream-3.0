<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class CookieConsent
 * 
 * @property int $id
 * @property string $ip
 * @property boolean $necessary
 * @property boolean $preferences
 * @property boolean $statistics
 * @property boolean $marketing
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class CookieConsent extends Model
{
	protected $fillable = [
		'ip', 'necessary', 'preferences', 'statistics', 'marketing'
	];
}
