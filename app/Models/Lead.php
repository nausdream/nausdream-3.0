<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Lead
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $language
 * @property string $notes
 * @property \Carbon\Carbon $date
 * @property string $coupon
 * @property bool $marketing_accepted
 * @property string $experience_business_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Lead extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'guests',
        'notes',
        'date',
        'coupon',
        'language',
        'marketing_accepted',
        'experience_business_id',
        'like_to_do',
        'location'
    ];
}
