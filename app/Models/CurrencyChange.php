<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class CurrencyChange
 * 
 * @property int $id
 * @property string $currency_in
 * @property string $currency_out
 * @property float $change
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class CurrencyChange extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'change' => 'float'
	];

	protected $fillable = [
		'currency_in',
		'currency_out',
		'change'
	];
}
