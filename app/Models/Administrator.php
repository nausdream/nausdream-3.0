<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Administrator
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $experiences
 *
 * @package App\Models
 */
class Administrator extends Authenticatable
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

    public $remember_token=false;
    protected $guard = 'admin';

    protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'phone'
	];

	public function experiences()
	{
		return $this->hasMany(\App\Models\Experience::class, 'admin_id');
	}
}
