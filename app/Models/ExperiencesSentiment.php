<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ExperiencesSentiment
 * 
 * @property int $id
 * @property int $experience_id
 * @property int $sentiment_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Experience $experience
 * @property \App\Models\Sentiment $sentiment
 *
 * @package App\Models
 */
class ExperiencesSentiment extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'experience_id' => 'int',
		'sentiment_id' => 'int'
	];

	protected $fillable = [
		'experience_id',
		'sentiment_id'
	];

	public function experience()
	{
		return $this->belongsTo(\App\Models\Experience::class);
	}

	public function sentiment()
	{
		return $this->belongsTo(\App\Models\Sentiment::class);
	}
}
