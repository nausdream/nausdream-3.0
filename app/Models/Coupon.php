<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Jul 2018 13:00:24 +0000.
 */

namespace App\Models;

use App\Constants;
use Carbon\Carbon;
use Eloquent as Model;

/**
 * Class Coupon
 * 
 * @property int $id
 * @property int $stock
 * @property string $code
 * @property \Carbon\Carbon $expiration
 * @property boolean $expired
 * @property int $type
 * @property float $value
 * @property string $currency
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 * @property Booking $referrer_booking
 *
 * @package App\Models
 */
class Coupon extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'stock' => 'int',
		'type' => 'int',
		'value' => 'float'
	];

	protected $dates = [
		'expiration'
	];

	protected $fillable = [
		'stock',
		'code',
		'expiration',
		'type',
		'value',
		'currency'
	];

	public function bookings()
	{
		return $this->hasMany(Booking::class);
	}

	public function referrer_booking()
	{
		return $this->belongsTo(Booking::class);
	}

	protected function getExpiredAttribute()
    {
        if (!isset($this->expiration)) return false;

        return Carbon::parse($this->expiration)->addHours(-Constants::TIMEZONE) < Carbon::now(Constants::TIMEZONE);
    }
}
