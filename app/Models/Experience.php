<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use App\Helpers\PriceHelper;
use App\Helpers\LocaleHelper;
use Eloquent as Model;
use NumberFormatter;

/**
 * Class Experience
 *
 * @property int $id
 * @property int $harbour_center_id
 * @property int $experience_type_id
 * @property string $trekksoft_id
 * @property string $departure_port
 * @property float $lat
 * @property float $lng
 * @property int $meeting_safety_margin
 * @property bool $is_searchable
 * @property bool $is_finished
 * @property int $admin_id
 * @property int $children_age
 * @property string $business_id
 * @property int $boat_type_id
 * @property int $difficulty_id
 * @property int $sentiment_id
 * @property float $price
 * @property string $currency
 * @property string $captain_name
 * @property string $captain_email
 * @property string $captain_phone
 * @property int $boat_length_min
 * @property int $boat_length_max
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\HarbourCenter $harbour_center
 * @property \App\Models\Administrator $administrator
 * @property \App\Models\BoatType $boat_type
 * @property \App\Models\Difficulty $difficulty
 * @property \App\Models\Sentiment $sentiment
 * @property \Illuminate\Database\Eloquent\Collection $experience_copies
 * @property \Illuminate\Database\Eloquent\Collection $experience_feedbacks
 * @property \Illuminate\Database\Eloquent\Collection $experience_photos
 * @property \Illuminate\Database\Eloquent\Collection $included_services
 * @property \Illuminate\Database\Eloquent\Collection $schedules
 * @property \Illuminate\Database\Eloquent\Collection $spoken_languages
 * @property \Illuminate\Database\Eloquent\Collection $stops
 *
 * @package App\Models
 */
class Experience extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'harbour_center_id' => 'int',
        'lat' => 'float',
        'lng' => 'float',
        'meeting_safety_margin' => 'int',
        'is_searchable' => 'bool',
        'is_finished' => 'bool',
        'admin_id' => 'int',
        'children_age' => 'int',
        'boat_type_id' => 'int',
        'difficulty_id' => 'int',
        'price' => 'float',
        'boat_length_min' => 'int',
        'boat_length_max' => 'int'
    ];

    protected $fillable = [
        'harbour_center_id',
        'trekksoft_id',
        'departure_port',
        'lat',
        'lng',
        'meeting_safety_margin',
        'is_searchable',
        'is_finished',
        'admin_id',
        'children_age',
        'business_id',
        'boat_type_id',
        'difficulty_id',
        'price',
        'currency',
        'boat_length_min',
        'boat_length_max',
        'captain_name',
        'captain_email',
        'captain_phone',
    ];

    public function harbour_center()
    {
        return $this->belongsTo(\App\Models\HarbourCenter::class);
    }

    public function administrator()
    {
        return $this->belongsTo(\App\Models\Administrator::class, 'admin_id');
    }

    public function boat_type()
    {
        return $this->belongsTo(\App\Models\BoatType::class);
    }

    public function difficulty()
    {
        return $this->belongsTo(\App\Models\Difficulty::class);
    }

    public function sentiment()
    {
        return $this->belongsToMany(\App\Models\Sentiment::class, 'experiences_sentiments')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }

    public function experience_copies()
    {
        return $this->hasMany(\App\Models\ExperienceCopy::class);
    }

    public function experience_feedbacks()
    {
        return $this->hasMany(\App\Models\ExperienceFeedback::class);
    }

    public function experience_photos()
    {
        return $this->hasMany(\App\Models\ExperiencePhoto::class);
    }

    public function included_services()
    {
        return $this->belongsToMany(\App\Models\IncludedService::class, 'experiences_included_services')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }

    public function schedules()
    {
        return $this->hasMany(\App\Models\Schedule::class);
    }

    public function spoken_languages()
    {
        return $this->belongsToMany(\App\Models\SpokenLanguage::class, 'spoken_languages_experiences')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }

    public function sentiments()
    {
        return $this->belongsToMany(\App\Models\Sentiment::class, 'experiences_sentiments')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }

    public function stops()
    {
        return $this->hasMany(\App\Models\Stop::class);
    }

    public function topPhotoId()
    {
        $top = $this->experience_photos->where('is_cover', 1)->first();
        if (!isset($top)) $top = $this->experience_photos->first();
        return $top->id;
    }

    public function getFormattedPrice()
    {
        $this->currency = $this->currency ?? Currency::first()->name;
        return PriceHelper::getFormattedPrice(\App::getLocale(), $this->price, $this->currency);
    }
}
