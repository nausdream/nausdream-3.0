<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Jul 2018 13:00:10 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class Token
 * 
 * @property int $id
 * @property string $token
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Token extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'token',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
