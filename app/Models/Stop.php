<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class Stop
 * 
 * @property int $id
 * @property int $stop_number
 * @property int $experience_id
 * @property string $language
 * @property string $text
 * @property int $duration
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Experience $experience
 *
 * @package App\Models
 */
class Stop extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'stop_number' => 'int',
		'experience_id' => 'int',
		'duration' => 'int'
	];

	protected $fillable = [
		'stop_number',
		'experience_id',
		'language',
		'text',
		'duration'
	];

	public function experience()
	{
		return $this->belongsTo(\App\Models\Experience::class);
	}
}
