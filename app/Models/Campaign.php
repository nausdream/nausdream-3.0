<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Jul 2018 13:00:17 +0000.
 */

namespace App\Models;

use App\Services\Campaigns\CampaignInterface;
use App\Services\Campaigns\CampaignService;
use Eloquent as Model;

/**
 * Class Campaign
 *
 * @property int $id
 * @property string $name
 * @property bool $is_active
 * @property array $attributes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 *
 * @package App\Models
 */
class Campaign extends Model implements CampaignInterface
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'is_active' => 'bool',
        'attributes' => 'array'
    ];

    protected $fillable = [
        'name',
        'is_active',
        'attributes'
    ];


    /**
     * Concrete CampaignInterface instance.
     *
     * @var CampaignInterface|null
     */
    protected $campaign;

    /**
     * Campaign constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->campaign = CampaignService::getCampaign($this->name);
    }


    public function bookings()
    {
        return $this->belongsToMany(\App\Models\Booking::class, 'bookings_campaigns')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }


    /**
     * Invoked when a booking is set to paid
     *
     * @param Booking $booking
     * @return string|null
     */
    public function onBookingPaid(Booking $booking)
    {
        return $this->callCampaignInterfaceFunction('onBookingPaid', $booking);
    }

    /**
     * Invoked when a booking is set to canceled
     *
     * @param Booking $booking
     * @return string|null
     */
    public function onBookingCanceled(Booking $booking)
    {
        return $this->callCampaignInterfaceFunction('onBookingCanceled', $booking);
    }

    /**
     * Return template for campaign banner.
     *
     * @param Booking $booking
     * @return string|null
     */
    public function emailBanner(Booking $booking)
    {
        return $this->callCampaignInterfaceFunction('emailBanner', $booking);
    }

    /**
     * Return template for opt-in view.
     *
     * @param Booking $booking
     * @return string|null
     */
    public function optIn(Booking $booking)
    {
        return $this->callCampaignInterfaceFunction('optIn', $booking);
    }

    /**
     * Return template for rules view.
     *
     * @return string|null
     */
    public function rules()
    {
        return $this->callCampaignInterfaceFunction('rules', null);
    }

    /**
     * Render campaign banner.
     *
     * @return mixed
     */
    public function banner()
    {
        return $this->callCampaignInterfaceFunction('banner', null);
    }

    /**
     * Invoke a function on a CampaignInterface instance
     *
     * @param string $function
     * @param Booking $booking
     * @return null
     */
    private function callCampaignInterfaceFunction(string $function, Booking $booking = null){
        if (!$this->is_active) return null;

        $this->campaign = $this->campaign ?? CampaignService::getCampaign($this->name);

        if (!isset($this->campaign)) return null;

        return $this->campaign->{$function}($booking);
    }
}
