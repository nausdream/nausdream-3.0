<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use App\Constants;
use App\Services\CouponService;
use Carbon\Carbon;
use Eloquent as Model;

/**
 * Class Booking
 *
 * @property int $id
 * @property int $status
 * @property string $language
 * @property string $experience_description
 * @property string $experience_title
 * @property string $experience_link
 * @property string $user_first_name
 * @property string $user_last_name
 * @property string $user_email
 * @property string $user_phone
 * @property string $user_place_of_birth
 * @property string $user_birth_date
 * @property string $user_city
 * @property string $user_address
 * @property string $user_zip_code
 * @property string $meeting_point
 * @property string $return_point
 * @property string $captain_name
 * @property string $captain_email
 * @property string $captain_phone
 * @property string $currency
 * @property string $operator
 * @property string $acquisition_channel
 * @property string $contact_channel
 * @property boolean $soft_complain
 * @property boolean $hard_complain
 * @property boolean $is_pre_experience_email_sent
 * @property boolean $is_post_experience_email_sent
 * @property string $notes
 * @property float $price
 * @property float $discounted_price
 * @property float $refund
 * @property boolean $partially_refunded
 * @property double $lat
 * @property double $lng
 * @property \Carbon\Carbon $booking_date
 * @property \Carbon\Carbon $experience_date
 * @property string $deleted_for
 * @property int $boat_type_id
 * @property int $payment_method_id
 * @property \Carbon\Carbon $payment_datetime
 * @property int $experience_id
 * @property int $user_id
 * @property \Carbon\Carbon $quote_expiration_datetime
 * @property int $meeting_safety_margin
 * @property \Carbon\Carbon $departure_time
 * @property \Carbon\Carbon $return_time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property boolean $expired
 *
 * @property \App\Models\PaymentMethod $payment_method
 * @property \App\Models\Experience $experience
 * @property \App\Models\BoatType $boat_type
 * @property \App\Models\User $user
 * @property \App\Models\Coupon $coupon
 * @property \Illuminate\Database\Eloquent\Collection $referral_coupons
 * @property \Illuminate\Database\Eloquent\Collection $adults
 * @property \Illuminate\Database\Eloquent\Collection $children
 * @property \Illuminate\Database\Eloquent\Collection $included_services
 * @property \Illuminate\Database\Eloquent\Collection $payment_methods
 * @property \Illuminate\Database\Eloquent\Collection $campaigns
 * @property \Illuminate\Database\Eloquent\Collection $active_campaigns
 *
 * @package App\Models
 */
class Booking extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [
        'experience_description',
        'experience_title',
        'experience_link',
        'user_first_name',
        'user_last_name',
        'user_email',
        'user_phone',
        'user_place_of_birth',
        'user_birth_date',
        'user_city',
        'user_address',
        'user_zip_code',
        'meeting_point',
        'return_point',
        'captain_name',
        'captain_email',
        'captain_phone',
        'language',
        'currency',
        'boat_type_id',
        'experience_id',
        'payment_method_id',
        'deleted_for',
        'payment_method_id',
        'payment_datetime',
        'experience_date',
        'meeting_safety_margin',
        'departure_time',
        'is_pre_experience_email_sent',
        'is_post_experience_email_sent',
        'return_time',
        'price',
        'lat',
        'lng',
        'status',
        'refund',
        'quote_expiration_datetime',
        'operator',
        'acquisition_channel',
        'soft_complain',
        'hard_complain',
        'notes',
        'contact_channel',
        'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function boat_type()
    {
        return $this->belongsTo(\App\Models\BoatType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo(\App\Models\Coupon::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function referral_coupons()
    {
        return $this->hasMany(\App\Models\Coupon::class, 'referrer_booking_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function included_services()
    {
        return $this->belongsToMany(\App\Models\IncludedService::class, 'bookings_included_services')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function payment_methods()
    {
        return $this->belongsToMany(\App\Models\PaymentMethod::class, 'bookings_payment_methods')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function campaigns()
    {
        return $this->belongsToMany(\App\Models\Campaign::class, 'bookings_campaigns')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function active_campaigns()
    {
        return $this->belongsToMany(\App\Models\Campaign::class, 'bookings_campaigns')
            ->withPivot('id', 'deleted_at')
            ->withTimestamps()
            ->where('is_active', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function guests()
    {
        return $this->hasMany(\App\Models\Guest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function adults()
    {
        return $this->hasMany(\App\Models\Guest::class)->where('guest_type', Constants::TYPE_ADULT);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(\App\Models\Guest::class)->where('guest_type', Constants::TYPE_CHILD);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function experience()
    {
        return $this->belongsTo(\App\Models\Experience::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment_method()
    {
        return $this->belongsTo(\App\Models\PaymentMethod::class);
    }

    /**
     * Get status.
     *
     * @param  int $value
     * @return int
     */
    public function getStatusAttribute($value)
    {
        if ($value === null) return null;

        // Set status to expired if expiration time is passed
        if ($value == Constants::BOOKING_PAYMENT_LINK_SENT && $this->getExpiredAttribute()) {
            $this->status = Constants::BOOKING_EXPIRED;
            $this->save();

            return Constants::BOOKING_EXPIRED;
        }

        return $value;
    }

    /**
     * Get quote_expiration datetime.
     *
     * @param  string $value
     * @return string
     */
    public function getPaymentDatetimeAttribute($value)
    {
        if ($value == null) return null;

        return Carbon::createFromFormat("Y-m-d H:i:s", $value)->format("Y-m-d\TH:i");
    }

    /**
     * Get quote expiration datetime.
     *
     * @param  string $value
     * @return string
     */
    public function getQuoteExpirationDatetimeAttribute($value)
    {
        if ($value == null) return null;

        return Carbon::createFromFormat("Y-m-d H:i:s", $value)->format("Y-m-d\TH:i");
    }

    /**
     * Get departure time.
     *
     * @param  string $value
     * @return string
     */
    public function getDepartureTimeAttribute($value)
    {
        if ($value == null) return null;

        return Carbon::createFromFormat("H:i:s", $value)->format("H:i");
    }

    /**
     * Get return time.
     *
     * @param  string $value
     * @return string
     */
    public function getReturnTimeAttribute($value)
    {
        if ($value == null) return null;

        return Carbon::createFromFormat("H:i:s", $value)->format("H:i");
    }

    /**
     * Get expired attribute.
     *
     * @return boolean
     */
    public function getExpiredAttribute()
    {
        if ($this->attributes['status'] == Constants::BOOKING_EXPIRED) return true;
        if (!isset($this->quote_expiration_datetime)) return false;

        return Carbon::parse($this->quote_expiration_datetime)->addHours(-Constants::TIMEZONE) < Carbon::now(Constants::TIMEZONE);
    }

    /**
     * Get paid attribute.
     *
     * @return boolean
     */
    public function getPaidAttribute()
    {
        return $this->status == Constants::BOOKING_PAID;
    }

    /**
     * Get partially refunded attribute.
     *
     * @return boolean
     */
    public function getPartiallyRefundedAttribute()
    {
        return $this->status == Constants::BOOKING_PARTIALLY_REFUNDED;
    }

    /**
     * Set departure time.
     *
     * @param  string $value
     * @return void
     */
    public function setDepartureTimeAttribute($value)
    {
        if ($value == null) {
            $this->attributes['departure_time'] = null;
        } else {
            $this->attributes['departure_time'] = Carbon::createFromFormat('H:i', $value);
        }
    }

    /**
     * Set return time.
     *
     * @param  string $value
     * @return void
     */
    public function setReturnTimeAttribute($value)
    {
        if ($value == null) {
            $this->attributes['return_time'] = null;
        } else {
            $this->attributes['return_time'] = Carbon::createFromFormat('H:i', $value);
        }
    }

    /**
     * Get price discounted by coupon.
     *
     * @return float|int
     */
    public function getDiscountedPriceAttribute(){
        if (!isset($this->coupon)) return $this->price;

        return CouponService::getDiscountedPrice($this->coupon, $this->price);
    }
}
