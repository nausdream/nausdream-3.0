<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class ExperienceFeedback
 * 
 * @property int $id
 * @property bool $score
 * @property string $text
 * @property int $experience_id
 * @property string $customer_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Experience $experience
 *
 * @package App\Models
 */
class ExperienceFeedback extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'experience_id' => 'int'
	];

	protected $fillable = [
		'score',
		'text',
		'experience_id',
		'customer_name'
	];

	public function experience()
	{
		return $this->belongsTo(\App\Models\Experience::class);
	}
}
