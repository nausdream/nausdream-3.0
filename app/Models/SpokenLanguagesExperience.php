<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class SpokenLanguagesExperience
 * 
 * @property int $id
 * @property int $experience_id
 * @property int $spoken_language_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Experience $experience
 * @property \App\Models\SpokenLanguage $spoken_language
 *
 * @package App\Models
 */
class SpokenLanguagesExperience extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'experience_id' => 'int',
		'spoken_language_id' => 'int'
	];

	protected $fillable = [
		'experience_id',
		'spoken_language_id'
	];

	public function experience()
	{
		return $this->belongsTo(\App\Models\Experience::class);
	}

	public function spoken_language()
	{
		return $this->belongsTo(\App\Models\SpokenLanguage::class);
	}
}
