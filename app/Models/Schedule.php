<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use App\Helpers\DateHelper;
use Carbon\Carbon;
use DateTime;
use Eloquent as Model;

/**
 * Class Schedule
 * 
 * @property int $id
 * @property \Carbon\Carbon $departure_time
 * @property \Carbon\Carbon $arrival_time
 * @property int $experience_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Experience $experience
 *
 * @package App\Models
 */
class Schedule extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'experience_id' => 'int'
	];

	protected $fillable = [
		'departure_time',
		'arrival_time',
		'experience_id'
	];

	public function experience()
	{
		return $this->belongsTo(\App\Models\Experience::class);
	}

    /**
     * Get departure time.
     *
     * @param  string  $value
     * @return string
     */
    public function getDepartureTimeAttribute($value)
    {
        return Carbon::createFromFormat("H:i:s", $value)->format("H:i");
    }

    /**
     * Get arrival time.
     *
     * @param  string  $value
     * @return string
     */
    public function getArrivalTimeAttribute($value)
    {
        return Carbon::createFromFormat("H:i:s", $value)->format("H:i");
    }

    /**
     * Get departure time formatted according to provided locale (fallback to app locale)
     *
     * @param $locale
     * @return string
     */
    public function getFormattedDepartureTime($locale = null){
        if (!isset($this->departure_time)) return null;

        return Carbon::parse($this->departure_time)->format(DateHelper::getTimeFormat($locale));
    }

    /**
     * Get arrival time formatted according to provided locale (fallback to app locale)
     *
     * @param $locale
     * @return string
     */
    public function getFormattedArrivalTime($locale = null){
        if (!isset($this->arrival_time)) return null;

        return Carbon::parse($this->arrival_time)->format(DateHelper::getTimeFormat($locale));
    }

    /**
     * Set departure time.
     *
     * @param  string  $value
     * @return void
     */
    public function setDepartureTimeAttribute($value)
    {
        $this->attributes['departure_time'] = Carbon::createFromFormat('H:i', $value);
    }

    /**
     * Set arrival time.
     *
     * @param  string  $value
     * @return void
     */
    public function setArrivalTimeAttribute($value)
    {
        $this->attributes['arrival_time'] = Carbon::createFromFormat('H:i', $value);
    }


    /**
     * Get duration of this schedule
     */
    public function getDuration() {
        return (new DateTime($this->arrival_time))->diff(new DateTime($this->departure_time))->format('%hh %im');
    }
}
