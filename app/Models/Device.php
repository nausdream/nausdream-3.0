<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class Device
 * 
 * @property int $id
 * @property string $name
 * @property int $height
 * @property int $width
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Device extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'height' => 'int',
		'width' => 'int'
	];

	protected $fillable = [
		'name',
		'height',
		'width'
	];
}
