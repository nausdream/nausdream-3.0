<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class Area
 *
 * @property int $id
 * @property string $name
 * @property int $order
 * @property boolean $hidden
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $harbour_centers
 *
 * @package App\Models
 */
class Area extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
        'name',
        'order',
        'hidden'
	];

	public function harbour_centers()
	{
		return $this->hasMany(\App\Models\HarbourCenter::class);
	}

	public function delete(){
	    $this->harbour_centers()->delete();
	    parent::delete();
    }
}
