<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Jul 2018 14:04:48 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class User
 * 
 * @property int $id
 * @property string $language
 * @property string $first_name
 * @property string $last_name
 * @property string $place_of_birth
 * @property \Carbon\Carbon $birth_date
 * @property string $city
 * @property string $address
 * @property string $zip_code
 * @property string $email
 * @property string $phone
 * @property bool $marketing_accepted
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 * @property \Illuminate\Database\Eloquent\Collection $tokens
 *
 * @package App\Models
 */
class User extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'marketing_accepted' => 'bool'
	];

	protected $dates = [
		'birth_date'
	];

	protected $fillable = [
		'language',
		'first_name',
		'last_name',
		'place_of_birth',
		'birth_date',
		'city',
		'address',
		'zip_code',
		'email',
		'phone',
		'marketing_accepted'
	];

	public function bookings()
	{
		return $this->hasMany(\App\Models\Booking::class);
	}

	public function tokens()
	{
		return $this->hasMany(\App\Models\Token::class);
	}
}
