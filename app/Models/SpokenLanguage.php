<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class SpokenLanguage
 * 
 * @property int $id
 * @property string $language
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $experiences
 *
 * @package App\Models
 */
class SpokenLanguage extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'language'
	];

	public function experiences()
	{
		return $this->belongsToMany(\App\Models\Experience::class, 'spoken_languages_experiences')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}
}
