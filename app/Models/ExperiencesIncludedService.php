<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class ExperiencesIncludedService
 * 
 * @property int $id
 * @property int $included_service_id
 * @property int $experience_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\IncludedService $included_service
 * @property \App\Models\Experience $experience
 *
 * @package App\Models
 */
class ExperiencesIncludedService extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'included_service_id' => 'int',
		'experience_id' => 'int'
	];

	protected $fillable = [
		'included_service_id',
		'experience_id'
	];

	public function included_service()
	{
		return $this->belongsTo(\App\Models\IncludedService::class);
	}

	public function experience()
	{
		return $this->belongsTo(\App\Models\Experience::class);
	}
}
