<?php
/**
 * User: Luca Puddu
 * Date: 02/03/2018
 * Time: 12:14
 */

namespace App\Models;

use Eloquent as Model;

class Seo extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'boat_type_id' => 'int',
        'harbour_center_id' => 'int'
    ];

    protected $fillable = [
        'query',
        'harbour_center_id',
        'boat_type_id',
        'language',
        'duration'
    ];

    public function harbour_center()
    {
        return $this->belongsTo(\App\Models\HarbourCenter::class);
    }

    public function boat_type()
    {
        return $this->belongsTo(\App\Models\BoatType::class);
    }
}