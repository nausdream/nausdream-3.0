<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use App\Constants;
use Eloquent as Model;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

/**
 * Class ExperienceCopy
 *
 * @property int $id
 * @property string $language
 * @property string $slug_url
 * @property string $meta_title
 * @property string $meta_description
 * @property string $redirect_slug_url
 * @property string $title
 * @property string $description
 * @property string $short_description
 * @property string $features
 * @property string $meeting_point
 * @property int $experience_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\Experience $experience
 *
 * @package App\Models
 */
class ExperienceCopy extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'experience_id' => 'int'
    ];

    protected $fillable = [
        'language',
        'slug_url',
        'meta_title',
        'meta_description',
        'redirect_slug_url',
        'title',
        'description',
        'short_description',
        'features',
        'meeting_point',
        'experience_id'
    ];

    public function experience()
    {
        return $this->belongsTo(\App\Models\Experience::class);
    }

    public function goTo(int $code = ResponseCodes::HTTP_MOVED_PERMANENTLY)
    {
        return redirect()->route(Constants::R_USERS_LISTING, [
            'lang' => $this->language,
            'slug_url' => $this->slug_url
        ], $code);
    }
}
