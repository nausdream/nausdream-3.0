<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class HarbourCenter
 * 
 * @property int $id
 * @property float $point_a_lat
 * @property float $point_a_lng
 * @property float $point_b_lat
 * @property float $point_b_lng
 * @property string $name
 * @property string $business_id
 * @property int $order
 * @property boolean $hidden
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $experiences
 * @property \App\Models\Area $area
 *
 * @package App\Models
 */
class HarbourCenter extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'point_a_lat' => 'float',
		'point_a_lng' => 'float',
		'point_b_lat' => 'float',
		'point_b_lng' => 'float'
	];

	protected $fillable = [
		'point_a_lat',
		'point_a_lng',
		'point_b_lat',
		'point_b_lng',
		'name',
        'business_id',
        'order',
        'hidden',
	];

	public function experiences()
	{
		return $this->hasMany(\App\Models\Experience::class);
	}

    public function seos()
    {
        return $this->hasMany(\App\Models\Seo::class);
    }

    public function area()
    {
        return $this->belongsTo(\App\Models\Area::class);
    }

    public function delete(){
        $this->seos()->delete();
        parent::delete();
    }
}
