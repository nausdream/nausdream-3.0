<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:50:25 +0000.
 */

namespace App\Models;

use Eloquent as Model;

/**
 * Class BoatType
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $experiences
 *
 * @package App\Models
 */
class BoatType extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'name'
	];

    public function experiences()
    {
        return $this->hasMany(\App\Models\Experience::class);
    }

    public function seos()
    {
        return $this->hasMany(\App\Models\Seo::class);
    }
}
