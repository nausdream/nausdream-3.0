<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:51:07 +0000.
 */

namespace App\TranslationModels;

use Eloquent as Model;

/**
 * Class Language
 * 
 * @property int $id
 * @property string $language
 * @property bool $rtl
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $sentence_translations
 *
 * @package App\TranslationModels
 */
class Language extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'rtl' => 'bool'
	];

	protected $fillable = [
		'language',
		'rtl'
	];

    protected $connection = 'mysql_translation';
    protected $table = 'languages';

	public function users()
	{
		return $this->belongsToMany(\App\TranslationModels\User::class, 'languages_users')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}

	public function sentence_translations()
	{
		return $this->hasMany(\App\TranslationModels\SentenceTranslation::class);
	}
}
