<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:51:07 +0000.
 */

namespace App\TranslationModels;

use Eloquent as Model;

/**
 * Class Sentence
 * 
 * @property int $id
 * @property string $name
 * @property int $page_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\TranslationModels\Page $page
 * @property \Illuminate\Database\Eloquent\Collection $sentence_translations
 *
 * @package App\TranslationModels
 */
class Sentence extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'page_id' => 'int'
	];

	protected $fillable = [
		'name',
		'page_id'
	];

	protected $connection = 'mysql_translation';
    protected $table = 'sentences';

	public function page()
	{
		return $this->belongsTo(\App\TranslationModels\Page::class);
	}

	public function sentence_translations()
	{
		return $this->hasMany(\App\TranslationModels\SentenceTranslation::class);
	}
}
