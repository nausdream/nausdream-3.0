<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:51:07 +0000.
 */

namespace App\TranslationModels;

use Eloquent as Model;

/**
 * Class LanguagesUser
 * 
 * @property int $id
 * @property int $user_id
 * @property int $language_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\TranslationModels\Language $language
 * @property \App\TranslationModels\User $user
 *
 * @package App\TranslationModels
 */
class LanguagesUser extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int',
		'language_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'language_id'
	];
    protected $connection = 'mysql_translation';
    protected $table = 'languages_users';

	public function language()
	{
		return $this->belongsTo(\App\TranslationModels\Language::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\TranslationModels\User::class);
	}
}
