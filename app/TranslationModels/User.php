<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:51:07 +0000.
 */

namespace App\TranslationModels;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property bool $is_admin
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $languages
 *
 * @package App\TranslationModels
 */
class User extends Authenticatable
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $guard = 'translator';
    public $remember_token=false;

    protected $casts = [
		'is_admin' => 'bool'
	];

	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'phone',
		'is_admin'
	];

    protected $connection = 'mysql_translation';
    protected $table = 'users';

	public function languages()
	{
		return $this->belongsToMany(\App\TranslationModels\Language::class, 'languages_users')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}
}
