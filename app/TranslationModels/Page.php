<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:51:07 +0000.
 */

namespace App\TranslationModels;

use Eloquent as Model;

/**
 * Class Page
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $sentences
 *
 * @package App\TranslationModels
 */
class Page extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'name'
	];

    protected $connection = 'mysql_translation';
    protected $table = 'pages';

	public function sentences()
	{
		return $this->hasMany(\App\TranslationModels\Sentence::class);
	}

    /**
     * Get all of the sentence translations for the page.
     */
    public function sentence_translations()
    {
        return $this->hasManyThrough('App\TranslationModels\SentenceTranslation', 'App\TranslationModels\Sentence');
    }
}
