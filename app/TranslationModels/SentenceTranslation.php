<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jan 2018 13:51:07 +0000.
 */

namespace App\TranslationModels;

use Eloquent as Model;

/**
 * Class SentenceTranslation
 * 
 * @property int $id
 * @property string $text
 * @property int $sentence_id
 * @property int $language_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\TranslationModels\Language $language
 * @property \App\TranslationModels\Sentence $sentence
 *
 * @package App\TranslationModels
 */
class SentenceTranslation extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'sentence_id' => 'int',
		'language_id' => 'int'
	];

	protected $fillable = [
		'text',
		'sentence_id',
		'language_id'
	];

    protected $connection = 'mysql_translation';
    protected $table = 'sentence_translations';

	public function language()
	{
		return $this->belongsTo(\App\TranslationModels\Language::class);
	}

	public function sentence()
	{
		return $this->belongsTo(\App\TranslationModels\Sentence::class);
	}
}
