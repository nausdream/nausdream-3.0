<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 05/01/2018
 * Time: 00:16
 */

namespace App;


class Constants
{
    // Database fields
    const INCLUDED_SERVICE_LENGTH = 100;
    const RULE_LENGTH = 100;
    const FIRST_NAME_LENGTH = 150;
    const LAST_NAME_LENGTH = 150;
    const EMAIL_LENGTH = 150;
    const PHONE_LENGTH = 150;
    const PLACE_OF_BIRTH_LENGTH = 100;
    const CURRENCY_LENGTH = 3;
    const LANGUAGE_LENGTH = 12;
    const FEEDBACK_CUSTOMER_NAME_LENGTH = 35;
    const FEEDBACK_TEXT_LENGTH = 120;
    const FEEDBACK_SCORE_MIN = 3;
    const FEEDBACK_SCORE_MAX = 5;
    const BUSINESS_ID_EXPERIENCE_TYPE_LENGTH = 4;
    const BUSINESS_ID_EXPERIENCE_LENGTH = 10;
    const BUSINESS_ID_HARBOUR_CENTER_LENGTH = 3;
    const PORT_NAME_LENGTH = 100;
    const TREKKSOFT_ID_LENGTH = 100;
    const HARBOUR_CENTER_NAME_LENGTH = 30;
    const SEASON_LENGTH = 20;
    const BOAT_TYPE_LENGTH = 20;
    const EXPERIENCE_TYPE_LENGTH = 20;
    const DEVICE_NAME_LENGTH = 20;
    const PAGE_NAME_LENGTH = 250;
    const EXPERIENCE_TITLE_LENGTH = 150;
    const BOOKING_EXPERIENCE_LINK_LENGTH = 100;
    const SENTENCE_NAME_LENGTH = 200;
    const DIFFICULTY_LENGTH = 15;
    const SENTIMENTS_LENGTH = 15;
    const EXPERIENCE_MEETING_POINT_LENGTH = 150;
    const MYSQL_TEXT_MAX_LENGTH = 65535;
    const QUERY_LENGTH = 100;
    const DURATION_LENGTH = 2;
    const AREA_NAME_LENGTH = 30;
    const PAYMENT_METHOD_NAME_LENGTH = 25;
    const CITY_NAME_LENGTH = 100;
    const ADDRESS_LENGTH = 300;
    const ZIP_CODE_LENGTH = 15;
    const CHANNEL_NAME_LENGTH = 100;
    const TOKEN_LENGTH = 64;
    const CAMPAIGN_NAME_LENGTH = 40;
    const COUPON_CODE_LENGTH = 25;

    // Other constants
    const LANGUAGE_COOKIE_DURATION = 60 * 24 * 365 * 10; // 10 years (in minutes)
    const GUESTS_ON_BOARD = 6000;
    const EXPERIENCES_COUNT = 30;
    const DEFAULT_CURRENCY = 'EUR';
    const LISTING_FORM_NAME_MAX_LENGTH = 50;
    const LISTING_FORM_COUPON_MAX_LENGTH = 50;
    const TIMEZONE = 2;
    const CHILDREN_DISCOUNT = 20;
    const PRE_EXPERIENCE_DELTA_HOURS = 48;
    const POST_EXPERIENCE_DELTA_HOURS = 120;
    const CUT_OFF_MINUTES = 960;
    const CONTACT_MIN_AGE = 18;
    const ADULT_MIN_AGE = 12;
    const CHILD_MIN_AGE = 0;
    const CHILD_MAX_AGE = 12;
    const DEFAULT_MEETING_SAFETY_MARGIN = 15;

    // Contacts
    const FB_PROFILE_LINK = 'https://facebook.com/nausdream';
    const INSTAGRAM_PROFILE_LINK = 'https://instagram.com/nausdream';
    const LINKEDIN_PROFILE_LINK = 'https://www.linkedin.com/company/nausdream';
    const COMPANY_NAME = 'Nausdream S.r.l.';
    const SITE_NAME = 'Nausdream.com';
    const TELEPHONE_NUMBER = '+39 070 85 50 052';
    const VAT_NUMBER = "03635840923";

    // Lat - lng
    const MAX_LATITUDE = 90;
    const MAX_LONGITUDE = 180;
    const OFFICE_LAT = 39.221220;
    const OFFICE_LNG = 9.107710;

    // Pagination
    const PAGES_PER_PAGE = 6;
    const MAX_PAGES_PER_PAGE = 50;
    const BOOKINGS_PER_PAGE = 6;
    const MAX_BOOKINGS_PER_PAGE = 50;
    const USERS_PER_PAGE = 6;
    const MAX_USERS_PER_PAGE = 50;
    const LISTINGS_PER_PAGE = 6;
    const MAX_LISTINGS_PER_PAGE = 50;
    const SEOS_PER_PAGE = 6;
    const MAX_SEOS_PER_PAGE = 50;

    // Photos
    const MAX_PHOTO_SIZE = 6291456; // in bytes
    const MAX_PHOTOS_PER_LISTING = 5;
    const EXPERIENCES_FOLDER = 'experiences/';

    // Seo
    const SEO_META_TITLE_LENGTH = 60;
    const SEO_META_DESCRIPTION_LENGTH = 300;
    const SEO_SLUG_URL_LENGTH = 55;
    const SEO_DOMAIN_NAME = 'Nausdream';
    const SEO_SEPARATOR = '|';
    const WEEKDAYS_LENGTH = 27;

    // Admin
    const ADMIN_DEFAULT_LANGUAGE = 'it';
    const ADMIN_PRODUCTS_PER_PAGE = 25;
    const ADMIN_PAGES_PER_PAGE = 25;
    const ADMIN_BOOKINGS_PER_PAGE = 50;
    const SUPPORT_EMAIL = 'support@nausdream.com';
    const SUPPORT_NAME = 'Nausdream';
    const BOOKING_EMAIL = 'booking@nausdream.com';
    const ADMINISTRATION_EMAIL = 'administration@nausdream.com';
    const COMPANY_WEBSITE = "www.nausdream.com";

    // Users
    const USERS_DEFAULT_LANGUAGE = 'en';

    // Guards
    const GUARD_ADMIN = 'admin';
    const GUARD_TRANSLATOR = 'translator';

    // Routes
    const LISTING_PREFIX = 'e';
    const HARBOUR_CENTER_PREFIX = 's';
    const HARBOUR_CENTER_DIRECT = 'destinations';
    const R_ADMIN_HOME = 'admin.home';
    const R_ADMIN_LOGIN = 'admin.login';
    const R_ADMIN_LOGOUT = 'admin.logout';
    const R_ADMIN_ACCOUNTKIT = 'admin.auth';
    const R_ADMIN_PRODUCTS_NEW = 'admin.products.new';
    const R_ADMIN_PRODUCTS_NEW_POST = 'admin.products.new.post';
    const R_ADMIN_PRODUCTS_LIST = 'admin.products.list';
    const R_ADMIN_PRODUCTS_EDIT_FORM = 'admin.products.edit';
    const R_ADMIN_PRODUCTS_EDIT_POST = 'admin.products.edit.post';
    const R_ADMIN_PRODUCTS_DELETE = 'admin.products.delete';
    const R_ADMIN_PRODUCTS_GET_COPY = 'admin.products.get_copy';
    const R_ADMIN_ERROR = 'admin.error';
    const R_ADMIN_SITEMAP_VIEW = 'admin.sitemap';
    const R_ADMIN_SITEMAP_GENERATE = 'admin.sitemap.generate';
    const R_ADMIN_SITEMAP_DOWNLOAD = 'admin.sitemap.download';
    const R_ADMIN_SITEMAP_DIRECT = 'admin.sitemap.direct';
    const R_ADMIN_INCLUDED_SERVICES = 'admin.included-services';
    const R_ADMIN_INCLUDED_SERVICES_ADD = 'admin.included-services.add';
    const R_ADMIN_INCLUDED_SERVICES_DELETE = 'admin.included-services.delete';
    const R_ADMIN_AREAS = 'admin.areas.list';
    const R_ADMIN_AREAS_EDIT = 'admin.areas.edit';
    const R_ADMIN_AREAS_NEW = 'admin.areas.new';
    const R_ADMIN_HC_NEW = 'admin.hc.new';
    const R_ADMIN_BOOKINGS_LIST = 'admin.bookings.list';
    const R_ADMIN_BOOKINGS_NEW = 'admin.bookings.new';
    const R_ADMIN_BOOKINGS_NEW_POST = 'admin.bookings.new.post';
    const R_ADMIN_BOOKINGS_EDIT_FORM = 'admin.bookings.edit';
    const R_ADMIN_BOOKINGS_EDIT_POST = 'admin.bookings.edit.post';
    const R_ADMIN_BOOKINGS_EMAIL_BODY = 'admin.bookings.body';
    const R_ADMIN_BOOKINGS_EMAIL_BODY_NEW = 'admin.bookings.body.new';
    const R_ADMIN_BOOKINGS_DELETE_POST = 'admin.bookings.delete.post';
    const R_SEO_META_TITLE = 'seo.meta-title';
    const R_SEO_SLUG_URL = 'seo.slug-url';
    const R_SEO_META_DESCRIPTION = 'seo.meta.description';
    const R_TRANSLATOR_HOME = 'translator.home';
    const R_TRANSLATOR_LOGIN = 'translator.login';
    const R_TRANSLATOR_LOGOUT = 'translator.logout';
    const R_TRANSLATOR_ACCOUNTKIT = 'translator.auth';
    const R_TRANSLATOR_PAGES_NEW_POST = 'translator.pages.new.post';
    const R_TRANSLATOR_PAGES_LIST = 'translator.pages.list';
    const R_TRANSLATOR_PAGES_EDIT_FORM = 'translator.pages.edit';
    const R_TRANSLATOR_PAGES_EDIT_POST = 'translator.pages.edit.post';
    const R_TRANSLATOR_PAGES_DELETE = 'translator.pages.delete';
    const R_TRANSLATOR_SENTENCES_NEW = 'translator.sentences.new';
    const R_TRANSLATOR_SENTENCES_DELETE = 'translator.sentences.delete';
    const R_TRANSLATOR_ERROR = 'translator.error';
    const R_USERS_HOME = 'users.home';
    const R_USERS_HARBOUR_CENTER = 'users.harbour-center';
    const R_USERS_LISTING = 'users.listing';
    const R_USERS_HARBOUR_CENTER_DIRECT = 'users.harbour-center-direct';
    const R_USERS_CONTACT_FORM = 'users.contact-form';
    const R_USERS_CONTACT_CUSTOM_FORM = 'users.contact-custom-form';
    const R_USERS_OLD_EXPERIENCE = 'users.old-experience';
    const R_USERS_NOT_FOUND = 'users.not-found';
    const R_USERS_COOKIES = 'users.cookies';
    const R_USERS_BOOKING_CHECKOUT = 'users.booking-checkout';
    const R_USERS_BOOKING_CHECKOUT_POST = 'users.booking.checkout';
    const R_USERS_BOOKING_PAY = 'users.booking-pay';
    const R_USERS_BOOKING_EXPIRED = 'users.booking-expired';
    const R_USERS_BOOKING_THANK_YOU = 'users.booking-thank-you';
    const R_USERS_OPT_IN = 'users.opt-in';
    const R_USERS_CAMPAIGNS_RULES = 'users.campaigns.rules';
    const R_USERS_COUPON_CHECK = 'users.coupon.check';

    // Analytics
    const TRACK_TIMINGS_RATIO = 100;
    const INSURANCE_POLICY = "112799339";

    // Booking Status
    const BOOKING_DRAFT = 0;
    const BOOKING_PAYMENT_LINK_SENT = 1;
    const BOOKING_PAID = 2;
    const BOOKING_EXPIRED = 3;
    const BOOKING_CANCELED = 4;
    const BOOKING_PARTIALLY_REFUNDED = 5;
    const BOOKING_REFUNDED = 6;

    // Operators
    const OPERATORS = [
        'RC' => "Ramona",
        'MM' => "Maria Antonietta",
        'OD' => "Ousmane"
    ];

    // Contact Channels
    const CONTACT_CHANNELS = [
        'phone' => "Telefono",
        'email' => "E-mail",
        'facebook' => "Facebook",
        'form' => "Form sito",
        'live_chat' => "Live Chat",
        'other' => "Altro"
    ];

    // Acquisition Channels
    const ACQUISITION_CHANNELS = [
        'google',
        'facebook',
        'instagram',
        'buzz',
        'other'
    ];

    // Coupons
    const COUPON_PERCENTAGE = 0;
    const COUPON_FIXED = 1;

    // Guest types
    const TYPE_ADULT = 0;
    const TYPE_CHILD = 1;
    const TRIPADVISOR_REVIEW_LINK = "https://www.tripadvisor.com/UserReviewEdit-g187881-d14543503-Nausdream-Cagliari_Province_of_Cagliari_Sardinia.html";
    const FB_REVIEW_LINK = "https://www.facebook.com/nausdream/reviews/";

    // Breakpoints
    const MOBILE_BREAKPOINT = 768;
}