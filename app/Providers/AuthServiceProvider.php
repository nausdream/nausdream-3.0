<?php

namespace App\Providers;

use App\Constants;
use Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('add-translations-page', function ($user) {
            return $user->is_admin;
        });
        Gate::define('delete-translations-page', function ($user) {
            return $user->is_admin;
        });
        Gate::define('add-sentence', function ($user) {
            return $user->is_admin;
        });
        Gate::define('delete-sentence', function ($user) {
            return $user->is_admin;
        });
    }
}
