<?php

namespace App\Providers;

use App\Helpers\StaticPagesHelper;
use App\TranslationModels\Sentence;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use PHPUnit\Framework\ExpectationFailedException;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Add page sentences validation rule
        Validator::extend('page_sentences_exist', function ($attribute, $value, $parameters, $validator) {
            $pageId = $parameters[0] ?? null;

            if (!isset($pageId)) {
                return false;
            }

            $valid = true;

            try {
                foreach ($value as $sentenceId => $sentenceTranslation){
                    $sentence = Sentence::findOrFail($sentenceId);

                    if ($sentence->page_id != $pageId) {
                        throw new Exception("Sentence doesn't exist on this page");
                    }
                }
            } catch (ModelNotFoundException $e) {
                $valid = false;
            } catch (Exception $e) {
                $valid = false;
            }


            return $valid;
        });

        // Add view composers
        View::composer('users.layout.header', 'App\Http\ViewComposers\HeaderComposer');
        View::composer('users.layout.footer', 'App\Http\ViewComposers\FooterComposer');
        View::composer('users.layout.base', 'App\Http\ViewComposers\BaseComposer');

        // Both are needed
        View::composer('errors::404', 'App\Http\ViewComposers\ErrorsComposer@notFound');
        View::composer('errors.404', 'App\Http\ViewComposers\ErrorsComposer@notFound');
        View::composer('errors::401', 'App\Http\ViewComposers\ErrorsComposer@notAuthorized');
        View::composer('errors.401', 'App\Http\ViewComposers\ErrorsComposer@notAuthorized');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Dev libraries
        if ($this->app->environment() !== 'production') {
            // Migration Generator
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
            // Models Generator
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
        }
    }
}
