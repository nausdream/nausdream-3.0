<?php
/**
 * User: Luca Puddu
 * Date: 15/03/2018
 * Time: 16:23
 */

namespace App\Services\Locale;


use App\Http\Controllers\Translations\TranslationsController;
use Cache;
use Illuminate\Translation\FileLoader;

class TranslationLoader extends FileLoader
{
    /**
     * Load the messages for the given locale.
     *
     * @param string $locale
     * @param string $group
     * @param string $namespace
     *
     * @return array
     */
    public function load($locale, $group, $namespace = null)
    {
        if ($namespace !== null && $namespace !== '*') {
            return $this->loadNamespaced($locale, $group, $namespace);
        }

        return Cache::remember("locale.fragments.{$locale}.{$group}", config('cache.time.translations'),
            function () use ($group, $locale) {
                $sentences = TranslationsController::getTranslations($group, $locale);

                // Return sentences
                return $sentences;
            });
    }
}