<?php
/**
 * User: Luca Puddu
 * Date: 15/03/2018
 * Time: 16:20
 */

namespace App\Services\Locale;

use Illuminate\Translation\TranslationServiceProvider as ServiceProvider;

class TranslationServiceProvider extends ServiceProvider
{
    protected function registerLoader()
    {
        $this->app->singleton('translation.loader', function ($app) {
            return new TranslationLoader($app['files'], $app['path.lang']);
        });
    }
}