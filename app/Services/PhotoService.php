<?php
/**
 * User: Luca Puddu
 * Date: 24/01/2018
 * Time: 15:31
 */

namespace App\Services;


use http\Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use JD\Cloudder\Facades\Cloudder;

class PhotoService
{
    public static function upload(UploadedFile $photo, String $photoPublicId, array $options = [], array $tags = []){
        if (!isset($photo) || !isset($photoPublicId)) return null;

        try{
            Cloudder::upload($photo, $photoPublicId, $options, $tags);
        } catch (Exception $exception) {
            Log::error("Error uploading photo.", ["exception" => $exception]);
        }
    }

    public static function delete(String $photoPublicId, array $options = []){
        if (!isset($photoPublicId)) {
            return;
        }
        try{
            Cloudder::destroyImage($photoPublicId, $options);
            Cloudder::delete($photoPublicId, $options);
        } catch (Exception $exception) {
            Log::error("Error deleting photo.", ["exception" => $exception]);
        }
    }
}