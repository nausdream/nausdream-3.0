<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 07/07/2018
 * Time: 13:17
 */

namespace App\Services;


use App\Constants;
use App\Helpers\PriceHelper;
use App\Models\Coupon;

/**
 * Class CouponService
 * @package App\Services
 */
class CouponService
{
    /**
     * Return discounted price given a coupon and a price value.
     *
     * @param Coupon $coupon
     * @param $price
     * @return float|int
     */
    public static function getDiscountedPrice(Coupon $coupon, $price)
    {
        if (!isset($coupon)) return $price;
        if ($coupon->stock == 0) return $price;
        if ($coupon->expired) return $price;

        switch ($coupon->type) {
            case Constants::COUPON_PERCENTAGE:
                return PriceHelper::getRoundedPrice(max(0, $price * (1 - ($coupon->value / 100))));
            case Constants::COUPON_FIXED:
                return PriceHelper::getRoundedPrice(max(0, $price - $coupon->value));
            default:
                return 0;
        }
    }
}