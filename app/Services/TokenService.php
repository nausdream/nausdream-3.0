<?php
/**
 * User: Luca Puddu
 * Date: 09/07/2018
 * Time: 11:56
 */

namespace App\Services;


use App\Constants;
use App\Models\Token;
use App\Models\User;

/**
 * Class TokenService
 * @package App\Services
 */
class TokenService
{
    /**
     * Create and return new token, and optionally associate it with a passed User
     *
     * @param User|null $user
     * @return Token
     */
    public static function createUnique(User $user = null)
    {
        do {
            $token = str_random(Constants::TOKEN_LENGTH);

            $isUnique = Token::where('token', $token)->get()->count() == 0;

        } while (!$isUnique);

        $token = Token::create([
            'token' => $token
        ]);

        if (isset($user)) {
           $token->user()->associate($user);
           $token->save();
        }

        return $token;
    }

    /**
     * Return true if token is valid for the passed user
     *
     * @param string $token
     * @param integer $user
     * @return bool
     */
    public static function isValidForUser($token, $user) {
        return Token::where('token', $token)
            ->where('user_id', $user)->get()->count() > 0;
    }

    /**
     * Return true is token exists.
     *
     * @param string|null $token
     * @return bool
     */
    public static function exists(string $token = null) {
        if (!isset($token)) return false;

        return Token::where('token', $token)->get()->count() > 0;
    }
}