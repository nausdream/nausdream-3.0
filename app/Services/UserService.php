<?php
/**
 * User: Luca Puddu
 * Date: 06/07/2018
 * Time: 18:21
 */

namespace App\Services;


use App\Models\Booking;
use App\Models\Lead;
use App\Models\User;

class UserService
{
    /**
     * Create a new user or update an existing one when a booking is created/edited
     *
     * @param Booking $booking
     */
    public static function createOrUpdateFromBooking(Booking $booking) {
        // Create or update user and associate him/her with this booking
        $user = User::updateOrCreate(['email' => $booking->user_email], [
            'first_name' => $booking->user_first_name,
            'last_name' => $booking->user_last_name,
            'place_of_birth' => $booking->user_place_of_birth,
            'birth_date' => $booking->user_birth_date,
            'city' => $booking->user_city,
            'address' => $booking->user_address,
            'zip_code' => $booking->user_zip_code,
            'language' => $booking->language,
            'email' => $booking->user_email,
            'phone' => $booking->user_phone
        ]);
        $booking->user()->associate($user);
        $booking->save();
    }

    /**
     * Create a new user or update an existing one when a request is sent from lead form
     *
     * @param Lead $lead
     */
    public static function createOrUpdateFromLead(Lead $lead) {
        $user = User::where('email', $lead->email)->get()->first();
        if (isset($user)) {
            $user->fill([
                'language' => $lead->language,
                'marketing_accepted' => $lead->marketing_accepted,
            ]);

            // If no booking is associated to the user, we can replace this kind of data
            if (!isset($user->last_name)) {
                $user->first_name = $lead->name;
                $user->phone = $lead->phone;
            }

            $user->save();
        } else {
            User::create([
                'first_name' => $lead->name,
                'language' => $lead->language,
                'email' => $lead->email,
                'phone' => $lead->phone,
                'marketing_accepted' => $lead->marketing_accepted,
            ]);
        }
    }
}