<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 06/07/2018
 * Time: 23:04
 */

namespace App\Services\Campaigns;


use App\Models\Booking;

/**
 * Interface CampaignInterface
 * @package App\Services\Campaigns
 */
interface CampaignInterface
{
    /**
     * Invoked when a booking is set to paid
     *
     * @param Booking $booking
     */
    public function onBookingPaid(Booking $booking);

    /**
     * Invoked when a booking is set to canceled
     *
     * @param Booking $booking
     */
    public function onBookingCanceled(Booking $booking);

    /**
     * Return template for campaign banner.
     *
     * @param Booking $booking
     * @return string|null
     * @throws \Throwable
     */
    public function emailBanner(Booking $booking);

    /**
     * Return template for opt-in view.
     *
     * @param Booking $booking
     * @return string|null
     * @throws \Throwable
     */
    public function optIn(Booking $booking);

    /**
     * Return template for rules view.
     *
     * @return string|null
     */
    public function rules();

    /**
     * Render campaign banner.
     *
     * @return mixed
     */
    public function banner();
}