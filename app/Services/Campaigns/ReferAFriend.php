<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 06/07/2018
 * Time: 23:04
 */

namespace App\Services\Campaigns;

use App\Constants;
use App\Mail\Campaigns\ReferAFriend\FriendCoupon;
use App\Mail\Campaigns\ReferAFriend\ReferralBooked;
use App\Models\Booking;
use App\Models\Campaign;
use App\Models\Coupon;
use App\Models\Token;
use App\Services\TokenService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class ReferAFriend implements CampaignInterface
{
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * ReferAFriend constructor.
     */
    public function __construct()
    {
        $campaign = Campaign::where('name', 'refer-a-friend')->get()->first();
        $this->campaign = $campaign ?? null;
    }


    /**
     * Create a new friend coupon and associate to booking.
     * Create a new referrer coupon if a friend coupon was used, and send it to referrer booking user via email.
     *
     * @param Booking $booking
     */
    public function onBookingPaid(Booking $booking)
    {
        if (!$this->campaign->is_active) return;

        // Timeframe for a new friend coupon creation
        if ($this->activeForNewUsers()) {
            // Create and associate friend coupon to booking
            $coupon = Coupon::create([
                'code' => $this->getUniqueCouponCode(),
                'value' => $this->campaign->attributes['friend_coupon']['value'],
                'type' => $this->campaign->attributes['friend_coupon']['type'],
                'expiration' => Carbon::parse(
                    $this->campaign->attributes['friend_coupon']['expiration']['date'],
                    $this->campaign->attributes['friend_coupon']['expiration']['timezone']
                ),
                'stock' => $this->campaign->attributes['friend_coupon']['stock']
            ]);

            $coupon->referrer_booking()->associate($booking);
            $coupon->save();

            // Create token for marketing opt-in
            TokenService::createUnique($booking->user);
        };

        // If a valid coupon was used for this booking
        if (isset($booking->coupon)) {
            // If the coupon being used is a friend coupon
            if (isset($booking->coupon->referrer_booking)) {
                // Generate new referrer_coupon
                $coupon = Coupon::create([
                    'code' => $this->getUniqueCouponCode(),
                    'value' => $this->campaign->attributes['referrer_coupon']['value'],
                    'type' => $this->campaign->attributes['referrer_coupon']['type'],
                    'expiration' => Carbon::parse(
                        $this->campaign->attributes['referrer_coupon']['expiration']['date'],
                        $this->campaign->attributes['referrer_coupon']['expiration']['timezone']
                    ),
                    'stock' => $this->campaign->attributes['referrer_coupon']['stock']
                ]);

                // Send email to the original booking user
                $referrerUser = $booking->coupon->referrer_booking->user;

                $friend = $booking->user_first_name . " " . $booking->user_last_name;

                if (isset($referrerUser) && isset($friend) && $referrerUser->marketing_accepted) {
                    // Unsub token for referrer
                    $token = TokenService::createUnique($referrerUser);
                    $unsubLink = route('users.unsubscribe-' . $referrerUser->language, ['token' => $token->token, 'uid' => $referrerUser->id]);

                    \App::setLocale($referrerUser->language);

                    Mail::queue((new ReferralBooked($unsubLink, $friend, $referrerUser, $coupon->code))->onQueue(config('queue.connections.sqs.mail')));
                }
            }
        }
    }

    /**
     * Invoked when a booking is set to canceled
     *
     * @param Booking $booking
     */
    public function onBookingCanceled(Booking $booking)
    {
    }

    /**
     * Return template for campaign banner in email.
     *
     * @param Booking $booking
     * @return string|null
     * @throws \Throwable
     */
    public function emailBanner(Booking $booking)
    {
        $token = $booking->user->tokens()->latest()->get()->first();
        if (isset($token)) $token = $token->token;

        return view('emails.users.campaigns.refer-a-friend.voucher', [
            'opt_in_link' => route(Constants::R_USERS_OPT_IN, [
                'lang' => $booking->language,
                'token' => $token,
                'buid' => $booking->id,
                'campaign' => $this->campaign ? $this->campaign->id : 0
            ]),
        ])->render();
    }

    /**
     * Return template for opt-in view.
     *
     * @param Booking $booking
     * @return string|null
     * @throws \Throwable
     */
    public function optIn(Booking $booking)
    {
        // Get unsubscribe link with token
        $uid = $booking->user->id;
        // Unsub token
        $token = TokenService::createUnique($booking->user);
        $unsubLink = route('users.unsubscribe-' . \App::getLocale(), ['token' => $token->token, 'uid' => $uid]);

        // Send email with friend coupon
        $coupon = $booking->referral_coupons()->get()->first();
        if (isset($coupon)) {
            Mail::queue((new FriendCoupon($booking, $unsubLink, $coupon->code))->onQueue(config('queue.connections.sqs.mail')));
        }

        return view('users.campaigns.refer-a-friend.opt-in', ['page' => 'refer-a-friend opt-in']);
    }

    public function getUniqueCouponCode()
    {
        do {
            $coupon = strtoupper(str_random(8));
        } while (Coupon::where('code', $coupon)->get()->count() > 0);

        return $coupon;
    }

    /**
     * Return template for rules view.
     *
     * @return string|null
     */
    public function rules()
    {
        $seo = [
            'title' => trans("refer_a_friend.rules_page_title") . " " . Constants::SEO_SEPARATOR . " " . Constants::SEO_DOMAIN_NAME,
            'description' => trans("refer_a_friend.rules_meta_description"),
            'og_type' => 'website',
            'og_url' => request()->url(),
            'alternates' => [],
            'og_images' => [config('services.cloudinary.secure_url') . '/' . config('services.cloudinary.base_folder') . "assets/img/campaigns/refer-a-friend/banner-" . \App::getLocale()],
        ];

        return view('users.campaigns.refer-a-friend.rules', [
            'page' => 'refer-a-friend rules',
            'seo' => $seo,
            'banner' => config('services.cloudinary.secure_url') . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/campaigns/refer-a-friend/banner-" . \App::getLocale()
        ]);
    }

    /**
     * Render campaign banner.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function banner()
    {
        if ($this->activeForNewUsers()) {
            return view('users.campaigns.refer-a-friend.banner')->render();
        }

        return null;
    }

    /**
     * Return true if new bookings are eligible.
     *
     * @return boolean
     */
    private function activeForNewUsers()
    {
        $start = Carbon::parse($this->campaign->attributes['start']['date'], $this->campaign->attributes['start']['timezone']) ?? Carbon::now()->addDay(2);
        $stop = Carbon::parse($this->campaign->attributes['stop']['date'], $this->campaign->attributes['stop']['timezone']) ?? Carbon::now()->addDay(-2);
        $now = Carbon::now(Constants::TIMEZONE);

        return $now->greaterThan($start) && $now->lessThan($stop);
    }
}