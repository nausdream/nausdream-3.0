<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 06/07/2018
 * Time: 23:12
 */

namespace App\Services\Campaigns;


use App\Models\Booking;

class CampaignService
{
    /**
     * Get a CampaignInterface instance given a name
     *
     * @param string $name
     * @return CampaignInterface|null
     */
    public static function getCampaign(string $name = null)
    {
            switch ($name){
                case 'refer-a-friend':
                    return new ReferAFriend();
                default:
                    return null;
            }
    }
}