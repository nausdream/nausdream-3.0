<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 13/01/2018
 * Time: 21:04
 */

namespace App\Services;

use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;
use Symfony\Component\HttpFoundation\Response;


class RequestService
{

    /**
     * Send an Ajax GET request with attached data to a given URL
     *
     * @param $url
     * @param $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Psr\Http\Message\ResponseInterface|\Symfony\Component\HttpFoundation\Response
     */
    public static function get($url, $data){
        $client = new \GuzzleHttp\Client();

        try {
            $res = $client->request('GET', $url, [
                'query' => $data
            ]);
        }
        catch (RequestException $e){
            return new Response($e->getMessage(), $e->getCode());
        }
        catch (\Exception $e){
            return new Response($e->getMessage(), ResponseCodes::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $res;
    }
}