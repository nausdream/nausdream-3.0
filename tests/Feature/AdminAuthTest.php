<?php

namespace Tests\Feature;

use App\Constants;
use App\Models\Administrator;
use const Grpc\CALL_ERROR_NOT_ON_SERVER;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

class AdminAuthTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @return void
     */
    public function redirects_to_login_page_with_401_if_not_authenticated_on_protected_pages()
    {
        $response = $this->get(\URL::route(Constants::R_ADMIN_HOME, [], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);

        $response = $this->get(\URL::route(Constants::R_ADMIN_LOGOUT, [], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);

        $response = $this->get(\URL::route(Constants::R_ADMIN_PRODUCTS_LIST, [], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);

        $response = $this->get(\URL::route(Constants::R_ADMIN_PRODUCTS_NEW, [], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);

        $response = $this->get(\URL::route(Constants::R_ADMIN_PRODUCTS_EDIT_FORM, ['id' => 1], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);
    }

    /**
     * @test
     * @return void
     */
    public function returns_403_if_facebook_code_is_wrong()
    {
        $this->withoutMiddleware();

        $response = $this->post(\URL::route(Constants::R_ADMIN_ACCOUNTKIT, ['code' => null], false));
        $response->assertStatus(ResponseCodes::HTTP_FORBIDDEN);

        $response = $this->post(\URL::route(Constants::R_ADMIN_ACCOUNTKIT, ['code' => ''], false));
        $response->assertStatus(ResponseCodes::HTTP_FORBIDDEN);

        $response = $this->post(\URL::route(Constants::R_ADMIN_ACCOUNTKIT, [], false));
        $response->assertStatus(ResponseCodes::HTTP_FORBIDDEN);

        $response = $this->post(\URL::route(Constants::R_ADMIN_ACCOUNTKIT, ['code' => '304930930493049'], false));
        $response->assertStatus(ResponseCodes::HTTP_FORBIDDEN);
    }

    /**
     * @test
     * @return void
     */
    public function returns_200_if_authenticated_on_protected_pages()
    {
        $user = factory(Administrator::class)->create([
            'phone' => '+393495867908',
        ]);

        $response = $this->actingAs($user, Constants::GUARD_ADMIN)->get(\URL::route(Constants::R_ADMIN_PRODUCTS_LIST, [], false));
        $response->assertStatus(ResponseCodes::HTTP_OK);

        $response = $this->get(\URL::route(Constants::R_ADMIN_PRODUCTS_NEW, [], false));
        $response->assertStatus(ResponseCodes::HTTP_OK);

        $response = $this->get(\URL::route(Constants::R_ADMIN_PRODUCTS_EDIT_FORM, ['id' => 1], false));
        $response->assertStatus(ResponseCodes::HTTP_OK);
    }

    /**
     * @test
     * @return void
     */
    public function returns_302_if_authenticated_on_root()
    {
        $user = factory(Administrator::class)->create([
            'phone' => '+393495867908',
        ]);

        $response = $this->actingAs($user, Constants::GUARD_ADMIN)->get(\URL::route(Constants::R_ADMIN_HOME, [], false));
        $response->assertStatus(ResponseCodes::HTTP_FOUND);
    }
}
