<?php

namespace Tests\Feature;

use App\Constants;
use App\Models\Administrator;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\User;
use const Grpc\CALL_ERROR_NOT_ON_SERVER;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response as ResponseCodes;

class TranslatorAuthTest extends TestCase
{
    use DatabaseTransactions;


    /**
     * @test
     * @return void
     */
    public function redirects_to_login_page_with_401_if_not_authenticated_on_protected_pages()
    {
        $response = $this->get(\URL::route(Constants::R_TRANSLATOR_HOME, [], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);

        $response = $this->get(\URL::route(Constants::R_TRANSLATOR_LOGOUT, [], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);

        $response = $this->get(\URL::route(Constants::R_TRANSLATOR_PAGES_LIST, [], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);

        $response = $this->get(\URL::route(Constants::R_TRANSLATOR_PAGES_EDIT_FORM, ['id' => 1], false));
        $response->assertStatus(ResponseCodes::HTTP_UNAUTHORIZED);
    }

    /**
     * @test
     * @return void
     */
    public function blocks_post_requests_with_419_if_not_authenticated()
    {
        $response = $this->post(\URL::route(Constants::R_TRANSLATOR_PAGES_NEW_POST, ['name' => 'prova'], false));
        $response->assertStatus(419); // Not a standard HTTP Code

        $page = Page::create(["name" => "sdkflksdlfkdslfkdsl"]);
        $sentence = $page->sentences()->create(["name" => "sjdkfjdskf"]);

        $response = $this->post(\URL::route(Constants::R_TRANSLATOR_PAGES_EDIT_POST, [
            "id" => $page->id,
            "language" => Language::first()->id,
            "sentences" => [$sentence->id, "Hey!"]
        ], false));
        $response->assertStatus(419); // Not a standard HTTP Code
    }

    /**
     * @test
     * @return void
     */
    public function returns_403_if_facebook_code_is_wrong()
    {
        $this->withoutMiddleware();

        $response = $this->post(\URL::route(Constants::R_TRANSLATOR_ACCOUNTKIT, ['code' => null], false));
        $response->assertStatus(ResponseCodes::HTTP_FORBIDDEN);

        $response = $this->post(\URL::route(Constants::R_TRANSLATOR_ACCOUNTKIT, ['code' => ''], false));
        $response->assertStatus(ResponseCodes::HTTP_FORBIDDEN);

        $response = $this->post(\URL::route(Constants::R_TRANSLATOR_ACCOUNTKIT, [], false));
        $response->assertStatus(ResponseCodes::HTTP_FORBIDDEN);

        $response = $this->post(\URL::route(Constants::R_TRANSLATOR_ACCOUNTKIT, ['code' => '304930930493049'], false));
        $response->assertStatus(ResponseCodes::HTTP_FORBIDDEN);
    }

    /**
     * @test
     * @return void
     */
    public function returns_200_if_authenticated_on_protected_pages()
    {
        $user = User::where('phone', '+393495867908')->first();

        if (!isset($user)) {
            $user = factory(User::class)->create([
                'phone' => '+393495867908',
            ]);
        }

        $response = $this->actingAs($user, Constants::GUARD_TRANSLATOR)->get(\URL::route(Constants::R_TRANSLATOR_PAGES_LIST, [], false));
        $response->assertStatus(ResponseCodes::HTTP_OK);

        $response = $this->get(\URL::route(Constants::R_TRANSLATOR_PAGES_EDIT_FORM, ['id' => 1], false));
        $response->assertStatus(ResponseCodes::HTTP_OK);
    }

    /**
     * @test
     * @return void
     */
    public function returns_302_if_authenticated_on_root()
    {
        $user = User::where('phone', '+393495867908')->first();

        if (!isset($user)) {
            $user = factory(User::class)->create([
                'phone' => '+393495867908',
            ]);
        }

        $response = $this->actingAs($user, Constants::GUARD_TRANSLATOR)->get(\URL::route(Constants::R_TRANSLATOR_HOME, [], false));
        $response->assertStatus(ResponseCodes::HTTP_FOUND);
    }
}
