<?php

use App\Http\Controllers\Translations\TranslationsController;

return TranslationsController::getTranslations('home', 'en');