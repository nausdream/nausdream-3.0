<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 22/01/2018
 * Time: 16:06
 */

use App\Http\Controllers\Translations\TranslationsController;

return TranslationsController::getTranslations('included_services', 'en');