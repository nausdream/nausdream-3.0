<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 22/01/2018
 * Time: 15:55
 */

use App\Http\Controllers\Translations\TranslationsController;

return TranslationsController::getTranslations('sentiments', 'it');