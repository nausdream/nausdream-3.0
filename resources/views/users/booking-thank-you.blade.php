@extends('users.layout.base')

@section('head')
    <title>{!! trans('booking.thank_you_page_title') !!}</title>
    <meta name="robots" content="noindex, nofollow">
@endsection

@section('content')
    <div class="pad-md mar-bottom-sm max-width text-center">
        <h4>{!! trans('booking.thank_you', ['name' => $booking->user_first_name]) !!}</h4>
        <p>{!! trans('booking.contact_us', ['email' => '<a href="mailto:'.\App\Constants::BOOKING_EMAIL.'">'.trans('booking.support').'</a>']) !!}</p>
    </div>
@endsection

@section('scripts')
@endsection