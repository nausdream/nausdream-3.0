@extends('users.layout.base')

@section('head')
    <title>{!! trans('unsubscribe.title') !!}</title>
@endsection

@section('content')
    <div class="pad-md mar-bottom-sm max-width text-center">
        <p>{!! trans('unsubscribe.sorry') !!}</p>
        <h2>{!! trans('unsubscribe.miss_you') !!}</h2>
        <p>{!! trans('unsubscribe.follow_us', ['fb' => $fb, 'ig' => $ig]) !!}</p>
    </div>
@endsection

@section('scripts')
@endsection