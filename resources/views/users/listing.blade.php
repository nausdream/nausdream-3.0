@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent
@endsection

@section('content')
    {{--CONTACT EMAIL SENT--}}
    @if($email_sent)
        <!-- Modal -->
        <div class="modal fade" id="contact-form-modal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content pad-md">
                    <button type="button" class="close pad-lg" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title w-100 text-center mar-bottom-md pad-left-md pad-right-md"
                        id="cart-abandonment-title">@lang('listing.request_sent')</h5>
                    <div class="text-center">
                        @lang('listing.thank_you')
                    </div>
                </div>
            </div>
        </div>
    @endif

    {{--SHOPPING CART ABANDONMENT--}}
    <!-- Modal -->
    <div class="modal fade" id="cart-abandonment" tabindex="-1" role="dialog" aria-labelledby="cart-abandonment-title"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content pad-md">
                <button type="button" class="close pad-lg" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title w-100 text-center mar-bottom-md pad-left-md pad-right-md"
                    id="cart-abandonment-title">@lang('listing.dont_go_text')</h5>
                <div class="mar-bottom-sm text-center">
                    @component('users.components.call-us-btn', [
                'phone' => $phone, 'phone_formatted' => $phone_formatted, 'analytics_label' => $listing['business_id'],
                'classes' => 'table-center-cells mar-auto'
                ])
                    @endcomponent
                </div>
                <div class="mar-bottom-md customer-support-times text-center">
                    <span class="text-md">@lang('listing.customer_support_times')</span>
                </div>
                <div class="text-center">
                    <a href="#book-form" class="btn btn-primary request"
                       data-scroll-offset="75"
                       data-dismiss="modal">@lang('listing.request')</a>
                </div>
            </div>
        </div>
    </div>

    {{--MOBILE PRICE--}}
    <div class="d-lg-none listing-price-box container-fluid">
        <div class="container-fluid pad-none mar-bottom-sm">
            <div class="row">
                <div class="col pad-right-sm">
                    <div class="center-vertically">
                            <span class="listing-price">{{\App\Helpers\PriceHelper::getFormattedPrice(
                App::getLocale(),
                $listing['price'],
                $listing['currency'])}}</span><br><span class="text-sm">@lang('listing.per_person')</span>
                    </div>
                </div>
                <div class="col pad-left-sm">
                    @component('users.components.call-us-btn', [
                    'phone' => $phone, 'phone_formatted' => $phone_formatted, 'analytics_label' => $listing['business_id'],
                    'classes' => 'table-center-cells center-vertically'
                    ])
                    @endcomponent
                </div>
            </div>
        </div>
        <div>
            <a href="#" class="read-more cta-btn" id="trekksoft-mobile"
               data-trekksoft-id="{{$listing['trekksoft_id']}}"
               data-form-id="#book-form" data-scroll-offset="0"
               data-analytics
               data-event-category="Button {{$action}}"
               data-event-label="{{$listing['business_id']}}">
                    <span class="text-lg">
                        @lang('listing.'.$action)
                    </span>
                <i class="icon icon-right-open"></i>
            </a>
        </div>
    </div>

    {{--CAROUSEL--}}
    <div id="mobile-listing-carousel" class="carousel listing-carousel slide" data-ride="carousel"
         data-interval="false">
        <div class="carousel-inner">
            @foreach($listing['photos'] as $id => $is_cover)
                <div class="w-100 carousel-item @if($is_cover) active @endif">
                    @component('users.components.picture', [
                        'versions' => ['320' => 'q_60,w_320', '500' => 'q_80,w_500'],
                        'def' => 'q_100,w_1280',
                        'url' => \App\Constants::EXPERIENCES_FOLDER . $listing['experience_id'] . '/' . $id,
                        'classes' => 'carousel-picture'
                    ])
                    @endcomponent
                </div>
            @endforeach
        </div>
        <a href="" class="d-md-none carousel-control carousel-control-prev" data-target="#mobile-listing-carousel"
           role="button"
           data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a href="" class="d-md-none carousel-control carousel-control-next" data-target="#mobile-listing-carousel"
           role="button"
           data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <div class="listing-title-desktop-container d-none d-md-block">
            <h1 class="listing-max-width listing-main-title pad-md">{{$listing['title']}}</h1>
        </div>
    </div>

    {{--BREADCRUMB--}}
    <div class="listing-breadcrumb">
        <a href="/{{App::getLocale()}}">@lang('listing.home')</a>
        <span class="breadcrumb-separator"> / </span>
        <a href="/{{App::getLocale()}}/{{\App\Constants::HARBOUR_CENTER_DIRECT}}/{{$listing['harbour_center']}}"
        >@lang("harbour_centers.{$listing['harbour_center']}")</a>
        <span class="breadcrumb-separator"> / </span>
        <span>{{$listing['title']}}</span>
    </div>

    {{--TITLE--}}
    <h1 class="listing-main-title d-md-none pad-md mar-none">{{$listing['title']}}</h1>

    {{--FIXED TITLE ON SCROLL DOWN--}}
    <div class="listing-title-fixed" data-affix data-affix-to="#desktop-price-box-affix" data-affix-offset="20">
        <div class="listing-title-fixed-content">
            <h1 class="listing-main-title">{{$listing['title']}}</h1>
        </div>
    </div>

    <hr class="mar-top-none">

    <div class="listing-max-width">
        {{--DESKTOP PRICE--}}
        <div id="desktop-price-box-affix"></div>
        <div class="desktop-price-box d-none d-lg-block" data-affix data-affix-to="#desktop-price-box-affix"
             data-affix-offset="20">
            <div class="mar-bottom-sm">
                <span class="listing-price">{{\App\Helpers\PriceHelper::getFormattedPrice(
                App::getLocale(),
                $listing['price'],
                $listing['currency'])}}</span><br>
                <span class="per-person">@lang('listing.per_person')</span>
            </div>
            <div class="mar-bottom-md">
                <a href="#" class="read-more cta-btn table-center-cells"
                   data-trekksoft-id="{{$listing['trekksoft_id']}}"
                   id="trekksoft-desktop" data-form-id="#book-form" data-scroll-offset="75"
                   data-analytics
                   data-event-category="Button {{$action}}"
                   data-event-label="{{$listing['business_id']}}">
                    <span class="text-left">
                        @lang('listing.'.$action)
                    </span>
                    <i class="icon icon-right-open"></i>
                </a>
            </div>
            <div>
                @component('users.components.call-us-btn', [
                    'phone' => $phone, 'phone_formatted' => $phone_formatted, 'analytics_label' => $listing['business_id'],
                    'classes' => 'table-center-cells'
                    ])
                @endcomponent
            </div>
        </div>

        {{--CONTENT--}}
        <div class="listing-body">
            {{--RECAP--}}
            <div class="listing-section">
                <table class="quick-info">
                    <tbody>
                    <tr>
                        <td class="td-icon"><i class="icon icon-calendar"></i></td>
                        <td class="td-text pad-left-sm">
                                <span>
                                    @if(count($listing['weekdays']) == 7)
                                        @lang('listing.every_day')
                                    @else
                                        @lang('listing.availability')
                                        @foreach($listing['weekdays'] as $weekday)
                                            @lang('weekdays.' . $weekday)@if(!$loop->last),@endif
                                        @endforeach
                                    @endif
                                </span>
                        </td>
                        <td class="td-icon d-none d-md-table-cell pad-left-lg"><i class="icon icon-clock"></i></td>
                        <td class="td-text d-none d-md-table-cell pad-left-sm">
                            <span>@lang('listing.duration', ['duration' => $schedules->first()->getDuration()])</span>
                        </td>
                    </tr>
                    <tr class="d-md-none">
                        <td><i class="icon icon-clock"></i></td>
                        <td class="pad-left-sm">
                            <span>@lang('listing.duration', ['duration' => $schedules->first()->getDuration()])</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="td-icon"><i class="icon icon-language"></i></td>
                        <td class="td-text pad-left-sm">
                                <span>
                                    @lang('listing.languages', ['languages'=>$spoken_languages])
                                </span>
                        </td>
                        <td class="td-icon d-none d-md-table-cell pad-left-lg"><i class="icon icon-child"></i></td>
                        <td class="td-text d-none d-md-table-cell pad-left-sm">
                            <span>@lang('listing.children', ['discount' => \App\Constants::CHILDREN_DISCOUNT])</span>
                        </td>
                    </tr>
                    <tr class="d-md-none">
                        <td><i class="icon icon-child"></i></td>
                        <td class="pad-left-sm">
                            <span>@lang('listing.children', ['discount' => \App\Constants::CHILDREN_DISCOUNT])</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--SCHEDULES--}}
            <div class="listing-section">
                <div>
                    <span class="listing-section-title">@lang('listing.departure_times')</span>
                </div>
                @if($schedules->count() == 1)
                    <div>
                    <span>
                        @lang('listing.departure_time', ['time' => $schedules->first()->getFormattedDepartureTime()])
                    </span>
                    </div>
                    <div>
                    <span>
                        @lang('listing.arrival_time', ['time' => $schedules->first()->getFormattedArrivalTime()])
                    </span>
                    </div>
                @else
                    @foreach($schedules as $schedule)
                        <div>
                            @lang('listing.schedule_time', [
                            'count' => $loop->iteration,
                            'departure' => $schedule->getFormattedDepartureTime(),
                            'arrival' => $schedule->getFormattedArrivalTime()
                            ])
                        </div>
                    @endforeach
                @endif
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--STOPS--}}
            <div class="listing-section">
                <div>
                    <span class="listing-section-title">@lang('listing.stops')</span>
                </div>
                <div class="timeline">
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-time">
                            <span class="font-bold">@lang('listing.departure_port')</span>
                        </div>
                        <div>
                            <span>{{$listing['departure_port']}}</span>
                        </div>
                    </div>
                    @foreach($stops as $stop)
                        <div class="timeline-item">
                            <div class="timeline-marker"></div>
                            <div class="timeline-time">
                            <span class="font-bold">@lang('listing.stop', [
                                'number' => $stop->stop_number
                            ])
                            </span>
                                @if($stop->duration)
                                    <span>@lang('listing.stop_duration', ['duration' => $stop->duration])</span>
                                @endif
                            </div>
                            <div class="timeline-info">{{$stop->text}}
                            </div>
                        </div>
                    @endforeach
                    <div class="timeline-item">
                        <div class="timeline-marker"></div>
                        <div class="timeline-time">
                            <span class="font-bold">@lang('listing.arrival_port')</span>
                        </div>
                        <div>
                            <span>{{$listing['departure_port']}}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--DESCRIPTION--}}
            <div class="listing-section">
                <div>
                    <span class="listing-section-title">@lang('listing.description')</span>
                </div>
                <p>{!! $listing['description'] !!}</p>
                <div id="desktop-listing-carousel" class="d-none d-md-block carousel listing-carousel slide"
                     data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($listing['photos'] as $id => $is_cover)
                            <div class="w-100 carousel-item @if($is_cover) active @endif">
                                @component('users.components.picture', [
                                    'versions' => ['320' => 'q_60,w_320', '500' => 'q_80,w_500'],
                                    'def' => 'q_100,w_1280',
                                    'url' => \App\Constants::EXPERIENCES_FOLDER . $listing['experience_id'] . '/' . $id,
                                    'classes' => 'carousel-picture'
                                ])
                                @endcomponent
                            </div>
                        @endforeach
                    </div>
                    <a href="" class="carousel-control carousel-control-prev" data-target="#desktop-listing-carousel"
                       role="button"
                       data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a href="" class="carousel-control carousel-control-next" data-target="#desktop-listing-carousel"
                       role="button"
                       data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--FEATURES--}}
            <div class="listing-section">
                <div>
                    <span class="listing-section-title">@lang('listing.features')</span>
                </div>
                <ul class="listing-list">
                    @if($boat_length == 1)
                        <li>@lang('listing.boat_length', ['length' => $listing['boat_length_max']])</li>
                    @elseif($boat_length == 2)
                        <li>@lang('listing.boat_length_range', ['min' => $listing['boat_length_min'], 'max' => $listing['boat_length_max']])</li>
                    @endif
                    <li>@lang('listing.sentiment', ['sentiments'=>$sentiments])</li>
                    <li>@lang('listing.difficulty', ['difficulty' => trans('difficulties.'.$listing['difficulty'])])</li>
                    <li>@lang('listing.languages', ['languages'=>$spoken_languages])</li>
                    <li>@lang('listing.duration', ['duration' => $schedules->first()->getDuration()])</li>
                    @foreach($features as $feature)
                        <li>{!! $feature !!}</li>
                    @endforeach
                </ul>
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--INCLUDED SERVICES--}}
            @if($services->count())
                <div class="listing-section">
                    <div>
                        <span class="listing-section-title">@lang('listing.included_services')</span>
                    </div>
                    <ul class="listing-list">
                        @foreach($services as $service)
                            <li>@lang("included_services.$service->name")</li>
                        @endforeach
                    </ul>
                </div>

                <div class="hr-container">
                    <hr>
                </div>
            @endif

            {{--MAP--}}
            <div class="listing-section mar-bottom-sm">
                <div>
                    <span class="listing-section-title">@lang('listing.meeting_point')</span>
                </div>
                <div class="mar-bottom-sm"><span class="font-bold">{{$listing['meeting_point']}}</span></div>
                <div class="map"
                     data-analytics
                     data-trigger="Scroll"
                     data-event-category="Page section map"
                     data-event-label="{{$listing['business_id']}}">
                    <a href="{{$map['link']}}" target="_blank"
                       data-analytics
                       data-event-category="Button map"
                       data-event-label="{{$listing['business_id']}}">
                        <img src="{{$map['image']}}">
                    </a>
                </div>
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--WHEN--}}
            <div class="listing-section">
                <div>
                    <span class="listing-section-title">@lang('listing.when')</span>
                </div>
                <div class="mar-bottom-sm">
                <span>@lang('listing.availability')
                    @foreach($listing['weekdays'] as $weekday)
                        @lang('weekdays.' . $weekday)@if(!$loop->last),@endif
                    @endforeach
                </span>
                </div>
                @if($schedules->count() == 1)
                    <div>
                    <span>
                        @lang('listing.departure_time', ['time' => $schedules->first()->getFormattedDepartureTime()])
                    </span>
                        <br>
                        <span>
                        @lang('listing.arrival_time', ['time' => $schedules->first()->getFormattedArrivalTime()])
                    </span>
                    </div>
                @else
                    <div>
                        @foreach($schedules as $schedule)
                            <span>
                            @lang('listing.schedule_time', [
                            'count' => $loop->iteration,
                            'departure' => $schedule->getFormattedDepartureTime(),
                            'arrival' => $schedule->getFormattedArrivalTime()
                            ])
                        </span><br>
                        @endforeach
                    </div>
                @endif
                <div class="mar-top-sm">
                    <span>@lang('listing.safety_margin_warning', ['margin' => $listing['meeting_safety_margin']])</span>
                </div>
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--THINGS TO REMEMBER--}}
            <div class="listing-section">
                <div>
                    <span class="listing-section-title">@lang('listing.remember')</span>
                </div>
                <ul class="listing-list">
                    <li>@lang('listing.smoking')</li>
                    <li>@lang('listing.heels')</li>
                </ul>
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--FEEDBACKS--}}
            @if($feedbacks->count())
                <div class="listing-section">
                    <div class="mar-bottom-sm">
                        <span class="listing-section-title">@lang('listing.feedbacks')</span>
                    </div>
                    @foreach($feedbacks as $feedback)
                        <div class="feedback mar-bottom-sm">
                            <div>
                                <span class="feedback-name font-bold">{{$feedback->customer_name}}</span>
                                <span class="feedback-stars">
                            @for($i = 0; $i < $feedback->score; $i++)
                                        <i class="icon icon-star"></i>
                                    @endfor
                                    @for($i = $feedback->score; $i < 5; $i++)
                                        <i class="icon icon-star-empty"></i>
                                    @endfor
                        </span>
                            </div>
                            <div class="feedback-text font-italic">{{$feedback->text}}</div>
                        </div>
                    @endforeach
                </div>

                <div class="hr-container">
                    <hr>
                </div>
            @endif

            {{--CANCELLATION POLICIES--}}
            <div class="listing-section">
                <div id="cancellation-policies">
                    <div class="card">
                        <div class="card-header" id="policy-title">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#policy-collapse"
                                        aria-expanded="false" aria-controls="policy-collapse">
                                    <i class="icon icon-doc-text"></i><span>@lang('listing.cancellation_policy_title')</span>
                                </button>
                            </h5>
                        </div>

                        <div id="policy-collapse" class="collapse" aria-labelledby="policy-title"
                             data-parent="cancellation-policies">
                            <div class="card-body">
                                <span>@lang('listing.cancellation_policy_text')</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hr-container">
                <hr>
            </div>

            {{--FORM--}}
            <div class="listing-section">
                <div id="book-form" class="pad-md bg-white">
                    <div>
                        <span class="listing-section-title">@lang('listing.form')</span>
                    </div>
                    <div class="mar-bottom-sm"><span>@lang('listing.form_cta')</span></div>
                    <div>
                        <form id="book" action="{{route(App\Constants::R_USERS_CONTACT_FORM, [
                            'lang' => App::getLocale(),
                            'slug_url' => $listing['slug_url']
                        ])}}"
                              method="POST"
                              data-analytics
                              data-trigger="Submit"
                              data-event-category="Form booking"
                              data-event-label="{{$listing['business_id']}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="form-name">@lang('listing.name')</label>
                                <input type="text" class="form-control" id="form-name" name="name"
                                       value="{{old('name')}}"
                                       maxlength="{{\App\Constants::LISTING_FORM_NAME_MAX_LENGTH}}"
                                       placeholder="{{trans('listing.name_placeholder')}}" required>
                                <span class="form-error">{{$errors->first('name')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="form-email">@lang('listing.email')</label>
                                <input type="email" class="form-control" id="form-email" name="email"
                                       value="{{old('email')}}"
                                       placeholder="{{trans('listing.email_placeholder')}}" required>
                                <span class="form-error">{{$errors->first('email')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="form-phone">@lang('listing.phone')</label>
                                <input type="text" class="form-control" id="form-phone" name="phone"
                                       value="{{old('phone')}}"
                                       placeholder="{{trans('listing.phone_placeholder')}}" required>
                                <span class="form-error">{{$errors->first('phone')}}</span>
                            </div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-6 pad-left-none">
                                        <label for="form-date">@lang('listing.date')</label>
                                        <div class="naus-datepicker"
                                             data-date="{{old('date')}}"
                                             data-name="date"
                                             data-disabled-days="{{$listing['disabled_weekdays']}}"
                                             data-start-date="{{$listing['start_date']}}"
                                             data-required="true"></div>
                                        <span class="form-error">{{$errors->first('date')}}</span>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 pad-left-none">
                                        <label for="form-guests">@lang('listing.guests')</label>
                                        <input type="number" class="form-control" id="form-guests" name="guests"
                                        value="{{old('guests') ?? '6'}}"
                                        min="6"
                                        placeholder="{{trans('listing.guests_placeholder')}}" required>
                                        <span class="form-error">{{$errors->first('guests')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="form-notes">@lang('listing.notes') <span
                                            class="font-italic">@lang('listing.optional')</span></label>
                                <textarea class="form-control" id="form-notes" rows="3"
                                          maxlength="{{\App\Constants::MYSQL_TEXT_MAX_LENGTH}}"
                                          name="notes">{{old('notes')}}</textarea>
                                <span class="form-error">{{$errors->first('notes')}}</span>
                            </div>

                            <div>
                                @lang('listing.i_have_read')
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="form-terms-privacy" checked>
                                    <label class="form-check-label"
                                           for="form-terms-privacy">@lang('listing.check_terms')</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="form-marketing"
                                           name="marketing"
                                           value="1">
                                    <label class="form-check-label"
                                           for="form-marketing">@lang('listing.check_marketing')</label>
                                </div>
                            </div>

                            <button id="form-submit" type="submit"
                                    class="btn btn-primary">@lang('listing.send')</button>

                            <input type="hidden" name="experience_business_id" value="{{$listing['business_id']}}">
                        </form>
                    </div>
                </div>
            </div>

            @if(isset($related) && count($related))
                <div class="hr-container">
                    <hr>
                </div>

                {{--RELATED--}}
                <div class="listing-section pad-none">
                    <div class="mar-bottom-sm text-center">
                        <span class="listing-section-title">@lang('listing.related')</span>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            @foreach($related as $related_listing)
                                <div class="col-12 col-lg-6">
                                    @component('users.components.listing-box', ['listing' => $related_listing, 'top' => $related_listing['top']])
                                    @endcomponent
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        window.nausdream.experience_business_id = "{{$listing['business_id']}}";
        window.nausdream.trekksoft_api_url = "{{config('services.trekksoft.api_domain').App::getLocale().config('services.trekksoft.api_path')}}";
        window.nausdream.live_chat_event_label = window.nausdream.experience_business_id;
        window.nausdream.email_sent = "{{$email_sent}}";
        window.nausdream.cutoff_minutes = "{{\App\Constants::CUT_OFF_MINUTES}}";

        window.nausdream.validation_messages = {
            required: "@lang('validation.required')",
            date_format: "@lang('validation.date_format')",
            number: "@lang('validation.number')",
            email: "@lang('validation.email')"
        };
    </script>
    <script src="{{mix ('js/listing.js')}}" async defer></script>

    {{--RICH SNIPPETS--}}
    <script type="application/ld+json">
        {
          "@context": "http://schema.org/",
          "@type": "Product",
          "name": "{{$listing['title']}}",
          "image": [
        @foreach($photos as $photo)"{{$photo}}"@if($loop->iteration != $photos->count()){{','}}@endif
        @endforeach
        ],
       "description": "{{$listing['meta_description']}}",
       @if($feedbacks->count())
            "aggregateRating": {
              "@type": "AggregateRating",
              "ratingValue": "{{$feedbacks->avg('score')}}",
         "reviewCount": "{{$feedbacks->count()}}"
        },
        @endif
        "offers": {
          "@type": "Offer",
          "priceCurrency": "{{$listing['currency']}}",
            "price": "{{$listing['price']}}"
          }
          @if($feedbacks->count())
            ,
          "review": [
            @foreach($feedbacks as $feedback)
                {
                  "@type": "Review",
                  "author": "{{$feedback->customer_name}}",
                  "datePublished": "{{$feedback->updated_at}}",
                  "reviewBody": "{{$feedback->text}}",
                  "reviewRating": "{{$feedback->score}}"
                }@if($loop->iteration != $feedbacks->count()){{','}}@endif
            @endforeach
        @endif
        ]
      }

















    </script>
@endsection