@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent
@endsection

@section('content')
    <div class="pad-md mar-bottom-md">
        <h1 class="text-center box-title mar-bottom-md">@lang('how_it_works.title')</h1>
        <div class="separator"></div>
        <div class="max-width">
            <div class="pad-top-lg pad-bottom-lg bg-white box">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col d-none d-md-block" id="get-aboard-img">
                            @component('users.components.picture', ['url' => 'assets/img/support/get-aboard',
                            'versions' => [],
                            'def' => 'q_90', 'classes' => 'full-width'])
                            @endcomponent
                        </div>
                        <div class="col">
                            <p>{!! trans('how_it_works.first_text') !!}
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="hidden-box"></div>
        </div>
    </div>
    <hr>
    <div class="pad-md mar-bottom-md">
        <h1 class="text-center box-title mar-bottom-md">@lang('how_it_works.how_to_book')</h1>
        <div class="separator"></div>
        <div class="max-width">
            <div class="pad-top-lg pad-bottom-lg bg-white box">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col support-text">
                            <p class="margin-top-sm">
                                @component('users.components.picture', ['url' => 'assets/img/support/how-it-works-guest',
                            'versions' => [],
                            'def' => 'q_90', 'classes' => 'float-right how-it-works-img'])
                                @endcomponent
                                {!! trans('how_it_works.second_text', ['contact_page' => route('users.contact-us-'.App::getLocale())]) !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden-box"></div>
        </div>
    </div>
    {{--<hr>
    <div class="pad-md mar-bottom-md">
        <h1 class="text-center box-title mar-bottom-md">@lang('how_it_works.faq')</h1>
        <div class="separator"></div>
        <div class="max-width">
            <div class="panel-group" role="tablist" aria-multiselectable="true" id="accordion">

                @for($i = 1; $i < 5; $i++)
                    <div class="panel panel-default mar-bottom-sm">
                        <div class="panel-heading pad-left-md pad-right-md" role="tab" id="heading-{{$i}}" data-toggle="collapse" data-target="#collapse-{{$i}}" aria-expanded="false" aria-controls="collapseExample">
                            <span class="panel-title">
                                @lang("how_it_works.faq_{$i}")
                            </span>
                            <i class="icon-5x more-less icon-plus float-right"></i>
                        </div>
                        <div id="collapse-{{$i}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{$i}}">
                            <div class="panel-body pad-md bg-white">
                                @lang("how_it_works.faq_{$i}_answer")
                            </div>
                        </div>
                    </div>
                @endfor

            </div>
        </div>
    </div>--}}
@endSection

@section('scripts')
    <script>
        window.nausdream.live_chat_event_label = "How-it-works";
    </script>
    <script src="{{mix ('js/static-pages.js')}}" async defer></script>
@endSection