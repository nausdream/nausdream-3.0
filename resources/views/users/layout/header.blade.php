{{--MOBILE--}}
<div class="header d-md-none">
    {{--HAMBURGER--}}
    <span class="menu-icon">
        <i class="icon-menu icon-3x"></i>
    </span>
    {{--LOGO--}}
    <a href="{{route(App\Constants::R_USERS_HOME, ['lang' => $language])}}">
        @if ($page != 'home')
            <img class="logo"
                 alt="@lang('home.alt_logo')"
                 src="{{$logo_url}}"/>
        @else
            <img class="logo"
                 alt="@lang('home.alt_logo')"
                 src="{{config('services.cloudinary.secure_url') . "c_scale,h_100/" . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/logo_nausdream"}}"/>
        @endif
    </a>

    {{--NAVBAR--}}
    <div id="navbar">
        <div class="naus-panel-title">
            <span class="close-icon">
                <i class="icon-close"></i>
            </span>

            <img class="logo"
                 alt="@lang('home.alt_logo')"
                 src="{{$logo_url}}"/>
        </div>

        {{--MENU--}}
        <div class="menu text-left">
            <a class="menu-item" href="{{route(\App\Constants::R_USERS_HOME, ['lang' => $language])}}">
                <span class="font-bold text-md">@lang("header.home")</span>
            </a>
            <a class="menu-item close-navbar" href="{{config('app.url')}}#our-partners">
                <span class="font-bold text-md">@lang("header.our_partners")</span>
            </a>
            <a class="menu-item close-navbar" href="{{config('app.url')}}#top-destinations">
                <span class="font-bold text-md">@lang("header.top_destinations")</span>
            </a>
            <a class="menu-item close-navbar" href="{{config('app.url')}}#nausdream-tour">
                <span class="font-bold text-md">@lang("header.nausdream_tour")</span>
            </a>
            <div class="menu-item language mar-top-md">
                <img alt="@lang("home.alt_language", ['lang' => $language])" class="pad-right-sm" src="{{"$language_flag$language.jpg"}}"> <span
                        class="font-bold text-md">@lang("languages.$language")</span> <i class="icon-down"></i>
            </div>
        </div>
    </div>
    {{--DARK OVERLAY--}}
    <div id="overlay"></div>

    {{--LANGUAGE MENU--}}
    <div class="naus-panel" id="languages">
        <div class="naus-panel-title">
        <span class="close-icon">
                <i class="icon-close icon-1x"></i>
            </span>

            <span class="font-bold text-lg">@lang('header.language')</span>
        </div>

        {{--MENU--}}
        <div class="menu text-left">
            @foreach($languages as $language_name)
                <a class="menu-item language change-language lang" href="#" data-lang="{{$language_name}}">
                    <img alt="@lang("home.alt_language", ['lang' => $language_name])" class="pad-right-sm" src="{{"$language_flag$language_name"}}"> <span
                            class="font-bold text-md">@lang("languages.$language_name")</span>
                </a>
            @endforeach
        </div>
    </div>
</div>

{{--DESKTOP--}}
<div class="header d-none d-md-block">
    <div class="dropdown language-dropdown">
        <div class="dropdown-toggle cursor-pointer" data-toggle="dropdown" id="languages-dropdown">
            <img alt="@lang("home.alt_language", ['lang' => $language])" class="pad-right-sm" src="{{"$language_flag$language"}}">
            <span>@lang("languages.$language")</span>
        </div>
        <div class="dropdown-menu" aria-labelledby="languages-dropdown">
            @foreach($languages as $language_name)
                <a class="dropdown-item language change-language lang" href="#" data-lang="{{$language_name}}">
                    <img alt="@lang("home.alt_language", ['lang' => $language_name])" class="pad-right-sm" src="{{"$language_flag$language_name.jpg"}}"> <span
                            class="font-bold text-md">@lang("languages.$language_name")</span>
                </a>
            @endforeach
        </div>
    </div>
    <div class="second-row max-width">
        <a href="{{route(App\Constants::R_USERS_HOME, ['lang' => $language])}}">
            @if ($page != 'home')
                <img class="logo"
                     alt="@lang('home.alt_logo')"
                     src="{{$logo_url}}"/>
            @else
                <img class="logo"
                     alt="@lang('home.alt_logo')"
                     src="{{config('services.cloudinary.secure_url') . "c_scale,h_55/" . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/logo_nausdream"}}"/>
            @endif
        </a>
        <div class="float-right menu">
            <a class="menu-item" href="{{config('app.url')}}#our-partners">@lang("header.our_partners")</a>
            <a class="menu-item" href="{{config('app.url')}}#top-destinations">@lang("header.top_destinations")</a>
            <a class="menu-item" href="{{config('app.url')}}#nausdream-tour">@lang("header.nausdream_tour")</a>
        </div>
    </div>
</div>
