<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    @if(!$block_analytics)
        {{--Google Analytics--}}
        <script>
            window.ga = window.ga || function () {
                (ga.q = ga.q || []).push(arguments)
            };
            ga.l = +new Date;
            ga('create', '{{$google_analytics_tracking_id}}', 'auto', {'siteSpeedSampleRate': {{\App\Constants::TRACK_TIMINGS_RATIO}}});
            ga('set', 'anonymizeIp', true);
            ga('send', 'pageview');
        </script>
        <script async src='https://www.google-analytics.com/analytics.js'></script>
        <!-- End Google Analytics -->
    @endif

    @yield('head')

    <link href="{{mix('/css/app.css')}}" rel="stylesheet">

    @if(!$block_marketing)
    <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '{{config('services.facebook.pixel_id')}}');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
                 src="https://www.facebook.com/tr?id={{config('services.facebook.pixel_id')}}&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    @endif
</head>
<body class="{{$page}}">
@component('users.components.cookie-policy-banner', ['language' => App::getLocale()])
@endcomponent

<div class="content-wrapper pad-bottom-lg">
    <header>
        @include('users.layout.header')
    </header>
    <div class="content">
        @yield('content')
    </div>
</div>

<footer>
    @include('users.layout.footer')
</footer>

<script>
    window.nausdream = {};
    window.nausdream.language = "{{App::getLocale()}}";
    window.nausdream.cloudinary_secure_url = "{{config('services.cloudinary.secure_url')}}";
    window.nausdream.cloudinary_base_folder = "{{config('services.cloudinary.base_folder')}}";
</script>

{{--RENDER BLOCKING--}}
<script src="{{mix('/js/manifest.js')}}"></script>
<script src="{{mix('/js/vendor.js')}}"></script>

{{--ASYNC--}}
<script src="{{mix('/js/header.js')}}" async defer></script>
<script src="{{mix ('js/base.js')}}" async defer></script>
<script src="{{mix ('js/cookie-policy-banner.js')}}" async defer></script>

{{--LOADED BY PAGES--}}
@yield('scripts')

{{--CAMPAIGNS--}}
<input type="hidden" name="page" value="{{$page}}">
@foreach($active_campaigns as $campaign)
    {!! $campaign->banner() !!}
@endforeach

<!-- begin olark code -->
<script type="text/javascript">
    {{--(function (o, l, a, r, k, y) {--}}
        {{--if (o.olark) return;--}}
        {{--r = "script";--}}
        {{--y = l.createElement(r);--}}
        {{--r = l.getElementsByTagName(r)[0];--}}
        {{--y.async = 1;--}}
        {{--y.src = "//" + a;--}}
        {{--r.parentNode.insertBefore(y, r);--}}
        {{--y = o.olark = function () {--}}
            {{--k.s.push(arguments);--}}
            {{--k.t.push(+new Date)--}}
        {{--};--}}
        {{--y.extend = function (i, j) {--}}
            {{--y("extend", i, j)--}}
        {{--};--}}
        {{--y.identify = function (i) {--}}
            {{--y("identify", k.i = i)--}}
        {{--};--}}
        {{--y.configure = function (i, j) {--}}
            {{--y("configure", i, j);--}}
            {{--k.c[i] = j--}}
        {{--};--}}
        {{--k = y._ = {s: [], t: [+new Date], c: {}, l: a};--}}
    {{--})(window, document, "static.olark.com/jsclient/loader.js");--}}
    {{--/* Add configuration calls below this comment */--}}
    {{--olark.identify("{{config('services.olark.site_id')}}");--}}
    {{--olark.configure("system.localization", "{{$live_chat_language}}");--}}

    @if(!$block_analytics)
        // olark('api.box.onExpand', function (event) {
        //
        //     ga('send', {
        //         hitType: 'event',
        //         eventCategory: 'Live chat opened',
        //         eventAction: 'Click',
        //         eventLabel: window.nausdream.live_chat_event_label || 'page',
        //         eventValue: undefined,
        //         nonInteraction: undefined,
        //         transport: 'beacon'
        //     });
        //
        // });
    @endif
</script>
<!-- end olark code -->

</body>
</html>