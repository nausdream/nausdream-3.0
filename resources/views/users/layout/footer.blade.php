{{--MOBILE--}}
<div id="footer-mobile" class="footer text-center text-md pad-top-xxl pad-bottom-xxl d-block d-md-none">
    <div class="text-uppercase mar-bottom-lg">
        <a class="text-uppercase" href="{{route('users.terms-of-service-'.$language)}}">@lang('footer.terms_of_service')</a><br>
        <a class="text-uppercase" href="{{route('users.cookie-policy-'.$language)}}">@lang('footer.cookie_policy')</a><br>
        <a class="text-uppercase" href="{{route('users.privacy-policy-'.$language)}}">@lang('footer.privacy_policy')</a><br>
        <a class="text-uppercase" href="{{route('users.contact-us-'.$language)}}">@lang('footer.contact_us')</a>
    </div>
    <div>
        <span>@lang('footer.copyright')</span><br>
        <span>@lang('footer.vat_number', ['vat' => \App\Constants::VAT_NUMBER])</span><br>
        <span>@lang('footer.tour_operator', ['insurance' => \App\Constants::INSURANCE_POLICY])</span>
    </div>
</div>

{{--DESKTOP--}}
<div id="footer-desktop" class="footer d-none d-md-block container-fluid">
    <div class="row max-width">
        <div class="col-sm text-center">
            <div class="d-inline-block text-left">
                <span>@lang('footer.copyright')</span><br>
                <span>@lang('footer.all_rights_reserved')</span><br>
                <span>@lang('footer.company_full_name')</span><br>
                <span>@lang('footer.vat_number', ['vat' => \App\Constants::VAT_NUMBER])</span><br>
            </div>
        </div>
        <div class="col-sm text-center">
            <div class="d-inline-block text-left">
                <a class="text-uppercase" href="{{route('users.terms-of-service-'.$language)}}">@lang('footer.terms_of_service')</a><br>
                <a class="text-uppercase" href="{{route('users.cookie-policy-'.$language)}}">@lang('footer.cookie_policy')</a><br>
                <a class="text-uppercase" href="{{route('users.privacy-policy-'.$language)}}">@lang('footer.privacy_policy')</a><br>
                <a class="text-uppercase" href="{{route('users.contact-us-'.$language)}}">@lang('footer.contact_us')</a>
            </div>
        </div>
        <div class="col-sm text-center">
            <div class="d-inline-block text-left">
                <span>@lang('footer.tour_operator', ['insurance' => \App\Constants::INSURANCE_POLICY])</span>
            </div>
        </div>
    </div>
</div>
