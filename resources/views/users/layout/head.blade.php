<title>{{$title}}</title>
<meta name="description" content="{{$description}}"/>

@foreach($alternates as $lang => $alternate)
    <link rel="alternate" hreflang="{{$lang}}" href="{{$alternate}}"/>
@endforeach

<!-- Open Graph Tags -->
<meta property="og:title" content="{{$title}}"/>
<meta property="og:description" content="{{$description}}"/>
<meta property="og:type" content="{{$og_type}}"/>
<meta property="og:url" content="{{$og_url}}"/>
@foreach($og_images as $image)
    <meta property="og:image" content="{{$image}}"/>
@endforeach

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=WGL93Yj6LR">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=WGL93Yj6LR">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=WGL93Yj6LR">
<link rel="manifest" href="/site.webmanifest?v=WGL93Yj6LR">
<link rel="mask-icon" href="/safari-pinned-tab.svg?v=WGL93Yj6LR" color="#5bbad5">
<link rel="shortcut icon" href="/favicon.ico?v=WGL93Yj6LR">
<meta name="apple-mobile-web-app-title" content="Nausdream">
<meta name="application-name" content="Nausdream">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">