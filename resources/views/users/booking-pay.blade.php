@extends('users.layout.base')

@section('head')
    <title>{!! trans('booking.pay_page_title') !!}</title>
    <meta name="robots" content="noindex, nofollow">
@endsection

@section('content')
    @if(isset($errors))
        <!-- Modal -->
        <div class="modal" tabindex="-1" role="dialog" id="error-modal">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{!! trans('booking.errors') !!}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{!! trans('booking.pay_again') !!}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">{!! trans('booking.retry') !!}</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="pad-md mar-bottom-sm max-width-thin">
        <h1 class="text-center mar-bottom-md">{!! trans('booking.summary') !!}</h1>
        <table class="booking-summary-table">
            <tr>
                <td class="left-col">{!! trans('booking.summary_experience') !!}</td>
                <td class="right-col">{{$booking->experience_title}}</td>
            </tr>
            <tr>
                <td class="left-col">{!! trans('booking.summary_adults') !!}</td>
                <td class="right-col">{{$booking->adults()->count()}}</td>
            </tr>

            @if($booking->children()->count())
                <tr>
                    <td class="left-col">{!! trans('booking.summary_children') !!}</td>
                    <td class="right-col">{{$booking->children()->count()}}</td>
                </tr>
            @endif

            @if(isset($booking->coupon))
                <tr class="base-price">
                    <td class="left-col"><b>{!! trans('booking.summary_base_price') !!}</b></td>
                    <td class="right-col"><b class="price">{{$price_formatted}}</b></td>
                </tr>

                <tr class="discount">
                    <td class="left-col">{!! trans('booking.summary_discount', ['code' => $coupon_code, 'val' => $coupon_value, 'symbol' => $coupon_symbol]) !!}</td>
                    <td class="right-col"><span class="text-danger">{{$discount_formatted}}</span></td>
                </tr>
            @endif

            <tr>
                <td class="left-col"><span class="text-lg"><b>{!! trans('booking.summary_total') !!}</b></span></td>
                <td class="right-col"><span class="text-lg"><b>{{$discounted_price_formatted}}</b></span></td>
            </tr>
        </table>
    </div>

    <hr class="max-width-thin">

    <div class="pad-md mar-bottom-sm max-width-thin">
        <h2>{!! trans('booking.summary_coupon') !!}</h2>
        <form id="coupon-form" action="{{route(\App\Constants::R_USERS_COUPON_CHECK, ['lang' => App::getLocale()])}}"
              method="POST">
            {{csrf_field()}}
            <div class="form-row mb-2">
                <div class="col-md-4">
                    <label for="coupon" class="sr-only">{!! trans('booking.summary_coupon') !!}</label>
                    <input required name="coupon" type="text" class="form-control" id="coupon"
                           placeholder="{!! trans('booking.summary_coupon_placeholder') !!}"
                           value="{{$booking->coupon ? $booking->coupon->code : ""}}">
                </div>
                <div class="col">
                    <button id="coupon-submit" type="submit"
                            class="btn btn-default mb-2">{!! trans('booking.summary_coupon_apply') !!}</button>
                </div>
            </div>

            <input type="hidden" name="booking" value="{{$booking->id}}">
        </form>
        @if($coupon_error ?? false)
            <small class="coupon-error text-danger">{!! trans('booking.summary_coupon_error') !!}</small>
        @endif
        @if(isset($booking->coupon))
            <small>
                <i class="text-success icon icon-ok"></i> @lang('booking.summary_coupon_valid', ['code' => $booking->coupon->code])
            </small>
        @endif
    </div>

    <hr class="max-width-thin">

    <div class="pad-md mar-bottom-sm max-width text-center">
        @if ($booking->status == \App\Constants::BOOKING_PAYMENT_LINK_SENT)
            @if(in_array('stripe', $payment_methods))

                <div class="mar-bottom-md">
                    <div class="mar-bottom-sm"><span>{!! trans('booking.last_step') !!}</span></div>
                    <form action="{{route(\App\Constants::R_USERS_BOOKING_PAY, ['id' => $booking->id, 'lang' => App::getLocale()])}}"
                          method="POST">
                        {{csrf_field()}}

                        @component('users.components.stripe', ['booking' => $booking])
                        @endcomponent

                        <input type="hidden" name="booking" value="{{$booking->id}}">
                    </form>
                </div>

            @endif

            @if(in_array('wire_transfer', $payment_methods))
                <hr class="max-width-thin">
                <div class="pad-md mar-bottom-sm max-width text-center">
                    {!! trans('booking.wire_transfer_data') !!}
                </div>
            @endif
        @else
            <p>{!! trans('booking.error_booking_status') !!}</p>
        @endif
    </div>

@endsection

@section('scripts')
    @component('users.components.error-messages') @endcomponent
    <script src="{{mix ('js/booking.js')}}" async defer></script>
@endsection