@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent
@endsection

@section('content')
    @if(session("success") ?? false)
        <!-- Modal -->
        <div class="modal fade" id="contact-custom-modal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content pad-md">
                    <button type="button" class="close pad-lg" data-dismiss="modal" aria-label="Close" style="position: absolute; top: 0; right: 0;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title w-100 text-center mar-bottom-md pad-left-md pad-right-md"
                        id="cart-abandonment-title">@lang('listing.request_sent')</h5>
                    <div class="text-center">
                        @lang('listing.thank_you')
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="above-the-fold with-background-image"
         data-versions="{{json_encode([
         '420' => 'c_fill,h_680,w_800,q_50/c_crop,h_480,w_420,x_420,y_135/v1525629442',
         '769' => 'c_fill,h_560,q_50/c_crop,g_xy_center,x_316,y_300/v1525629442',
         '1300' => 'c_fill,g_east,w_1300,h_568,q_50/v1525629442',
         '1369' => 'c_fill,g_east,w_1368,h_568,q_50/v1525629442',
         '1441' => 'c_fill,g_east,w_1368,h_568,q_50/v1525629442',
         ])}}"
         data-url="assets/img/home/header2.jpg" data-def="c_fill,g_east,w_1920,h_517,q_50/v1525629442">
        <h1 class="headline">@lang('home.headline')</h1>
    </div>

    {{--WHAT IS NAUSDREAM--}}
    <div class="separator"></div>
    <h2 class="text-center pad-left-md pad-right-md box-title">@lang('home.what_is_nausdream_title')</h2>
    <div class="max-width mar-bottom-lg seo-container">
        <div class="pad-top-lg pad-bottom-lg bg-white box">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="float-left d-none d-md-inline">
                            @component('users.components.picture', ['url' => 'assets/img/home/img-prefooter2.jpg',
                        'versions' => ['1024' => 'q_75,w_300', '2048' => 'q_75,w_500'],
                        'def' => 'q_75', 'classes' => 'seo-box-img',
                        'alt' => trans('home.alt_seo_img')])
                            @endcomponent
                        </div>
                        <div class="mar-bottom-md">{!! trans('home.what_is_nausdream_text') !!}</div>
                        <div class="def-btn-container center-block">
                            <a class="def-btn"
                               href="{{route('users.contact-us-'.App::getLocale())}}">@lang('home.work_with_us')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-box"></div>
    </div>

    {{--PARTNERS--}}
    <div class="separator" id="our-partners"></div>
    <h2 class="mar-bottom-md text-center pad-left-md pad-right-md">@lang('home.our_partners')</h2>
    <div class="container-fluid mar-bottom-md text-center max-width-thin">
        <div class="row">
            <div class="col-12 col-sm-2 mar-bottom-sm">
                @component('users.components.picture', [
                'url' => 'assets/img/home/costa_crociere.png',
                'cloudinary_version' => 'v1538119747',
                'versions' => [], 'classes' => 'full-width', 'alt' => trans('home.costa_crociere')])
                @endcomponent
            </div>
            <div class="col-12 col-sm-2 mar-bottom-sm">
                @component('users.components.picture', [
                'url' => 'assets/img/home/msc.png',
                'cloudinary_version' => 'v1538119747',
                'versions' => [], 'classes' => 'full-width', 'alt' => trans('home.msc')])
                @endcomponent
            </div>
            <div class="col-12 col-sm-2 mar-bottom-sm">
                @component('users.components.picture', [
                'url' => 'assets/img/home/nicolaus.png',
                'cloudinary_version' => 'v1538119747',
                'versions' => [], 'classes' => 'full-width', 'alt' => trans('home.nicolaus')])
                @endcomponent
            </div>
            <div class="col-12 col-sm-2 mar-bottom-sm">
                @component('users.components.picture', [
                'url' => 'assets/img/home/tui_cruises.png',
                'cloudinary_version' => 'v1559513028',
                'versions' => [], 'classes' => 'full-width', 'alt' => trans('home.tui_cruises')])
                @endcomponent
            </div>
            <div class="col-12 col-sm-2 mar-bottom-sm">
                @component('users.components.picture', [
                'url' => 'assets/img/home/aida.png',
                'cloudinary_version' => 'v1559521819',
                'versions' => [], 'classes' => 'full-width', 'alt' => trans('home.aida')])
                @endcomponent
            </div>
            <div class="col-12 col-sm-2 mar-bottom-sm">
                @component('users.components.picture', [
                'url' => 'assets/img/home/evolution_travel.png',
                'cloudinary_version' => 'v1559747889',
                'versions' => [], 'classes' => 'full-width', 'alt' => trans('home.evolution_travel')])
                @endcomponent
            </div>
        </div>
    </div>

    {{--TOP DESTINATIONS--}}
    <div class="separator" id="top-destinations"></div>
    <h2 class="mar-bottom-md text-center pad-left-md pad-right-md">@lang('home.top_destinations')</h2>
    <div class="container-fluid top-destinations-container">
        <div class="row">
            @foreach($top_destinations as $top_destination)
                <div class="col-sm-6 pad-md mar-bottom-md">
                    @component('users.components.top-destination', $top_destination)
                    @endcomponent
                </div>
            @endforeach
        </div>
    </div>

    {{--CUSTOM FORM--}}
    <div class=" pad-md mar-bottom-md">
        <div class="container text-center">
            <h1>
                <a href="#" data-toggle="modal" data-target="#custom-form-modal" class="a-color">
                    @lang("home.custom_group_experience")
                </a>
            </h1>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="custom-form-modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang("listing.form")</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mar-bottom-sm"><span>@lang('listing.form_cta')</span></div>
                    @component('users.components.custom-form')
                    @endcomponent
                </div>
            </div>
        </div>
    </div>


    {{--NAUSDREAM TOUR--}}
    <div class="bg-white why-nausdream-container margin-top-sm" id="nausdream-tour">
        <h2 class="mar-bottom-md text-center pad-left-md pad-right-md">@lang('home.what_is_a_nausdream_tour')</h2>
        <div class="container-fluid text-center max-width">
            <div class="row">
                <div class="col-sm-12 col-md-4 mar-bottom-lg">
                    <div class="badge">
                        @component('users.components.picture', [
                        'url' => 'assets/img/home/sea.jpg',
                        'versions' => [],
                        'classes' => 'full-width',
                        'def' => 'q_78'])
                        @endcomponent
                    </div>
                    <div class="mar-bottom-sm"><span class="text-lg">@lang('home.sea')</span></div>
                    <div class="separator yellow small"></div>
                    <div><span>@lang('home.sea_text')</span></div>
                </div>
                <div class="col-sm-12 col-md-4 mar-bottom-lg">
                    <div class="badge">
                        @component('users.components.picture', [
                        'url' => 'assets/img/home/experience.jpg',
                        'versions' => [], 'def' => 'q_78', 'classes' => 'full-width'
                        ])
                        @endcomponent
                    </div>
                    <div class="mar-bottom-sm"><span class="text-lg">@lang('home.experience')</span></div>
                    <div class="separator small"></div>
                    <div><span>@lang('home.experience_text')</span></div>
                </div>
                <div class="col-sm-12 col-md-4 mar-bottom-lg">
                    <div class="badge">
                        @component('users.components.picture', [
                        'url' => 'assets/img/home/authenticity.jpg',
                        'classes' => 'full-width', 'versions' => [], 'def' => 'q_78'
                        ])
                        @endcomponent
                    </div>
                    <div class="mar-bottom-sm"><span class="text-lg">@lang('home.authenticity')</span></div>
                    <div class="separator pink small"></div>
                    <div><span>@lang('home.authenticity_text')</span></div>
                </div>
            </div>
        </div>
    </div>


    {{--FEEDBACKS--}}
    <div class="position-relative feedbacks-bg with-background-image mar-bottom-lg"
         data-versions="{{json_encode([
         '400' => 'q_48,w_600',
         '800' => 'q_48,w_800',
         '1024' => 'q_48,w_1024',
         '1280' => 'q_48,w_1280',
         ])}}"
         data-url="assets/img/home/about-us" data-def="c_scale,w_1920/c_crop,w_1920,h_493,y_120,q_50"
         data-analytics
         data-trigger="Scroll"
         data-event-category="Page section feedbacks"
         data-event-label="home"
         data-non-interaction="true">
        <div class="feedbacks-heading">
            <div class="quotation-marks"><i>"</i></div>
        </div>

        <div id="feedbacks" class="carousel slide max-width" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#feedbacks" data-slide-to="0" class="active"></li>
                <li data-target="#feedbacks" data-slide-to="1"></li>
                <li data-target="#feedbacks" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">

                <div class="carousel-item active">
                    <div class="feedback-text">
                        <p class="feedback-text-inner">@lang('home.feedback_text_1')</p>
                    </div>
                    <div class="carousel-caption">
                        <div class="feedback-user-pic">
                            @component('users.components.picture', [
                            'url' => 'assets/img/home/feed_1',
                            'classes' => 'full-width',
                            'versions' => [], 'def' => 'c_scale,w_50,q_50', 'alt' => trans('home.alt_feed_1')])
                            @endcomponent
                        </div>
                        <p class="feedback-user-data">
                            <span class="feedback-user-name">@lang('home.feedback_name_1')</span><br>
                            <span class="feedback-user-role">@lang('home.feedback_role_1')</span>
                        </p>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="feedback-text">
                        <p class="feedback-text-inner">@lang('home.feedback_text_2')</p>
                    </div>
                    <div class="carousel-caption">
                        <div class="feedback-user-pic">
                            @component('users.components.picture', [
                            'url' => 'assets/img/home/feed_2', 'classes' => 'full-width', 'versions' => [],
                            'def' => 'c_scale,w_50,q_50', 'alt' => trans('home.alt_feed_1')])
                            @endcomponent
                        </div>
                        <p class="feedback-user-data">
                            <span class="feedback-user-name">@lang('home.feedback_name_2')</span><br>
                            <span class="feedback-user-role">@lang('home.feedback_role_2')</span></p>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="feedback-text">
                        <p class="feedback-text-inner">@lang('home.feedback_text_3')</p>
                    </div>
                    <div class="carousel-caption">
                        <div class="feedback-user-pic">
                            @component('users.components.picture', ['url' => 'assets/img/home/feed_3',
                            'classes' => 'full-width', 'versions' => [],
                            'def' => 'c_crop,fl_preserve_transparency,g_xy_center,h_550,r_350,w_550,x_2550,y_840,q_50/c_scale,w_50',
                            'alt' => trans('home.alt_feed_1')])
                            @endcomponent
                        </div>
                        <p class="feedback-user-data">
                            <span class="feedback-user-name">@lang('home.feedback_name_3')</span><br>
                            <span class="feedback-user-role">@lang('home.feedback_role_3')</span></p>
                    </div>
                </div>
            </div>
            <a class="carousel-control carousel-control-prev" href="#feedbacks" role="button" data-slide="prev">
                <span class="carousel-control-icon carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">@lang('home.feedbacks_previous')</span>
            </a>
            <a class="carousel-control carousel-control-next" href="#feedbacks" role="button" data-slide="next">
                <span class="carousel-control-icon carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">@lang('home.feedbacks_next')</span>
            </a>
        </div>
    </div>
@endSection

@section('scripts')
    <script>
        window.nausdream.live_chat_event_label = "Home";
        window.nausdream.success = "{{session("success") ?? false}}";
        window.nausdream.errorform = "{{$errors->any()}}";
        window.nausdream.validation_messages = {
            required: "@lang('validation.required')",
            date_format: "@lang('validation.date_format')",
            number: "@lang('validation.number')",
            email: "@lang('validation.email')"
        };
    </script>
    <script src="{{mix ('js/home.js')}}" async defer></script>
@endSection
