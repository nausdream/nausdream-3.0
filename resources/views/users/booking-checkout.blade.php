@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent
    <meta name="robots" content="noindex, nofollow">
@endsection

@section('content')
    @if($booking->status != \App\Constants::BOOKING_PAYMENT_LINK_SENT)
        <div class="pad-md mar-bottom-sm max-width">
            <h2 class="text-center">{!! trans('booking.hello', ['name' => $booking->user_first_name]) !!}</h2>
            @if($booking->status == \App\Constants::BOOKING_EXPIRED)
                <p class="text-center">{!! trans('booking.expired', ['email' => \App\Constants::SUPPORT_EMAIL]) !!}</p>
            @else
                <p class="text-center">{!! trans('booking.error_booking_status') !!}</p>
            @endif
            @if(isset($booking->experience_link))
                <div class="text-center">
                    <div class="d-inline-block">
                        <a href="{{$booking->experience_link}}" class="def-btn">@lang('booking.product_page')</a>
                    </div>
                </div>
            @endif
        </div>
    @else
        @if($errors->any())
            <!-- Modal -->
            <div class="modal" tabindex="-1" role="dialog" id="error-modal">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">{!! trans('booking.errors') !!}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>{!! trans('booking.check_again') !!}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">{!! trans('booking.fix_errors') !!}</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="pad-md mar-bottom-sm max-width">
            <form id="form" method="POST"
                  action="{{route(\App\Constants::R_USERS_BOOKING_CHECKOUT_POST, ['lang' => App::getLocale(), 'id' => $booking->id])}}">
                {{csrf_field()}}
                {{--INTRO--}}
                <div>
                    <h2>{!! trans('booking.hello', ['name' => $booking->user_first_name]) !!}</h2>
                    <p>{!! trans('booking.enter_data', [
                'title' => $booking->experience_link ? '<a href="'.$booking->experience_link.'" target="_blank">'.$booking->experience_title.'</a>' : $booking->experience_title,
                'date' => $booking->experience_date
            ]) !!}</p>
                </div>
                {{--CONTACT GUEST DATA--}}
                <div class="mar-bottom-lg">
                    <h2 class="text-center mar-bottom-sm">{!! trans('booking.contact_guest_section') !!}</h2>

                    <div class="guests-section">
                        <div class="form-row">
                            <div class="form-group col-lg-3">
                                <label for="user_first_name">{!! trans('booking.first_name') !!}</label>
                                <input type="text" class="form-control" id="user_first_name"
                                       placeholder="{!! trans('booking.first_name_placeholder') !!}"
                                       name="user_first_name"
                                       value="{{old('user_first_name') ?? $booking->user_first_name}}"
                                       maxlength="{{$rules['user_first_name']['max']}}"
                                       required>
                                @foreach ($errors->get('user_first_name') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="user_last_name">{!! trans('booking.last_name') !!}</label>
                                <input type="text" class="form-control" id="user_last_name"
                                       placeholder="{!! trans('booking.last_name_placeholder') !!}"
                                       name="user_last_name"
                                       value="{{old('user_last_name') ?? $booking->user_last_name}}"
                                       maxlength="{{$rules['user_last_name']['max']}}"
                                       required>
                                @foreach ($errors->get('user_last_name') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="user_place_of_birth">{!! trans('booking.place_of_birth') !!}</label>
                                <input type="text" class="form-control" id="user_place_of_birth"
                                       placeholder="{!! trans('booking.place_of_birth_placeholder') !!}"
                                       name="user_place_of_birth"
                                       value="{{old('user_place_of_birth') ?? $booking->user_place_of_birth}}"
                                       maxlength="{{$rules['guest_place_of_birth']['max']}}"
                                       required>
                                @foreach ($errors->get('user_place_of_birth') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                            <div class="form-group col-lg-2">
                                <label for="user_birth_date">{!! trans('booking.birth_date') !!}</label>
                                <input class="naus-datepicker form-control" id="user_birth_date"
                                       data-name="user_birth_date"
                                       data-date="{{old('user_birth_date') ?? $booking->user_birth_date}}"
                                       value="{{\App\Helpers\DateHelper::stringToFormat(old('user_birth_date') ?? $booking->user_birth_date)}}"
                                       placeholder="{{trans('listing.date_placeholder')}}"
                                       data-end-date="{{$contact_end_date}}"
                                       data-view="decade"
                                       readonly
                                       data-required="true">
                                @foreach ($errors->get('user_birth_date') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-3">
                                <label for="user_city">{!! trans('booking.city') !!}</label>
                                <input type="text" class="form-control" id="user_city"
                                       placeholder="{!! trans('booking.city_placeholder') !!}"
                                       name="user_city"
                                       value="{{old('user_city') ?? $booking->user_city}}"
                                       maxlength="{{$rules['user_city']['max']}}"
                                       required>
                                @foreach ($errors->get('user_city') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="user_address">{!! trans('booking.address') !!}</label>
                                <input type="text" class="form-control" id="user_address"
                                       placeholder="{!! trans('booking.address_placeholder') !!}"
                                       name="user_address"
                                       value="{{old('user_address') ?? $booking->user_address}}"
                                       maxlength="{{$rules['user_address']['max']}}"
                                       required>
                                @foreach ($errors->get('user_address') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                            <div class="form-group col-lg-2">
                                <label for="user_zip_code">{!! trans('booking.zip_code') !!}</label>
                                <input type="text" class="form-control" id="user_zip_code"
                                       placeholder="{!! trans('booking.zip_code_placeholder') !!}"
                                       name="user_zip_code"
                                       value="{{old('user_zip_code') ?? $booking->user_zip_code}}"
                                       maxlength="{{$rules['user_zip_code']['max']}}"
                                       required>
                                @foreach ($errors->get('user_zip_code') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-3">
                                <label for="user_email">{!! trans('booking.email') !!}</label>
                                <input type="text" class="form-control" id="user_email"
                                       placeholder="{!! trans('booking.email_placeholder') !!}"
                                       name="user_email"
                                       value="{{old('user_email') ?? $booking->user_email}}"
                                       maxlength="{{$rules['user_email']['max']}}"
                                       required>
                                @foreach ($errors->get('user_email') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="user_phone">{!! trans('booking.phone') !!}</label>
                                <input type="text" class="form-control" id="user_phone"
                                       placeholder="{!! trans('booking.phone_placeholder') !!}"
                                       name="user_phone"
                                       value="{{old('user_phone') ?? $booking->user_phone}}"
                                       maxlength="{{$rules['user_phone']['max']}}"
                                       required>
                                @foreach ($errors->get('user_phone') as $message)
                                    @component('users.components.error'){!! trans($message) !!}@endcomponent
                                    <br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                {{--GUESTS DATA--}}
                <div class="mar-bottom-md">
                    <h2 class="text-center mar-bottom-sm">{!! trans('booking.guests_on_board_section') !!}</h2>

                    {{--ADULTS--}}
                    <div class="guests-section">
                        <p class="title">{!! trans('booking.adults') !!}</p>
                        @foreach($adults as $adult)
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-lg-1">
                                    <table class="full-height">
                                        <tr>
                                            <td>{!! trans('booking.adult', ['number' => $loop->iteration]) !!}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="guest_{{$adult->id}}_first_name">{!! trans('booking.first_name') !!}</label>
                                    <input type="text" class="form-control" id="guest_{{$adult->id}}_first_name"
                                           placeholder="{!! trans('booking.guest_first_name_placeholder') !!}"
                                           name="guests[{{$adult->id}}][first_name]"
                                           value="{{old("guests.$adult->id.first_name") ?? $adult->first_name}}"
                                           maxlength="{{$rules['guest_first_name']['max']}}"
                                           required>
                                    @foreach ($errors->get("guests.$adult->id.first_name") as $message)
                                        @component('users.components.error'){!! trans($message) !!}@endcomponent
                                        <br>
                                    @endforeach
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="guest_{{$adult->id}}_last_name">{!! trans('booking.last_name') !!}</label>
                                    <input type="text" class="form-control" id="guest_{{$adult->id}}_last_name"
                                           placeholder="{!! trans('booking.guest_last_name_placeholder') !!}"
                                           name="guests[{{$adult->id}}][last_name]"
                                           value="{{old("guests.$adult->id.last_name") ?? $adult->last_name}}"
                                           maxlength="{{$rules['guest_last_name']['max']}}"
                                           required>
                                    @foreach ($errors->get("guests.$adult->id.last_name") as $message)
                                        @component('users.components.error'){!! trans($message) !!}@endcomponent
                                        <br>
                                    @endforeach
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="guest_{{$adult->id}}_place_of_birth">{!! trans('booking.place_of_birth') !!}</label>
                                    <input type="text" class="form-control" id="guest_{{$adult->id}}_place_of_birth"
                                           placeholder="{!! trans('booking.guest_place_of_birth_placeholder') !!}"
                                           name="guests[{{$adult->id}}][place_of_birth]"
                                           value="{{old("guests.$adult->id.place_of_birth") ?? $adult->place_of_birth}}"
                                           maxlength="{{$rules['guest_place_of_birth']['max']}}"
                                           required>
                                    @foreach ($errors->get("guests.$adult->id.place_of_birth") as $message)
                                        @component('users.components.error'){!! trans($message) !!}@endcomponent
                                        <br>
                                    @endforeach
                                </div>
                                <div class="form-group col-lg-2">
                                    <label for="guest_{{$adult->id}}_birth_date">{!! trans('booking.birth_date') !!}</label>
                                    <input class="naus-datepicker form-control" id="guest_{{$adult->id}}_birth_date"
                                           data-name="guests[{{$adult->id}}][birth_date]"
                                           data-date="{{old("guests.$adult->id.birth_date") ?? $adult->birth_date}}"
                                           value="{{\App\Helpers\DateHelper::stringToFormat(old("guests.$adult->id.birth_date") ?? $adult->birth_date)}}"
                                           placeholder="{{trans('listing.date_placeholder')}}"
                                           data-end-date="{{$adult_end_date}}"
                                           data-view="decade"
                                           readonly
                                           data-required="true">
                                    @foreach ($errors->get("guests.$adult->id.birth_date") as $message)
                                        @component('users.components.error'){!! trans($message) !!}@endcomponent
                                        <br>
                                    @endforeach
                                </div>
                                <input type="hidden" name="guests[{{$adult->id}}][id]" value="{{$adult->id}}">
                            </div>
                        @endforeach
                    </div>

                    {{--CHILDREN--}}
                    @if($children->count())
                        <div class="guests-section">
                            <p class="title">{!! trans('booking.children') !!}</p>
                            @foreach($children as $child)
                                <hr>
                                <div class="form-row">
                                    <div class="form-group col-lg-1">
                                        <table class="full-height">
                                            <tr>
                                                <td>{!! trans('booking.child', ['number' => $loop->iteration]) !!}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="guest_{{$child->id}}_first_name">{!! trans('booking.first_name') !!}</label>
                                        <input type="text" class="form-control" id="guest_{{$child->id}}_first_name"
                                               placeholder="{!! trans('booking.guest_first_name_placeholder') !!}"
                                               name="guests[{{$child->id}}][first_name]"
                                               value="{{old("guests.$child->id.first_name") ?? $child->first_name}}"
                                               maxlength="{{$rules['guest_first_name']['max']}}"
                                               required>
                                        @foreach ($errors->get("guests.$adult->id.first_name") as $message)
                                            @component('users.components.error'){!! trans($message) !!}@endcomponent
                                            <br>
                                        @endforeach
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="guest_{{$child->id}}_last_name">{!! trans('booking.last_name') !!}</label>
                                        <input type="text" class="form-control" id="guest_{{$child->id}}_last_name"
                                               placeholder="{!! trans('booking.guest_last_name_placeholder') !!}"
                                               name="guests[{{$child->id}}][last_name]"
                                               value="{{old("guests.$child->id.last_name") ?? $child->last_name}}"
                                               maxlength="{{$rules['guest_last_name']['max']}}"
                                               required>
                                        @foreach ($errors->get("guests.$child->id.last_name") as $message)
                                            @component('users.components.error'){!! trans($message) !!}@endcomponent
                                            <br>
                                        @endforeach
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="guest_{{$child->id}}_place_of_birth">{!! trans('booking.place_of_birth') !!}</label>
                                        <input type="text" class="form-control" id="guest_{{$child->id}}_place_of_birth"
                                               placeholder="{!! trans('booking.guest_place_of_birth_placeholder') !!}"
                                               name="guests[{{$child->id}}][place_of_birth]"
                                               value="{{old("guests.$child->id.place_of_birth") ?? $child->place_of_birth}}"
                                               maxlength="{{$rules['guest_place_of_birth']['max']}}"
                                               required>
                                        @foreach ($errors->get("guests.$adult->id.place_of_birth") as $message)
                                            @component('users.components.error'){!! trans($message) !!}@endcomponent
                                            <br>
                                        @endforeach
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <label for="guest_{{$child->id}}_birth_date">{!! trans('booking.birth_date') !!}</label>
                                        <input class="naus-datepicker form-control" id="guest_{{$child->id}}_birth_date"
                                               data-name="guests[{{$child->id}}][birth_date]"
                                               data-date="{{old("guests.$child->id.birth_date") ?? $child->birth_date}}"
                                               value="{{\App\Helpers\DateHelper::stringToFormat(old("guests.$child->id.birth_date") ?? $child->birth_date)}}"
                                               placeholder="{{trans('listing.date_placeholder')}}"
                                               data-start-date="{{$child_start_date}}"
                                               data-end-date="{{$child_end_date}}"
                                               data-view="decade"
                                               readonly
                                               data-required="true">
                                        @foreach ($errors->get("guests.$child->id.birth_date") as $message)
                                            @component('users.components.error'){!! trans($message) !!}@endcomponent
                                            <br>
                                        @endforeach
                                    </div>
                                    <input type="hidden" name="guests[{{$child->id}}][id]" value="{{$child->id}}">
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>

                {{--CHANNEL--}}
                <div class="form-row">
                    <div class="form-group mar-bottom-md">
                        <label for="channel">@lang('booking.channel')</label>
                        <select name="acquisition_channel" id="channel" class="form-control" required>
                            <option value="" selected disabled>@lang('booking.channel_blank')</option>
                            @foreach ($channels as $channel)
                                <option value="{{$channel}}">@lang('booking.channel_'.$channel)</option>
                            @endforeach
                        </select>
                        @foreach ($errors->get("channel") as $message)
                            @component('users.components.error'){!! trans($message) !!}@endcomponent
                            <br>
                        @endforeach
                    </div>
                </div>


                {{--PRIVACY--}}
                <div class="form-row">
                    <div class="form-group mar-bottom-md">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="form-terms-privacy" name="privacy"
                                   required>
                            <label class="form-check-label"
                                   for="form-terms-privacy">@lang('listing.check_terms')</label>
                        </div>
                        @foreach ($errors->get("privacy") as $message)
                            @component('users.components.error'){!! trans($message) !!}@endcomponent
                            <br>
                        @endforeach
                    </div>
                </div>

                <div class="text-center">
                    <div class="d-inline-block">
                        <button id="submit" type="submit" class="def-btn">{!! trans('booking.next') !!}</button>
                    </div>
                </div>

            </form>
        </div>

        @include('users.components.loader')
    @endif
@endsection

@section('scripts')
    <script>
        window.nausdream.errors = "{{$errors->any()}}";
    </script>
    @component('users.components.error-messages') @endcomponent
    <script src="{{mix ('js/booking.js')}}" async defer></script>
@endsection