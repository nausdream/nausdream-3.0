<form id="custom-book" action="{{route(App\Constants::R_USERS_CONTACT_CUSTOM_FORM, [
                            'lang' => App::getLocale()
                            ])}}"
      method="POST"
      data-event-category="Form booking"
      data-event-label="CUSTOM"
novalidate>
    {{csrf_field()}}
    <div class="form-group">
        <label for="form-name">@lang('listing.name')</label>
        <input type="text" class="form-control" id="form-name" name="name"
               value="{{old('name')}}"
               maxlength="{{\App\Constants::LISTING_FORM_NAME_MAX_LENGTH}}"
               placeholder="{{trans('listing.name_placeholder')}}" required>
        <span class="form-error">{{$errors->first('name')}}</span>
    </div>
    <div class="form-group">
        <label for="form-email">@lang('listing.email')</label>
        <input type="email" class="form-control" id="form-email" name="email"
               value="{{old('email')}}"
               placeholder="{{trans('listing.email_placeholder')}}" required>
        <span class="form-error">{{$errors->first('email')}}</span>
    </div>
    <div class="form-group">
        <label for="form-phone">@lang('listing.phone')</label>
        <input type="text" class="form-control" id="form-phone" name="phone"
               value="{{old('phone')}}"
               placeholder="{{trans('listing.phone_placeholder')}}" required>
        <span class="form-error">{{$errors->first('phone')}}</span>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 pad-left-none">
                <label for="form-date">@lang('listing.date')</label>
                <div class="naus-datepicker"
                     data-date="{{old('date')}}"
                     data-name="date"
                     data-start-date="{{\Carbon\Carbon::tomorrow()}}"
                     data-required="true"></div>
                <span class="form-error">{{$errors->first('date')}}</span>
            </div>
            <div class="form-group col-xs-12 col-sm-6 pad-left-none">
                <label for="form-guests">@lang('listing.guests')</label>
                <input type="number" class="form-control" id="form-guests" name="guests"
                       value="{{old('guests') ?? '6'}}"
                       min="6"
                       placeholder="{{trans('listing.guests_placeholder')}}" required>
                <span class="form-error">{{$errors->first('guests')}}</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="form-text">@lang('listing.location') </label>
        <input type="text" class="form-control" id="form-location" name="location"
               value="{{old('location')}}"
               placeholder="{{trans('listing.location_placeholder')}}" required>

        <span class="form-error">{{$errors->first('notes')}}</span>
    </div>

    <div class="form-group">
        <label for="form-notes">@lang('listing.like_to_do') </label>
        <textarea class="form-control" id="form-like-to-do" rows="6"
                  maxlength="{{\App\Constants::MYSQL_TEXT_MAX_LENGTH}}"
                  placeholder="{{trans('listing.like_to_do_placeholder')}}" required
                  name="like_to_do">{{old('like_to_do')}}</textarea>
        <span class="form-error">{{$errors->first('like-to-do')}}</span>
    </div>

    <div>
        @lang('listing.i_have_read')
    </div>

    <div class="form-group">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="form-terms-privacy" checked>
            <label class="form-check-label"
                   for="form-terms-privacy">@lang('listing.check_terms')</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="form-marketing"
                   name="marketing"
                   value="1">
            <label class="form-check-label"
                   for="form-marketing">@lang('listing.check_marketing')</label>
        </div>
    </div>
    <button id="form-submit" type="submit"
            class="btn btn-primary">@lang('listing.send')</button>
</form>