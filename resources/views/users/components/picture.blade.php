@php($versions = $versions ?? [])
@php($cloudinary_version = $cloudinary_version ?? 'v1')

<img
        srcset="
        @foreach($versions as $width => $version)
        {{config('services.cloudinary.secure_url') . $version . '/' . $cloudinary_version . '/' . config('services.cloudinary.base_folder') . $url}} {{$width}}w,
        @endforeach
        {{config('services.cloudinary.secure_url') . ($def ?? 'q_100') . '/' . $cloudinary_version . '/' . config('services.cloudinary.base_folder') . $url}} {{$fallback ?? '800'}}w
                "
        sizes="
        @foreach($versions as $width => $version)
                (max-width: {{$width}}px) {{$width}}px,
        @endforeach
        {{$fallback ?? '800'}}px
                "

        src="{{config('services.cloudinary.secure_url') . ($def ?? 'q_100') . '/' . $cloudinary_version . '/' . config('services.cloudinary.base_folder') . $url}}"
        alt="{{$alt ?? trans('home.default_alt')}}"
        class="{{$classes ?? ''}}">