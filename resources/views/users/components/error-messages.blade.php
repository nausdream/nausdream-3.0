<script>
    window.nausdream.validation_messages = {
        required: "@lang('validation.required')",
        date_format: "@lang('validation.date_format')",
        number: "@lang('validation.number')",
        email: "@lang('validation.email')"
    };
</script>