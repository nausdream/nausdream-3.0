<div class="top-destination">
    @component('users.components.picture', [
    'versions' => $versions,
    'def' => $def,
    'url' => $photo_url,
    'classes' => 'top-destination-picture',
    'alt' => trans('home.alt_text_top_'.$destination),
    'cloudinary_version' => $cloudinary_version ?? 'v1'
    ])
    @endcomponent
    <span class="label">@lang("harbour_centers.$destination")</span>
</div>
