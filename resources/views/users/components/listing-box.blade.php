<div class="listing-box {{$top ? 'top' :''}} mar-bottom-md">
    <a href="{{$listing['slug_url']}}" class="photo with-background-image"
       data-versions="{{json_encode([])}}"
       data-url="{{'experiences/'.$listing['cover_photo']}}" data-def="a_exif,c_limit,h_350,w_1000">
        <div class="price text-xl">
            @lang('listing.price', ['price' => $listing['price']])
        </div>
    </a>
    <div class="pad-sm listing-details">
        <div class="listing-title mar-bottom-sm">
            <a href="{{$listing['slug_url']}}">
                {{$listing['title']}}
            </a>
        </div>
        <p class="listing-description">
            {{$listing['description']}}
        </p>
        <div class="btn-container">
            <a class="def-btn"
               href="{{$listing['slug_url']}}">@lang('harbour_center.listing_cta')</a>
        </div>
    </div>
</div>