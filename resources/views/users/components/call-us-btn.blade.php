<a href="tel:{{$phone}}" class="call-us-btn {{$classes ?? ""}}"
   data-analytics
   data-event-category="Button call us"
   data-event-label="{{$analytics_label}}">
    <i class="icon-phone icon-1x"></i><span>{!! trans('listing.call_us', ['number' => $phone_formatted]) !!}</span>
</a>