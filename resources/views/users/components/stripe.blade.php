<script
        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
        data-key="{{config('services.stripe.key')}}"
        data-amount="{{$booking->discounted_price * 100}}"
        data-currency="{{$booking->currency}}"
        data-name="{{\App\Constants::SITE_NAME}}"
        data-description="{{$booking->experience_title}}"
        data-email="{{$booking->user_email}}"
        data-image="{{config('services.cloudinary.secure_url') . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/logo_nausdream.png"}}"
        data-locale="{{App::getLocale()}}"
        data-label="@lang('booking.pay')"
        data-zip-code="false">
</script>
