<div id="cookie-policy-banner">
    <div class="cookie-policy-banner-title text-center pad-top-sm pad-bottom-sm">
        <span class="text-uppercase">
            @lang('home.cookie_policy_title')
        </span>
    </div>
    <a href="#" class="position-absolute pad-sm close-button" id="cookie-policy-banner-close-button">
        <i class="icon-close"></i>
    </a>

    <hr class="mar-none">

    <div>
        <div class="max-width text-center pad-sm">
            <span>
                {!! trans('home.cookie_policy', [
                'link' => '<a class="cookie-policy-link" href="'.route('users.cookie-policy-'.$language).'" target="_blank">'. trans('home.cookie_policy_name') .'</a>'
                ]) !!}
            </span>
        </div>
    </div>

    <div class="max-width text-center pad-sm">
        <div class="checkboxes-table mar-auto">
            <div class="checkboxes-container">
                <form id="cookies">
                    {{csrf_field()}}

                    <div class="checkbox-container">
                        <input id="necessary-checkbox" name="necessary_checkbox" type="checkbox" checked disabled>
                        <label for="necessary-checkbox">@lang('home.cookie_necessary')</label>
                    </div>
                    <div class="checkbox-container">
                        <input id="preferences-checkbox" name="preferences_checkbox" type="checkbox" checked>
                        <label for="preferences-checkbox">@lang('home.cookie_preferences')</label>
                    </div>
                    <div class="checkbox-container">
                        <input id="statistics-checkbox" name="statistics_checkbox" type="checkbox" checked>
                        <label for="statistics-checkbox">@lang('home.cookie_statistics')</label>
                    </div>
                    <div class="checkbox-container">
                        <input id="marketing-checkbox" name="marketing_checkbox" type="checkbox" checked>
                        <label for="marketing-checkbox">@lang('home.cookie_marketing')</label>
                    </div>
                </form>
            </div>
            <div class="ok-btn-container"><div id="cookie-policy-banner-ok-button">@lang('home.cookies_ok_btn')</div></div>
        </div>
    </div>
</div>