@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent
@endsection

@section('content')
    <div id="map" style="height: 400px"></div>
    <div class="pad-md">
        <h1 class="text-center mar-bottom-md">@lang('contact_us.title')</h1>
        <div class="separator mar-bottom-lg"></div>
        <div class="container-fluid max-width">
            <div class="row">
                <div class="col-lg-3 mar-bottom-md">
                    <h5><i class="icon-home"></i> @lang('contact_us.office_title')</h5>
                    <p><strong>{{\App\Constants::COMPANY_NAME}}</strong></p>
                    <p>@lang('contact_us.office')</p>
                    <p>@lang('footer.vat_number', ['vat' => \App\Constants::VAT_NUMBER])</p>
                    <h5><i class="icon-mobile"></i> @lang('contact_us.social_title')</h5>
                    <p>@lang('contact_us.social_cta')</p>
                    <a href="{{\App\Constants::LINKEDIN_PROFILE_LINK}}" target="_blank">@lang('contact_us.linkedin')</a><br>
                </div>
                <div class="col-lg-9">
                    <h5><i class="icon-phone"></i> @lang('contact_us.call_us_title')</h5>
                    <p>@lang('contact_us.call_us_times')</p>
                    <p><a href="tel:{{$phone}}">{{\App\Constants::TELEPHONE_NUMBER}}</a></p>
                    <h5><i class="icon-mail"></i> @lang('contact_us.contact_us_title')</h5>
                    <p>@lang('contact_us.contact_us_cta')</p>
                    <p>{!! trans('contact_us.contact_us_email', ['type' => trans('contact_us.support'), 'email' => \App\Constants::SUPPORT_EMAIL]) !!}</p>
                    {{--<h5><i class="icon-chat"></i> @lang('contact_us.chat_with_us')</h5>
                    <p>@lang('contact_us.chat_with_us_text')</p>--}}
                </div>
            </div>
        </div>
    </div>
@endSection

@section('scripts')
    <script>
        window.nausdream.office_lat = {{\App\Constants::OFFICE_LAT}};
        window.nausdream.office_lng = {{\App\Constants::OFFICE_LNG}};
        window.nausdream.live_chat_event_label = "Contact-us";
    </script>
    <script src="{{mix ('js/static-pages.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google.maps.api_key')}}&callback=initMap&language={{App::getLocale()}}"
            async defer></script>
@endsection