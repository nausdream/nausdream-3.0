@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent

    <meta property="og:image"
          content="{{config('services.cloudinary.secure_url').config('services.cloudinary.base_folder').'assets/img/harbour_centers/'.$harbour_center->business_id}}"/>
@endsection

@section('content')
    @if(session("success") ?? false)
        <!-- Modal -->
        <div class="modal fade" id="contact-custom-modal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content pad-md">
                    <button type="button" class="close pad-lg" data-dismiss="modal" aria-label="Close"
                            style="position: absolute; top: 0; right: 0;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title w-100 text-center mar-bottom-md pad-left-md pad-right-md"
                        id="cart-abandonment-title">@lang('listing.request_sent')</h5>
                    <div class="text-center">
                        @lang('listing.thank_you')
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{--ABOVE THE FOLD--}}
    <div class="with-background-image color-text-inverse head mar-bottom-md"
         data-versions="{{json_encode($covers['versions'])}}"
         data-url="/assets/img/harbour_centers/{{$harbour_center->business_id}}"
         data-def="{{$covers['def']}}">
        <div class="max-width pad-left-md pad-right-md">
            <div class="title text-center">
                <span class="text-uppercase">@lang("harbour_centers.$harbour_center->business_id")</span>
            </div>
            <h1 class="subtitle text-center">
                @lang("$harbour_center->business_id.subtitle")
            </h1>
        </div>
        <div class="description">
            <div class="max-width">
                <p class="mar-none">
                    {{--MOBILE--}}
                    <span class="d-md-none">{!! str_limit(trans("$harbour_center->business_id.description"), 75) !!}</span>
                    {{--DESKTOP--}}
                    <span class="d-none d-md-block">{!! str_limit(trans("$harbour_center->business_id.description"), 450) !!}</span>
                    <a class="read-more" href="#seo-text"
                       data-analytics
                       data-non-interaction="true"
                       data-event-category="Button"
                       data-event-label="Read more {{$harbour_center->business_id}}">@lang('harbour_center.read_more')</a>
                </p>
            </div>
        </div>
    </div>

    <div class="max-width pad-none">
        {{--TOP EXPERIENCES--}}
        <div class="text-center pad-left-md pad-right-md">
            <span class="text-xxl">@lang("$harbour_center->business_id.top")</span>
        </div>
        <div class="separator"></div>
        {{--OTHER EXPERIENCES--}}
        <div class="text-center mar-bottom-sm pad-left-md pad-right-md">
            <span class="text-xl">@lang("harbour_center.other_text")</span>
        </div>
        <div class="container-fluid mar-bottom-sm">
            <div class="row justify-content-md-center">
                @foreach($listings as $listing)
                    <div class="col-md-4">
                        @component('users.components.listing-box', ['listing' => $listing, 'top' => false])
                        @endcomponent
                    </div>
                    {{--Prevent 3 products on one line followed by a single product on the next line--}}
                    @if(count($listings) % 3 == 1 && (count($listings) - $loop->iteration == 2))
                        {{--Break column--}}
                        <div class="w-100"></div>
                    @endif
                @endforeach
            </div>
        </div>

        <div class="separator mar-bottom-md"></div>
        <h2 class="text-center pad-left-md pad-right-md text-xl hc-description-heading mar-bottom-md">@lang($harbour_center->business_id . '.what_to_do_title')</h2>
        <div id="seo-text" class="max-width-thin pad-left-md pad-right-md">
            <p>
                @lang($harbour_center->business_id . '.description')
            </p>
            <p class="mar-bottom-md">
                {!! trans($harbour_center->business_id . '.what_to_do_text') !!}
            </p>
        </div>
    </div>

    {{--CUSTOM FORM--}}
    <div class=" pad-md mar-bottom-md">
        <div class="container text-center">
            <h1>
                <a href="#" data-toggle="modal" data-target="#custom-form-modal" class="a-color">
                    @lang("harbour_center.custom_group_experience_locality")
                </a>
            </h1>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="custom-form-modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang("listing.form")</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mar-bottom-sm"><span>@lang('listing.form_cta')</span></div>
                    @component('users.components.custom-form')
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
@endSection

@section('scripts')
    <script>
        window.nausdream.live_chat_event_label = "{{$harbour_center->business_id}}";
    </script>
    <script src="{{mix ('js/harbour-center.js')}}" async defer></script>

    <script type="application/ld+json">
    {
      "@context":"http://schema.org",
      "@type":"ItemList",
      "itemListElement":[
        @foreach($listings as $listing)
            {
              "@type":"ListItem",
              "position":{{$loop->iteration}},
          "url":"{{$listing['slug_url']}}"
        }
        @endforeach
        ]
      }

    </script>
    <script>
        window.nausdream.success = "{{session("success") ?? false}}";
        window.nausdream.errorform = "{{$errors->any()}}";
        window.nausdream.validation_messages = {
            required: "@lang('validation.required')",
            date_format: "@lang('validation.date_format')",
            number: "@lang('validation.number')",
            email: "@lang('validation.email')"
        };
    </script>

@endsection