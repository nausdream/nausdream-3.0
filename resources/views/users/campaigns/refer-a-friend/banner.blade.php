{{--BIG BANNERS--}}
<div id="refer-a-friend-desktop" class="refer-a-friend-banner">
    <span class="icon icon-close"></span>
    @lang('refer_a_friend.banner', ['link' => route(\App\Constants::R_USERS_CAMPAIGNS_RULES, ['lang' => App::getLocale(), 'c' => 'refer-a-friend'])])
</div>
<div id="refer-a-friend-mobile" class="refer-a-friend-banner"
     data-affix data-affix-to="#footer-mobile" data-affix-position="bottom" data-affix-offset="50">
    <span class="icon icon-close"></span>
    @lang('refer_a_friend.banner_mobile', ['link' => route(\App\Constants::R_USERS_CAMPAIGNS_RULES, ['lang' => App::getLocale(), 'c' => 'refer-a-friend'])])
</div>

{{--MINIMIZED BANNERS--}}
<div id="refer-a-friend-desktop-minimized" class="refer-a-friend-banner-minimized {{App::getLocale()}}">
    @lang('refer_a_friend.banner_minimized', ['link' => route(\App\Constants::R_USERS_CAMPAIGNS_RULES, ['lang' => App::getLocale(), 'c' => 'refer-a-friend'])])
</div>
<div id="refer-a-friend-mobile-minimized" class="refer-a-friend-banner-minimized {{App::getLocale()}}"
     data-affix data-affix-to="#footer-mobile" data-affix-position="bottom" data-affix-offset="50">
    @lang('refer_a_friend.banner_minimized', ['link' => route(\App\Constants::R_USERS_CAMPAIGNS_RULES, ['lang' => App::getLocale(), 'c' => 'refer-a-friend'])])
</div>

<script src="{{mix('/js/refer-a-friend.js')}}" async defer></script>