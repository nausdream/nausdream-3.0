@extends('users.layout.base')

@section('head')
    <title>{!! trans('refer_a_friend.opt_in_title') !!}</title>
    <meta name="robots" content="noindex, nofollow">
@endsection

@section('content')
    <div class="pad-md mar-bottom-sm max-width text-center">
        <p>{!! trans('refer_a_friend.opt_in_thank_you') !!}</p>
    </div>
@endsection

@section('scripts')
@endsection