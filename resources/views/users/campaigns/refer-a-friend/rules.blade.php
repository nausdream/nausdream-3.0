@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent
    <meta name="robots" content="noindex, nofollow">
@endsection

@section('content')
    <div class="pad-md mar-bottom-md">
        <h1 class="text-center mar-bottom-md">@lang('refer_a_friend.rules_title')</h1>
        <div class="separator mar-bottom-md"></div>
        <div class="banner mar-bottom-md text-center">
            <img src="{{$banner}}">
        </div>
        <div class="max-width">
            {!! trans('refer_a_friend.rules_text') !!}
        </div>
    </div>
@endsection

@section('scripts')
@endsection