@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent
    <meta name="robots" content="noindex, nofollow">
@endsection

@section('content')
    <div class="pad-md mar-bottom-md">
        <h1 class="text-center mar-bottom-md">@lang('cookie_policy.title')</h1>
        <div class="separator mar-bottom-lg"></div>
        <div class="max-width">
            {!! trans('cookie_policy.text') !!}
        </div>
    </div>
@endSection

@section('scripts')
    <script src="{{mix ('js/static-pages.js')}}" async defer></script>
@endSection