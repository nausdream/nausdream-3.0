<!DOCTYPE html>
<html>
<head>
    <title>{!! trans('voucher.page_title') !!}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        @page {
            size: A4;
            margin: 0;
        }

        .page-break {
            page-break-after: always;
        }
    </style>

</head>
<body style="color: #5B5C59; font-family: 'Montserrat', sans-serif; font-size: 12px; margin: 0;">
<div id="container">
    <div style="background-color: #37A29E; padding: 30px 0; text-align: center;">
        <img id="logo"
             src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUYAAACDCAYAAADxhbJDAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAECxJREFUeNrsnf2VmzgXxvF79v91ByEVhKlgSAUhFSxTwToVxFOBNxV4tgJPKoBUYKcCnApwKvCiN5dEo4AQ0hVffn7ncDwfNhZX9z5cfSAFAQAAAAAAADpWMMFtcb1e19XLhn49VcelOs6r1eoM6wAAYbxVYYyrl6zl32fp+FYduRDOSjRPsByAMIJbEEdx3NOrCScSzK8kmMgyAYQRLFooI0Uo14YfvZBgfqHXvBLLCywKIIxgqUKZ9Mwo5eZ4XoslmuEAwgiWKJJrEsd39Br2PMVFEspnNL8BhBEsOZsUQhlZnKLOKD+j6Q0AWKJIhtWxqY7sak9G54hgUQDA4prc1ZFWx8FBJIvq2EEkAQBLFUmRBR4hkgAA0N7cLiCSAADwu0hGJHClo0huheDCogCApYlk4tgfWQ/cpDSlCAAA0NSWEBnoHk1tAMASRTImgXNBDPiksCYAYGkCuWbKInfoiwQALFEkOfoiD7SiEAAALEogQ4YR7QzNbADAUpvZKcO8SAgkAGCxzezMVSAx3QeoYHUdsASBFNN0/q4O2yxQrPDzqTr+metqPyTuvqYrnW5tFSQII1iSQIaSQNpkgefqeKxE4GlgEQuDl+tc/tkicvFETV+v5N70968NNj6/EKHVKocwAjBM9rQhkVxbBrrIIJ9MF9aVRr1lkXsl/ayKH2gn14hrvbPl/0XW18LHEEZwCwL5l4MonShQv0t/e0OCC7GbBs/V8cDZ3IcwglsRSdG8/gghWyziBvaWSxwhjODWMshdYD9IA6bNA1f/8B+wJViY+MX0o3itBzF8jtiC6cDWGoAwgjmKX0RBIF5fST9jPuLvnANlFNhReMJbMBqEEUxV/EJF/KKFB2au/P5F+b1tSsxk5hjSDUu9OcXK7/cehTbnOhH6GMFUBDCWsr94xpckC5g61SSfoqBNzA9kMRWj/4nhKcSe5e8hjAACOE6TVBa7n83UKU5SXojfCB/JDN76mnNOI4QR+HJmuQk89f6/etKwCKxvctYHwZuEP1073iKeVtpyfieEEXBlgW+CX/2AUxU+8fpd/h3N2cn7WFq97Du6Ll5z1yOEEfQRwTr7u59YFliLXp3xQfiW43dFx832wcez7RBG0OSMtQDWWWA8IfGrsz7RxL1UQXFCjd1stphX9f/Wx3dDGOF8U8sE8+DXAMeJxC9HTd2kbx4D/cT8O183RsxjvC1HW0sZ4JgiKGd/ddPX20opYJa+GneI4j8+WwvIGJffJI6pSSxewwkIIPr9gGu2eKZs0ZsfIWNcVjYYS03ieIQmMAQQcPhy2pEtfvDtW8gY550NyiI4VDZYj/5+lQQQTWDA6du6kWjWJ1wgjPN3llhpFg/RN/giC8QgCBjAz7fBj3Uzm/AyZxHCiGZxoHE4IYBfgl8DIZgGA8bw+0Jz039f+eXzEGVBH+O0hPBeEsShRBBNYTAVdhpRfBpKFJExLl8IIYJgLjEh4qBtsQjhs3dDDuZBGJclhLkshBBBMKMY0U3PuRu6awdN6fkK4UkRQfQJgrnGy0YTI49j+DYyRt6mgDjeeRDCC2WDYopMjtFhsLAkom3AZZCpOYC3QiNxp6uO7MrPsTp2YqIrPcsMwFLjaN8SAwWJ5iggYzSvwFBqHicB3zxCZIPglltZbQMud+gemm7FJZS5FYzZYEF3yZSeXgHgVuPr2BIjKayz/OZx3SxOxmwaADCxWNu0xMt+CuVb3XjlrKlZzPm8sWgKf0GzGABt3DUNuIjZFXew0HhZ4VaTxvclo/PFsC4ARjG4n9pgy03enag/T1RGCSEEYNR4jBtiqkR/+3BZIVdfIYQQAL7YbGqpIbY8GjyhrLCAEAIwyRjdYATav5FDaiIfuEaNYVUAvMXruqErawPL8DWRXQdO6nmEmD4DwHCxu5/itJxbbiKXlFVu8HgdAKPEcAxR5Em5XZvIR/QTAjCZmD5CFO2byBuHJnIpPWaH5jEA04ntjZKwID4N0muX55DrrBDznwCYbpyXcxTF1YAGqhdvFesV9l2dJg9+rEKDLTsBmFnGGPx45PZhTvuMrzwbRYhfGvxaqktHvT+JELxvJIZnCCAAYGnCuA1+3yP2TMfP/YopA7ygOgAAtyCMEWWKZ8r+cpgcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAm2QltES+VtYBAAAYXy5odIVFgG++R9MAGYAdpYDEEYAAIAwAgAAhBEAACCMAAAAYQQAAAgjAABAGAEAAMIIAAAQRgAAgDACAAAI/tD983q9xuJ1tVrlmveIx7Ui9e+6z/iiKktYvYRUHlGuS3WcquNclefc8Vnx/lS837TsZB9xPFefOTFfS0rX8FSd+8JwvtomMf2pts2J4/wOdXPxYLu473lrXyc6/aWnja3Ox2ADOTZz07ikz0ZdvifFvvb6JLsErjbuqDf/2iMCU3puf6sahB7sP171HCjAfZYzqo59dRQdZRH/31KANp3nIL03NQl06f0lOQnXNcmLJhwc6/Bw7eZI37n2UDc7g7opqQ7jlvNsTReRoO8zqkeyT9ZSntA0MKnspYH/7UzPa2HrNV3P0aC+hU8kmjr7WeaO75S/K7Swi5VNDP36SO9bcxtadsZMCdry2o+izeldMpAWpzZhrxpMOdfWMCBkYt+273MXNRCjNoHaujqTY91kqi17CmNnPVLAdPlw7MnGtf+FzDfS0qIcRYOt4x62/s1eDnW/7/I7IeYWNi9ZkzM1OJU18WzZMWazpcEdo8tg8dKEUcn0m645kw5dwCQM2S6Lr3AJYw8fzjoysx3D9TkHLJXlaCB+pakouQijYVx2lTVq+a69o70PLNmj4ozHlgooKBBipbJicp4mI+0dy7XXVG7SkAmuO1LvdCnC2FAmuY5CzV0409mGoW6Oqp9IzTZd3dQ3ZGdh1IhI3ZyLHYSo1PhfpGmyW8cDnbfU1HfUs2vjKMWujTDuTbtI6HsSzWciTZ1qr5OucaO7Rs7gbCr8xtCRtlyZY4shDz36g9rS/HQhwlj0bZ50NA0zx7o5mtqF6ubQco6dizCSHxY2PmwgitseNo5a/G/PIIq9MlAqd9lg68RSGK0yNLqWY1sfb4tfbQyvsSmDzXwJ47Et3e0IutJFSFqaiCnjtRVzFsYG++wZvjtxsOfW8vvTlqC3EsYWQSssfDhzPUdHd0PqINBWmVCLKBUMwrixKMu+pZWhzSYtbyJbbmE0Hq1rabJdTUe8GrKJkkMUDfvi5iiMB65Rcgq+sIfjXZnrJtL1VfUUxp2riDQEqHOTrMH/jGKr4Xoyx3Jo+ykthHHrUJaukeaIyZ/s46NFGBPGgO+TkRw4A0/jZHMWxoyrH9cxk9ownTdmEMaMIeMIG4IqZLrGTR+Ra7CJe59Zx42opzAeGUS65BbclsRsyyViGUMFqBd+MKy0K8e8PsN+OQijvXhlzOffOgrj1bWOGpp4CfM1ZqZlbEgQIsZybBiEMfZU54UHWxdcBUyYCmfckd7imKHn4L75prRD04e7bpoGTWyFcWf5/aUv4Zcy0s6+YdP3cScJPYSxYKxz1mxR033RemMxflZ6tVo9M9n/s5oRdrxfFuQn7ker6PGhczB/ZLsKUdz7FEc6t++6EY+j/ctwKnGeR4vPJcHLR9keue1INntq8fdA8/dHD9Xqcs5nxjrPfZy74TzOwpgzGl99fnWty8qU/3/2FOef5q6KlUM9KbYVgZRxP3GkCVRfdfMPR/1aPg9+L/189vgM7oubWkudvZNjyMez1+RDtnxlLMoXpVwnpuu7KElQ6JwxMmcBpkSeslafwj8mD5QdyfbLpOfEI8bvejNE3ZC/uATGxUFcYw9Zi0lrLO4oy2ePPmQbC3NpdRmVc+rLjq01mSanY56CBUDXcdfg3OLO+LE6jtJo7cFRLKMBbywu5//ksHqQnFFwPepoMtL+qqHbYqgb+ZcATF4Y75U7v08uS6hQ0cSqjreUPZ41mVAiiWX96JatSPrOFr47fPZphtUY6lpOAMI4JKclXYzoL6qO19WP76kpeerIzFMSSZt+yW8TNcPz0OsgLiBryyEFHQvV3hiLvCtT/9Wz1CyLKWN8Q9ccNmSTYirSY/XZreHXvJro5XMOCHwY6OZ5GblrJ4YUTF8YT1JF+Z6Xt76FCqeR1VwSSiGOf1HGKNvgY/U/EaQmAxfhDZjuMsaq9EtszaAp7c73ITI65tFaDv4cUCjF1A+RDYlm95Pyb93qyqcBs+37kepBboa/GfFmdhkwq7sPwOSFMVcELPH0PVNrPkRjBF91PDSI40eDZura182FRmTHqh91XuhUYuHdknwPwmjX7LsM4BB/W5bNV3NSPtdlYJs/KEGYmNy0bGxoyJiCJM8XDD1Olu9blsjH3jHSBmwQxhmUUZ78mvp4VtpB1M7coi3tplczxgjlp65skEZ7c59105GxDu173spCz91HEyjLX5DE+Qij+rjejvn8Lg/jv8ismITh747MbAjULLUti/jXZ93Q4gHhWI5HfXty10LsYXUdsVqUWJxCu9p5y42Ic3WdNMCI9HyEkaYuqAKUMjnDzjHwPnHexUlY5fUMTyNN3Qg1mbFcN0I0Tp7qJho5W6xRF1ZgW5yDbCULbTxGWcjWuwDMKmMUqM8A7zlW8FZEiEO0U4ftFoSDHzqEt+2zCT29wrVNrZy1dm2M/qEhWFOGQM2m4Hh07Y9K9pwxrODdJEZPHWXJlfdEDT5j43f7AH2L5s0YjwuQ9l7HcGp7vsgOzrC7XtPy8n02olLXINxZ7gGyblj/cmtpz41l3SRdW3D2sMWW0WePrnsfKb5cWm7w1LRGJeeeL0eXfaU9aM+VWXvc/GNqwkifc90lMOLaJdBAGIx2j2vZna/X8vktWzSUFjvYNW2MtHaom+zab5fAvY9dApl8lmOXwLadEJewSyCEcSxh1ASgbl/psGNf35TDYJo9cncNe9/qylTaZCKaXR3Ljj23jff3daibjK63zQ4HzY6ULPtKexbHup5jjf8dJrCvtG6/d9d9pSGMYwojfZ5jCahS/m6OgNJsrmWK9ZacmqzP1jYRh1M78HNgYSrCKInjgcnGrn2xIVN9y7aGMAYzXV2Hnt9tWnfQFNGB/Zr72Vd6tE6sZnO2LNOdyyg0Pd4n7PIQ2C8FVtvmZFmGrWPdiHK/FRPNHdZR9Ol74gmh9w71HJBt7hxXzK6XmBO2FoNDl6XZepIoWRl3k6Tg2umM7nD7rg57+s7WZ3+VLCBluMbU4G5eN3NDT3WYGNrGdT1GXQZr+v2HtjmCShZT9GjObwaIk8Qwgyx82FjJZDeGGeShzceVwcSiRxyHjNeSemyt7k3ifNUljlKGxlk4YURRqJwzayOnE+dWV5fumnJST1sQZbq43skbzhvRUfc/nahMg81RlJ6okZs8g5WD6mZt+/3ScmnaTbd81aPpTZqusbf/cYtkg8+dA8N9a3rY2ksc1zcc8tcnzmx2TP8AAAAAAAAAAAAWzH8CDAA4sVdM/pzSEwAAAABJRU5ErkJggg=="
             style="width: 200px;">
    </div>
    <div style="padding: 30px 50px 30px;">
        <table class="table" style="border-collapse: collapse; width: 100%;">

            <tr>
                <td colspan="4" style="padding: 4px 0;">
                    <div class="h3 bold text-uppercase margin-bottom-xxl"
                         style="font-size: 20px !important; font-weight: bold !important; margin-bottom: 30px !important; text-transform: uppercase !important;">
                        {{$title}}
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2" class="padding-right-md bold"
                    style="font-weight: bold !important; padding: 4px 0; padding-right: 25px !important;">{!! trans('voucher.experience_date') !!}
                </td>
            </tr>
            <tr>
                <td colspan="2" class="padding-right-md"
                    style="padding: 4px 0; padding-right: 25px !important; vertical-align: top;">{{$experience_date}}
                </td>
            </tr>
            <tr>
                <td colspan="2" class="padding-right-md"
                    style="padding: 4px 0; padding-right: 25px !important; width: 50%;">
                    <hr class="margin-bottom-lg"
                        style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 25px !important;">
                </td>
            </tr>

            <tr>
                <td class="padding-right-sm"
                    style="padding: 4px 0; padding-right: 12px !important; vertical-align: top;">
                    <span class="bold"
                          style="font-weight: bold !important;">{!! trans('voucher.departure_point') !!}</span>
                </td>
                <td class="padding-left-sm padding-right-md"
                    style="padding: 4px 0; padding-left: 12px !important; padding-right: 25px !important; vertical-align: top;">
                    <span class="bold"
                          style="font-weight: bold !important;">{!! trans('voucher.return_point') !!}</span></td>
                <td class="padding-left-md padding-right-sm"
                    style="padding: 4px 0; padding-left: 25px !important; padding-right: 12px !important; vertical-align: top;">
                    <span class="bold"
                          style="font-weight: bold !important;">{!! trans('voucher.departure_time') !!}</span></td>
                <td class="padding-left-sm" style="padding: 4px 0; padding-left: 12px !important; vertical-align: top;">
                    <span class="bold" style="font-weight: bold !important;">{!! trans('voucher.return_time') !!}</span>
                </td>
            </tr>

            <tr>
                <td class="padding-right-sm"
                    style="padding: 4px 0; padding-right: 12px !important; vertical-align: top;">{{$meeting_point}}
                </td>
                <td class="padding-left-sm padding-right-md"
                    style="padding: 4px 0; padding-left: 12px !important; padding-right: 25px !important; vertical-align: top;">
                    {{$return_point}}
                </td>


                <td class="padding-left-md padding-right-sm"
                    style="padding: 4px 0; padding-left: 25px !important; padding-right: 12px !important; vertical-align: top;">
                    {{$departure_time}}
                </td>
                <td class="padding-left-sm" style="padding: 4px 0; padding-left: 12px !important; vertical-align: top;">
                    {{$return_time}}
                </td>
            </tr>

            <tr>
                <td class="padding-right-sm" style="padding: 4px 0; padding-right: 12px !important; width: 20%;">
                    <hr class="margin-bottom-xl"
                        style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 25px !important;">
                </td>
                <td class="padding-left-sm padding-right-md"
                    style="padding: 4px 0; padding-left: 12px !important; padding-right: 25px !important; width: 30%;">
                    <hr class="margin-bottom-xl"
                        style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 25px !important;">
                </td>

                <td class="padding-left-md padding-right-sm"
                    style="padding: 4px 0; padding-left: 25px !important; padding-right: 12px !important; width: 23%;">
                    <hr class="margin-bottom-xl"
                        style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 25px !important;">
                </td>
                <td class="padding-left-sm" style="padding: 4px 0; padding-left: 12px !important; width: 27%;">
                    <hr class="margin-bottom-xl"
                        style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 25px !important;">
                </td>
            </tr>


            {{--GUESTS--}}
            {{--ADULTS--}}
            <tr>
                <td colspan="2" style="font-weight: bold !important; padding: 4px 0; padding-right: 25px;">
                    <span class="bold"
                          style="font-weight: bold !important;">
                     {!! trans('voucher.adults') !!}
                    </span>
                </td>

                @if($children->count())
                    <td colspan="2" style="font-weight: bold !important; padding: 4px 0; padding-left: 25px;">
                    <span class="bold"
                          style="font-weight: bold !important;">
                     {!! trans('voucher.children') !!}
                    </span>
                    </td>
                @endif
            </tr>

            <tr>
                <td colspan="2" style="padding: 4px 0; padding-right: 25px; vertical-align: top;">
                    @foreach($adults as $adult)
                        {!! trans('voucher.guest', [
                        'count' => $loop->iteration,
                        'first_name' => $adult->first_name, 'last_name' =>$adult->last_name,
                        'place'=>$adult->place_of_birth, 'date'=>Carbon\Carbon::parse($adult->birth_date)->format(\App\Helpers\DateHelper::getDateFormat())]) !!}
                        <br>
                    @endforeach
                </td>

                @if($children->count())
                    <td colspan="2" style="padding: 4px 0; padding-left: 25px; vertical-align: top;">
                        @foreach($children as $child)
                            {!! trans('voucher.guest', [
                        'count' => $loop->iteration,
                        'first_name' => $child->first_name, 'last_name' =>$child->last_name,
                        'place'=>$child->place_of_birth, 'date'=> Carbon\Carbon::parse($child->birth_date)->format(\App\Helpers\DateHelper::getDateFormat())]) !!}
                            <br>
                        @endforeach
                    </td>
                @endif
            </tr>

            <tr>
                <td colspan="2" style="padding: 4px 0; padding-right: 25px;">
                    <hr class="margin-bottom-xl"
                        style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 25px !important;">
                </td>

                @if($children->count())
                    <td colspan="2" style="padding: 4px 0; padding-left: 25px;">
                        <hr class="margin-bottom-xl"
                            style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 25px !important;">
                    </td>
                @endif
            </tr>

            {{--INCLUDED SERVICES--}}
            @if($included_services->count())
                <tr>
                    <td colspan="4" class="bold"
                        style="font-weight: bold !important; padding: 4px 0;">
                    <span class="bold"
                          style="font-weight: bold !important;">
                     {!! trans('voucher.included_services') !!}
                    </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="padding: 4px 0">
                        @foreach($included_services as $service)
                            {!! trans('included_services.'.$service->name) !!}@if(!$loop->last),@endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="padding: 4px 0;">
                        <hr class="margin-bottom-xl"
                            style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 25px !important;">
                    </td>
                </tr>
            @endif

            {{--CAPTAIN--}}
            @if(isset($captain_name) || isset($captain_phone))
                <tr>
                    @if(isset($captain_name))
                        <td colspan="2" class="padding-right-md bold"
                            style="font-weight: bold !important; padding: 4px 0; padding-right: 25px !important;">
                            <span class="bold"
                                  style="font-weight: bold !important;">
                             {!! trans('voucher.captain_name') !!}
                            </span>
                        </td>
                    @endif

                    @if(isset($captain_name))
                        <td colspan="2" class="padding-left-md bold"
                            style="font-weight: bold !important; padding: 4px 0; padding-left: 25px !important;">
                            <span class="bold"
                                  style="font-weight: bold !important;">
                             {!! trans('voucher.captain_phone') !!}
                            </span>
                        </td>
                    @endif
                </tr>

                <tr>
                    @if(isset($captain_name))
                        <td colspan="2" class="padding-right-md"
                            style="padding: 4px 0; padding-right: 25px !important;">
                            {{$captain_name}}
                        </td>
                    @endif

                    @if(isset($captain_name))
                        <td colspan="2" class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important;">
                            {{$captain_phone}}
                        </td>
                    @endif
                </tr>
            @endif

            <tr>
                <td colspan="2" class="padding-right-md"
                    style="padding: 4px 0; padding-right: 25px !important; width: 50%; margin-bottom: 25px !important;">
                    <hr class="margin-bottom-xl"
                        style="border: 2px solid; border-bottom: none; color: #BEBEBE;">
                </td>
                <td colspan="2" class="padding-left-md"
                    style="padding: 4px 0; padding-left: 25px !important; width: 50%; margin-bottom: 25px !important;">
                    <hr class="margin-bottom-xl"
                        style="border: 2px solid; border-bottom: none; color: #BEBEBE;">
                </td>
            </tr>

            {{--NOTES AND POLICIES--}}
            <tr>
                <td colspan="4">
                    <p style="margin-bottom: 10px">
                        <i>{!! trans('voucher.notes', ['min' => $minutes]) !!}</i>
                    </p>
                    <p style="margin-bottom: 30px">
                        <i>{!! trans('voucher.cancellation_policies') !!}</i>
                    </p>
                </td>
            </tr>
        </table>

        @if((($adults->count() > 4 || $children->count() > 4) && $included_services->count()) || ($adults->count() > 6 || $children->count() > 6))
            <div class="page-break"></div>
    @endif


    <!--FOOTER-->
        <div style="padding-top: 25px">
            <p style="text-align: center;"><span style="font-weight: bold !important;">{{$company_name}}</span></p>
            <p style="text-align: center; ">
                <span>{!! trans('voucher.footer_second_row', ['vat' => $company_vat]) !!}</span>
            </p>
            <p style="text-align: center; margin-bottom: 0;">
                <span>{!! trans('voucher.footer_third_row', ['phone' => $company_phone, 'email' => $company_booking_email, 'website' => $company_website]) !!}</span>
            </p>
        </div>
    </div>
</div>
</body>
</html>