@extends('admin.layout.base')

@section('head')
    <title>Modifica Servizi Inclusi</title>
@endsection

@section('content')
    <div class="loader"></div>
    <div class="row">
        <div class="col-xs-12 title margin-bottom-md">
            Modifica Servizi Inclusi
        </div>
        <div class="col-xs-12 form" id="new-services">
            @foreach($services as $service)
                <div class="row service" data-id="{{$service->id}}">
                    <div class="col-xs-4">
                        <pre>{{$service->name}}</pre>
                    </div>
                    <div class="col-xs-2">
                        <div class="btn btn-danger delete-service" data-id="{{$service->id}}">
                            Elimina
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-xs-10 form margin-top-lg margin-bottom-xl">
            <hr>
            <form id="new-service-form"
                  method="POST">
                {{csrf_field()}}

                <span>Nuovo servizio incluso: </span>
                <input type="text" name="name" placeholder="es. fresh-water"
                       required minlength="{{$newServiceMinLength}}" maxlength="{{$newServiceMaxLength}}">
                <button class="btn btn-primary">Aggiungi</button>
            </form>
            <div class="row new-service-error" style="display: none;">

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            let newServicesContainer = $("#new-services");
            let loader = $('.loader');
            loader.hide();

            let deleteServiceButtonClickHandler = (e) => {
                loader.show();
                let serviceId = $(e.target).data("id");
                let csrf = "{{csrf_token()}}";
                let url = "{{$deleteServicePath}}";

                $.ajax({
                    method: "POST",
                    url: url,
                    data: {"id": serviceId, "_token": csrf}
                })
                    .done((res) => {
                        loader.hide();

                        // Remove service from dom
                        $(`.service[data-id=${serviceId}]`).remove();
                    })
                    .fail((res) => {
                        loader.hide();
                        console.log(res);
                    });
            };

            // Delete service with ajax
            $(".delete-service").on("click", deleteServiceButtonClickHandler);

            // Add service with ajax
            $("#new-service-form").submit(function (e) {
                loader.show();

                let url = "{{$newServicePath}}";
                let error = $('.new-service-error');
                let newService = $('[name="name"]');
                let name = newService.val();
                name = name.trim();
                name = name.toLowerCase();
                name = name.replace(/(\s|_)/g, '-');
                newService.val(name);

                $.ajax({
                    method: "POST",
                    url: url,
                    data: $(e.target).serialize()
                })
                    .done((res) => {
                        console.log(res);
                        loader.hide();
                        error.hide();
                        // Add newly added service to available services
                        newServicesContainer.append(`
                        <div class="row service" data-id="${res.id}">
                    <div class="col-xs-4">
                        <pre>${res.name}</pre>
                    </div>
                </div>
                            `);

                        let newDeleteButton = $(`
                            <div class="col-xs-2">
                                <div class="btn btn-danger delete-service" data-id="${res.id}">Elimina</div>
                            </div>`)
                            .on("click", deleteServiceButtonClickHandler)
                            .appendTo($(`.service[data-id=${res.id}]`));

                        // Clear new service input
                        $('input[name=name]').val(null)
                    })
                    .fail((res) => {
                        loader.hide();
                        error.show();
                        error.html(`
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div class="alert alert-danger">
                                    ${JSON.stringify(res.responseJSON.errors)}
                                </div>
                            </div>
                        `);
                    });

                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
        });
    </script>
@endsection