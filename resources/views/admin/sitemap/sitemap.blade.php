@extends('admin.layout.base')

@section('head')
    <title>Sitemap - Nausdream Admin</title>
@endsection

@section('content')
    @if($create)
        <div class="alert alert-success">
            <strong>Success!</strong> Sitemap created.
        </div>
        <div>
            <a href="{{route(\App\Constants::R_ADMIN_SITEMAP_DOWNLOAD)}}">Download sitemap</a>
        </div>
    @else
        <div>
            <form action="{{route(\App\Constants::R_ADMIN_SITEMAP_GENERATE)}}" method="POST">
                {{csrf_field()}}
                <button type="submit">Genera sitemap</button>
            </form>
        </div>
    @endif
@endsection

@section('scripts')
    <script>

    </script>
@endsection