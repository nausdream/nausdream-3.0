@extends('admin.layout.base')

@section('head')
    <title>Prodotto creato! - Nausdream Admin</title>
@endsection

@section('content')
        <p>Hai modificato il prodotto. Le modifiche sono già visibili sul sito.</p>
        <p><a href="{{$url}}" target="_blank">VEDI SU NAUSDREAM.COM</a></p>
        <p><a href="{{route(App\Constants::R_ADMIN_PRODUCTS_EDIT_FORM, ["id" => $id])}}">Torna al prodotto</a></p>
@endsection