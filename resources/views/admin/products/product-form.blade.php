@section('head')
    <title>{{$pageTitle}} - Nausdream</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
          rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
@endsection

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if($just_saved ?? false)
        <h4>Prodotto aggiornato</h4>
    @endif

    @if(isset($errors) && $errors->count())
        <span>{{$errors}}</span>
    @endif

    <div class="row product">
        <div class="col-xs-12 title">
            {{$pageTitle}}</div>
        <div class="col-xs-10 form">
            <form action="{{$formAction}}" method="POST"
                  id="{{$formId}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}

                {{--BUSINESS ID--}}
                @component('backoffice.layout.input-row',
                    [
                    "label" => "ID Prodotto Business",
                    "name" => "business_id", "placeholder" => "Es. BCN_1",
                    "max" => $validationRules['business_id']['max'],
                    "value" => $experience->business_id
                    ]
                )
                @endcomponent

                {{--TREKKSOFT ID--}}
                @component('backoffice.layout.input-row',
                    ["label" => "ID Trekksoft", "name" => "trekksoft_id", "placeholder" => "Es. 12345",
                    "max" => $validationRules['trekksoft_id']['max'],
                    "value" => $experience->trekksoft_id,
                    "optional" => isset($validationRules['trekksoft_id']['optional']) ? $validationRules['trekksoft_id']['optional'] : false
                    ]
                )
                    @component('backoffice.layout.input-tooltip', ["placement" => "bottom"])
                        Se lasci vuoto, il prodotto sarà prenotabile solo su richiesta.
                        Lo trovi su Inventory > Activities.
                        Dopo essere entrato su un'attività, copialo dalla barra degli indirizzi.
                        Es. [...]/admin/activity/edit/id/156026. In questo caso, l'ID è 156026
                    @endcomponent
                @endcomponent

                {{--CP--}}
                @component('backoffice.layout.input-row',
                    [
                        "label" => "Centro portuale",
                        "name" => "harbour_center",
                        "type" => "select",
                        "value" => $harbourCenters,
                        "selected" => $experience->harbour_center->business_id,
                        "translationPage" => "harbour_centers"
                    ])
                @endcomponent

                <div class="row margin-top-sm">
                    <div class="col-xs-4">
                        @component('backoffice.layout.label', ["label" => "Nome fornitore (senza cognome)", "optional" => $validationRules["captain_name"]["optional"]])
                        @endcomponent
                        <br>
                        {{--CAPTAIN NAME--}}
                        @component('backoffice.layout.input',
                            [
                                "label" => "",
                                "name" => "captain_name",
                                "type" => "text",
                                "value" => $experience->captain_name,
                                "min" => $validationRules["captain_name"]["min"],
                                "max" => $validationRules["captain_name"]["max"],
                                "optional" => $validationRules["captain_name"]["optional"],
                            ])
                        @endcomponent
                    </div>
                    <div class="col-xs-4">
                        @component('backoffice.layout.label', ["label" => "Email fornitore", "optional" => $validationRules["captain_email"]["optional"]])
                        @endcomponent
                        <br>
                        {{--CAPTAIN EMAIL--}}
                        @component('backoffice.layout.input',
                            [
                                "label" => "",
                                "name" => "captain_email",
                                "type" => "email",
                                "value" => $experience->captain_email,
                                "min" => $validationRules["captain_email"]["min"],
                                "max" => $validationRules["captain_email"]["max"],
                                "optional" => $validationRules["captain_email"]["optional"],
                            ])
                        @endcomponent
                    </div>
                    <div class="col-xs-4">
                        @component('backoffice.layout.label', ["label" => "Telefono fornitore (con prefisso internazionale)", "optional" => $validationRules["captain_phone"]["optional"]])
                        @endcomponent
                        <br>
                        {{--CAPTAIN PHONE--}}
                        @component('backoffice.layout.input',
                            [
                                "label" => "",
                                "name" => "captain_phone",
                                "type" => "text",
                                "value" => $experience->captain_phone,
                                "min" => $validationRules["captain_phone"]["min"],
                                "max" => $validationRules["captain_phone"]["max"],
                                "optional" => $validationRules["captain_phone"]["optional"],
                            ])
                        @endcomponent
                    </div>
                </div>

                {{--BOAT TYPE--}}
                @component('backoffice.layout.input-row',
                    [
                        "label" => "Tipo barca",
                        "name" => "boat_type",
                        "type" => "select",
                        "selected" => $experience->boat_type->name,
                        "value" => $boatTypes
                    ])
                @endcomponent

                {{--BOAT LENGTH--}}
                <div class="input-row row">
                    <div class="col-xs-12 col-sm-3">
                        @component('backoffice.layout.label', ["label" => "Lunghezza barca (m)", "optional" => isset($validationRules['boat_length_min']['optional']) ? $validationRules['boat_length_min']['optional'] : false])
                        @endcomponent
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        @component('backoffice.layout.input', [
                            "label" => "Da",
                            "type" => "number",
                            "name" => "boat_length_min",
                            "min" => $validationRules['boat_length_min']['min'],
                            "optional" => isset($validationRules['boat_length_min']['optional']) ? $validationRules['boat_length_min']['optional'] : false,
                            "value" => $experience->boat_length_min
                            ])
                        @endcomponent
                        @component('backoffice.layout.input', [
                            "label" => "A",
                            "type" => "number",
                            "name" => "boat_length_max",
                            "min" => $validationRules['boat_length_max']['min'],
                            "optional" => isset($validationRules['boat_length_min']['optional']) ? $validationRules['boat_length_min']['optional'] : false,
                            "value" => $experience->boat_length_max
                            ])
                        @endcomponent
                    </div>
                </div>

                {{--MULTILANGUAGE FIELDS--}}
                {{--LANGUAGES--}}
                @component('backoffice.layout.input-row',
                    [
                        "label" => "Campi multilingua: ",
                        "name" => "language",
                        "type" => "select",
                        "value" => $languages
                    ])
                    @component('backoffice.layout.input-tooltip')
                        Tutti i campi da tradurre a mano. Obbligatorio almeno l'italiano.
                    @endcomponent
                @endcomponent

                @php
                    $copies = [];
                @endphp
                @foreach($languages as $language)
                    @php
                        $copies[$language] = $experienceCopies->where("language", $language)->first() ?? new \App\Models\ExperienceCopy();
                        $stops[$language] = $experienceStops->where("language", $language)->sortBy("stop_number")->all();
                    @endphp
                    <div class="input-row row" id="multi-{{$language}}-fields">
                        <div class="col-xs-12">
                            {{--TITLE--}}
                            @component('backoffice.layout.input-row',
                            [
                                "label" => "Titolo esperienza",
                                "placeholder" => "es. Escursione in barca a vela alla Sella del Diavolo",
                                "name" => $language."[title]",
                                "class" => "title full-width",
                                "min" => $validationRules["title"]["min"],
                                "max" => $validationRules["title"]["max"],
                                "optional" => $language !== "it", "additionalAttributes" => "data-lang=$language",
                                "value" => $copies[$language]->title
                            ])
                            @endcomponent

                            {{--DESCRIPTION--}}
                            <div class="input-row row">
                                <div class="col-xs-12 col-sm-3">
                                    <span class="input-label">Descrizione</span>
                                    @component('backoffice.layout.input-tooltip')
                                        Se stai incollando del testo da un'altra fonte, assicurati di farlo così: tasto
                                        destro -> incolla come solo testo, per evitare problemi di formattazione
                                    @endcomponent
                                </div>
                                <div class="col-xs-12 col-sm-9">
                                    <textarea class="summernote" name="{{$language}}[description]"
                                              @if($language === "it")
                                              required
                                              @endif
                                              data-lang="{{$language}}"
                                    >{{$copies[$language]->description}}</textarea>
                                </div>
                            </div>


                            {{--MEETING POINT--}}
                            @component('backoffice.layout.input-row',
                            [
                                "label" => "Punto d'incontro",
                                "placeholder" => "es. Via Roma 200, fronte Rinascente lato porto",
                                "name" => $language."[meeting_point]",
                                "class" => "full-width",
                                "max" => $validationRules["meeting_point"]["max"],
                                "optional" => $language !== "it",
                                "value" => $copies[$language]->meeting_point
                            ])
                            @endcomponent

                            {{--FEATURES--}}
                            <div id="features"></div>
                            <div class="input-row row">
                                <div class="col-xs-12 col-sm-3">
                                    @component('backoffice.layout.label', [
                                        "label" => "Caratteristiche",
                                        "optional" => isset($validationRules['features']["optional"]) ? $validationRules['features']["optional"] : false
                                    ])
                                        @component('backoffice.layout.input-tooltip')
                                            Premere invio per ogni nuova caratteristica che si vuole inserire
                                        @endcomponent
                                    @endcomponent
                                </div>

                                <div class="col-xs-12 col-sm-9">
                                    <input class="full-width" data-role="tagsinput" name="{{$language}}[features]"
                                           value="{{$copies[$language]->features ?? ""}}"
                                           placeholder="es. Lunga camminata: si consiglia di indossare scarpe comode"
                                           {{!isset($validationRules['features']["optional"]) ? "required" : ""}}
                                           {{isset($validationRules['features']["optional"]) && !$validationRules['features']["optional"] ? "required" : ""}}
                                           maxlength="{{$validationRules["features"]['max']}}"/>

                                </div>
                            </div>

                            {{--STOPS--}}
                            <div class="input-row row">
                                <div class="col-xs-12 col-sm-3"></div>
                                <div class="col-xs-9 col-sm-6 text-center">Testo</div>
                                <div class="col-xs-3 col-sm-3 text-center">Durata<br>(opzionale)</div>
                            </div>
                            <div class="input-row row">
                                <div class="col-xs-12 stops-list" data-lang="{{$language}}">
                                    @foreach($stops[$language] as $stop)
                                        <div class="row stop">
                                            <div class="col-xs-12 col-sm-3">
                                                @if($loop->index == 0)
                                                    @component('backoffice.layout.label', [
                                                        "label" => "Tappe"
                                                    ])
                                                        @component('backoffice.layout.input-tooltip')
                                                            La prima è obbligatoria. Quelle lasciate vuote vengono
                                                            ignorate dal
                                                            sistema. La durata è sempre opzionale
                                                        @endcomponent
                                                    @endcomponent
                                                @endif
                                            </div>

                                            <div class="col-xs-9 col-sm-6">
                                                <span class="item-counter stop-item-counter">{{$stop->stop_number}}</span>
                                                @component('backoffice.layout.input', [
                                                    "name" => $language."[stops][$stop->stop_number][text]",
                                                    "placeholder" => "es. Sosta bagno al Poetto, fronte Marina Piccola",
                                                    "label" => "Testo",
                                                    "class" => "full-width",
                                                    "type" => "textarea",
                                                    "max" => $validationRules["stop_text"]["max"],
                                                    "optional" => $loop->iteration != 1 || $language !== "it",
                                                    "value" => $stop->text
                                                ])
                                                @endcomponent
                                            </div>

                                            <div class="col-xs-3 col-sm-3">
                                                @component('backoffice.layout.input', [
                                                    "name" => $language."[stops][$stop->stop_number][duration]",
                                                    "placeholder" => "es. 45",
                                                    "label" => "Durata",
                                                    "type" => "number",
                                                    "class" => "full-width",
                                                    "min" => $validationRules['stop_duration']['min'],
                                                    "optional" => isset($validationRules["stop_duration"]["optional"]) ? $validationRules["stop_duration"]["optional"] : false,
                                                    "value" => $stop->duration
                                                ])
                                                @endcomponent
                                            </div>
                                        </div>
                                    @endforeach
                                    @if(! count($stops[$language]))
                                        <div class="row stop">
                                            <div class="col-xs-12 col-sm-3">

                                                @component('backoffice.layout.label', [
                                                    "label" => "Tappe"
                                                ])
                                                    @component('backoffice.layout.input-tooltip')
                                                        La prima è obbligatoria. Quelle lasciate vuote vengono
                                                        ignorate dal
                                                        sistema. La durata è sempre opzionale
                                                    @endcomponent
                                                @endcomponent
                                            </div>

                                            <div class="col-xs-9 col-sm-6">
                                                <span class="item-counter stop-item-counter">1</span>
                                                @component('backoffice.layout.input', [
                                                    "name" => $language."[stops][1][text]",
                                                    "placeholder" => "es. Sosta bagno al Poetto, fronte Marina Piccola",
                                                    "label" => "Testo",
                                                    "class" => "full-width",
                                                    "type" => "textarea",
                                                    "max" => $validationRules["stop_text"]["max"],
                                                    "optional" => $language !== "it"
                                                ])
                                                @endcomponent
                                            </div>

                                            <div class="col-xs-3 col-sm-3">
                                                @component('backoffice.layout.input', [
                                                    "name" => $language."[stops][1][duration]",
                                                    "placeholder" => "es. 45",
                                                    "label" => "Durata",
                                                    "type" => "number",
                                                    "class" => "full-width",
                                                    "min" => $validationRules['stop_duration']['min'],
                                                    "optional" => isset($validationRules["stop_duration"]["optional"]) ? $validationRules["stop_duration"]["optional"] : false
                                                ])
                                                @endcomponent
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="input-row row">
                                <div class="col-xs-12 col-sm-3"></div>
                                <div class="col-xs-12 col-sm-9 text-center">
                                    <div class="btn btn-default new-stop-btn" data-lang="{{$language}}">Aggiungi
                                        un'altra tappa
                                    </div>
                                </div>
                            </div>

                            {{--SLUG URL--}}
                            @component('backoffice.layout.input-row',
                                ["label" => "Slug URL", "name" => $language."[slug_url]", "placeholder" => "", "class" => "slug-url full-width",
                                        "max" => $validationRules["slug_url"]["max"],
                                        "optional" => $language !== "it", "additionalAttributes" => "data-lang=$language",
                                "value" => $copies[$language]->slug_url
                                ]
                            )
                                @component('backoffice.layout.input-tooltip')
                                    [SEO] A cura di DEV
                                @endcomponent
                            @endcomponent

                            {{--META TITLE--}}
                            @component('backoffice.layout.input-row',
                                ["label" => "Meta title", "name" => $language."[meta_title]", "placeholder" => "", "class" => "meta-title full-width",
                                "max" => $validationRules["meta_title"]["max"],
                                        "optional" => $language !== "it", "additionalAttributes" => "data-lang=$language",
                                "value" => $copies[$language]->meta_title]
                            )
                                @component('backoffice.layout.input-tooltip')
                                    [SEO] A cura di DEV
                                @endcomponent
                            @endcomponent

                            {{--META DESCRIPTION--}}
                            @component('backoffice.layout.input-row',
                                ["label" => "Meta description", "name" => $language."[meta_description]", "placeholder" => "", "class" => "meta-description full-width",
                                 "max" => $validationRules["meta_description"]["max"],
                                 "type" => "textarea",
                                        "optional" => $language !== "it", "additionalAttributes" => "data-lang=$language",
                                "value" => $copies[$language]->meta_description]
                            )
                                @component('backoffice.layout.input-tooltip')
                                    [SEO] A cura di DEV
                                @endcomponent
                            @endcomponent
                        </div>
                    </div>
                @endforeach

                {{--DEPARTURE PORT--}}
                @component('backoffice.layout.input-row',
                    ["label" => "Porto di partenza", "name" => "departure_port", "placeholder" => "Es. Porto di Olbia",
                    "max" => $validationRules["departure_port"]["max"],
                    "value" => $experience->departure_port]
                )
                @endcomponent

                {{--MAP--}}
                @component('backoffice.layout.input-row',
                    ["label" => "Latitudine", "name" => "lat",
                    "value" => $experience->lat]
                )
                @endcomponent
                @component('backoffice.layout.input-row',
                    ["label" => "Longitudine", "name" => "lng",
                    "value" => $experience->lng]
                )
                @endcomponent
                <div class="row input-row margin-bottom-lg">
                    <div class="col-xs-3">
                        @component('backoffice.layout.label', ["label"=>"Link"])
                        @endcomponent
                    </div>
                    <div class="col-xs-9">
                        <button id="maps-btn" type="button" class="btn btn-primary" target="_blank">Vedi su Google
                            Maps
                        </button>
                    </div>
                </div>

                {{--WEEKDAYS--}}
                <div class="row input-row">
                    <div class="col-xs-3">
                        @component('backoffice.layout.label', ["label"=>"Giorni della settimana"])
                        @endcomponent
                    </div>
                    <div class="col-xs-9">
                        <select class="full-width select2" name="weekdays[]" multiple="multiple" required>
                            @foreach($weekdays as $weekday)
                                <option value="{{$weekday}}"
                                        @if(strpos($experience->weekdays, $weekday) !== false) selected @endif>@lang("weekdays.$weekday")</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--MEETING SAFETY MARGIN--}}
                @component('backoffice.layout.input-row',
                    [
                        "label" => "Anticipo su orario di partenza (minuti)",
                        "name" => "meeting_safety_margin",
                        "type" => "number",
                        "placeholder" => "Es. 15",
                        "min" => $validationRules["meeting_safety_margin"]['min'],
                        "value" => $experience->meeting_safety_margin
                    ]
                )
                    @component('backoffice.layout.input-tooltip')
                        Quanti minuti prima rispetto all'orario di partenza il cliente deve trovarsi nel luogo
                        d'incontro.
                    @endcomponent
                @endcomponent

                {{--SCHEDULES--}}
                <div class="row input-row">
                    <div class="col-xs-12 schedules-list">
                        @foreach($experience->schedules as $schedule)
                            <div class="row schedule">
                                <div class="col-xs-12 col-sm-3">
                                    @if($loop->iteration == 1)
                                        @component('backoffice.layout.label', [
                                            "label" => "Orari",
                                        ])
                                            @component('backoffice.layout.input-tooltip')
                                                Per le esperienze che si fanno più di una volta al giorno, aggiungere
                                                altri orari. Il primo è obbligatorio, quelli vuoti vengono ignorati dal
                                                sistema
                                            @endcomponent
                                        @endcomponent
                                    @endif
                                </div>

                                <div class="col-xs-12 col-sm-9">
                                    <span class="schedule-item-counter item-counter">{{$loop->iteration}}</span>
                                    <div class="inline-block">
                                        @if($loop->iteration == 1) Partenza<br> @endif
                                        @component('backoffice.layout.input', [
                                        "name" => "schedules[$loop->iteration][departure_time]",
                                        "label" => "Testo",
                                        "type" => "time",
                                        "value" => $schedule->departure_time,
                                        "optional" => $loop->iteration != 1
                                    ])
                                        @endcomponent
                                    </div>
                                    <div class="inline-block">
                                        @if($loop->iteration == 1) Arrivo<br> @endif
                                        @component('backoffice.layout.input', [
                                        "name" => "schedules[$loop->iteration][arrival_time]",
                                        "label" => "Durata",
                                        "type" => "time",
                                        "value" => $schedule->arrival_time,
                                        "optional" => $loop->iteration != 1
                                    ])
                                        @endcomponent
                                    </div>

                                </div>
                            </div>
                        @endforeach
                        @if(!$experience->schedules->count())
                            <div class="row schedule">
                                <div class="col-xs-12 col-sm-3">
                                    @component('backoffice.layout.label', [
                                            "label" => "Orari",
                                        ])
                                        @component('backoffice.layout.input-tooltip')
                                            Per le esperienze che si fanno più di una volta al giorno, aggiungere
                                            altri
                                            orari. Il primo è obbligatorio, quelli vuoti vengono ignorati dal
                                            sistema
                                        @endcomponent
                                    @endcomponent
                                </div>

                                <div class="col-xs-12 col-sm-9">
                                    <span class="schedule-item-counter item-counter">1</span>
                                    <div class="inline-block">
                                        Partenza<br>
                                        @component('backoffice.layout.input', [
                                        "name" => "schedules[1][departure_time]",
                                        "label" => "Testo",
                                        "type" => "time"
                                    ])
                                        @endcomponent
                                    </div>
                                    <div class="inline-block">
                                        Arrivo<br>
                                        @component('backoffice.layout.input', [
                                        "name" => "schedules[1][arrival_time]",
                                        "label" => "Durata",
                                        "type" => "time"
                                    ])
                                        @endcomponent
                                    </div>

                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="input-row row">
                    <div class="col-xs-12 col-sm-3"></div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="btn btn-default new-schedule-btn">Aggiungi altri orari</div>
                    </div>
                </div>

                {{--FEEDBACKS--}}
                <div class="row input-row">
                    <div class="col-xs-12 feedbacks-list">
                        @foreach($experience->experience_feedbacks as $feedback)
                            <div class="row feedback">
                                <div class="col-xs-12 col-sm-3">
                                    @if($loop->iteration == 1)
                                        @component('backoffice.layout.label', [
                                            "label" => "Feedback",
                                        ])
                                        @endcomponent
                                    @endif
                                </div>

                                <div class="col-xs-12 col-sm-9">
                                    <span class="feedback-item-counter item-counter">{{$loop->iteration}}</span>
                                    <div class="inline-block">
                                        Nome utente<br>
                                        @component('backoffice.layout.input', [
                                        "name" => "feedbacks[$loop->iteration][user_name]",
                                        "max" => $validationRules["feedback_user_name"]["max"],
                                        "label" => "Nome utente",
                                        "placeholder" => "Es. Claudio P.",
                                        "optional" => isset($validationRules['feedback_user_name']['optional']) ? $validationRules['feedback_user_name']['optional'] : false,
                                        "value" => $feedback->customer_name
                                    ])
                                        @endcomponent
                                    </div>
                                    <div class="inline-block">
                                        Punteggio<br>
                                        @component('backoffice.layout.input', [
                                        "name" => "feedbacks[$loop->iteration][score]",
                                        "placeholder" => "Da 3 a 5",
                                        "type" => "number",
                                        "min" => $validationRules["feedback_score"]["min"],
                                        "max" => $validationRules["feedback_score"]["max"],
                                        "optional" => isset($validationRules['feedback_score']['optional']) ? $validationRules['feedback_score']['optional'] : false,
                                        "value" => $feedback->score
                                    ])
                                        @endcomponent
                                    </div>
                                    <div class="inline-block">
                                        Testo<br>
                                        @component('backoffice.layout.input', [
                                        "name" => "feedbacks[$loop->iteration][text]",
                                        "placeholder" => "Es. Bellissima esperienza! ecc",
                                        "type" => "textarea",
                                        "max" => $validationRules["feedback_text"]["max"],
                                        "optional" => isset($validationRules['feedback_text']['optional']) ? $validationRules['feedback_text']['optional'] : false,
                                        "value" => $feedback->text

                                    ])
                                            @component('backoffice.layout.input-tooltip')
                                                Da 3 a 5
                                            @endcomponent
                                        @endcomponent
                                    </div>

                                </div>
                            </div>
                        @endforeach
                        @if(!$experience->experience_feedbacks->count())
                            <div class="row feedback">
                                <div class="col-xs-12 col-sm-3">
                                    @component('backoffice.layout.label', [
                                        "label" => "Feedback",
                                    ])
                                    @endcomponent
                                </div>

                                <div class="col-xs-12 col-sm-9">
                                    <span class="feedback-item-counter item-counter">1</span>
                                    <div class="inline-block">
                                        Nome utente<br>
                                        @component('backoffice.layout.input', [
                                        "name" => "feedbacks[1][user_name]",
                                        "max" => $validationRules["feedback_user_name"]["max"],
                                        "label" => "Nome utente",
                                        "placeholder" => "Es. Claudio P.",
                                        "optional" => isset($validationRules['feedback_user_name']['optional']) ? $validationRules['feedback_user_name']['optional'] : false
                                    ])
                                        @endcomponent
                                    </div>
                                    <div class="inline-block">
                                        Punteggio<br>
                                        @component('backoffice.layout.input', [
                                        "name" => "feedbacks[1][score]",
                                        "placeholder" => "Da 3 a 5",
                                        "type" => "number",
                                        "min" => $validationRules["feedback_score"]["min"],
                                        "max" => $validationRules["feedback_score"]["max"],
                                        "optional" => isset($validationRules['feedback_score']['optional']) ? $validationRules['feedback_score']['optional'] : false
                                    ])
                                        @endcomponent
                                    </div>
                                    <div class="inline-block">
                                        Testo<br>
                                        @component('backoffice.layout.input', [
                                        "name" => "feedbacks[1][text]",
                                        "placeholder" => "Es. Bellissima esperienza! ecc",
                                        "type" => "textarea",
                                        "max" => $validationRules["feedback_text"]["max"],
                                        "optional" => isset($validationRules['feedback_text']['optional']) ? $validationRules['feedback_text']['optional'] : false

                                    ])
                                            @component('backoffice.layout.input-tooltip')
                                                Da 3 a 5
                                            @endcomponent
                                        @endcomponent
                                    </div>

                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="input-row row">
                    <div class="col-xs-12 col-sm-3"></div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="btn btn-default new-feedback-btn">Aggiungi nuovo feedback</div>
                    </div>
                </div>

                {{--CHILDREN AGE--}}
                @component('backoffice.layout.input-row',
                    [
                        "label" => "Età bambini ammessi",
                        "name" => "children_age",
                        "type" => "number",
                        "placeholder" => "Es. 7",
                        "min" => $validationRules['children_age']['min'],
                        "optional" => isset($validationRules['children_age']['optional']) ? $validationRules['children_age']['optional'] : false,
                        "value" => $experience->children_age
                    ]
                )
                    @component('backoffice.layout.input-tooltip')
                        Lascia vuoto se non sono ammessi bambini
                    @endcomponent
                @endcomponent

                {{--DIFFICULTY--}}
                @component('backoffice.layout.input-row',
                    [
                        "label" => "Difficoltà",
                        "name" => "difficulty",
                        "type" => "select",
                        "value" => $difficulties,
                        "selected" => $experience->difficulty->name
                    ])
                @endcomponent

                {{--SENTIMENTS--}}
                <div class="row input-row">
                    <div class="col-xs-3">
                        @component('backoffice.layout.label', ["label"=>"Sentiments", "optional" => isset($validationRules['sentiments']['optional']) ? $validationRules['sentiments']['optional'] : false])
                        @endcomponent
                    </div>
                    <div class="col-xs-9">
                        <select class="full-width select2" name="sentiments[]" multiple="multiple" required>
                            @foreach($sentiments as $sentiment)
                                <option value="{{$sentiment->id}}"
                                        @if(!is_null($experience->sentiments()->where("sentiment_id", $sentiment->id)->first())) selected @endif>@lang("sentiments.$sentiment->name")</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--INCLUDED SERVICES--}}
                <div class="row input-row">
                    <div class="col-xs-3">
                        @component('backoffice.layout.label', ["label"=>"Servizi inclusi", "optional" => isset($validationRules['included_services']['optional']) ? $validationRules['included_services']['optional'] : false])
                        @endcomponent
                    </div>
                    <div class="col-xs-9">
                        <select class="full-width select2" name="included_services[]" multiple="multiple">
                            @foreach($includedServices as $service)
                                <option value="{{$service->id}}"
                                        @if(!is_null($experience->included_services()->where("included_service_id", $service->id)->first())) selected @endif>@lang("included_services.$service->name")</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--SPOKEN LANGUAGES--}}
                <div class="row input-row">
                    <div class="col-xs-3">
                        @component('backoffice.layout.label', ["label"=>"Lingue parlate"])
                        @endcomponent
                    </div>
                    <div class="col-xs-9">
                        <select class="full-width select2" name="spoken_languages[]" multiple="multiple" required>
                            @foreach($spokenLanguages as $language)
                                <option value="{{$language->id}}"
                                        @if(!is_null($experience->spoken_languages()->where("spoken_language_id", $language->id)->first())) selected @endif>@lang("spoken_languages.$language->language")</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--PHOTOS--}}
                <div class="photos-container row input-row">
                    <div class="col-xs-4">
                        @component('backoffice.layout.label', [
                            "label" => "Carica foto (max. ". $validationRules['photo']['max']/(1024*1024) ." MB)",
                        ])
                        @endcomponent
                    </div>
                    <div class="col-xs-5" id="select-photo-label">
                        @component('backoffice.layout.label', [
                            "label" => "Seleziona foto di copertina",
                        ])
                            @component('backoffice.layout.input-tooltip')
                                Se non selezioni nulla, la foto di copertina è la prima inserita.
                            @endcomponent
                        @endcomponent
                    </div>
                    <div class="col-xs-3" id="remove-photo-label">
                        @component('backoffice.layout.label', [
                            "label" => "Rimuovi",
                        ])
                        @endcomponent
                    </div>
                    <div class="col-xs-12" id="photos">
                        @foreach($photos as $photo)
                            <div class="row" id="old-photo-{{$photo['id']}}">
                                <div class="col-xs-4 photo-entry"><img width="200" src="{{$photo['link']}}"/></div>
                                <div class="col-xs-5"><input type="radio" data-cover-photo-type="old" name="cover_photo"
                                                             value="{{$photo['id']}}"
                                                             @if($photo['is_cover']) checked @endif></div>
                                <div class='col-xs-3'>
                                    <div class="btn btn-danger remove-old-photo-btn" data-photo-id="{{$photo['id']}}">
                                        REMOVE
                                    </div>
                                </div>
                                <input type="hidden" name="old_photos[]" value="{{$photo['id']}}"/>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-xs-12">
                        <input class="photo-upload-input" type="file" accept="image/*"
                               name="new_photos[{{count($photos)}}]"
                               @if(count($photos) == 0)
                               required
                                @endif
                        >
                        <div id="photo-upload-box-error"></div>
                    </div>
                    <input type="hidden" name="cover_photo_type" value="old">
                </div>


                {{--PRICE--}}
                @component('backoffice.layout.input-row',
                    [
                        "label" => "Prezzo",
                        "name" => "price",
                        "type" => "number",
                        "placeholder" => "Es. 59.99",
                        "min" => $validationRules['price']['min'],
                        "value" => $experience->price
                    ]
                )
                @endcomponent

                {{--CURRENCY--}}
                @component('backoffice.layout.input-row',
                    [
                        "label" => "Valuta",
                        "name" => "currency",
                        "type" => "select",
                        "value" => $currencies,
                        "selected" => $experience->currency
                    ])
                @endcomponent

                {{--DRAFT--}}
                <input type="hidden" name="draft"/>

                {{$submitSection}}

            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.select2').select2();

            $('[data-toggle="tooltip"]').tooltip();

            let description = $('.summernote');
            description.summernote({
                height: 300,
                minHeight: null,
                maxHeight: null,
                focus: false, // set focus to editable area after initializing summernote
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic']],
                    ['para', ['ul']],
                ],
                placeholder: '',
                // Manual maxlength
                callbacks: {
                    onKeydown: function (e) {
                        let t = e.currentTarget.innerText;
                        if (t.trim().length >= {{$validationRules['description']['max']}}) {
                            //delete key
                            if (e.keyCode !== 8)
                                e.preventDefault();
                        }
                    },
                    onPaste: function (e) {
                        let t = e.currentTarget.innerText;
                        let bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        let all = t + bufferText;
                        document.execCommand('insertText', false, all.trim().substring(0, {{$validationRules['description']['max']}}));
                    }
                }
            });

            // PHOTOS
            let photos = [];
            let photosContainer = $('#photos');
            let errorMessage = $("#photo-upload-box-error");
            errorMessage.hide();
            let selectPhotoLabel = $("#select-photo-label");
            let removePhotoLabel = $("#remove-photo-label");
            let maxSize = {{$validationRules['photo']['max']}};
            let maxPhotoQty = {{$validationRules['photo']['max_quantity']}};
            let droppedFiles;
            let photoUploadInput = $(".photo-upload-input").first();
            // ID of new photos
            let photoCounter = {{count($photos)}};
            // Quantity of photos (for validation)
            let photosQty = photoCounter;
            // Old photo remove
            $('.remove-old-photo-btn').on("click", (e) => {
                let id = $(e.target).data("photo-id");
                $(`#old-photo-${id}`).remove();

                photosQty--;
                if (photosQty === 0) {
                    photoUploadInput.attr('required', 'true');
                }
            });

            function coverPhotoRadioChangeListener(e) {
                $('input[name=cover_photo_type]').val($(e.target).data("cover-photo-type"));
            }

            function getPhotoUID(file) {
                return file.name + "-" + file.size;
            }

            function addPhoto(file) {
                let uid = getPhotoUID(file);
                let current = photoCounter;
                let photoRow = $(`<div class="row" id="new-photo-${uid}"></div>`);
                let photo = $(`<div class="col-xs-4 photo-entry"><img width="200" src="${window.URL.createObjectURL(file)}" /></div>`);
                let radioBox = $(`<div class="col-xs-5"></div>`);
                let radio = $(`<input type="radio" name="cover_photo" data-cover-photo-type="new" value="${current}">`).change((e) => {
                    coverPhotoRadioChangeListener(e)
                });
                radioBox.append(radio);
                let remove = $("<div class='col-xs-3'></div>").append($("<div class='btn btn-danger'>REMOVE</div>").on("click", () => {
                    removePhoto(photoRow, current, uid);
                }));

                photos.push(uid);

                photosContainer.append(photoRow.append(photo, radioBox, remove));

                photosQty++;
            }

            function removePhoto(DOMElement, photoNumber, uid) {
                $(`input[name="new_photos[${photoNumber}]"]`).remove();
                DOMElement.remove();

                delete photos[photos.indexOf(uid)];

                photosQty--;
                if (photosQty === 0) {
                    photoUploadInput.attr('required', 'true');
                }
            }

            function isInvalidPhoto(file) {
                let errors = {
                    size: false,
                    type: false,
                    duplicate: false,
                    count: false
                };
                if (file.size > maxSize) {
                    errors.size = true;
                }
                if (!file.type.match("image/")) {
                    errors.type = true;
                }
                if (photos.indexOf(getPhotoUID(file)) !== -1) {
                    errors.duplicate = true;
                }
                if (photosQty >= maxPhotoQty) {
                    errors.count = true;
                }

                if (errors.size || errors.type || errors.duplicate || errors.count) {
                    return errors;
                }
                return false;
            }

            function getErrorMessage(text) {
                return $(`<p class="invalid-feedback">${text}</p>`);
            }

            function getDroppedFiles(droppedFiles, input) {
                $.each(droppedFiles, function (i, file) {
                    // If it's a valid photo, add to photos
                    let errors = isInvalidPhoto(file);
                    if (errors) {
                        if (errors.size) {
                            errorMessage.append(getErrorMessage(`${file.name} eccede la dimensione massima di ${maxSize / (1024 * 1024)} MB`));
                        }
                        if (errors.type) {
                            errorMessage.append(getErrorMessage(file.name + " non è un'immagine"));
                        }
                        if (errors.duplicate) {
                            errorMessage.append(getErrorMessage(file.name + " già presente"));
                        }
                        if (errors.count) {
                            errorMessage.append(getErrorMessage("Hai raggiunto il massimo di foto che si possono caricare."));
                        }

                        errorMessage.show();

                        // Clear invalid photo from photo input
                        $(input).val(null);

                    } else {
                        addPhoto(file);
                    }
                });
            }

            function fileInputClickListener(e) {
                droppedFiles = e.target.files;

                // Clear old errors
                errorMessage.html(null);

                // Generate logic (radio, remove) for CURRENT last photo
                getDroppedFiles(droppedFiles, e.target);

                // Move head to the next photo
                photoCounter++;

                // Hide old input
                $(e.target).hide();

                photoUploadInput = $("<input>")
                    .addClass("photo-upload-input")
                    .attr("type", "file")
                    .attr("accept", "image/*")
                    .attr("name", `new_photos[${photoCounter}]`)
                    .change(fileInputClickListener);

                $(e.target).after(photoUploadInput);
            }

            photoUploadInput.change((e) => {
                fileInputClickListener(e)
            });

            // Set hidden input describing cover photo type (new photo/old photo)
            $('input[type=radio][name=cover_photo]').change((e) => {
                coverPhotoRadioChangeListener(e)
            });

            /**
             * Show only selected language fields
             */
                // Select language selector
            let languageSelect = $("select[name='language']");
            // Get selected language
            let selectedLanguage = languageSelect.children(":selected").text();
            // Show selected language inputs
            let selectedLanguageFields = $(`#multi-${selectedLanguage}-fields`);
            selectedLanguageFields.show();
            // Show selected language inputs when language changes
            languageSelect.change(function () {
                // Hide previuos language fields
                selectedLanguageFields.hide();
                // Set new selected language
                selectedLanguage = $(this).children(":selected").text();
                // Update selectedLanguageFields variable with new fields
                selectedLanguageFields = $(`#multi-${selectedLanguage}-fields`);
                // Show newly selected fields
                selectedLanguageFields.show();
            });

            // STOPS
            let languages = ["it", "en"];

            let stopLists = {};
            let stops = {};
            languages.forEach((language) => {
                let stopsListLanguage = $(`.stops-list[data-lang="${language}"]`);
                stopLists[language] = stopsListLanguage;
                stops[language] = stopsListLanguage.find(".stop").length + 1;
            });

            function addStopToDom(language, count) {
                stopLists[language].append(
                    `<div class="row stop">
                        <div class="col-xs-12 col-sm-3">
                        </div>
                        <div class="col-xs-9 col-sm-6">
                            <span class="item-counter stop-item-counter">${count}</span>
                            <textarea class="full-width input-textarea" name="${language}[stops][${count}][text]"
                            placeholder="es. Sosta bagno al Poetto, fronte Marina Piccola" maxlength="{{$validationRules["stop_text"]["max"]}}"
                            ></textarea>
                        </div>
                        <div class="col-xs-3 col-sm-3">
                        <input type="number" name="${language}[stops][${count}][duration]" class="full-width"
                        placeholder="es. 45"
                         min="{{$validationRules["stop_duration"]["min"]}}"/>
                        </div>
                    </div>`
                )
            }

            $(".new-stop-btn").on("click", (e) => {
                let language = $(e.target).data("lang");
                addStopToDom(language, stops[language]);
                stops[language]++;
            });

            // SCHEDULES
            let schedulesList = $(`.schedules-list`);
            let schedulesCount = schedulesList.find(".schedule").length;

            function addScheduleToDom(count) {
                schedulesList.append(
                    `<div class="row schedule">
                        <div class="col-xs-12 col-sm-3"></div>
                        <div class="col-xs-12 col-sm-9">
                            <span class="schedule-item-counter item-counter">${count}</span>
                            <div class="inline-block">
                                <input type="time" name="schedules[${count}][departure_time]"/>
                            </div>
                            <div class="inline-block">
                                <input type="time" name="schedules[${count}][arrival_time]"/>
                            </div>
                        </div>
                    </div>`
                )
            }

            $(".new-schedule-btn").on("click", (e) => {
                schedulesCount++;
                addScheduleToDom(schedulesCount);
            });

            // FEEDBACKS
            let feedbacksList = $(`.feedbacks-list`);
            let feedbacksCount = feedbacksList.find('.feedback').length;

            function addFeedbackToDom(count) {
                feedbacksList.append(
                    `<div class="row feedback">
                        <div class="col-xs-12 col-sm-3"></div>
                        <div class="col-xs-12 col-sm-9">
                            <span class="feedback-item-counter item-counter">${count}</span>
                            <input type="text" name="feedbacks[${count}][user_name]" maxlength="{{$validationRules["feedback_user_name"]["max"]}}"/>
                            <input type="number" name="feedbacks[${count}][score]"
                            min="{{$validationRules["feedback_score"]["min"]}}"
                            max="{{$validationRules["feedback_score"]["max"]}}"/>
                            <textarea name="feedbacks[${count}][text]" maxlength="{{$validationRules["feedback_text"]["max"]}}"></textarea>
                        </div>
                    </div>`
                )
            }

            $(".new-feedback-btn").on("click", (e) => {
                feedbacksCount++;
                addFeedbackToDom(feedbacksCount);
            });

            // LAT/LNG
            let mapsBtn = $("#maps-btn");
            mapsBtn.hide();

            function updateMapsBtnLink(lat, lng) {
                mapsBtn.hide();
                $.ajax({
                    'url': "{{route('maps.link')}}",
                    'data': {
                        'lat': lat,
                        'lng': lng
                    }
                })
                    .done(function (response) {
                        mapsBtn.off('click');
                        if (response) {
                            mapsBtn.show();
                            mapsBtn.click(function () {
                                window.open(response);
                            })
                        }
                    });
            }

            // Update mapsBtn link on lat/lng change
            $('[name="lat"]').change(function () {
                let lat = $(this).val();
                let lng = $('[name="lng"]').val();
                updateMapsBtnLink(lat, lng);
            });
            $('[name="lng"]').change(function () {
                let lat = $('[name="lat"]').val();
                let lng = $(this).val();
                updateMapsBtnLink(lat, lng);
            });

            let lat = "{{$experience->lat}}";
            let lng = "{{$experience->lng}}";
            updateMapsBtnLink(lat, lng);
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google.maps.api_key')}}&callback=initMap"
            async defer></script>

    {{$submitLogic}}
@endsection