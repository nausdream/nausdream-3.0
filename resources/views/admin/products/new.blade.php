@extends('admin.layout.base')

@component('admin.products.product-form', [
    "formId" => "new-product-form",
    "formAction" => route(App\Constants::R_ADMIN_PRODUCTS_NEW_POST),
    "experience" => $experience,
    "experienceCopies" => $experienceCopies,
    "experienceStops" => $experienceStops,
    "photos" => $photos,
    "availableLanguages" => $availableLanguages,
    "boatTypes" => $boatTypes,
    "harbourCenters" => $harbourCenters,
    "languages" => $languages,
    "difficulties" => $difficulties,
    "sentiments" => $sentiments,
    "includedServices" => $includedServices,
    "spokenLanguages" => $spokenLanguages,
    "currencies" => $currencies,
    "validationRules" => $validationRules,
    "weekdays" => $weekdays,
])

    @slot('pageTitle')
        Nuovo prodotto
    @endslot

    @slot('submitSection')
        {{--SUBMIT--}}
        <div class="row">
            <div class="col-xs-12 text-center margin-top-lg margin-bottom-sm">
                <div class="btn btn-default" data-toggle="modal" id="submit-draft">Salva
                    come bozza
                </div>
                @component('backoffice.layout.input-tooltip')
                    Attenzione: salvando come bozza, il prodotto verrà nascosto dal sito se già pubblicato
                @endcomponent
            </div>
            <div class="col-xs-12 text-center margin-top-lg margin-bottom-sm">
                <div class="btn btn-default" data-toggle="modal" data-target="#confirm-modal">Salva e pubblica
                </div>
            </div>
        </div>

        {{--PUBLISH CONFIRMATION--}}
        @component('backoffice.layout.modal',
        ["id" => "confirm-modal", "confirmButtonID" => "submit-publish", "confirmButtonText" => "Pubblica",
        "cancelButtonText" => "Non pubblicare ancora", "confirmButtonClass" => "pull-left"])
            Stai per pubblicare l'esperienza, sei sicuro?
        @endcomponent
    @endslot

    @slot('submitLogic')
        <script>
            // Submit form
            let form = $("#new-product-form");
            $('[data-target="#confirm-modal"]').on("click", (e) => {
                form.validate();

                if (!form.valid()) {
                    e.preventDefault();
                    $('#confirm-modal').modal('toggle');
                }
            });
            let submitDraft = $("#submit-draft");
            submitDraft.on("click", () => {
                $('input[name="draft"]').val(true);
                form.validate().cancelSubmit = true;
                form.submit();
            });

            let submitPublish = $("#submit-publish");
            submitPublish.on("click", () => {
                $('input[name="draft"]').val(false);
                formValidator = form.validate({
                    ignore: []
                });
                form.submit();
                $('#confirm-modal').modal('toggle')
            });
        </script>
    @endslot
@endcomponent