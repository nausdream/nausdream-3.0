@extends('admin.layout.base')

@section('head')
    <title>Lista prodotti - Nausdream Admin</title>
@endsection

@section('content')
    @if (app('request')->deleted)
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="alert alert-success">
                    <p>Prodotto cancellato.</p>
                </div>
            </div>
        </div>
    @endif

    @if(isset($errors) && count($errors))
        {{$errors}}
    @endif

    <div class="row list">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">LISTA PRODOTTI</div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{route(App\Constants::R_ADMIN_PRODUCTS_LIST)}}" method="GET"
                          enctype="multipart/form-data" id="search">
                        <input class="search input" name="search" value="{{app('request')->search}}" maxlength="30"/>
                        <input type="hidden" name="order_by" value="{{app('request')->order_by}}"/>
                        <input type="hidden" name="direction" value="{{app('request')->direction}}"/>
                        <button class="search btn btn-primary">CERCA</button>
                        @component('backoffice.layout.input-tooltip', ["placement" => "right"])
                            Puoi filtrare per CP usando i codici dei Centri Portuali (CAG, BCN, ecc)
                        @endcomponent

                        <div class="pull-right">
                            <label for="language">Lingua: </label>
                            <select id="language" name="language">
                                @foreach($languages as $language)
                                    <option value="{{$language}}"
                                            @if(app('request')->language == $language) selected @endif>{{$language}}</option>
                                @endforeach
                            </select>
                        </div>

                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @if(!$experiences->count())
                        Nessun risultato.
                    @else
                        <table class="table">
                            <colgroup>
                                <col class="col-md-1">
                                <col class="col-md-6">
                                <col class="col-md-5">
                            </colgroup>
                            <thead>
                            <tr>
                                {{--ID--}}
                                <th scope="col">ID
                                    @if(app('request')->order_by == "business_id" && app('request')->direction == 'desc')
                                        <i class="pointer i-2 icon-down" data-order-by="business_id"
                                           data-direction="asc"></i>
                                    @else
                                        <i class="pointer i-2 icon-up" data-order-by="business_id"
                                           data-direction="desc"></i>
                                    @endif
                                </th>
                                {{--TITLE--}}
                                <th scope="col">Titolo
                                    @if(app('request')->order_by == "title" && app('request')->direction == 'desc')
                                        <i class="pointer i-2 icon-down" data-order-by="title"
                                           data-direction="asc"></i>
                                    @else
                                        <i class="pointer i-2 icon-up" data-order-by="title"
                                           data-direction="desc"></i>
                                    @endif
                                </th>
                                {{--ACTIONS--}}
                                <th scope="col">Azioni</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($experiences as $experience)
                                <tr>
                                    {{--ID--}}
                                    <td>
                                        <span class="search-match">{{$experience->business_id}}</span>
                                    </td>
                                    {{--TITLE--}}
                                    <td>
                                        <a class="search-match"
                                           href="{{route(App\Constants::R_ADMIN_PRODUCTS_EDIT_FORM, ["id" => $experience->id])}}">{{$experience->title}}</a>
                                    </td>
                                    {{--ACTIONS--}}
                                    <td>
                                        <a href="{{route(App\Constants::R_ADMIN_PRODUCTS_EDIT_FORM, ["id" => $experience->id])}}"
                                           class="inline-block btn btn-default">Modifica</a>
                                        @if($experience->is_finished ?? false)<a
                                                href="{{config('app.url') . $experience->language . "/".\App\Constants::LISTING_PREFIX."/" . $experience->slug_url}}"
                                                class="inline-block btn btn-default">Vedi su nausdream.com</a> @endif
                                        <a class="delete-button inline-block btn btn-danger"
                                           data-experience-id="{{$experience->id}}"
                                           data-toggle="modal" data-target="#delete-modal">Elimina</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$experiences->links()}}
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{--DELETE CONFIRMATION--}}
    @component('backoffice.layout.modal',
    ["id" => "delete-modal", "confirmButtonID" => "submit-delete", "confirmButtonText" => "Elimina",
    "cancelButtonText" => "Annulla", "confirmButtonClass" => "btn-danger pull-left"])
        Per eliminare l'esperienza inserisci qui sotto il risultato dell'operazione
        <strong>{{$deleteConfirmationValue/2}} + {{$deleteConfirmationValue/2}}</strong>:<br>
        <form action="{{route(App\Constants::R_ADMIN_PRODUCTS_DELETE)}}" id="delete" method="POST">
            {{csrf_field()}}
            <input type="text" name="delete_confirmation"/>
            <input type="hidden" name="delete_id"/>
        </form>
    @endcomponent

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            let form = $('#search');
            let orderByInput = $('input[name="order_by"]');
            let directionInput = $('input[name="direction"]');
            let search = "{{app('request')->search}}";

            $('.pointer').on("click", (e) => {
                let orderBy = $(e.target).data('order-by');
                let direction = $(e.target).data('direction');

                orderByInput.val(orderBy);
                directionInput.val(direction);

                form.submit();
            });

            $("#language").change(() => {
                form.submit();
            });

            // Delete experience
            $(".delete-button").on("click", (e) => {
                // Set delete_id value to be the same of delete-button data-experience-id value
                $("input[name='delete_id']").val($(e.target).data('experience-id'));
            });
            $('#submit-delete').on("click", () => {
                if ($("input[name=delete_confirmation]").val() === "{{$deleteConfirmationValue}}") {
                    $("#delete").submit();
                }
            });

            // Highlight matches
            if (search) {
                $(".search-match").each(function () {
                    let regex = new RegExp(search, "i");
                    $(this).html($(this).html().replace(regex, "<strong>$&</strong>"));
                });
            }


        });
    </script>
@endsection