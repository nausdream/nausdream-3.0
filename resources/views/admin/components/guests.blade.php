<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-12">
            {{$guestsName}}<br>
            <input class="form-control"
                   value="{{$guests->count()}}"
                   min="0"
                   name="{{$guestType}}"
                   data-guests="{{$guestType}}"
                   data-count="{{$guests->count()}}"
                   data-deleted="0"
                   type="number"/>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse-{{$guestType}}">DATI OSPITI {{$guestType}} (INSERITI DA UTENTE)</a>
                        </h4>
                    </div>
                    <div id="collapse-{{$guestType}}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="full-width">
                                        <tr>
                                            <td data-guests-container="{{$guestType}}">
                                                @foreach($guests as $guest)
                                                    <table class="full-width guests-table" data-guest-id="old_{{$guest->id}}" data-guest-old>
                                                        <tr>
                                                            <td rowspan="2">
                                                                {{$loop->iteration}}
                                                            </td>
                                                            <td>
                                                                <input class="form-control" type="text"
                                                                       name="old_{{$guestType}}[{{$guest->id}}][first_name]"
                                                                       value="{{$guest->first_name}}"
                                                                       maxlength="{{$rules['guest_first_name']['max']}}" placeholder="Nome">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" type="text"
                                                                       name="old_{{$guestType}}[{{$guest->id}}][last_name]"
                                                                       value="{{$guest->last_name}}"
                                                                       maxlength="{{$rules['guest_last_name']['max']}}" placeholder="Cognome">
                                                            </td>
                                                            <td rowspan="2">
                                        <span data-delete-guests="{{$guestType}}" data-delete-id="old_{{$guest->id}}"
                                              data-delete-old
                                              class="btn btn-danger">X</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" type="date"
                                                                       name="old_{{$guestType}}[{{$guest->id}}][birth_date]"
                                                                       value="{{$guest->birth_date}}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" type="text"
                                                                       name="old_{{$guestType}}[{{$guest->id}}][place_of_birth]"
                                                                       value="{{$guest->place_of_birth}}"
                                                                       placeholder="Luogo di nascita">
                                                            </td>
                                                        </tr>
                                                        <input type="hidden" name="old_{{$guestType}}[{{$guest->id}}][id]"
                                                               value="{{$guest->id}}">
                                                    </table>
                                                @endforeach
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>