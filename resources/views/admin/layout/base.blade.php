<!doctype html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('/css/app.css', 'backoffice/') }}" rel="stylesheet">
    <script src="{{ mix('/js/manifest.js', 'backoffice/') }}"></script>
    <script src="{{ mix('/js/vendor.js', 'backoffice/') }}"></script>
    <script src="{{ mix('/js/app.js', 'backoffice/') }}"></script>
    <link rel="stylesheet" href="{{ mix('/css/app.css', 'backoffice/') }}"/>
    @yield('head')

</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-2">
            <div class="row sidebar">
                <div class="col-xs-12 text-center">
                    <a href="{{route(App\Constants::R_ADMIN_HOME)}}">
                        <img class="logo"
                             src="{{config('services.cloudinary.secure_url') . "c_scale,h_100/" . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/logo_nausdream.png"}}">
                    </a>
                </div>
                {{--LOGOUT--}}
                <div class="col-xs-12 sidebar-item">
                    <a class="link" href="{{route(App\Constants::R_ADMIN_LOGOUT)}}">LOGOUT</a></div>
                {{--BOOKINGS--}}
                <div class="col-xs-12 padding-top-lg sidebar-item text-center"><strong>Bookings</strong></div>
                <div class="col-xs-12 sidebar-item">
                    <a class="link" href="{{route(App\Constants::R_ADMIN_BOOKINGS_LIST)}}">LISTA BOOKINGS</a></div>
                <div class="col-xs-12 sidebar-item">
                    <a class="link" href="{{route(App\Constants::R_ADMIN_BOOKINGS_NEW)}}">NUOVA BOOKING</a></div>

                {{--PRODUCTS--}}
                <div class="col-xs-12 padding-top-lg sidebar-item text-center"><strong>Prodotti</strong></div>
                <div class="col-xs-12 sidebar-item">
                    <a class="link" href="{{route(App\Constants::R_ADMIN_PRODUCTS_LIST)}}">LISTA PRODOTTI</a></div>
                <div class="col-xs-12 sidebar-item">
                <a class="link" href="{{route(App\Constants::R_ADMIN_PRODUCTS_NEW)}}">NUOVO PRODOTTO</a></div>


                {{--SUPER ADMIN--}}
                @if(\Auth::guard(App\Constants::GUARD_ADMIN)->user()->super_admin)
                    <hr>
                    {{--OTHER--}}
                    <div class="col-xs-12 sidebar-item padding-top-lg text-center"><strong>Altro</strong></div>
                    <div class="col-xs-12 sidebar-item">
                        <a class="link" href="{{route(App\Constants::R_ADMIN_AREAS)}}">AREE E CP</a></div>
                    <div class="col-xs-12 sidebar-item">
                        <a class="link" href="{{route(App\Constants::R_ADMIN_INCLUDED_SERVICES)}}">SERVIZI INCLUSI</a></div>
                    <hr>
                    {{--DEV ONLY--}}
                    <div class="col-xs-12 sidebar-item padding-top-lg text-center"><strong>Solo DEV</strong></div>
                    <div class="col-xs-12 sidebar-item">
                        <a class="link" href="{{route(App\Constants::R_ADMIN_SITEMAP_VIEW)}}">Sitemap</a></div>
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 content">
            @yield('content')
        </div>
    </div>
</div>

@yield('scripts')

</body>
</html>
