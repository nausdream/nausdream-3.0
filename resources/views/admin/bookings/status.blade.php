@if(isset($status))
    <span class="booking-status status-{{$status}} text-uppercase">{{\App\Helpers\BookingStatusHelper::getName($status)}}</span>
    @if(isset($payment_id) && $status == \App\Constants::BOOKING_PAID)
        <span>con {{trans('payment_methods.'.\App\Models\PaymentMethod::find($payment_id)->name)}}</span>
    @endif
@endif