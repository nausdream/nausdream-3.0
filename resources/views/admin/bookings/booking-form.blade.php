@section('head')
    <title>{{$pageTitle}} - Nausdream</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
          rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <!-- include codemirror (codemirror.css, codemirror.js, xml.js, formatting.js) -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>
@endsection

@section('content')
    @if($just_saved ?? false)
        <h4 class="alert alert-success">Booking aggiornata</h4>
    @endif

    @if(isset($errors) && $errors->count())
        <p class="alert alert-danger">{{$errors}}</p>
    @endif

    <div class="row product">
        <div class="col-md-12 title margin-bottom-xl">
            <span class="text-lg margin-right-md">{{$pageTitle}}</span> @component('admin.bookings.status', ['status' => $booking->status, 'payment_id' => $booking->payment_method_id])@endcomponent
        </div>
        <div class="col-md-10 form">
            <form action="{{$formAction}}" method="POST"
                  id="{{$formId}}"
                  enctype="multipart/form-data">
                {{csrf_field()}}

                {{--LANGUAGE--}}
                <div class="row margin-bottom-lg">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                Lingua utente
                            </div>
                            <div class="col-md-10">
                                <select name="language">
                                    @foreach($languages as $language)
                                        <option value="{{$language}}"
                                                @if($booking->language == $language) selected @endif >{{$language}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    Campagne marketing attive su questa booking
                                </div>
                                <select class="full-width select2" name="campaigns[]" multiple="multiple">
                                    @foreach($campaigns as $id => $name)
                                        <option value="{{$id}}"
                                                selected>{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                {{--USER--}}
                <div class="row">
                    <div class="col-md-12"><strong>UTENTE</strong></div>
                </div>
                {{--USER NAME--}}
                <div class="row margin-bottom-sm">
                    <div class="col-md-2">
                        Nome<br>
                        <input class="form-control" name="user_first_name" type="text"
                               value="{{$booking->user_first_name}}"/>
                    </div>
                    <div class="col-md-2">
                        Cognome<br>
                        <input class="form-control" name="user_last_name" type="text"
                               value="{{$booking->user_last_name}}"/>
                    </div>
                    <div class="col-md-2">
                        E-mail<br>
                        <input class="form-control" name="user_email" type="text" value="{{$booking->user_email}}"/>
                    </div>
                    <div class="col-md-2">
                        Telefono<br>
                        <input class="form-control" name="user_phone" type="text" value="{{$booking->user_phone}}"/>
                    </div>
                </div>

                {{--DATA ENTERED BY THE USER--}}
                <div class="row">
                    <div class="col-xs-8">
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#invoice-data">DATI DI FATTURAZIONE (INSERITI
                                            DALL'UTENTE)</a>
                                    </h4>
                                </div>
                                <div id="invoice-data" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row margin-bottom-sm">
                                            <div class="col-md-6">
                                                Luogo di nascita<br>
                                                <input class="form-control" name="user_place_of_birth" type="text"
                                                       placeholder="Es. Roccapizzopapero (MI)"
                                                       value="{{$booking->user_place_of_birth}}"/>
                                            </div>
                                            <div class="col-md-6">
                                                Data di nascita<br>
                                                <input class="form-control" name="user_birth_date" type="date"
                                                       value="{{$booking->user_birth_date}}"/>
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-lg">
                                            <div class="col-md-2">
                                                <h5><b>RESIDENZA</b></h5>
                                            </div>
                                            <div class="col-md-3">
                                                Città<br>
                                                <input class="form-control" name="user_city" type="text"
                                                       placeholder="Es. Settimo Milanese (MI)"
                                                       value="{{$booking->user_city}}"/>
                                            </div>
                                            <div class="col-md-4">
                                                Indirizzo<br>
                                                <input class="form-control" name="user_address" type="text"
                                                       placeholder="Es. Via Garibaldi 15"
                                                       value="{{$booking->user_address}}"/>
                                            </div>
                                            <div class="col-md-3">
                                                CAP/ZIP<br>
                                                <input class="form-control" name="user_zip_code" type="text"
                                                       placeholder="Es. 09100"
                                                       value="{{$booking->user_zip_code}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--PRODUCT--}}
                <div class="row">
                    <div class="col-md-12"><strong>PRODOTTO</strong></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        ID Prodotto<br>
                        <select name="experience_id">
                            <option value="" @if(!isset($booking->experience_id)) selected @endif>Esperienza personalizzata</option>
                            @foreach($experiences as $experience)
                                <option value="{{$experience->id}}"
                                        @if($booking->experience_id == $experience->id) selected @endif>{{$experience->business_id}}</option>
                            @endforeach
                        </select>
                        <a id="reload-product" class="btn btn-default">Ricarica</a>
                    </div>
                    <div class="col-md-2">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        Titolo<br>
                        <input class="form-control" name="experience_title" type="text"
                               value="{{$booking->experience_title}}"/>
                    </div>
                    <div class="col-md-3">
                        Link<br>
                        <input class="form-control" name="experience_link" type="text"
                               value="{{$booking->experience_link}}"/>
                    </div>
                    <div class="col-md-3">
                        Servizi inclusi<br>
                        <select class="full-width select2" name="included_services[]" multiple="multiple">
                            @foreach($includedServices as $service)
                                <option value="{{$service->id}}"
                                        @if(!is_null($booking->included_services()->where("included_service_id", $service->id)->first())) selected @endif>@lang("included_services.$service->name")</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row margin-bottom-lg">
                    <div class="col-md-6">
                        Descrizione breve<br>
                        <textarea id="experience-description" class="summernote"
                                  name="experience_description">{{$booking->experience_description}}</textarea>
                    </div>
                </div>

                {{--EXPERIENCE--}}
                <div class="row">
                    <div class="col-md-12"><strong>USCITA</strong></div>
                </div>
                <div class="row margin-bottom-md">
                    <div class="col-md-4">
                        Punto d'incontro<br>
                        <input class="form-control" name="experience_meeting_point" type="text"
                               value="{{$booking->meeting_point}}"/>
                    </div>
                    <div class="col-md-4">
                        Punto di rientro<br>
                        <input class="form-control" name="experience_return_point" type="text"
                               value="{{$booking->return_point}}"/>
                    </div>
                </div>
                <div class="row margin-bottom-md">
                    <div class="col-md-2">
                        <b>Prezzo</b><br>
                        <input class="form-control"
                               value="{{$booking->price ?? ''}}"
                               min="0"
                               name="price"
                               step="any"
                               type="number"/>
                    </div>
                    <div class="col-md-2">
                        Valuta<br>
                        <select name="currency" class="form-control">
                            @foreach($currencies as $currency)
                                <option value="{{$currency}}"
                                        @if($booking->currency == $currency) selected @endif>{{$currency}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        Data uscita<br>
                        <input class="form-control" name="experience_date" type="date"
                               value="{{$booking->experience_date}}"/>
                    </div>
                    <div class="col-md-2">
                        Orario partenza<br>
                        <input class="form-control" name="experience_departure_time" type="time"
                               value="{{$booking->departure_time}}"/>
                    </div>
                    <div class="col-md-2">
                        Orario rientro<br>
                        <input class="form-control" name="experience_return_time" type="time"
                               value="{{$booking->return_time}}"/>
                    </div>
                </div>
                <div class="row margin-bottom-sm">
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        Anticipo su orario di partenza (minuti)
                        @component('backoffice.layout.input-tooltip')
                            Quanti minuti prima rispetto all'orario di partenza il cliente deve trovarsi nel luogo
                            d'incontro.
                        @endcomponent
                        <br>
                        <input class="form-control" name="experience_meeting_safety_margin" type="number"
                               value="{{$booking->meeting_safety_margin ?? \App\Constants::DEFAULT_MEETING_SAFETY_MARGIN}}"/>
                    </div>
                </div>
                {{--GUESTS--}}
                <div class="row margin-bottom-lg">
                    <div class="col-md-6">
                        <div class="row">
                            @component('admin.components.guests', ['guestType' => 'adults', 'guests' => $booking->adults()->get(), 'guestsName' => 'Adulti', 'rules' => $validationRules])
                            @endcomponent
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            @component('admin.components.guests', ['guestType' => 'children', 'guests' => $booking->children()->get(), 'guestsName' => 'Bambini', 'rules' => $validationRules])
                            @endcomponent
                        </div>
                    </div>
                </div>

                {{--SUPPLIER--}}
                <div class="row">
                    <div class="col-md-12"><strong>FORNITORE</strong></div>
                </div>
                <div class="row margin-bottom-sm">
                    <div class="col-md-3">
                        Nome<br>
                        <input class="form-control" name="captain_name" type="text" value="{{$booking->captain_name}}"/>
                    </div>
                    <div class="col-md-3">
                        E-mail<br>
                        <input class="form-control" name="captain_email" type="text"
                               value="{{$booking->captain_email}}"/>
                    </div>
                    <div class="col-md-2">
                        Telefono<br>
                        <input class="form-control" name="captain_phone" type="text"
                               value="{{$booking->captain_phone}}"/>
                    </div>
                </div>
                <div class="row margin-bottom-lg">
                    <div class="col-xs-12"><b>Posizione fornitore</b></div>
                    <div class="col-md-3">
                        Latitudine<br>
                        <input class="form-control" name="lat" type="text"
                               value="{{$booking->lat}}"/>
                    </div>
                    <div class="col-md-3">
                        Longitudine<br>
                        <input class="form-control" name="lng" type="text"
                               value="{{$booking->lng}}"/>
                    </div>
                    <div class="col-md-5">
                        Link<br>
                        <button id="maps-btn" type="button" class="btn btn-primary" target="_blank">Vedi su Google
                            Maps
                        </button>
                    </div>
                </div>

                {{--MISC--}}
                <div class="row">
                    <div class="col-md-12"><strong>ALTRO</strong></div>
                </div>
                <div class="row margin-bottom-lg">
                    <div class="col-md-6">
                        <div class="row margin-bottom-md">
                            <div class="col-md-6">
                                Metodi di pagamento ammessi<br>
                                <select class="full-width select2" name="payment_methods[]" multiple="multiple">
                                    @foreach($paymentMethods as $method)
                                        <option value="{{$method->id}}"
                                                @if(!is_null($booking->payment_methods()->where("payment_method_id", $method->id)->first()) || in_array($method->name, $preselectedPaymentMethods))
                                                selected
                                                @endif
                                        >@lang("payment_methods.$method->name")</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                Scadenza preventivo<br>
                                <input class="form-control" type="datetime-local" name="quote_expiration_datetime"
                                       value="{{$booking->quote_expiration_datetime}}">
                            </div>
                        </div>

                        <div class="row margin-bottom-md">
                            <div class="col-md-12">
                                <button type="button" id="view-email" class="btn btn-info btn-lg margin-bottom-lg">
                                    Vedi email
                                </button>
                            </div>
                        </div>

                        <div class="row margin-bottom-md">
                            <div class="col-md-12">
                                {{$submitSection}}
                            </div>
                        </div>
                    </div>

                    {{--OTHER DATA--}}
                    <div class="col-md-6 margin-bottom-xl">
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#other-data">ALTRI DATI</a>
                                    </h4>
                                </div>
                                <div id="other-data" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row margin-bottom-sm">
                                            <div class="col-md-6">
                                                Operatore<br>
                                                <select class="form-control" name="operator">
                                                    <option disabled selected value> -- Assegna a un operatore --
                                                    </option>
                                                    @foreach($operators as $code => $name)
                                                        <option value="{{$code}}"
                                                                @if($booking->operator == $code) selected @endif >{{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                Canale di contatto<br>
                                                <select class="form-control" name="contact_channel">
                                                    <option disabled selected value> -- Seleziona il canale di contatto
                                                        --
                                                    </option>
                                                    @foreach($channels as $code => $name)
                                                        <option value="{{$code}}"
                                                                @if($booking->contact_channel == $code) selected @endif >{{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-sm">
                                            <div class="col-md-6">
                                                Soft complaint<br>
                                                <input class="form-control" name="soft_complain" type="checkbox"
                                                       value="1"
                                                       @if($booking->soft_complain) checked @endif/>
                                            </div>
                                            <div class="col-md-6">
                                                Hard complaint<br>
                                                <input class="form-control" name="hard_complain" type="checkbox"
                                                       value="1"
                                                       @if($booking->hard_complain) checked @endif/>
                                            </div>
                                        </div>
                                        <div class="row margin-bottom-lg">
                                            <div class="col-md-12">
                                                Note<br>
                                                <textarea rows="7" class="form-control"
                                                          name="notes">{{$booking->notes}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--DELETED GUESTS--}}
                <div id="deleted-guests" class="hidden"></div>
            </form>
        </div>
    </div>

    <div class="loader"></div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            // Delete guest logic
            let deleteGuest = $('[data-delete-guests]');
            let deletedGuests = $("#deleted-guests");

            function deleteGuests() {
                let guestId = $(this).data('delete-id');
                let guestType = $(this).data('delete-guests');
                let input = $("[data-guests='" + guestType + "']");
                let deleted = parseInt(input.data('deleted'));

                let guestTable = $("[data-guests-container='" + guestType + "']").find($("[data-guest-id='" + guestId + "']"));
                guestTable.remove();

                input.val(input.val() - 1);
                input.data('count', input.val());
                input.data('deleted', deleted + 1);

                // Add guest to deleted ones
                if (guestTable.is('[data-guest-old]')) {
                    let id = guestId.split("old_")[1];
                    deletedGuests.append($("<input type='hidden' value='" + id + "' name='deleted_" + guestType + "[]'>"))
                }
            }

            deleteGuest.click(deleteGuests);

            // Add guest logic
            let guestCount = $("[data-guests]");
            guestCount.change(function () {
                let guestType = $(this).data('guests');
                let deleted = parseInt($(this).data('deleted'));
                let count = parseInt($(this).data('count'));
                let newValue = $(this).val();
                let container = $("[data-guests-container='" + guestType + "']");

                // Removing guests
                let delta = newValue - count;
                if (delta < 0) {
                    $(this).data('count', newValue);

                    // If removing old guests, add their IDs to the deleted ones
                    container.children().slice(delta).each(function () {
                        $(this).remove();

                        if ($(this).is('[data-guest-old]')) {
                            let id = $(this).data('guest-id').split("old_")[1];
                            deletedGuests.append($("<input type='hidden' value='" + id + "' name='deleted_" + guestType + "[]'>"))
                        }
                    });
                }
                // Adding guests
                else {
                    // Add val - old guests
                    for (let i = 0; i < delta; i++) {
                        let id = i + count + deleted;
                        let newGuest = $('<table class="full-width guests-table" data-guest-id="new_' + id + '">' +
                            '    <tr>' +
                            '        <td>' +
                            '            <input class="form-control" type="text"' +
                            '                   name="new_' + guestType + '[' + id + '][first_name]"' +
                            '                   placeholder="Nome" maxlength="{{$validationRules['guest_first_name']['max']}}">' +
                            '        </td>' +
                            '        <td>' +
                            '            <input class="form-control" type="text"' +
                            '                   name="new_' + guestType + '[' + id + '][last_name]"' +
                            '                   placeholder="Cognome" maxlength="{{$validationRules['guest_last_name']['max']}}">' +
                            '        </td>' +
                            '        <td rowspan="2">' +
                            '            <span data-delete-guests="' + guestType + '" data-delete-id="new_' + id + '" class="btn btn-danger">X</span>' +
                            '        </td>' +
                            '    </tr>' +
                            '    <tr>' +
                            '        <td>' +
                            '            <input class="form-control" type="date"' +
                            '                   name="new_' + guestType + '[' + id + '][birth_date]">' +
                            '        </td>' +
                            '        <td>' +
                            '            <input class="form-control" type="text"' +
                            '                   name="new_' + guestType + '[' + id + '][place_of_birth]" placeholder="Luogo di nascita">' +
                            '        </td>' +
                            '    </tr>' +
                            '</table>');
                        container.append(newGuest);
                    }
                    let deleteGuest = $('[data-delete-guests]');
                    deleteGuest.off('click', deleteGuests);
                    deleteGuest.click(deleteGuests);

                    $(this).data('count', newValue);
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();

            let mapsBtn = $("#maps-btn");
            mapsBtn.hide();
            let viewEmailBtn = $("#view-email");
            let form = $("#{{$formId}}");
            let loader = $(".loader");
            let description = $('#experience-description');
            let summernoteOptions = {
                height: 300,
                minHeight: null,
                maxHeight: null,
                focus: false, // set focus to editable area after initializing summernote
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['link'],
                    ['codeview']
                ],
                codemirror: {
                    theme: 'monokai',
                    htmlMode: true,
                    mode: 'text/html'
                },
                placeholder: '',
                // Manual maxlength
                callbacks: {
                    onKeydown: function (e) {
                        let t = e.currentTarget.innerText;
                        if (t.trim().length >= {{$validationRules['experience_description']['max']}}) {
                            //delete key
                            if (e.keyCode !== 8)
                                e.preventDefault();
                        }
                    },
                    onPaste: function (e) {
                        let t = e.currentTarget.innerText;
                        let bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        let all = t + bufferText;
                        document.execCommand('insertText', false, all.trim().substring(0, {{$validationRules['experience_description']['max']}}));
                    }
                }
            };

            loader.hide();

            description.summernote(summernoteOptions);

            let reload = $("#reload-product");
            reload.hide();

            let productSelected = false;
            let languageSelect = $('[name="language"]');
            let productSelect = $('[name="experience_id"]');
            // On language change
            languageSelect.change(function () {
                if (productSelected) {
                    reload.show();
                }
            });

            let includedServices = $("[name='included_services[]']");

            function updateMapsBtnLink(lat, lng){
                mapsBtn.hide();
                $.ajax({
                    'url': "{{route('maps.link')}}",
                    'data': {
                        'lat': lat,
                        'lng': lng
                    }
                })
                    .done(function (response) {
                        mapsBtn.off('click');
                        if (response) {
                            mapsBtn.show();
                            mapsBtn.click(function(){
                                window.open(response);
                            })
                        }
                    });
            }

            function loadProduct() {
                let experienceId = $('[name="experience_id"]').val();
                console.log(experienceId);
                if (!experienceId) {
                    loader.hide();
                    return;
                }

                $.ajax({
                    method: "GET",
                    url: "{{$getProductInfoURL}}",
                    data: {
                        'language': $('[name="language"]').val(),
                        'id': experienceId,
                    }
                })
                    .done(function (response) {
                        // Pre-fill fields
                        $('[name="experience_title"]').val(response.copy.title);
                        $('[name="experience_link"]').val(response.copy.slug_url);

                        let desc = $('#experience-description');
                        desc.summernote("reset");
                        desc.summernote('code', response.copy.short_description);
                        $('[name="experience_meeting_point"]').val(response.copy.meeting_point);

                        // Fill supplier data
                        $('[name="captain_name"]').val(response.experience.captain_name);
                        $('[name="captain_email"]').val(response.experience.captain_email);
                        $('[name="captain_phone"]').val(response.experience.captain_phone);

                        // Set first schedule
                        if (response.schedules) {
                            $('[name="experience_departure_time"]').val(response.schedules[0].departure_time);
                            $('[name="experience_return_time"]').val(response.schedules[0].arrival_time);

                            // Set departure time as quote expiration time by default
                            $('[name="quote_expiration_time"]').val(response.schedules[0].departure_time);
                        }

                        // Lat/Lng
                        $('[name="lat"]').val(response.experience.lat);
                        $('[name="lng"]').val(response.experience.lng);
                        updateMapsBtnLink(response.experience.lat, response.experience.lng);

                        if (response.services) {
                            // Get array of services IDs
                            let services = $.map(response.services, function (service, i) {
                                return service.id;
                            });
                            // Select services
                            includedServices.val(services);
                            includedServices.trigger('change'); // Notify any JS components that the value changed
                        }

                        loader.hide();
                    })
                    .fail(function (response) {
                        console.log(response);
                    });
            }

            // On product change
            reload.click(function () {
                loader.show();
                reload.hide();
                loadProduct();
            });
            productSelect.change(function () {
                productSelected = true;
                loader.show();
                reload.hide();

                loadProduct();
            });

            // View email in iframe
            viewEmailBtn.click(function () {
                // Set form action and target
                form.attr('action', '{{$getEmailBody}}');
                form.attr('target', '_blank');

                form.submit();

                // Ret form action and target
                form.attr('action', '{{$formAction}}');
                form.attr('target', null);
            });

            // Update mapsBtn link on lat/lng change
            $('[name="lat"]').change(function(){
                let lat = $( this ).val();
                let lng = $('[name="lng"]').val();
                updateMapsBtnLink(lat, lng);
            });
            $('[name="lng"]').change(function(){
                let lat = $('[name="lat"]').val();
                let lng = $( this ).val();
                updateMapsBtnLink(lat, lng);
            });

            let lat = "{{$booking->lat}}";
            let lng = "{{$booking->lng}}";
            updateMapsBtnLink(lat, lng);
        });
    </script>

    {{$submitLogic}}
@endsection