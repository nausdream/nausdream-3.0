@extends('admin.layout.base')

@section('head')
    @if($email_sent ?? false)
        <title>Preventivo inviato! - Nausdream Admin</title>
    @else
        <title>Preventivo salvato come bozza! - Nausdream Admin</title>
    @endif
@endsection

@section('content')
    @if($email_sent ?? false)
        <p>Hai inviato il preventivo.</p>
        <p><a href="{{$email_url}}" target="_blank">VEDI EMAIL INVIATA ALL'UTENTE</a></p>
    @else
        <p>Hai salvato il preventivo come bozza.</p>
    @endif
        <p><a href="{{$url}}">VEDI PREVENTIVO</a></p>
@endsection