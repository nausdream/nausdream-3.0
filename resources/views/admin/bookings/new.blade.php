@extends('admin.layout.base')

@component('admin.bookings.booking-form', [
    "getProductInfoURL" => $getProductInfoURL,
    "getEmailBody" => $getEmailBody,
    "formId" => "new-booking-form",
    "formAction" => route(App\Constants::R_ADMIN_BOOKINGS_NEW_POST),
    "booking" => $booking,
    "experiences" => $experiences,
    "boatTypes" => $boatTypes,
    "languages" => $languages,
    "includedServices" => $includedServices,
    'paymentMethods' => $paymentMethods,
    'preselectedPaymentMethods' => $preselectedPaymentMethods,
    "currencies" => $currencies,
    "validationRules" => $validationRules,
    "operators" => \App\Constants::OPERATORS,
    "channels" => \App\Constants::CONTACT_CHANNELS,
    'campaigns' => $campaigns
])

    @slot('pageTitle')
        Nuova booking
    @endslot

    @slot('submitSection')
        <input type="hidden" name="send_email" value="0">
        <input type="hidden" name="status" value="{{\App\Constants::BOOKING_PAYMENT_LINK_SENT}}">
        <div class="margin-bottom-lg">
            <button id="save-button" type="button" class="btn btn-info btn-lg margin-bottom-lg">
                Salva senza inviare preventivo
            </button>
        </div>
        <div>
            <button id="submit-button" type="button" class="btn btn-info btn-lg margin-bottom-lg">
                Invia preventivo
            </button>
        </div>

        {{--SUBMIT CONFIRMATION--}}
        @component('backoffice.layout.modal',
        ["id" => "warning-modal", "confirmButtonID" => "submit-confirm", "confirmButtonText" => "Invia comunque",
        "cancelButtonText" => "Correggi errori", "confirmButtonClass" => "btn-danger pull-left", "cancelButtonClass" => "btn-success",
        "title" => "Potenziali problemi"])
            <ul id="warnings-list">
                <li id="warning-price">Prezzo a 0</li>
                <li id="warning-experience-title">Titolo esperienza non impostato</li>
                <li id="warning-experience-date">Data uscita non impostata</li>
                <li id="warning-experience-departure-time">Orario di partenza non impostato</li>
                <li id="warning-experience-return-time">Orario di rientro non impostato</li>
                <li id="warning-adults">N. di adulti a 0</li>
                <li id="warning-quote-expiration-datetime">Nessuna scadenza del preventivo</li>
                <li id="warning-captain-name">Nome fornitore non impostato</li>
                <li id="warning-captain-phone">Telefono fornitore non impostato</li>
                <li id="warning-user-name">Nome utente non impostato</li>
                <li id="warning-user-email">Email utente non impostata</li>
                <li id="warning-payment-methods">Nessun metodo di pagamento</li>
            </ul>
        @endcomponent
    @endslot

    @slot('submitLogic')
        <script>
            $(document).ready(function () {
                var form = $("#new-booking-form");
                var submitButton = $("#submit-button");
                var saveButton = $("#save-button");
                var warnings = $("#warnings-list");

                var modal = $("#warning-modal");
                modal.find("[data-dismiss='modal']").click(function () {
                    warnings.find("li").hide();
                });

                warnings.find("li").hide();

                submitButton.click(function () {
                    $('[name="send_email"]').val(1);
                    var error = false;

                    // Validate form fields
                    var price = form.find("[name='price']");
                    var experience_title = form.find("[name='experience_title']");
                    var experience_date = form.find("[name='experience_date']");
                    var experience_departure_time = form.find("[name='experience_departure_time']");
                    var experience_return_time = form.find("[name='experience_return_time']");
                    var adults = form.find("[name='adults']");
                    var quote_expiration_datetime = form.find("[name='quote_expiration_datetime']");
                    var captain_name = form.find("[name='captain_name']");
                    var captain_phone = form.find("[name='captain_phone']");
                    var user_name = form.find("[name='user_first_name']");
                    var user_email = form.find("[name='user_email']");
                    var payment_methods = form.find("[name='payment_methods[]']");

                    // Show errors
                    if (price.val() == 0) {
                        error = true;
                        $("#warning-price").show();
                    }
                    if (!experience_title.val()) {
                        error = true;
                        $("#warning-experience-title").show();
                    }
                    if (!experience_departure_time.val()) {
                        error = true;
                        $("#warning-experience-departure-time").show();
                    }
                    if (!experience_return_time.val()) {
                        error = true;
                        $("#warning-experience-return-time").show();
                    }
                    if (!experience_date.val()) {
                        error = true;
                        $("#warning-experience-date").show();
                    }
                    if (adults.val() == 0) {
                        error = true;
                        $("#warning-adults").show();
                    }
                    if (!quote_expiration_datetime.val()) {
                        error = true;
                        $("#warning-quote-expiration-datetime").show();
                    }
                    if (!captain_name.val()) {
                        error = true;
                        $("#warning-captain-name").show();
                    }
                    if (!captain_phone.val()) {
                        error = true;
                        $("#warning-captain-phone").show();
                    }
                    if (!user_name.val()) {
                        error = true;
                        $("#warning-user-name").show();
                    }
                    if (!user_email.val()) {
                        error = true;
                        $("#warning-user-email").show();
                    }
                    if (payment_methods.val().length == 0) {
                        error = true;
                        $("#warning-payment-methods").show();
                    }

                    // Show modal
                    if (error) {
                        modal.modal("toggle");
                    } else {
                        form.submit();
                    }
                });
                $("#submit-confirm").click(function () {
                    console.log('submitting');
                    form.submit();
                });

                saveButton.click(function () {
                    $('[name="send_email"]').val(0);
                    form.submit();
                });
            });
        </script>
    @endslot
@endcomponent