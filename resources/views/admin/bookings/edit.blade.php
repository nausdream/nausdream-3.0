@extends('admin.layout.base')

@component('admin.bookings.booking-form', [
    "getProductInfoURL" => $getProductInfoURL,
    "getEmailBody" => $getEmailBody,
    "formId" => "edit-booking-form",
    "formAction" => route(App\Constants::R_ADMIN_BOOKINGS_EDIT_POST, ['id' => $booking->id]),
    "booking" => $booking,
    "experiences" => $experiences,
    "boatTypes" => $boatTypes,
    "languages" => $languages,
    "includedServices" => $includedServices,
    'paymentMethods' => $paymentMethods,
    'preselectedPaymentMethods' => $preselectedPaymentMethods,
    "currencies" => $currencies,
    "validationRules" => $validationRules,
    "just_saved" => $just_saved,
    "operators" => \App\Constants::OPERATORS,
    "channels" => \App\Constants::CONTACT_CHANNELS,
    'campaigns' => $campaigns
])

    @slot('pageTitle')
        Booking {{$booking->user_first_name}} @if(isset($booking->experience)){{$booking->experience->business_id}}@endif
    @endslot

    @slot('submitSection')
        <div class="row margin-bottom-lg">
            <div class="col-xs-8">
                <div class="row margin-bottom-sm">
                    <div class="col-xs-3">
                        Stato
                    </div>
                    <div class="col-xs-8">
                        <select class="form-control" name="status">
                            @foreach($status as $st)
                                <option value="{{$st}}"
                                        @if($booking->status == $st) selected @endif >{{\App\Helpers\BookingStatusHelper::getName($st)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--PAID DETAILS--}}
                <div class="row margin-bottom-sm" data-paid-div style="display: none">
                    <div class="col-xs-3">
                        Pagata il:
                    </div>
                    <div class="col-xs-8">
                        <input data-paid-control class="form-control" type="datetime-local" name="payment_datetime"
                               value="{{$booking->payment_datetime}}">
                    </div>
                </div>
                <div class="row" data-paid-div>
                    <div class="col-xs-3">
                        Pagata con:
                    </div>
                    <div class="col-xs-8">
                        <select data-paid-control class="form-control" name="payment_method_id">
                            <option disabled selected>-- Seleziona metodo di pagamento --</option>
                            @foreach($paymentMethods as $method)
                                <option value="{{$method->id}}"
                                        @if($booking->payment_method_id == $method->id) selected @endif >{!! trans('payment_methods.'.$method->name) !!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{--REFUNDED DETAILS--}}
                <div class="row" data-refunded-div style="display: none">
                    <div class="col-xs-3">
                        Ammontare rimborso:
                    </div>
                    <div class="col-xs-8">
                        <input class="form-control" data-refunded-control
                               value="{{$booking->refund ?? ''}}"
                               min="1"
                               name="refund"
                               step="any"
                               max="{{$booking->discounted_price}}"
                               type="number"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="margin-bottom-md">
            <button type="submit" class="btn btn-info btn-lg">Salva senza inviare nuova email</button>
            @component('backoffice.layout.input-tooltip')
                I dati verranno aggiornati in tutte le fasi del flusso (es voucher, pagamento, ecc)
            @endcomponent
        </div>
        <input type="hidden" name="send_email" value="0">
        <div class="margin-bottom-lg">
            <button id="send-email" type="button" class="btn btn-info btn-lg">Invia preventivo modificato</button>
            @component('backoffice.layout.input-tooltip')
                Invia i dati del preventivo aggiornati via email
            @endcomponent
        </div>
    @endslot

    @slot('submitLogic')
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();

                // Set "send-email" to true
                $("#send-email").click(function () {
                    $('[name="send_email"]').val(true);

                    $("#edit-booking-form").submit();
                });

                // Hide or show paid calendar
                let paidDiv = $("[data-paid-div]");
                let paidControl = $("[data-paid-control]");
                let refundedDiv = $("[data-refunded-div]");
                let refundedControl = $("[data-refunded-control]");
                let paid = "{{\App\Constants::BOOKING_PAID}}";
                let partiallyRefunded = "{{\App\Constants::BOOKING_PARTIALLY_REFUNDED}}";
                let refunded = "{{\App\Constants::BOOKING_REFUNDED}}";
                let status = $("[name='status']");
                let softComplain = $('[name="soft_complain"]');

                function changeStatus(status) {
                    paidControl.removeAttr('required');
                    paidDiv.hide();
                    refundedControl.removeAttr('required');
                    refundedDiv.hide();

                    switch (status) {
                        case paid:
                            paidControl.prop('required', true);
                            paidDiv.show();
                            break;
                        case partiallyRefunded:
                            refundedControl.prop('required', true);
                            refundedDiv.show();
                            softComplain.prop('checked', true);
                            break;
                        case refunded:
                            $('[name="refund"]').val({{$booking->discounted_price}});
                            softComplain.prop('checked', true);
                            break;
                        default:
                            break;
                    }
                }

                changeStatus(status.val());

                status.change(function () {
                    let val = $(this).val();
                    changeStatus(val);
                });
            });
        </script>
    @endslot
@endcomponent