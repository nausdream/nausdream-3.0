@extends('admin.layout.base')

@section('head')
    <title>Lista Bookings - Nausdream Admin</title>
@endsection

@section('content')
    @if(isset($errors) && count($errors))
        {{$errors}}
    @endif

    <div class="row list">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">LISTA BOOKINGS</div>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <form action="{{route(App\Constants::R_ADMIN_BOOKINGS_LIST)}}" method="GET"
                          enctype="multipart/form-data" id="search">
                        <input class="search input" name="search" value="{{app('request')->search}}" maxlength="30"/>
                        <input type="hidden" name="order_by" value="{{app('request')->order_by}}"/>
                        <input type="hidden" name="direction" value="{{app('request')->direction}}"/>
                        <button class="search btn btn-primary">CERCA</button>
                        @component('backoffice.layout.input-tooltip', ["placement" => "right"])
                            Cerca per nome utente o per ID prodotto
                        @endcomponent
                    </form>
                </div>
                <div class="col-xs-4 text-right">
                    <button id="delete-modal-button" class="delete-button inline-block btn btn-danger"
                            disabled="disabled"
                            data-toggle="modal" data-target="#delete-modal">Annulla uscite selezionate
                    </button>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @if(!$bookings->count())
                        Nessun risultato.
                    @else
                        <form id="delete-form" action="{{route(\App\Constants::R_ADMIN_BOOKINGS_DELETE_POST)}}"
                              method="POST">
                            {{csrf_field()}}
                            <table class="table">
                                <colgroup>
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                    <col class="col-md-1">
                                </colgroup>
                                <thead>
                                <tr>
                                    {{--ID--}}
                                    <th scope="col">ID
                                        @if(app('request')->order_by == "bookings.id" && app('request')->direction == 'desc')
                                            <i class="pointer i-2 icon-down" data-order-by="bookings.id"
                                               data-direction="asc"></i>
                                        @else
                                            <i class="pointer i-2 icon-up" data-order-by="bookings.id"
                                               data-direction="desc"></i>
                                        @endif
                                    </th>
                                    {{--USER--}}
                                    <th scope="col">Utente
                                        @if(app('request')->order_by == "bookings.user_first_name" && app('request')->direction == 'desc')
                                            <i class="pointer i-2 icon-down" data-order-by="bookings.user_first_name"
                                               data-direction="asc"></i>
                                        @else
                                            <i class="pointer i-2 icon-up" data-order-by="bookings.user_first_name"
                                               data-direction="desc"></i>
                                        @endif
                                    </th>
                                    {{--PRODUCT--}}
                                    <th scope="col">
                                        Prodotto
                                    </th>
                                    {{--EXPERIENCE DATE--}}
                                    <th scope="col">
                                        Data uscita
                                        @if(app('request')->order_by == "bookings.experience_date" && app('request')->direction == 'desc')
                                            <i class="pointer i-2 icon-down" data-order-by="bookings.experience_date"
                                               data-direction="asc"></i>
                                        @else
                                            <i class="pointer i-2 icon-up" data-order-by="bookings.experience_date"
                                               data-direction="desc"></i>
                                        @endif
                                    </th>
                                    {{--CREATED AT--}}
                                    <th scope="col">
                                        Data invio primo preventivo
                                        @if(app('request')->order_by == "bookings.created_at" && app('request')->direction == 'desc')
                                            <i class="pointer i-2 icon-down" data-order-by="bookings.created_at"
                                               data-direction="asc"></i>
                                        @else
                                            <i class="pointer i-2 icon-up" data-order-by="bookings.created_at"
                                               data-direction="desc"></i>
                                        @endif
                                    </th>
                                    {{--UPDATED AT--}}
                                    <th scope="col">
                                        Data ultima modifica
                                        @if(app('request')->order_by == "bookings.updated_at" && app('request')->direction == 'desc')
                                            <i class="pointer i-2 icon-down" data-order-by="bookings.updated_at"
                                               data-direction="asc"></i>
                                        @else
                                            <i class="pointer i-2 icon-up" data-order-by="bookings.updated_at"
                                               data-direction="desc"></i>
                                        @endif
                                    </th>
                                    {{--PRICE--}}
                                    <th scope="col">
                                        Prezzo pieno
                                        @if(app('request')->order_by == "bookings.price" && app('request')->direction == 'desc')
                                            <i class="pointer i-2 icon-down" data-order-by="bookings.price"
                                               data-direction="asc"></i>
                                        @else
                                            <i class="pointer i-2 icon-up" data-order-by="bookings.price"
                                               data-direction="desc"></i>
                                        @endif
                                    </th>
                                    {{--DISCOUNTED PRICE--}}
                                    <th scope="col">
                                        Prezzo pagato (con sconti e buoni)
                                    </th>
                                    {{--REFUND--}}
                                    <th scope="col">
                                        Ammontare rimborso
                                    </th>
                                    {{--STATUS--}}
                                    <th scope="col">
                                        Status
                                        @if(app('request')->order_by == "bookings.status" && app('request')->direction == 'desc')
                                            <i class="pointer i-2 icon-down" data-order-by="bookings.status"
                                               data-direction="asc"></i>
                                        @else
                                            <i class="pointer i-2 icon-up" data-order-by="bookings.status"
                                               data-direction="desc"></i>
                                        @endif
                                    </th>
                                    {{--Actions--}}
                                    <th scope="col">
                                    </th>
                                    {{--Cancel--}}
                                    <th scope="col" class="text-center">
                                        Seleziona uscite da annullare
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bookings as $booking)
                                    <tr>
                                        {{--ID--}}
                                        <td>
                                            <span class="search-match">{{$booking->id}}</span>
                                        </td>
                                        {{--USER--}}
                                        <td>
                                            <span class="search-match">{{$booking->user_first_name . ' ' . $booking->user_last_name}}</span>
                                        </td>
                                        {{--PRODUCT--}}
                                        <td>
                                            <span class="search-match">{{$booking->experience_id ? $booking->experience->business_id : "Personalizzato"}}</span>
                                        </td>
                                        {{--EXPERIENCE DATE--}}
                                        <td>
                                            <span class="search-match">{{$booking->experience_date ? \Carbon\Carbon::parse($booking->experience_date)->format(\App\Helpers\DateHelper::getDateFormat()) : ""}}</span>
                                        </td>
                                        {{--CREATED AT--}}
                                        <td>
                                            <span class="search-match">{{$booking->created_at->format(\App\Helpers\DateHelper::getDatetimeFormat())}}</span>
                                        </td>
                                        {{--UPDATED AT--}}
                                        <td>
                                            <span class="search-match">{{$booking->updated_at->format(\App\Helpers\DateHelper::getDatetimeFormat())}}</span>
                                        </td>
                                        {{--PRICE--}}
                                        <td>
                                            <span class="search-match">{{$booking->price}}</span>
                                        </td>
                                        {{--DISCOUNTED PRICE--}}
                                        <td>
                                            <span class="search-match">@if(isset($booking->payment_datetime)){{$booking->discounted_price}}@else - @endif</span>
                                        </td>
                                        {{--REFUND--}}
                                        <td>
                                            <span class="search-match">{{$booking->refund ?? '-'}}</span>
                                        </td>
                                        {{--STATUS--}}
                                        <td>
                                            <span class="search-match">@component('admin.bookings.status', ['status' => $booking->status])@endcomponent</span>
                                        </td>
                                        {{--ACTION--}}
                                        <td>
                                            <a href="{{route(App\Constants::R_ADMIN_BOOKINGS_EDIT_FORM, ["id" => $booking->id])}}"
                                               class="inline-block btn btn-default">Modifica</a>
                                        </td>
                                        {{--Cancel--}}
                                        <td>
                                            @if($booking->status == \App\Constants::BOOKING_PAID || $booking->status == \App\Constants::BOOKING_PAYMENT_LINK_SENT)
                                                <input data-booking-delete name="booking_delete[]" class="form-control"
                                                       type="checkbox" value="{{$booking->id}}">
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{--DELETE CONFIRMATION--}}
                            @component('backoffice.layout.modal',
                            ["id" => "delete-modal", "confirmButtonID" => "submit-delete", "confirmButtonText" => "Annulla uscite",
                            "cancelButtonText" => "Indietro", "confirmButtonClass" => "btn-danger pull-left",
                            "title" => "Inserisci le ragioni della cancellazione nelle varie lingue"])
                                Inserisci solo nelle lingue che ti servono
                                @foreach($languages as $language)
                                    <div class="margin-bottom-sm">
                                        <h4>{{$language}}</h4>
                                        <textarea class="summernote"
                                                  name="deleted_for[{{$language}}]"></textarea>
                                    </div>
                                @endforeach
                            @endcomponent
                        </form>

                        {{$bookings->appends(request()->query())->links()}}
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            let form = $('#search');
            let orderByInput = $('input[name="order_by"]');
            let directionInput = $('input[name="direction"]');
            let search = "{{app('request')->search}}";

            $('.pointer').on("click", (e) => {
                let orderBy = $(e.target).data('order-by');
                let direction = $(e.target).data('direction');

                orderByInput.val(orderBy);
                directionInput.val(direction);

                form.submit();
            });

            // Highlight matches
            if (search) {
                $(".search-match").each(function () {
                    let regex = new RegExp(search, "i");
                    $(this).html($(this).html().replace(regex, "<strong>$&</strong>"));
                });
            }

            // Cancel bookings
            let deleteModalButton = $("#delete-modal-button");
            let deleteCount = 0;
            $("[data-booking-delete]").change(function () {
                if (this.checked) {
                    deleteCount++;
                } else {
                    deleteCount--;
                }

                if (deleteCount > 0) {
                    deleteModalButton.attr('disabled', null);
                } else {
                    deleteModalButton.attr('disabled', true);
                }
            });
            let summernoteOptions = {
                height: 300,
                minHeight: null,
                maxHeight: null,
                focus: false, // set focus to editable area after initializing summernote
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['link'],
                    ['codeview']
                ],
                codemirror: {
                    theme: 'monokai',
                    htmlMode: true,
                    mode: 'text/html'
                },
                placeholder: '',
                // Manual maxlength
                callbacks: {
                    onKeydown: function (e) {
                        let t = e.currentTarget.innerText;
                        if (t.trim().length >= {{$validationRules['deleted_for']['max']}}) {
                            //delete key
                            if (e.keyCode !== 8)
                                e.preventDefault();
                        }
                    },
                    onPaste: function (e) {
                        let t = e.currentTarget.innerText;
                        let bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        let all = t + bufferText;
                        document.execCommand('insertText', false, all.trim().substring(0, {{$validationRules['deleted_for']['max']}}));
                    }
                }
            };
            let deletedFor = $(".summernote");
            deletedFor.summernote(summernoteOptions);
            let deleteForm = $("#delete-form");
            let submitDelete = $("#submit-delete");
            submitDelete.click(function () {
                deleteForm.submit();
            });
        });
    </script>
@endsection