@extends('admin.layout.base')

@section('head')
    <title>Aree & CP - Nausdream Admin</title>
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="row">
        <div class="col-xs-6">
            <form action="{{route(\App\Constants::R_ADMIN_AREAS_NEW)}}" method="POST">
                {{csrf_field()}}
                <button class="new-area btn btn-success">Aggiungi Area</button>
                <input type="text" name="name" required/>
            </form>
        </div>
        <div class="col-xs-6">
            <form class="pull-right" action="{{route(\App\Constants::R_ADMIN_HC_NEW)}}" method="POST">
                {{csrf_field()}}
                <button class="new-area btn btn-success">Aggiungi CP</button>
                <select name="area_id" required>
                    @foreach($areas as $area)
                        <option value="{{$area->id}}">{{$area->name}}</option>
                    @endforeach
                </select>
                <input type="text" name="business_id" required placeholder="Business ID (es. nap)" required maxlength="{{\App\Constants::BUSINESS_ID_HARBOUR_CENTER_LENGTH}}"/>
                <input type="text" name="name" required placeholder="Nome (es. naples)" required maxlength="{{\App\Constants::HARBOUR_CENTER_NAME_LENGTH}}"/>
            </form>
        </div>
    </div>
    <div class="row margin-bottom-lg">
        <div class="col-xs-12">Salva il lavoro in corso, perché aggiungendo aree e CP, la pagina sarà aggiornata.</div>
    </div>

    <div class="row list margin-bottom-lg">
        <div class="col-xs-12">
            <form class="form-horizontal" id="form" action="{{route(\App\Constants::R_ADMIN_AREAS_EDIT)}}"
                  method="POST">
                {{csrf_field()}}
                <table class="table">
                    <colgroup>
                        <col class="col-md-1">
                        <col class="col-md-1">
                        <col class="col-md-1">
                        <col class="col-md-3">
                        <col class="col-md-1">
                        <col class="col-md-5">
                    </colgroup>
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">
                            NASCOSTO
                        </th>
                        <th scope="col">
                            NOME
                        </th>
                        <th scope="col">
                            ORDINE
                        </th>
                        <th scope="col">
                            URL
                        </th>
                    </tr>
                    </thead>
                    <tbody id="list">
                    @foreach($areas as $area)
                        <tr class="area" data-id="{{$area->id}}">
                            <td>
                                <div class="btn btn-danger" data-delete="area" data-id="{{$area->id}}">×</div>
                            </td>
                            <td colspan="2">
                                <input class="form-control" name="areas[{{$area->id}}][hidden]" type="checkbox"
                                       value="1" @if($area->hidden) checked @endif/>
                                <input name="areas[{{$area->id}}][id]" type="hidden" value="{{$area->id}}"/></td>
                            </td>
                            <td>{{$area->name}}</td>
                            <td><input class="form-control order" name="areas[{{$area->id}}][order]" type="number"
                                       value="{{$area->order}}"/>
                            <td></td>
                        </tr>
                        @foreach($area->harbour_centers as $hc)
                            <tr class="harbour-center" data-id="{{$hc->id}}">
                                <td>
                                    <div class="btn btn-danger" data-delete="harbour-center" data-id="{{$hc->id}}">×
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input class="form-control" name="harbour_centers[{{$hc->id}}][hidden]"
                                           type="checkbox" value="1" @if($hc->hidden) checked @endif/>
                                    <input name="harbour_centers[{{$hc->id}}][id]" type="hidden" value="{{$hc->id}}"/>
                                </td>
                                <td>{{$hc->business_id}}</td>
                                <td><input class="form-control order" name="harbour_centers[{{$hc->id}}][order]"
                                           type="number" value="{{$hc->order}}"/>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="col-xs-2">
                                            <select class="form-control language_hc" data-id="{{$hc->id}}"
                                                    name="language_hc[{{$hc->id}}]">
                                                @foreach($languages as $lang)
                                                    <option value="{{$lang->language}}">{{$lang->language}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-10">
                                            @foreach($languages as $lang)
                                                @php ($seo = $hc->seos()
                                                            ->where('language', $lang->language)
                                                            ->where('duration', null)
                                                            ->where('boat_type_id', null)
                                                            ->first())
                                                <input
                                                        class="seo form-control"
                                                        data-seo="{{$seo->id}}"
                                                        data-hc="{{$hc->id}}"
                                                        data-lang="{{$lang->language}}"
                                                        style="display: none"
                                                        type="text"
                                                        name="seos[{{$seo->id}}][url]"
                                                        value="{{$seo->query}}"
                                                        maxlength="{{\App\Constants::QUERY_LENGTH}}"/>
                                                <input name="seos[{{$seo->id}}][id]" type="hidden" value="{{$hc->id}}"/>
                                                <input name="seos[{{$seo->id}}][lang]" type="hidden" value="{{$lang->language}}"/>
                                            @endforeach
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    <button class="btn btn-primary">Salva</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            let form = $("#form");

            let urls = form.find('.language_hc');

            urls.each((i, el) => {
                let lang = $(el).val();
                let id = $(el).data('id');

                $(".seo[data-hc='" + id + "'][data-lang='" + lang + "']").show();
            });

            urls.change((e) => {
                let id = $(e.target).data('id');
                let lang = $(e.target).val();

                $(".seo[data-hc='" + id + "'][data-lang!='" + lang + "']").hide();
                $(".seo[data-hc='" + id + "'][data-lang='" + lang + "']").show();
            });

            // DELETE AREAS
            let deleteAreas = $("[data-delete='area']");
            deleteAreas.click((e) => {
                let state = $(e.target).data('state');
                let id = $(e.target).data('id');
                let area = $(".area[data-id='" + id + "']");

                if (state === "deleted") {
                    area.css({
                        'background-color': 'lightblue'
                    });

                    $(e.target).data('state', 'not-deleted');
                    $(e.target).removeClass('btn-success');
                    $(e.target).addClass('btn-danger');
                    $(e.target).text("×");

                    // Add area to the ones that should be deleted
                    $("input[name='deleted_areas[]'][value='" + id + "']").remove();
                } else {
                    area.css({
                        'background-color': 'lightpink'
                    });

                    $(e.target).data('state', 'deleted');
                    $(e.target).removeClass('btn-danger');
                    $(e.target).addClass('btn-success');
                    $(e.target).text("+");

                    // Add area to the ones that should be deleted
                    form.append('<input type="hidden" name="deleted_areas[]" value="' + id + '" />');
                }
            });

            // DELETE HCS
            let deleteHcs = $("[data-delete='harbour-center']");
            deleteHcs.click((e) => {
                let state = $(e.target).data('state');
                let id = $(e.target).data('id');
                let hc = $(".harbour-center[data-id='" + id + "']");

                if (state === "deleted") {
                    hc.css({
                        'background-color': '#fbfbfb'
                    });

                    $(e.target).data('state', 'not-deleted');
                    $(e.target).removeClass('btn-success');
                    $(e.target).addClass('btn-danger');
                    $(e.target).text("×");

                    // Add area to the ones that should be deleted
                    $("input[name='deleted_hcs[]'][value='" + id + "']").remove();
                } else {
                    hc.css({
                        'background-color': 'lightpink'
                    });

                    $(e.target).data('state', 'deleted');
                    $(e.target).removeClass('btn-danger');
                    $(e.target).addClass('btn-success');
                    $(e.target).text("+");

                    // Add area to the ones that should be deleted
                    form.append('<input type="hidden" name="deleted_hcs[]" value="' + id + '" />');
                }
            });
        });
    </script>
@endsection