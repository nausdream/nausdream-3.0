<!doctype html>
<html>
<head>

    <title>Error - Nausdream Admin</title>

    <link href="{{ mix('/css/backoffice-app.css', 'backoffice/') }}" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 text-center">
            <p><strong>ERROR</strong>!</p>

            <p>{{$e}}</p>
        </div>
    </div>

    </div>

</body>
</html>