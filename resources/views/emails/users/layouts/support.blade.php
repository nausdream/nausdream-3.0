<table class="support" width="100%">
    <tr>
        <td>
            <strong>{!!  trans('emails.need_assistance')  !!}</strong>
        </td>
    </tr>
    <tr>
        <td><small><strong>{!!  trans('emails.send_email')  !!}</strong> {!! \App\Constants::SUPPORT_EMAIL !!}</small></td>
    </tr>
    <tr>
        <td><small><strong>{!! trans('emails.call_us') !!}</strong> {!! \App\Constants::TELEPHONE_NUMBER !!}</small></td>
    </tr>
</table>