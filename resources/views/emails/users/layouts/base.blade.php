<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>@yield('page-title')</title>
    <style type="text/css">
        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            font-family: "Montserrat-Light", "Helvetica", "Arial", sans-serif;
            color: #5B5C59;
        }

        /* ELEMENTI VARI*/

        a {
            color: #31BEF8;
            text-decoration: none;
        }

        .btn {
            text-decoration: none;
            color: #fff;
            background-color: #31bef8;
            font-weight: normal;
            font-size: 18px;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 100px;
            width: 100%;
            max-width: 300px;
            line-height: 2.5;
        }

        .prenotazione {
            background-color: #fff;
            border-top: solid 1px #D8D8D8;
            border-collapse: separate;
            border-spacing: 0 22px;
        }

        .prenotazione tr td:last-child {
            text-align: right;
        }

        .prenotazione tr td:first-child {
            text-align: left;
        }

        .calltoaction {
            padding: 25px 0;
            border-top: solid 1px #D8D8D8;
        }

        .rettangolo {
            width: 32px;
            height: 4px;
            background: #31BEF8;
            border-radius: 100px;
        }

        /* PREHEADER */

        .header.container table {
            padding-top: 10px;
        }

        /* BODY */

        table.body-wrap {
            width: 100%;
        }

        /* FOOTER */

        table.footer-wrap {
            width: 100%;
            background-color: #101C33;
            clear: both !important;
            padding-top: 0;
        }

        .footer-wrap a {
            color: white;
            text-decoration: none;
        }

        .content-social {
            padding-top: 15px;
            padding-bottom: 15px;
        }

        /* CARATTERI */

        h1, h2, h3, h4, h5, h6 {
            line-height: 1.1;
        }

        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small {
            font-size: 60%;
            line-height: 0;
            text-transform: none;
        }

        h1 {
            font-weight: 200;
            font-size: 44px;
        }

        h2 {
            font-weight: 100;
            font-size: 26px;
            color: #31BEF8;
        }

        h3 {
            font-weight: 100;
            font-size: 16px;
            font-style: italic;
            color: #31BEF8;
        }

        h4 {
            font-weight: 500;
            font-size: 12px;
        }

        h5 {
            font-weight: 600;
            font-size: 18px;
            text-align: left;
        }

        h6 {
            font-weight: 200;
            font-size: 11px;
            color: #31BEF8;
        }

        p {
            margin-bottom: 10px;
            font-size: 16px;
            line-height: 1.6;
        }

        ul, li {
            margin-bottom: 10px;
            margin-left: 10px;
            font-size: 16px;
            line-height: 1.6;
        }

        p.lead {
            font-size: 16px;
        }

        p.headertext {
            font-size: 14px;
            color: #fff;
            text-align: right
        }

        .support {
            border-spacing: 0 10px;
            text-align: center;
            padding-top: 20px;
            border-top: solid 1px #D8D8D8;
        }

        .prefooter {
            padding-top: 10px;
        }

        .prefooter h4 {
            margin: 0;
        }

        /* CONTAINER */

        .container {
            display: block !important;
            max-width: 600px !important;
            margin: 0 auto !important; /* per centrare */
            clear: both !important;
        }

        .content {
            padding: 20px 20px 5px;
            max-width: 600px;
            margin: 0 auto;
            display: block;
        }

        .contentheader {
            background-color: #101c33;
            padding: 25px;
            max-width: 600px;
            margin: 0 auto;
            display: block;
        }

        .content table {
            width: 100%;
        }

        .column {
            width: 100%;
        }

        .column tr td {
            padding: 14px;
        }

        .text-blue {
            color: #31BEF8;
        }

        .text-center {
            text-align: center;
        }

        .margin-bottom-md {
            margin-bottom: 20px;
        }

        .unsubscribe a {
            text-decoration: underline;
            color: #5B5C59;
        }

        .unsubscribe h4 {
            margin-bottom: 0;
            text-align: center;
        }

        hr.light {
            border: none;
            border-bottom: 1px solid #D8D8D8;
        }

        /* Specifiche Font */

        @font-face {
            font-family: 'Montserrat-Light';
            src: url('https://res.cloudinary.com/naustrip/raw/upload/v1489142575/production/assets/fonts/Montserrat-Light.ttf');

        }

        @font-face {
            font-family: 'Montserrat-Regular';
            src: url('{{config('services.cloudinary.secure_url') . 'v1/' . config('services.cloudinary.base_folder')}}assets/fonts/Montserrat-Regular.ttf');

        }

        /* TELEFONO */
        @media only screen and (max-width: 600px) {

            a[class="btn"] {
                display: block !important;
                margin-bottom: 10px !important;
                background-image: none !important;
                margin-right: 0 !important;
            }

            div[class="column"] {
                width: auto !important;
                float: none !important;
            }

            table #prenotazione div[class="column"] {
                width: auto !important;
            }

        }
    </style>

    @yield('custom-css')
</head>

<body bgcolor="#FFFFFF">

<table class="body-wrap" bgcolor="#EEEEEE">
    <tr>
        <td class="container" bgcolor="#FFFFFF">
            <div class="contentheader">
                <table bgcolor="#101c33" style="width:100%">
                    <tr>
                        <td>
                            <img src="{{config('services.cloudinary.secure_url') . "c_scale,h_60/" . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/logo_nausdream"}}"
                                 alt="Logo Nausdream"/></td>
                        <td align="right"><p class="headertext">@yield('header-text')</p>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="content">
                @yield('content')
            </div>

        </td>
    </tr>
</table>

@yield('footer')
</body>
</html>
