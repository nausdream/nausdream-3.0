<table class="footer-wrap"
       style="margin: 0;padding: 0;font-family: 'Montserrat',Helvetica, Arial, sans-serif;width: 100%;background-color: #eee;padding-top: 0px;clear: both!important;">
    <tr style="">
        <td style=""></td>
        <td class="container"
            style="margin: 0 auto!important;padding: 0;display: block!important;max-width: 600px!important;clear: both!important;">

            <!-- Social -->
            <div class="content-social" align="center"
                 style="background-color: #101C33;padding-top: 15px;padding-bottom: 15px;">
                <table style="">
                    <tr style="">
                        <td align="center"
                            style="">
                            <p class="footertext"
                               style="margin: 0;padding: 0;font-family: 'Montserrat-Light',sans-serif;color: #fff;margin-bottom: 10px;font-size: 14px;line-height: 1.6;">
                                {!! trans('emails.follow_us') !!}</p>
                            <p style="margin: 0;padding: 0;font-family: 'Montserrat-Light',sans-serif;color: #5B5C59;margin-bottom: 10px;font-size: 16px;line-height: 1.6;">
                                <a href="{{\App\Constants::FB_PROFILE_LINK}}"
                                   style="color: #31BEF8;text-decoration: none;"><img
                                            src="https://res.cloudinary.com/naustrip/raw/upload/production/assets/img/facebook_icon.png"
                                            alt="facebook"
                                            style="max-width: 100%;"></a>
                                <a href="{{\App\Constants::INSTAGRAM_PROFILE_LINK}}"
                                   style="color: #31BEF8;text-decoration: none;"><img
                                            src="https://res.cloudinary.com/naustrip/raw/upload/production/assets/img/istagram_icon.png"
                                            alt="instagram"
                                            style="max-width: 100%;"></a>
                            </p>
                            <p class="footertext"
                               style="margin: 0;padding: 0;font-family: 'Montserrat-Light',sans-serif;color: #fff;margin-bottom: 10px;font-size: 14px;line-height: 1.6;">
                                {!! trans('emails.footer_text', [
                                    'email' => \App\Constants::SUPPORT_EMAIL, 'phone' => \App\Constants::TELEPHONE_NUMBER,
                                    'company' => \App\Constants::COMPANY_NAME, 'vat' => \App\Constants::VAT_NUMBER
                                ]) !!}
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /social -->

        </td>
        <td style=""></td>
    </tr>
</table>