<table class="unsubscribe" width="100%">
    <tr>
        <td>
            <h4>{!! trans('emails.unsubscribe', ['link' => $unsub_link]) !!}</h4>
        </td>
    </tr>
</table>