@extends('emails.users.layouts.base')

@section('page-title', trans('emails_post_experience.subject', ['name' => $user_first_name]))

@section('custom-css')
    <style type="text/css">
        .btn {
            font-size: 16px;
            margin: 0 0 10px;
            border-radius: 3px;
            font-weight: bold;
        }

        .ta-btn {
            background-color: #ff6500;
        }

        .fb-btn {
            background-color: #3b5998;
        }

        .review {
            margin: 20px 0 10px;
        }

        .separator {
            border: none;
            border-top: 1px solid #D8D8D8;
            max-width: 300px;
        }

        .share-social {
            max-width: 375px;
            display: block;
            margin: 30px auto;
        }

    </style>
@endsection

@section('header-text')
    {!! $email_heading !!}
@endsection

@section('content')
    <h2 class="text-blue">{!! trans('emails_post_experience.hello', ['name' => $user_first_name]) !!}</h2>
    <p class="lead">{!! trans('emails_post_experience.remember', [
                        'title' => $experience_link ? '<a href="'.$experience_link.'" target="_blank">'.$experience_title.'</a>' : $experience_title,
                        'date' => $experience_date,
                        'time' => $departure_time,
                        'meeting_point' => $meeting_point
                    ]) !!}</p>

    <p class="review text-center"><span style="font-size: 20px">{!! trans('emails_post_experience.leave_review_with') !!}</span></p>

    <div class="margin-bottom-md text-center">
        <a class="btn ta-btn" href="{{\App\Constants::TRIPADVISOR_REVIEW_LINK}}">{!! trans('emails_post_experience.tripadvisor') !!}</a>
        <a class="btn fb-btn" href="{{\App\Constants::FB_REVIEW_LINK}}">{!! trans('emails_post_experience.facebook') !!}</a>
    </div>

    <hr class="separator">

    <p class="review text-center"><span style="font-size: 20px">{!! trans('emails_post_experience.share_on_socials') !!}</span></p>

    <p class="text-center">{!! trans('emails_post_experience.socials') !!}</p>

    <img class="share-social" src="{{$social_image}}" />

    <p>{!! trans('emails_post_experience.see_you_soon') !!}</p>
    <h3 class="text-blue"><strong>{!! trans('emails.nausdream_team') !!}</strong></h3>

    @include('emails.users.layouts.prefooter')
@endsection

@section('footer')
    @include('emails.users.layouts.footer')
@endsection