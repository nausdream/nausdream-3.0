@extends('emails.users.layouts.base')

@section('page-title', $subject)

@section('custom-css')
    <style>

    </style>
@endsection

@section('header-text')
    {!! $email_heading !!}
@endsection

@section('content')
    <h2 class="text-blue">{!! trans('emails_quote.hello', ['name' => $user_first_name]) !!}</h2>
    <p class="lead">{!! trans('emails_quote.quote', [
                    'title' => $experience_link ? '<a href="'.$experience_link.'" target="_blank">'.$experience_title.'</a>' : $experience_title,
                    'link' => $experience_link,
                    'date' => $created_at
                ]) !!}</p>
    @if(isset($quote_expiration_datetime))
        <p class="lead">{!! trans('emails_quote.expires', ['date' => $quote_expiration_datetime]) !!}</p>
    @endif


    @if(isset($experience_description))
        <p>{!! trans('emails_quote.description') !!}</p>

        <p>
            {!! $experience_description !!}
        </p>
    @endif
    <table style="table-layout:fixed;" class="prenotazione">
        <tr>
            <td>
                <h5 style="margin: 0 0 20px 0">{!! trans('emails_quote.details') !!}</h5>
                <div class="rettangolo"></div>
            </td>
        </tr>

        {{--EXPERIENCE DATE AND MEETING POINT--}}
        @if(isset($experience_date))
            <tr>
                <td colspan="2">
                    {!! trans('emails_quote.experience_date', ['date' => $experience_date]) !!}
                </td>
            </tr>
        @endif
        @if(isset($meeting_point))
            <tr>
                <td colspan="2">
                    {!! trans('emails_quote.meeting_point', ['meeting_point' => $meeting_point]) !!}
                </td>
            </tr>
        @endif

        {{--DEPARTURE AND RETURN--}}
        @if(isset($departure_time) || isset($return_time))
            <tr>
                @if(isset($departure_time))
                    <td @if(!isset($return_time))colspan="2"@endif>
                        {!! trans('emails_quote.departure_time', ['time' => $departure_time]) !!}
                    </td>
                @endif
                @if(isset($return_time))
                    <td @if(!isset($departure_time))colspan="2"@endif>
                        {!! trans('emails_quote.return_time', ['time' => $return_time]) !!}
                    </td>
                @endif
            </tr>
        @endif

        {{--ADULTS AND CHILDREN--}}
        @if((isset($adults) && $adults > 0) || (isset($children) && $children > 0))
            <tr>
                @if((isset($adults) && $adults > 0))
                    <td>
                        {!! trans('emails_quote.adults', ['count' => $adults]) !!}
                    </td>
                @endif
                @if((isset($children) && $children > 0))
                    <td>
                        {!! trans('emails_quote.children', ['count' => $children]) !!}
                    </td>
                @endif
            </tr>
        @endif

        {{--INCLUDED SERVICES--}}
        @if(isset($included_services) && count($included_services))
            <tr>
                <td colspan="2">
                    {!! trans('emails_quote.included_services') !!}
                    @foreach($included_services as $service)
                        {{trans('included_services.'.$service)}}@if(!$loop->last), @endif
                    @endforeach
                </td>
            </tr>
        @endif
    </table>

    @if($raw_price > 0)
        <table class="prenotazione">
            <tr>
                <td>
                    <span><b>{!! trans('emails_quote.total') !!}</b></span>
                </td>
                <td>
                    <span>{{$price}}</span>
                </td>
            </tr>
        </table>

        <table class="calltoaction" width="100%">
            <tr>
                <td align="center">
                    <a href="{!! $payment_link !!}"><b><span
                                    class="btn">{!! trans('emails_quote.pay') !!}</span></b></a>
                </td>
            </tr>
        </table>
    @endif

    <p>{!! trans('emails.good_sailing') !!}</p>
    <h3 class="text-blue"><strong>{!! trans('emails.nausdream_team') !!}</strong></h3>

    @include('emails.users.layouts.support')
    @include('emails.users.layouts.prefooter')
@endsection

@section('footer')
    @include('emails.users.layouts.footer')
@endsection