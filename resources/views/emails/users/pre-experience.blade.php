@extends('emails.users.layouts.base')

@section('page-title', trans('emails_pre_experience.subject', ['name' => $user_first_name]))

@section('custom-css')
    <style>

    </style>
@endsection

@section('header-text')
    {!! $email_heading !!}
@endsection

@section('content')
    <h2 class="text-blue">{!! trans('emails_pre_experience.hello', ['name' => $user_first_name]) !!}</h2>
    <p class="lead">{!! trans('emails_pre_experience.remember', [
                        'title' => $experience_link ? '<a href="'.$experience_link.'" target="_blank">'.$experience_title.'</a>' : $experience_title,
                        'date' => $experience_date,
                        'time' => $departure_time,
                        'meeting_point' => $meeting_point,
                        'minutes' => $minutes
                    ]) !!}</p>

    <p>{!! trans('emails.good_sailing') !!}</p>
    <h3 class="text-blue"><strong>{!! trans('emails.nausdream_team') !!}</strong></h3>

    @include('emails.users.layouts.support')
    @include('emails.users.layouts.prefooter')
@endsection

@section('footer')
    @include('emails.users.layouts.footer')
@endsection