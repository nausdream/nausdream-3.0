@extends('emails.users.layouts.base')

@section('page-title', trans('emails_voucher.page_title', ['title' => $experience_title]))

@section('custom-css')
    <style>
        .campaign-banner {
            margin: 20px 0;
        }

        .campaign-banner.refer-a-friend {
            padding: 5px 15px;
            background-color: #fffcf5;
            border: 2px dashed orange;
        }
    </style>
@endsection

@section('header-text')
    {!! $email_heading !!}
@endsection

@section('content')
    <h2 class="text-blue">{!! trans('emails_voucher.hello', ['name' => $user_first_name]) !!}</h2>
    <p class="lead">{!! trans('emails_voucher.thanks', [
                    'title' => $experience_link ? '<a href="'.$experience_link.'" target="_blank">'.$experience_title.'</a>' : $experience_title,
                    'link' => $experience_link,
                    'date' => $experience_date,
                    'meeting_point' => $meeting_point,
                    'position' => $position
                ]) !!}</p>

    <p class="lead">{!! trans('emails_voucher.find_attached') !!}</p>

    {{--INJECT CAMPAIGN CONTENT INTO THE EMAIL--}}
    @foreach($campaigns as $campaign)
        <div class="campaign-banner {{$campaign->name}}">
            {!! $campaign->emailBanner($booking) !!}
        </div>
    @endforeach

    <p>{!! trans('emails.good_sailing') !!}</p>
    <h3 class="text-blue"><strong>{!! trans('emails.nausdream_team') !!}</strong></h3>

    @include('emails.users.layouts.support')
    @include('emails.users.layouts.prefooter')
@endsection

@section('footer')
    @include('emails.users.layouts.footer')
@endsection