@extends('emails.users.layouts.base')

@section('page-title', $subject)

@section('custom-css')
    <style>

    </style>
@endsection

@section('header-text')
    {!! $email_heading !!}
@endsection

@section('content')
    <h2 class="text-blue">{!! trans('emails_cancel.hello', ['name' => $user_first_name]) !!}</h2>
    <p class="lead">{!! trans('emails_cancel.canceled', [
                    'title' => $experience_link ? '<a href="'.$experience_link.'" target="_blank">'.$experience_title.'</a>' : $experience_title,
                    'link' => $experience_link,
                    'date' => $experience_date,
                    'for' => $deleted_for
                ]) !!}</p>

    <p class="lead">{!! trans('emails_cancel.refund', ['price' => $price]) !!}</p>
    <h3 class="text-blue"><strong>{!! trans('emails.nausdream_team') !!}</strong></h3>

    @include('emails.users.layouts.support')
    @include('emails.users.layouts.prefooter')
@endsection

@section('footer')
    @include('emails.users.layouts.footer')
@endsection