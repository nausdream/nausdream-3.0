<p class="text-center">{!! trans('refer_a_friend.participate') !!}</p>
<p class="text-center">{!! trans('refer_a_friend.follow') !!}</p>
<p class="text-center"><a class="btn" href="{{$opt_in_link}}">{!! trans('refer_a_friend.accept') !!}</a></p>
<p class="text-center"><small>{!! trans('refer_a_friend.disclaimer', ['link' => route('users.privacy-policy-'.App::getLocale())]) !!}</small></p>