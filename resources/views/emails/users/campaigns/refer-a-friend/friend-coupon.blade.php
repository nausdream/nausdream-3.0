@extends('emails.users.layouts.base')

@section('page-title', $subject)

@section('custom-css')
    <style>
        .coupon {
            padding: 15px;
            border: 2px dashed #fdc731;
            background-color: #fffcf5;
            font-size: 1.8rem;
            text-align: center;
        }
        .coupon-box {
            border-spacing: 5px 10px;
        }
    </style>
@endsection

@section('header-text')
    {!! $email_heading !!}
@endsection

@section('content')
    <h2 class="text-blue">{!! trans('emails_friend_coupon.hello', ['name' => $booking->user_first_name]) !!}</h2>

    <p>{!! trans('emails_friend_coupon.thanks') !!}</p>

    <table class="coupon-box">
        <tr>
            <td width="25%"><b>{!! trans('emails_friend_coupon.coupon') !!}</b></td>
            <td class="coupon">{{$coupon}}</td>
        </tr>
    </table>

    <p>{!! trans('emails_friend_coupon.share_with_friends') !!}</p>

    <p>{!! trans('emails_friend_coupon.read_rules', ['link' => $rules]) !!}</p>

    <p>{!! trans('emails_friend_coupon.enjoy_your_sharing') !!}</p>
    <h3 class="text-blue"><strong>{!! trans('emails.nausdream_team') !!}</strong></h3>

    <hr class="light">

    @include('emails.users.layouts.unsubscribe')
    @include('emails.users.layouts.prefooter')
@endsection

@section('footer')
    @include('emails.users.layouts.footer')
@endsection