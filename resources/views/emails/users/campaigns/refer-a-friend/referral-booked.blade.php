@extends('emails.users.layouts.base')

@section('page-title', $subject)

@section('custom-css')
    <style>
        .coupon {
            padding: 15px;
            border: 2px dashed #fdc731;
            background-color: #fffcf5;
            font-size: 1.8rem;
            text-align: center;
        }
        .coupon-box {
            border-spacing: 5px 10px;
        }
    </style>
@endsection

@section('header-text')
    {!! $email_heading !!}
@endsection

@section('content')
    <h2 class="text-blue">{!! trans('emails_referral_booked.hello', ['name' => $name]) !!}</h2>

    <p>{!! trans('emails_referral_booked.friend_booked', ['name' => $friend]) !!}</p>

    <p>{!! trans('emails_referral_booked.you_have_coupon') !!}</p>

    <table class="coupon-box">
        <tr>
            <td width="25%"><b>{!! trans('emails_referral_booked.coupon') !!}</b></td>
            <td class="coupon">{{$coupon}}</td>
        </tr>
    </table>

    <p>{!! trans('emails_referral_booked.waiting_for_you') !!}</p>
    <h3 class="text-blue"><strong>{!! trans('emails.nausdream_team') !!}</strong></h3>

    <hr class="light">

    <p class="text-center"><small>{!! trans('emails_referral_booked.rules', [
       'link' => route($rules, ['lang' => App::getLocale(), 'c' => 'refer-a-friend']),
       'unsub_link' => $unsub_link
    ]) !!}</small></p>
    @include('emails.users.layouts.prefooter')
@endsection

@section('footer')
    @include('emails.users.layouts.footer')
@endsection