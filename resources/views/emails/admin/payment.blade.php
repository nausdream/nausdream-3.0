<p>
    Ciao Amministratore Nausdream,<br><br>
    Pagata prenotazione n. {{$id}}<br>
    <strong>Prodotto:</strong> {{$experience_business_id}}<br>
    <strong>Data uscita:</strong> {{$date}}<br>
    <strong>Adulti:</strong> {{$adults}}<br>
    <strong>Bambini:</strong> {{$children}}<br>
    <strong>Prezzo:</strong> @if($price != $discounted_price)<del>{{$price}}</del> {{$discounted_price}}@else {{$price}} @endif<br>
    @if(isset($coupon))<strong>Coupon:</strong> {{$coupon}}<br>@endif
    <strong>E-mail:</strong> {{$email}}<br>
    <strong>Telefono:</strong> {{$phone}}<br>
    <strong>Nome:</strong> {{$first_name}}<br>
    <strong>Cognome:</strong> {{$last_name}}<br>
    <strong>Data di nascita:</strong> {{$birth_date}}<br>
    <strong>Luogo di nascita:</strong> {{$place_of_birth}}<br>
    <strong>[Residenza] Città:</strong> {{$city}}<br>
    <strong>[Residenza] Indirizzo:</strong> {{$address}}<br>
    <strong>[Residenza] CAP/ZIP:</strong> {{$zip}}<br>
</p>