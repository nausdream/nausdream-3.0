<p>
    Ciao Amministratore Nausdream,<br><br>
    C'è una nuova richiesta di prenotazione:<br>
    <strong>Prodotto:</strong> {{$experience_business_id}}<br>
    <strong>Nome:</strong> {{$name}}<br>
    <strong>E-mail:</strong> {{$email}}<br>
    <strong>Telefono:</strong> {{$phone}}<br>
    <strong>Ospiti:</strong> {{$guests}}<br>
    <strong>Data:</strong> {{$date}}<br>
    <strong>Lingua dell'utente:</strong> {{$language}}<br>
    <strong>Note:</strong> {{$notes ?? '[nessuna nota]'}}<br>
    <strong>Coupon:</strong> {{$coupon ?? '[no coupon]'}}<br>
    <strong>Utilizzo email a fini di marketing:</strong> {{$marketing == "1" ? "Sì" : "No"}}<br>
</p>