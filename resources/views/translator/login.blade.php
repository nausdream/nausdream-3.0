<!doctype html>
<html>
<head>

    <title>Login - Nausdream Translator</title>

    <!-- HTTPS required. HTTP will give a 403 forbidden response -->
    <script>
        // initialize Account Kit with CSRF protection
        AccountKit_OnInteractive = function () {
            AccountKit.init(
                {
                    appId: "{{config('services.facebook.app_id')}}",
                    state: "12345",
                    version: "v1.1"
                }
            );
        };
    </script>
    <script src="https://sdk.accountkit.com/en_US/sdk.js"></script>

    <link href="{{ mix('css/app.css', 'backoffice/') }}" rel="stylesheet">
</head>
<body>

<div id="page-login" class="container-fluid">
    <div class="row">
        @if(isset($e))
            <div class="col-sm-12 text-center">
                <p>
                    {{$e}}
                </p>
            </div>
        @endif
        <div class="col-sm-12 text-center">
            <p>Ciao, <strong>traduttore</strong>!</p>

            <p class="logo-container">
                <img class="logo" src="{{config('services.cloudinary.secure_url') . "c_scale,h_100/" . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/logo_nausdream.png"}}">
            </p>

            <button class="btn" onclick="phone_btn_onclick()">
                LOGIN
            </button>
        </div>
    </div>

    <form id="login-form" action="{{route(App\Constants::R_TRANSLATOR_ACCOUNTKIT)}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="code"/>
        <input type="hidden" name="to" value="{{$to ?? ''}}">
    </form>
</div>

<script>
    var form = document.getElementById("login-form");

    // login callback
    function loginCallback(response) {
        console.log('\nResponse received from Fb.');
        if (response.status === "PARTIALLY_AUTHENTICATED") {
            form.elements['code'].value = response.code;

            // Send POST request to login endpoint
            form.submit();
        }
        else if (response.status === "NOT_AUTHENTICATED") {
            // handle authentication failure
        }
        else if (response.status === "BAD_PARAMS") {
            // handle bad parameters
        }
    }

    // phone form submission handler
    function phone_btn_onclick() {
        var country_code_default = '+39';
        AccountKit.login('PHONE',
            {countryCode: country_code_default}, // will use default values if this is not specified
            loginCallback);
    }

</script>

</body>
</html>
