@extends('translator.layout.base')

@section('head')
    <title>Modifica {{$page->name}} - Nausdream Translator</title>
@endsection

@section('content')
    <div class="loader"></div>
    <div class="row">
        <div class="col-xs-12 title margin-bottom-md">
            Traduzioni pagina <span class="pre">{{$page->name}}</span></div>
        <div class="col-xs-12 form">
            <form action="{{route(App\Constants::R_TRANSLATOR_PAGES_EDIT_FORM, ['id' => $page->id])}}" method="GET"
                  id="language_form">
                <div class="row margin-bottom-md">
                    <div class="col-xs-2">Elementi</div>
                    <div class="col-xs-4">
                        @component('backoffice.layout.input-row',
                                [
                                    "label" => "Lingua di origine: ",
                                    "name" => "origin",
                                    "type" => "select",
                                    "value" => $languages,
                                    "selected" => $languages[$origin],
                                    "labelSize" => 6
                                ])
                        @endcomponent
                    </div>
                    <div class="col-xs-4">
                        @component('backoffice.layout.input-row',
                                [
                                    "label" => "Lingua di destinazione: ",
                                    "name" => "destination",
                                    "type" => "select",
                                    "value" => $languages,
                                    "selected" => $languages[$destination],
                                    "labelSize" => 6
                                ])
                        @endcomponent
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-12 form">
            <form action="{{route("translator.pages.edit.post", ['id' => $page->id])}}" method="POST"
                  enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-xs-12">
                        @foreach($sentences as $sentence)
                            <div class="row sentence" data-id="{{$sentence->id}}">
                                <div class="col-xs-2"><span class="pre">{{$sentence->name}}</span></div>
                                <div class="col-xs-4">
                <textarea
                        disabled="true"
                        class="full-width">{{$sentence->sentence_translations()->where("language_id", $origin)->get()->first()->text ?? ""}}</textarea>
                                </div>
                                <div class="col-xs-4">
                <textarea
                        class="full-width"
                        maxlength="{{$sentenceTranslationMaxLength}}"
                        name="sentences[{{$sentence->id}}]">{{$sentence->sentence_translations()->where("language_id", $destination)->get()->first()->text ?? ""}}</textarea>
                                </div>
                                @if($admin)
                                    <div class="col-xs-2">
                                        <div class="btn btn-danger delete-sentence" data-id="{{$sentence->id}}">
                                            Elimina
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" id="new-sentences">

                    </div>
                </div>

                <input type="hidden" name="language" value="{{$destination}}">

                <div class="row" id="save-btn" style="display: {{count($sentences) == 0 ? 'none' : 'block'}};">
                    <div class="col-xs-6"></div>
                    <div class="col-xs-4 text-center">
                        <button class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>

            @if (isset($errors) && count($errors) > 0)
                <div class="row alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>
        @if($admin)
            <div class="col-xs-10 form margin-top-lg margin-bottom-xl">
                <hr>
                <form id="new-sentence-form"
                      method="POST">
                    {{csrf_field()}}

                    <span>Nuovo elemento: </span>
                    <input type="hidden" name="page_id" value="{{$page->id}}">
                    <input type="text" name="new_sentence" title="sentence" placeholder="es. page_title"
                           required="true" minlength="{{$newSentenceMinLength}}" maxlength="{{$newSentenceMaxLength}}">
                    <button class="btn btn-primary">Aggiungi</button>
                </form>
                <div class="row new-sentence-error" style="display: none;">

                </div>
            </div>
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            let saveBtn = $('#save-btn');
            let newSentencesContainer = $("#new-sentences");
            let loader = $('.loader');
            loader.hide();
            let languageForm = $('#language_form');

            $('select[name=origin]').change(() => {
                languageForm.submit();
            });
            $('select[name=destination]').change(() => {
                languageForm.submit();
            });

            let deleteSentenceButtonClickHandler = (e) => {
                loader.show();
                let sentenceId = $(e.target).data("id");
                let csrf = "{{csrf_token()}}";
                let url = "{{$deleteSentencePath}}";

                $.ajax({
                    type: "POST",
                    url: url,
                    data: {"id": sentenceId, "_token": csrf}
                })
                    .done((res) => {
                        loader.hide();

                        // Remove sentence from dom
                        $(`.sentence[data-id=${sentenceId}]`).remove();

                        // If it was the last sentence, hide the "Save" button
                        if (!$('.sentence').length) {
                            saveBtn.hide();
                        }
                    })
                    .fail((res) => {
                        loader.hide();
                        console.log(res);
                    });
            };

            // Delete sentence with ajax
            $(".delete-sentence").on("click", deleteSentenceButtonClickHandler);

            // Add sentence with ajax
            $("#new-sentence-form").submit(function (e) {
                loader.show();

                let url = "{{$newSentencePath}}";
                let error = $('.new-sentence-error');
                let maxLength = "{{$sentenceTranslationMaxLength}}";

                let newSentence = $('input[name=new_sentence]').val();

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $(e.target).serialize()
                })
                    .done((res) => {
                        loader.hide();
                        error.hide();
                        // Add newly added sentence to available sentences
                        newSentencesContainer.append(`
                        <div class="row sentence" data-id="${res.id}">
                                <div class="col-xs-2"><span class="pre">${newSentence}</span></div>
                                <div class="col-xs-4">
                <textarea
                        disabled="true"
                        class="full-width"></textarea>
                                </div>
                                <div class="col-xs-4">
                <textarea
                        class="full-width"
                        maxlength="${maxLength}"
                        name="sentences[${res.id}]"></textarea>
                                </div>
                            `);

                        let newDeleteButton = $(`
                            <div class="col-xs-2">
                                <div class="btn btn-danger delete-sentence" data-id="${res.id}">Elimina</div>
                            </div>`)
                            .on("click", deleteSentenceButtonClickHandler)
                            .appendTo($(`.sentence[data-id=${res.id}]`));

                        // If this is the first sentence, make the save-btn visible
                        saveBtn.show();

                        // Clear new sentence input
                        $('input[name=new_sentence]').val(null)
                    })
                    .fail((res) => {
                        loader.hide();
                        error.show();
                        error.html(`
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div class="alert alert-danger">
                                    ${JSON.stringify(res.responseJSON.errors)}
                                </div>
                            </div>
                        `);
                    });

                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
        });
    </script>
@endsection