@extends('translator.layout.base')

@section('head')
    <title>Lista pagine - Nausdream Admin</title>
@endsection

@section('content')
    @if (app('request')->deleted)
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="alert alert-success">
                    <p>Pagina <span class="pre">{{app('request')->deleted}}</span> cancellata.</p>
                </div>
            </div>
        </div>
    @endif

    @if(isset($errors) && count($errors))
        {{$errors}}
    @endif

    <div class="row list">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">LISTA PAGINE</div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{route(App\Constants::R_TRANSLATOR_PAGES_LIST)}}" method="GET"
                          enctype="multipart/form-data" id="search">
                        <input class="search input" name="search" value="{{app('request')->search}}" maxlength="30"/>
                        <input type="hidden" name="order_by" value="{{app('request')->order_by}}"/>
                        <input type="hidden" name="direction" value="{{app('request')->direction}}"/>
                        <button class="search btn btn-primary">CERCA</button>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @if(!$pages->count())
                        Nessun risultato.
                    @else
                        <table class="table">
                            <colgroup>
                                <col class="col-md-3">
                                <col class="col-md-3">
                                <col class="col-md-3">
                                <col class="col-md-3">
                            </colgroup>
                            <thead>
                            <tr>
                                {{--ID--}}
                                <th scope="col">Nome
                                    @if(app('request')->order_by == "name" && app('request')->direction == 'desc')
                                        <i class="pointer i-2 icon-down" data-order-by="name"
                                           data-direction="asc"></i>
                                    @else
                                        <i class="pointer i-2 icon-up" data-order-by="name"
                                           data-direction="desc"></i>
                                    @endif</th>
                                {{--CREATED ON--}}
                                <th scope="col">Creata il
                                    @if(app('request')->order_by == "created_at" && app('request')->direction == 'desc')
                                        <i class="pointer i-2 icon-down" data-order-by="created_at"
                                           data-direction="asc"></i>
                                    @else
                                        <i class="pointer i-2 icon-up" data-order-by="created_at"
                                           data-direction="desc"></i>
                                    @endif</th>
                                {{--UPDATED ON--}}
                                <th scope="col">Aggiornata il
                                    @if(app('request')->order_by == "updated_at" && app('request')->direction == 'desc')
                                        <i class="pointer i-2 icon-down" data-order-by="updated_at"
                                           data-direction="asc"></i>
                                    @else
                                        <i class="pointer i-2 icon-up" data-order-by="updated_at"
                                           data-direction="desc"></i>
                                    @endif</th>
                                {{--ACTIONS--}}
                                <th scope="col">Azioni</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pages as $page)
                                <tr>
                                    {{--TITLE--}}
                                    <td>
                                        <a class="search-match"
                                           href="{{route(App\Constants::R_TRANSLATOR_PAGES_EDIT_FORM, ["id" => $page->id])}}">{{$page->name}}</a>
                                    </td>
                                    {{--CREATED ON--}}
                                    <td>
                                        {{$page->created_at}}
                                    </td>
                                    {{--UPDATED ON--}}
                                    <td>
                                        {{$page->updated_at}}
                                    </td>
                                    {{--ACTIONS--}}
                                    <td>
                                        <a href="{{route(App\Constants::R_TRANSLATOR_PAGES_EDIT_FORM, ["id" => $page->id])}}"
                                           class="inline-block btn btn-default">Modifica</a>
                                        @if($admin ?? false)
                                            <a class="delete-button inline-block btn btn-danger"
                                               data-page-id="{{$page->id}}"
                                               data-toggle="modal" data-target="#delete-modal">Elimina</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$pages->links()}}
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{--DELETE CONFIRMATION--}}
    @component('backoffice.layout.modal',
    ["id" => "delete-modal", "confirmButtonID" => "submit-delete", "confirmButtonText" => "Elimina",
    "cancelButtonText" => "Annulla", "confirmButtonClass" => "btn-danger pull-left"])
        Per eliminare la pagina inserisci qui sotto il risultato dell'operazione
        <strong>{{$deleteConfirmationValue/2}} + {{$deleteConfirmationValue/2}}</strong>:<br>
        <form action="{{route(App\Constants::R_TRANSLATOR_PAGES_DELETE)}}" id="delete" method="POST">
            {{csrf_field()}}
            <input type="text" name="delete_confirmation"/>
            <input type="hidden" name="delete_id"/>
        </form>
    @endcomponent

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            let form = $('#search');
            let orderByInput = $('input[name="order_by"]');
            let directionInput = $('input[name="direction"]');
            let search = "{{app('request')->search}}";

            $('.pointer').on("click", (e) => {
                let orderBy = $(e.target).data('order-by');
                let direction = $(e.target).data('direction');

                orderByInput.val(orderBy);
                directionInput.val(direction);

                form.submit();
            });

            $("#language").change(() => {
                form.submit();
            });

            // Delete experience
            $(".delete-button").on("click", (e) => {
                // Set delete_id value to be the same of delete-button data-experience-id value
                $("input[name='delete_id']").val($(e.target).data('page-id'));
            });
            $('#submit-delete').on("click", () => {
                if ($("input[name=delete_confirmation]").val() === "{{$deleteConfirmationValue}}") {
                    $("#delete").submit();
                }
            });

            // Highlight matches
            if (search) {
                $(".search-match").each(function () {
                    let regex = new RegExp(search, "i");
                    $(this).html($(this).html().replace(regex, "<strong>$&</strong>"));
                });
            }


        });
    </script>
@endsection