<!doctype html>
<html>
<head>

    <title>Error - Nausdream Translator</title>

    <link href="{{ mix('css/app.css', '/backoffice') }}" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 text-center">
            <p><strong>ERROR</strong>!</p>

            <p>{{$e}}</p>
        </div>
    </div>

</div>

</body>
</html>