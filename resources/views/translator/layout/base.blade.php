<!doctype html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('css/app.css', '/backoffice') }}" rel="stylesheet">
    <script src="{{ mix('/js/manifest.js', 'backoffice/') }}"></script>
    <script src="{{ mix('/js/vendor.js', 'backoffice/') }}"></script>
    <script src="{{ mix('/js/app.js', 'backoffice/') }}"></script>
    <link rel="stylesheet" href="{{ mix('css/app.css', '/backoffice') }}"/>
    @yield('head')

</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-2">
            <div class="row sidebar">
                <div class="col-xs-12 text-center">
                    <a href="{{route(App\Constants::R_TRANSLATOR_HOME)}}">
                        <img class="logo" src="{{config('services.cloudinary.secure_url') . "c_scale,h_100/" . 'v1/' . config('services.cloudinary.base_folder') . "assets/img/logo_nausdream.png"}}">
                    </a>
                </div>
                <div class="col-xs-12 sidebar-item">
                    <a class="link" href="{{route(App\Constants::R_TRANSLATOR_PAGES_LIST)}}">LISTA PAGINE</a></div>
                @if($admin ?? false)
                    <div class="col-xs-12 sidebar-item">
                        <a class="link" data-toggle="modal" data-target="#new-page-modal" href="#">NUOVA PAGINA</a>
                    </div>
                @endif
                <div class="col-xs-12 sidebar-item">
                    <a class="link" href="{{route(App\Constants::R_TRANSLATOR_LOGOUT)}}">LOGOUT</a></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 content">
            @yield('content')
        </div>
    </div>
</div>

@if($admin ?? false)
    {{--NEW PAGE--}}
    @component('backoffice.layout.modal',
    ["id" => "new-page-modal", "confirmButtonID" => "submit-new", "confirmButtonText" => "Crea",
    "cancelButtonText" => "Annulla", "confirmButtonClass" => "btn-primary pull-left", "title" => "Crea nuova pagina"])
        <form action="{{route(App\Constants::R_TRANSLATOR_PAGES_NEW_POST)}}" id="new-page" method="POST">
            {{csrf_field()}}
            <label for="page-name">Nome pagina: </label>
            @component('backoffice.layout.input-tooltip', ["placement" => "right"])
                Il nome verrà salvato tutto minuscolo e con gli spazi sostituiti da underscore ( _ ). Non sono ammessi
                caratteri speciali.
            @endcomponent
            <input id=page-name type="text" name="name" minlength="2" maxlength="{{$pageNameLength ?? 65536}}" required="true"/>
        </form>
    @endcomponent
@endif

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        let newPageForm = $("#new-page");
        $('#submit-new').on("click", () => {
            $('<input type="submit">').hide().appendTo(newPageForm).click().remove();
        });

        let search = "{{app('request')->search}}";
        // Highlight matches
        if (search) {
            $(".search-match").each(function () {
                let regex = new RegExp(search, "i");
                $(this).html($(this).html().replace(regex, "<strong>$&</strong>"));
            });
        }
    });
</script>

@yield('scripts')

</body>
</html>
