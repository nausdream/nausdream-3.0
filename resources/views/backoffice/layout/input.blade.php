@switch($type ?? "text")
    @case("select")
    <select id="{{$id ?? ""}}" class="{{$class ?? ""}} input-select" name="{{$name ?? "select"}}"
            @if($optional ?? false)
            required
            @endif
    @if($additionalAttributes ?? false)
        {{$additionalAttributes}}
            @endif
    >
        @if ($optional ?? false)
            <option disabled selected value> -- seleziona una voce --</option>
        @endif
        @foreach($value as $val)
            @if(isset($translationPage))
                <option value="{{$val}}" @if(isset($selected) && $selected == $val) selected @endif>@lang("$translationPage.$val")</option>
            @else
                <option value="{{$val}}" @if(isset($selected) && $selected == $val) selected @endif>{{$val}}</option>
            @endif
        @endforeach
    </select>
    @break

    @case("number")
    <input id="{{$id ?? ""}}" class="{{$class ?? ""}} input-number" name="{{$name ?? "input"}}" type="number"
           value="{{$value ?? old($name ?? "input")}}"
           placeholder="{{$placeholder ?? $label ?? null}}"
           @if($additionalAttributes ?? false)
           {{$additionalAttributes}}
           @endif
           @if(!isset($optional) || (isset($optional) && !$optional))
           required
           @endif
           @if(isset($min))
           min="{{$min}}"
           @endif
           @if(isset($max))
           max="{{$max}}"
            @endif
    />
    @break

    @case("text")
    <input id="{{$id ?? ""}}" class="{{$class ?? ""}} input-text" name="{{$name ?? "input"}}" type="text"
           value="{{$value ?? old($name ?? "input")}}"
           placeholder="{{$placeholder ?? $label ?? null}}"
           @if($additionalAttributes ?? false)
           {{$additionalAttributes}}
           @endif
           @if(!isset($optional) || (isset($optional) && !$optional))
           required
           @endif
           @if(isset($min))
           minlength="{{$min}}"
           @endif
           @if(isset($max))
           maxlength="{{$max}}"
            @endif
    />
    @break

    @case("email")
    <input id="{{$id ?? ""}}" class="{{$class ?? ""}} input-text" name="{{$name ?? "input"}}" type="email"
           value="{{$value ?? old($name ?? "input")}}"
           placeholder="{{$placeholder ?? $label ?? null}}"
           @if($additionalAttributes ?? false)
           {{$additionalAttributes}}
           @endif
           @if(!isset($optional) || (isset($optional) && !$optional))
           required
           @endif
           @if(isset($min))
           minlength="{{$min}}"
           @endif
           @if(isset($max))
           maxlength="{{$max}}"
            @endif
    />
    @break

    @case("textarea")
    <textarea id="{{$id ?? ""}}" class="{{$class ?? ""}} input-textarea" name="{{$name ?? "input"}}"
              placeholder="{{$placeholder ?? $label ?? null}}"
              @if($additionalAttributes ?? false)
              {{$additionalAttributes}}
              @endif
              @if(!isset($optional) || (isset($optional) && !$optional))
              required
              @endif
              @if(isset($min))
              minlength="{{$min}}"
              @endif
              @if(isset($max))
              maxlength="{{$max}}"
            @endif
    >{{$value ?? old($name ?? "input")}}</textarea>
    @break

    @case("time")
    <input id="{{$id ?? ""}}" class="{{$class ?? ""}} input-time" name="{{$name ?? "input"}}" type="time"
           value="{{$value ?? old($name ?? "input")}}"
           @if(!isset($optional) || (isset($optional) && !$optional))
           required
            @endif
    @if($additionalAttributes ?? false)
        {{$additionalAttributes}}
            @endif
    />
    @break

    @default
    <div>Wrong input type.</div>

@endswitch