<div class="{{$class ?? ""}} modal" tabindex="-1" role="dialog" id="{{$id}}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">{{$title ?? "Conferma azione"}}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>{{$slot}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary confirm {{$confirmButtonClass ?? ""}}" id="{{$confirmButtonID}}" {{$confirmButtonAttributes ?? ""}}>{{$confirmButtonText ?? "Sì"}}</button>
        <button type="button" class="btn btn-secondary cancel {{$cancelButtonClass ?? ""}}" data-dismiss="modal" {{$cancelButtonAttributes ?? ""}}>{{$cancelButtonText ?? "No"}}</button>
      </div>
    </div>
  </div>
</div>