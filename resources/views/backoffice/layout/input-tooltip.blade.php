<span class="input-tooltip">
    <i class="icon-question" data-toggle="tooltip" data-placement="{{$placement ?? "top"}}"
       title="{{ $slot }}"></i>
</span>