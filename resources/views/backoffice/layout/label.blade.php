<span class="input-label">{{$label ?? "Label"}}</span> {{$slot}}
@if($optional ?? false)
    <br>
    <span>(opzionale)</span>
@endif