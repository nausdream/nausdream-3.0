@extends('users.layout.base')

@section('head')
    @component('users.layout.head', $seo)
    @endComponent
@endsection

@section('content')
    <div class="page-error page-401">
        <div class="background">
            <div class="container-fluid max-width">
                <div class="row support-section support-container">
                    <div class="col-md-6 hidden-xs hidden-sm">
                        <img src="https://res.cloudinary.com/naustrip/image/upload/v1496186435/production/assets/img/support/404_fish.png">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 text-center">
                        <div class="page-code">@lang('errors.401_code')</div>
                        <div class="page-message mar-bottom-lg">@lang('errors.401_can')</div>
                        <a href="/" class="def-btn">@lang("errors.go_home")</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection