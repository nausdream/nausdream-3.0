import affix from '../libraries/affix.js';
import Cookies from 'js-cookie';

$(document).ready(() => {
    let bannerClosed = Cookies.get("refer_a_friend_banner_closed") === "true";
    let banners = $('.refer-a-friend-banner');
    let bannersMinimized = $('.refer-a-friend-banner-minimized');
    let page = $('input[name="page"]').val();
    let mobileBanner = $("#refer-a-friend-mobile");
    let mobileBannerMin = $("#refer-a-friend-mobile-minimized");
    let desktopBanner = $("#refer-a-friend-desktop");
    let desktopBannerMin = $("#refer-a-friend-desktop-minimized");

    // Hide and minimize
    function hideBanners() {
        // Hide banners
        banners.removeClass("d-lg-block");
        banners.removeClass("d-block");
        banners.removeClass("d-md-block");
        banners.hide();

        // Show minimized banners
        switch (page) {
            case 'listing':
                desktopBannerMin.addClass("d-none d-lg-block");
                mobileBannerMin.addClass("d-block d-lg-none");
                $('.listing-price-box').append(mobileBannerMin);
                break;
            default:
                desktopBannerMin.addClass("d-none d-md-block");
                mobileBannerMin.addClass("d-block d-md-none");
                break;
        }
        bannersMinimized.show();

        // Apply affix
        affix.affix();
    }

    function showBanners() {
        switch (page) {
            case 'listing':
                desktopBanner.addClass("d-none d-lg-block");
                mobileBanner.addClass("d-block d-lg-none");
                $('.listing-price-box').append(mobileBanner);
                break;
            default:
                desktopBanner.addClass("d-none d-md-block");
                mobileBanner.addClass("d-block d-md-none");
                break;
        }
        banners.show();

        // Hide minimized banners
        bannersMinimized.removeClass("d-lg-block");
        bannersMinimized.removeClass("d-block");
        bannersMinimized.removeClass("d-md-block");
        bannersMinimized.hide();

        // Apply affix
        affix.affix();
    }

    if (bannerClosed) {
        hideBanners();
    } else {
        showBanners();
    }

    // Hide banner on X click
    banners.find('.icon-close').click((e)=>{
        $(e.target).parent('.refer-a-friend-banner').remove();

        // Hide big banners and show minimized banners
        hideBanners();

        // Set session cookie for banner closed
        Cookies.set("refer_a_friend_banner_closed", "true");
    });

    // Fix refer banner on footer
    affix.affix();
});