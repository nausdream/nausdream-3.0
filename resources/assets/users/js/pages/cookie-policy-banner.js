import Cookies from 'js-cookie';

let banner = $("#cookie-policy-banner");
let closeBtn;
let cookieBannerName = "cookie_banner_closed_gdpr";

if (Cookies.get(cookieBannerName)) {
    $(document).ready(() => {
        banner.hide();
    });
} else {
    let setCookiePolicy = (event, timeout) => {
        let necessary = $("#necessary-checkbox").is(":checked");
        let preferences = $("#preferences-checkbox").is(":checked");
        let statistics = $("#statistics-checkbox").is(":checked");
        let marketing = $("#marketing-checkbox").is(":checked");

        // Set cookie policy cookie
        Cookies.set(cookieBannerName, true, {expires: 365});

        Cookies.set("cookie_necessary", necessary, {expires: 9999});
        Cookies.set("cookie_preferences", preferences, {expires: 9999});
        Cookies.set("cookie_statistics", statistics, {expires: 9999});
        Cookies.set("cookie_marketing", marketing, {expires: 9999});

        // Hide banner
        setTimeout(() => {
            banner.hide()
        }, timeout);

        // Detach listeners
        closeBtn.off('click', setCookiePolicy);

        // Send cookies preferences to backend
        let url = '/cookies';
        $.ajax({
            type: "POST",
            url: url,
            data: $("#cookies").serialize()
        })
    };

    $(document).ready(() => {
        banner.show();
        closeBtn = banner.find("#cookie-policy-banner-close-button");

        $("#cookie-policy-banner-close-button").on('click', setCookiePolicy);
        $("#cookie-policy-banner-ok-button").on('click', setCookiePolicy);
    });
}

