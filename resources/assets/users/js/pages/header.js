$(document).ready(function () {
    let navbar = $("#navbar");
    // OPEN MENU
    $(".menu-icon").click(() => {
        navbar.addClass("open");
        $("body").addClass("navbar-open");
    });

    // CLOSE MENU
    let closeMenu = () => {
        navbar.removeClass("open");
        $("body").removeClass("navbar-open");
    };
    navbar.find(".close-icon").click(closeMenu);
    $("#overlay").click(closeMenu);
    $(".close-navbar").click(closeMenu);

    // OPEN LANGUAGE PANEL
    let languages = $("#languages");
    $(".menu-item.language").click(() => {
        languages.addClass("open");
    });
    // CLOSE LANGUAGE PANEL
    languages.find(".close-icon").click(() => {
        languages.removeClass("open");
    });

    // CHANGE LANGUAGE
    let changeLanguage = $(".change-language");
    changeLanguage.each((index, element) => {
        let lang = $(element).data('lang');

        let href = $(`link[rel=alternate][hreflang=${lang}]`).attr('href');

        // If no alternate is set, replace the current language in the url (E.g. /it/ -> /en/)
        // TODO prevent potential bug where '/it/' is present elsewhere BEFORE the actual language
        if (!href) {
            let currentLanguage = window.nausdream.language;
            href = window.location.href.replace(`/${currentLanguage}/`, `/${lang}/`)
        }

        $(element).attr('href', href);
    })
});