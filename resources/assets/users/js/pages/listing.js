import smoothScroll from '../libraries/smooth-scroll.js';
import affix from '../libraries/affix.js';
import Stopwatch from '../libraries/stopwatch.js';
import analytics from '../libraries/google-analytics.js';
import dateInput from '../libraries/date-input.js';
import validate from 'jquery-validation';
import messages from '../libraries/jquery-validation-messages.js';

$(document).ready(function () {
    if (window.nausdream.email_sent) {
        $('#contact-form-modal').modal('show');
    }

    let cutoffMinutes = parseInt(window.nausdream.cutoff_minutes);
    // Create fancy calendars
    dateInput.init(window.nausdream.language, cutoffMinutes);

    let ctaBtn = $('.cta-btn');
    let trekksoftId = ctaBtn.data('trekksoft-id');

    // Register trekksoft behaviour
    if (trekksoftId) {

        (function (d, script) {
            script = d.createElement('script');
            script.type = 'text/javascript';
            script.async = true;
            script.onload = function () {
                // remote script has loaded
                (function () {
                    ctaBtn.each((index, element) => {
                        let button = new TrekkSoft.Embed.Button();
                        button
                            .setAttrib("target", "fancy")
                            .setAttrib("entryPoint", "tour")
                            .setAttrib("tourId", trekksoftId)
                            //Trekksoft popup width can't overflow window width
                            .setAttrib("fancywidth", ($(window).width() / 2 + 150) + 'px')
                            .registerOnClick('#' + $(element).attr('id'));

                        // Define action to trigger when the user closes trekksoft popup
                        $(element).click(() => {
                            let stopWatch = new Stopwatch();
                            stopWatch.start();

                            setTimeout(() => {
                                $(".trekksoft-close-btn").click(() => {
                                    stopWatch.stop();

                                    let time = stopWatch.getSeconds();

                                    // Show shopping cart abandonment modal
                                    $("#cart-abandonment").modal('show');

                                    // Send Abandonment event to analytics
                                    analytics.send({
                                        hitType: 'event',
                                        eventCategory: 'Button close',
                                        eventAction: 'Click',
                                        eventLabel: window.nausdream.experience_business_id,
                                        transport: 'beacon'
                                    });
                                })
                            }, 500);
                        });
                    });
                })();
            };
            script.src = window.nausdream.trekksoft_api_url;

            d.getElementsByTagName('head')[0].appendChild(script);
        }(document));

    } else {
        // Scroll to form
        ctaBtn.attr("href", ctaBtn.data('form-id'));
    }

    // Activate smoothScroll for links
    smoothScroll.smoothScroll();

    // Fix price box on top of page when scrolling down
    affix.affix();

    // Higlight listing box on mouse hover
    function toggleHighlightListing(element) {
        $(element).toggleClass('mouse-hover');
    }

    let listings = $('.listing-box');
    listings.find('.listing-title, .photo, .def-btn').hover((event) => {
        toggleHighlightListing($(event.target).parents('.listing-box')[0]);
    });

    // Enable submit when form is valid
    let submit = $("#form-submit");
    let termsCheck = $("#form-terms-privacy");
    termsCheck.change(function () {
        if (this.checked) {
            submit.attr('disabled', null);
        } else {
            submit.attr('disabled', true);
        }
    });

    // Validates form
    messages();
    let form = $("#book");
    submit.click(()=>{
        form.validate({
            errorClass: 'form-error',
            // Validate hidden date too
            ignore: []
        });
    });
});