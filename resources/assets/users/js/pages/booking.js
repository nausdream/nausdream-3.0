import dateInput from "../libraries/date-input.js";
import validate from 'jquery-validation';
import messages from '../libraries/jquery-validation-messages.js';

$(document).ready(function () {
    if (window.nausdream.errors) {
        $('#error-modal').modal('show');
    }

    // Create calendars
    dateInput.init(window.nausdream.language);

    // Load validation messages
    messages();
    // Validates form
    let form = $("#form");
    $("#submit").click(()=>{
        form.validate({
            errorClass: 'form-error',
            // Validate hidden dates too
            ignore: []
        });
    });

    // Hide loader
    $(".loader-container").hide();
});