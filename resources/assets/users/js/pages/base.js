import analytics from '../libraries/google-analytics.js';
import Cookies from 'js-cookie';

$(document).ready(function () {
    let cloudinary = window.nausdream.cloudinary_secure_url;
    let cloudinaryFolder = window.nausdream.cloudinary_base_folder;

    // Get user resolution
    let res = $(window).width();

    // TODO refactor
    // Set background image with correct size based on user resolution
    $('.with-background-image').each((id, elem) => {
        let versions = $(elem).data('versions');
        let url = $(elem).data('url');

        let selectedRes = $(elem).data('def');
        for (let width in versions) {
            if (versions.hasOwnProperty(width)) {
                if (res < width) {
                    selectedRes = versions[width];
                    break;
                }
            }
        }

        $(elem).css({
            "background-image": `url("${cloudinary}${selectedRes.toString()}/${cloudinaryFolder}${url}")`
        });

        window.addEventListener('resize', function(){
            let res = $(window).width();
            for (let width in versions) {
                if (versions.hasOwnProperty(width)) {
                    if (res < width) {
                        selectedRes = versions[width];
                        break;
                    }
                }
            }


            $(elem).css({
                "background-image": `url("${cloudinary}${selectedRes.toString()}/${cloudinaryFolder}${url}")`
            });
        }, true);
    });

    let internalTraffic = Cookies.get('internal_traffic') === "true";
    let statisticsConsent = Cookies.get('cookie_statistics') === undefined || Cookies.get('cookie_statistics') === "true";

    if (!internalTraffic && statisticsConsent) {
        // Activate Google Analytics triggers
        analytics.init();
    } else {
        console.log("Not sending data to Google Analytics.");
    }
});