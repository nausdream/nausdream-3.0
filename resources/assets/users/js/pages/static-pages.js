// Maps
window.initMap = function(){
    let office = {lat: window.nausdream.office_lat, lng: window.nausdream.office_lng};
    // Create a map object and specify the DOM element for display.
    let map = new google.maps.Map(document.getElementById('map'), {
        center: office,
        zoom: 17
    });
    marker = new google.maps.Marker({
        position: office,
        map: map,
        draggable: false
    });
};

$(document).ready(()=>{
    let panelHeadings = $('.panel-heading');
    function toggle(e) {
        // With currentTarget it works even when you click on child elements (e.g. title or icon)
        $(e.currentTarget)
            .find(".more-less")
            .toggleClass('icon-plus icon-minus');

        $(e.currentTarget).toggleClass('opened');
        $(e.currentTarget).find('.panel-title').toggleClass('font-bold');
    }
    panelHeadings.on('click', toggle);
});