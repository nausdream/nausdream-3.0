import scrollCounter from '../libraries/scroll-counter.js';
import smoothScroll from '../libraries/smooth-scroll.js';
import dateInput from '../libraries/date-input.js';
import validate from 'jquery-validation';
import messages from '../libraries/jquery-validation-messages.js';

$(document).ready(function () {
    // Activate smoothScroll for links
    smoothScroll.smoothScroll();

    scrollCounter.scrollCounter();

    // Open destination dropdown on button click
    let exploreBtn = $('#explore-btn');
    let exploreBtnMenu = $('#explore-btn-menu');
    // Open/menu
    exploreBtn.on('click', (event) => {
        exploreBtnMenu.toggle();
    });
    // Prevent menu from hiding when mouse up on explore btn occurs
    exploreBtn.on('mouseup', (event) => {
        event.stopPropagation();
    });

    // Hide element when clicking outside of it
    $(document).on('mouseup', (e) => {
        // if the target of the click isn't the container nor a descendant of the container
        if (!exploreBtnMenu.is(e.target) && exploreBtnMenu.has(e.target).length === 0) {
            exploreBtnMenu.hide();
        }
    });

    // Go to harbour center when an option is selected
    let menu = $('.harbour-center-select');
    menu.change(() => {
        window.location.href = menu.val();
    });
    dateInput.init(window.nausdream.language);
    if (window.nausdream.success) {
        $('#contact-custom-modal').modal('show');
    }
    if (window.nausdream.errorform) {
        $('#custom-form-modal').modal('show');
    }


    // Enable submit when form is valid
    let submit = $("#form-submit");
    let termsCheck = $("#form-terms-privacy");
    termsCheck.change(function () {
        if (this.checked) {
            submit.attr('disabled', null);
        } else {
            submit.attr('disabled', true);
        }
    });

    // Validates form
    messages();
    let form = $("#custom-book");
    submit.click(()=>{
        form.validate({
            errorClass: 'form-error',
            // Validate hidden date too
            ignore: []
        });
    });
});
