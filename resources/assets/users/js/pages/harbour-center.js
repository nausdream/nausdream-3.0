import smoothScroll from '../libraries/smooth-scroll.js';
import dateInput from '../libraries/date-input.js'
import validate from 'jquery-validation';
import messages from "../libraries/jquery-validation-messages";
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    // Turn # links into smooth scrolling links
    smoothScroll.smoothScroll();

    // Higlight listing box on mouse hover
    function toggleHighlightListing(element) {
        $(element).toggleClass('mouse-hover');
    }
    let listings = $('.listing-box');
    listings.find('.listing-title, .photo, .def-btn').hover((event)=>{
        toggleHighlightListing($(event.target).parents('.listing-box')[0]);
    });
    dateInput.init(window.nausdream.language);
    if (window.nausdream.success) {
        $('#contact-custom-modal').modal('show');
    }
    if (window.nausdream.errorform) {
        $('#custom-form-modal').modal('show');
    }

    // Enable submit when form is valid
    let submit = $("#form-submit");
    let termsCheck = $("#form-terms-privacy");
    termsCheck.change(function () {
        if (this.checked) {
            submit.attr('disabled', null);
        } else {
            submit.attr('disabled', true);
        }
    });

    // Validates form
    messages();
    let form = $("#custom-book");
    submit.click(()=>{
        form.validate({
            errorClass: 'form-error',
            // Validate hidden date too
            ignore: []
        });
    });
});