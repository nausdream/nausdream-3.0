import scrollTrigger from "./scroll-trigger";

function send(options){
    ga('send', options);
}

/**
 * Takes all elements with data-analytics to send Google Analytics events on a certain trigger
 *
 * Element attributes:
 * data-analytics
 * data-trigger="Click|Scroll" (def: Click)
 * data-event-category
 * data-event-action
 * data-event-label
 * data-event-value
 * data-non-interaction="true|false" (def: true)
 */
function analytics() {
    let analytics = $('[data-analytics]');

    analytics.each((index, element) => {
        element = $(element);
        let trigger = element.data('trigger') || 'Click';
        // Default values. Undefined means "do not send that property"
        let options = {
            hitType: 'event',
            eventCategory: undefined,
            eventAction: trigger,
            eventLabel: undefined,
            eventValue: undefined,
            nonInteraction: undefined,
            transport: 'beacon'
        };

        let category = element.data('event-category');
        options.eventCategory = element.data('event-category') || options.eventCategory;
        options.eventLabel = element.data('event-label') || options.eventLabel;
        options.eventValue = element.data('event-value') || options.eventValue;
        options.nonInteraction = element.data('non-interaction') || options.nonInteraction;

        switch (trigger) {
            case 'Click':
                element.on('click.analytics', () => {
                    send(options);
                    element.off('click.analytics');
                });
                break;
            case 'Scroll':
                scrollTrigger.onScrollIn(element, () => {
                    if (element.data('event-consumed') !== 'true') {
                        send(options);
                        element.data('event-consumed', 'true');
                    }
                });
                break;
            case 'Submit':
                element.on('submit.analytics', () => {
                    send(options);
                    element.off('submit.analytics');
                });
                break;
            default:
                break;
        }
    });
}

export default {
    init: analytics,
    send: send
}