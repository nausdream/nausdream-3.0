import scrollTrigger from './scroll-trigger.js';

let handlers = {};

let handler = (id, element) => {
    return () => {
        if (scrollTrigger.isElementInViewport(element)) {
            $(element).prop('Counter',0).animate({
                Counter: $(element).data('counter-to')
            }, {
                duration: $(element).data('duration'),
                easing: 'swing',
                step: function (now) {
                    $(element).text(Math.ceil(now));
                },
                complete: () => {
                    $(element).text($(element).text()+'+');
                }
            });

            scrollTrigger.offChange(handlers[id]);
        }
    };
};

function init() {
    $('[data-scroll-counter]').each((id, element) => {
        handlers[id] = handler(id, element);

        scrollTrigger.onChange(handlers[id]);
    });
}

const scrollCounter = {
    scrollCounter: init,
};

export default scrollCounter;