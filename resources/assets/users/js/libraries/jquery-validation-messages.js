function messages(){
    if (!window.nausdream.validation_messages) {
        window.nausdream.validation_messages = {};
    }

    $.extend($.validator.messages, {
        required: window.nausdream.validation_messages.required || "This field is required.",
        email: window.nausdream.validation_messages.email || "Please enter a valid email address.",
        number: window.nausdream.validation_messages.number || "Please enter a valid number."
    });
}

export default messages;