export default class Stopwatch {
    constructor(startTime, endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    start() {
        this.startTime = new Date();
    };

    stop() {
        this.endTime = new Date();
    };

    clear() {
        this.startTime = null;
        this.endTime = null;
    };

    getTime() {
        if (!this.endTime) {
            return 0;
        }
        return Math.round((this.endTime.getTime() - this.startTime.getTime()));
    };

    getSeconds() {
        return this.getTime() / 1000;
    };

    getMinutes() {
        return this.getSeconds() / 60;
    };

    getHours() {
        return this.getSeconds() / 60 / 60;
    };

    getDays() {
        return this.getHours() / 24;
    };
};