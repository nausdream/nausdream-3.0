import datepicker from 'bootstrap-datepicker';
import moment from 'moment';

function init(language, cutoffMinutes) {
    $(document).ready(function () {
        let dates = $('.naus-datepicker');

        let language = language || window.nausdream.language || 'en';
        cutoffMinutes = cutoffMinutes || 0;

        $.fn.datepicker.dates['en'] = {
            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            today: "Today",
            clear: "Clear",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };
        $.fn.datepicker.dates['it'] = {
            days: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"],
            daysShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
            daysMin: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa"],
            months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
            monthsShort: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
            today: "Oggi",
            monthsTitle: "Mesi",
            clear: "Cancella",
            weekStart: 1,
            format: "dd/mm/yyyy"
        };

        // Create hidden date fields where the value in the correct format will be stored
        dates.each((i, element) => {
            let name = $(element).attr('name') || $(element).data('name');
            let required = $(element).prop('required') || $(element).data('required');
            let val = $(element).data('date');
            let shadowInput = $(`<input type="hidden" name="${name}" value="${val}"/>`);
            if (required) {
                shadowInput.prop('required', true);
            }
            $(element).after(shadowInput);
            $(element).attr('name', `datepicker-${name}`);
            $(element).data('name', name);

            let elementName = $(element).data('name');
            // Get start and end dates from element
            let startDate = moment($(element).data('start-date'), "YYYY-MM-DD HH:mm");
            if (startDate.isValid()) {
                startDate = startDate.toDate()
            } else {
                startDate = "";
            }

            let endDate = moment($(element).data('end-date'), "YYYY-MM-DD");
            if (endDate.isValid()) {
                endDate = endDate.toDate()
            } else {
                endDate = "";
            }

            let disabledDaysOfWeek = $(element).data('disabled-days') || "";

            let startView = $(element).data('view') || 0;

            $(element).datepicker({
                autoclose: true,
                startView: startView,
                startDate: startDate,
                endDate: endDate,
                daysOfWeekDisabled: disabledDaysOfWeek,
                language: language,
                format: {
                    toDisplay: function (date, format, language) {
                        return date.toLocaleDateString(language);
                    },
                    toValue: function (date, format, language) {
                        return date;
                    }
                }
            }).on('changeDate', (e) => {
                $(`[name="${elementName}"]`).val(moment(e.date).format().split("T")[0]);
            }).on('clearDate', () => {
                $(`[name="${elementName}"]`).val(null);
            });

            // Set initial date
            if (val) {
                console.log(moment(val).toDate());
                $(element).datepicker('setDate', moment(val).toDate());
            }
        });
    });
}

export default {
    init: init
};