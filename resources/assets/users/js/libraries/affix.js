import scrollTrigger from "./scroll-trigger";

function affix(){
    let affix = $('[data-affix]');
    let offset = 0;
    let position = "top";

    affix.each((index, element) => {
        element = $(element);

        offset = element.data('affix-offset') || offset;
        position = element.data('affix-position') || position;

        scrollTrigger.onChange(() => {
            let flag = false;

            switch (position) {
                case "bottom":
                    flag = scrollTrigger.isElementOffViewportBottom($(element.data('affix-to')), offset);
                    break;
                case "top":
                // Intentional fallthrough
                default:
                    flag = scrollTrigger.isElementOffViewportTop($(element.data('affix-to')), offset);
                    break;
            }

            if (flag) {
                element.addClass('affix');
            } else {
                element.removeClass('affix');
            }
        });
    });
}

export default {
    affix: affix
}