function smoothScroll() {
    let trigger = $('a[href*="#"]');
    // Smooth scroll
    trigger
    // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
                &&
                location.hostname === this.hostname
            ) {
                // Figure out element to scroll to
                let scrollTo = $(this.hash);
                scrollTo = scrollTo.length ? scrollTo : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (scrollTo.length) {
                    // Only prevent default if animation is actually gonna happen
                    let offset = $(event.currentTarget).data('scroll-offset');
                    if (offset == null) offset = 0;

                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: scrollTo.offset().top - offset
                    }, 1000, function() {
                        // Callback after animation
                        // Must change focus!
                        let $scrollTo = $(scrollTo);
                        $scrollTo.focus();
                    });
                }
            }
        });
}

export default {
    smoothScroll: smoothScroll
}