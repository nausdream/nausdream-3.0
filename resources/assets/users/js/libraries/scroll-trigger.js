// Return true if element is visible
function isElementInViewport(el) {
    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    let rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

// Return true if element is gone out of view (top)
function isElementOffViewportTop(el, deltaTop) {
    if (!deltaTop) deltaTop = 0;

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    let rect = el.getBoundingClientRect();

    return (
        rect.top <= 0 + deltaTop
    );
}

// Return true if element is gone out of view (bottom)
function isElementOffViewportBottom(el, deltaBottom) {
    if (!deltaBottom) deltaBottom = 0;

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    let rect = el.getBoundingClientRect();

    return (
        rect.bottom >= $(window).height() + deltaBottom
    );
}

function onChange(callback){
    $(window).on('DOMContentLoaded load resize scroll', callback);
}

function offChange(callback){
    $(window).off('DOMContentLoaded load resize scroll', callback);
}

function onScrollIn(element, callback) {
    onChange(()=>{
        if (isElementInViewport(element)) {
            callback();
        }
    });
}

export default {
    isElementInViewport: isElementInViewport,
    isElementOffViewportTop: isElementOffViewportTop,
    isElementOffViewportBottom: isElementOffViewportBottom,
    onChange: onChange,
    offChange: offChange,
    onScrollIn: onScrollIn
};