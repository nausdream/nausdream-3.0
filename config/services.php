<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY', 'AKIAASDFASDFADSF'),
        'secret' => env('SES_SECRET', 'AvcSUeGbiX352DGSDFGDGFGSdSFFKpBqcPkHAzLqQo8PX'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'graph_url' => env('FB_GRAPH_URL'),
        'app_id' => env('FB_APP_ID'),
        'account_kit' => [
            'app_secret' => env('FB_AK_APP_SECRET'),
            'app_version' => env('FB_AK_APP_VERSION'),
        ],
        'pixel_id' => env('FB_PIXEL_ID')
    ],

    'cloudinary' => [
        'api_key' => env('CLOUDINARY_API_KEY'),
        'api_secret' => env('CLOUDINARY_API_SECRET'),
        'cloud_name' => env('CLOUDINARY_CLOUD_NAME'),
        'base_url' => env('CLOUDINARY_BASE_URL'),
        'secure_url' => env('CLOUDINARY_SECURE_URL'),
        'api_base_url' => env('CLOUDINARY_API_BASE_URL'),
        'base_folder' => env('CLOUDINARY_BASE_FOLDER'),
    ],

    'google' => [
        'analytics' => [
            'tracking_id' => env('GA_TRACKING_ID')
        ],
        'maps' => [
            'api_key' => env('MAPS_API_KEY')
        ]
    ],

    'trekksoft' => [
        'api_domain' => env('TREKKSOFT_API_DOMAIN'),
        'api_path' => env('TREKKSOFT_API_PATH'),
    ],

    'olark' => [
        'site_id' => env('OLARK_SITE_ID')
    ],
];
